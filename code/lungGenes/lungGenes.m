#import <Foundation/Foundation.h>
#import <BioSwarm/BCOptimizationDataHandler.h>
#import <BioSwarm/BCOptimization.h>
#import <BioSwarm/BCOptimizationController.h>
#import <BioCocoa/BCParseSOFT.h>
#import <BioCocoa/BCSample.h>
#import <BioCocoa/BCPlatform.h>
#import <BioCocoa/BCExpressionData.h>

#define USE_PRIOR 0

int parseLungData(NSString *descFile, NSString *outputFile)
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  int i, j, k, l;

  // parse the file
  NSString *softFile = [NSString stringWithFormat:
                        @"%@/Projects/research/lung/data/GSE14334_family.soft",
                        NSHomeDirectory()];
    
  BCParseSOFT *parser1 = [BCParseSOFT new];
  printf("Parsing file: %s\n", [softFile UTF8String]);
  [parser1 parseFile: softFile];

  // Gene list
  NSDictionary *dataDesc = [NSDictionary dictionaryWithContentsOfFile: descFile];
  NSArray *geneList = [dataDesc objectForKey: @"parseGenes"];
  
  // 1 platform
  BCPlatform *p1 = [BCPlatform platformWithId: @"GPL570"];
  NSArray *probeSet = [p1 valuesForKey: @"Probe Set"];

  //
  // map genes to probes
  //
  NSMutableArray *geneMap = [NSMutableArray array];
  for (j = 0; j < [geneList count]; ++j) {
    NSString *geneID = [geneList objectAtIndex: j];
    NSMutableArray *probeList = [NSMutableArray array];
    [geneMap addObject: probeList];
    for (i = 0; i < [probeSet count]; ++i) {
      NSArray *a1 = [probeSet objectAtIndex: i];
      NSString *probeGene = [a1 objectAtIndex: 10];
      NSRange r = [probeGene rangeOfString: @" "];
      if (r.location != NSNotFound) {
        r.length = r.location; r.location = 0;
        probeGene = [probeGene substringWithRange: r];
      }
      //printf("%s\n", [probeGene UTF8String]);
      //return 0;
      if ([geneID isEqualToString: probeGene]) {
        printf("%s\n", [probeGene UTF8String]);
        [probeList addObject: [NSNumber numberWithInt: i]];
      }
    }
    //return 0;
    if ([probeList count] == 0) {
      NSLog(@"gene does not map to any probes: %@", geneID);
      return 1;
    }
  }

  int numOfVariables = 0;
  NSMutableArray *colNames = [NSMutableArray array];
  for (j = 0; j < [geneList count]; ++j) {
    NSString *geneID = [geneList objectAtIndex: j];
    NSArray *probeList = [geneMap objectAtIndex: j];
    printf("%s = ", [geneID UTF8String]);

    for (l = 0; l < [probeList count]; ++l) {
      int probeIndex = [[probeList objectAtIndex: l] intValue];
      NSArray *a1 = [probeSet objectAtIndex: probeIndex];
      NSString *probeName = [a1 objectAtIndex: 0];
      printf("%s, ", [probeName UTF8String]);
      ++numOfVariables;
      [colNames addObject: probeName];
    }
    printf("\n");
  }

  printf("geneSet = (");
  for (i = 0; i < [colNames count]; ++i) {
    printf("%s, ", [[colNames objectAtIndex: i] UTF8String]);
  }
  printf(");\n");
  
  // samples
  NSMutableArray *sampleList = [NSMutableArray array];
  [sampleList addObject: @"GSM358631"];
  [sampleList addObject: @"GSM358632"];
  [sampleList addObject: @"GSM358633"];
  [sampleList addObject: @"GSM358634"];
  [sampleList addObject: @"GSM358635"];
  [sampleList addObject: @"GSM358636"];
  [sampleList addObject: @"GSM358637"];
  [sampleList addObject: @"GSM358638"];
  [sampleList addObject: @"GSM358639"];
  [sampleList addObject: @"GSM358640"];
  [sampleList addObject: @"GSM358641"];
  [sampleList addObject: @"GSM358642"];
  [sampleList addObject: @"GSM358643"];
  [sampleList addObject: @"GSM358644"];
  [sampleList addObject: @"GSM358645"];
  [sampleList addObject: @"GSM358646"];
  [sampleList addObject: @"GSM358647"];
  [sampleList addObject: @"GSM358648"];
  [sampleList addObject: @"GSM358649"];
  [sampleList addObject: @"GSM358650"];
  [sampleList addObject: @"GSM358651"];
  [sampleList addObject: @"GSM358652"];
  [sampleList addObject: @"GSM358653"];
  [sampleList addObject: @"GSM358654"];
  [sampleList addObject: @"GSM358655"];
  [sampleList addObject: @"GSM358656"];
  [sampleList addObject: @"GSM358657"];
  [sampleList addObject: @"GSM358658"];
  [sampleList addObject: @"GSM358659"];
  [sampleList addObject: @"GSM358660"];
  [sampleList addObject: @"GSM358661"];
  [sampleList addObject: @"GSM358662"];
  [sampleList addObject: @"GSM358663"];
  [sampleList addObject: @"GSM358664"];
  [sampleList addObject: @"GSM358665"];
  [sampleList addObject: @"GSM358666"];
  [sampleList addObject: @"GSM358667"];
  [sampleList addObject: @"GSM358668"];

  int numOfObservations = [sampleList count];
  BCDataMatrix *dMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfObservations andColumns: numOfVariables andEncode: BCdoubleEncode];
	double (*DM)[numOfObservations][numOfVariables];
	DM = [dMatrix dataMatrix];

  printf("%d observations\n", numOfObservations);
	printf("%d variables\n", numOfVariables);

  for (i = 0; i < [sampleList count]; ++i) {
    BCSample *aSample = [BCSample sampleWithId: [sampleList objectAtIndex: i]];
    NSArray *sampleData = [aSample valuesForKey: @"Sample Data"];
    
    int currentGene = 0;
    for (j = 0; j < [geneList count]; ++j) {
      //NSString *geneID = [geneList objectAtIndex: j];
      NSArray *probeList = [geneMap objectAtIndex: j];
      
      for (l = 0; l < [probeList count]; ++l) {
        int probeIndex = [[probeList objectAtIndex: l] intValue];
        NSArray *a1 = [probeSet objectAtIndex: probeIndex];
        NSString *probeName = [a1 objectAtIndex: 0];

        for (k = 0; k < [sampleData count]; ++k) {
          NSArray *aData = [sampleData objectAtIndex: k];
          if ([probeName isEqualToString: [aData objectAtIndex: 0]]) {
            double value = [[aData objectAtIndex: 1] doubleValue];
            (*DM)[i][currentGene] = value;
            break;
          }
        }
        ++currentGene;
      }
    }
  }

  print_matrix("Lung = ", numOfObservations, numOfVariables, DM);

  //[dMatrix setColumnNames: colNames];
  //[dMatrix writeToFile: @"GSE14334.txt"];
  NSMutableString *dataString = [NSMutableString new];
  [dataString appendFormat: @"%@", [colNames objectAtIndex: 0]];
  for (i = 1; i < [colNames count]; ++i) {
    [dataString appendFormat: @" %@", [colNames objectAtIndex: i]];
  }
  [dataString appendString: @"\n"];

  for (i = 0; i < numOfObservations; ++i) {
    [dataString appendFormat: @"%f", (*DM)[i][0]];
    for (j = 1; j < numOfVariables; ++j) {
      [dataString appendFormat: @" %f", (*DM)[i][j]];
    }
    [dataString appendString: @"\n"];
  }
  [dataString writeToFile: outputFile atomically: YES encoding: NSUTF8StringEncoding error: NULL];

  [pool drain];
  return 0;
}

void newParseLungData(NSString *softFile, NSString *descFile, NSString *outputFile)
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  int i, j;
  
  // expression data
  BCExpressionData *eData = [[BCExpressionData alloc] initWithExpressionFile: softFile];
  
  // Gene list
  NSDictionary *dataDesc = [NSDictionary dictionaryWithContentsOfFile: descFile];
  NSArray *geneList = [dataDesc objectForKey: @"parseGenes"];
  
  // samples
  NSMutableArray *sampleList = [NSMutableArray array];
  [sampleList addObject: @"GSM358631"];
  [sampleList addObject: @"GSM358632"];
  [sampleList addObject: @"GSM358633"];
  [sampleList addObject: @"GSM358634"];
  [sampleList addObject: @"GSM358635"];
  [sampleList addObject: @"GSM358636"];
  [sampleList addObject: @"GSM358637"];
  [sampleList addObject: @"GSM358638"];
  [sampleList addObject: @"GSM358639"];
  [sampleList addObject: @"GSM358640"];
  [sampleList addObject: @"GSM358641"];
  [sampleList addObject: @"GSM358642"];
  [sampleList addObject: @"GSM358643"];
  [sampleList addObject: @"GSM358644"];
  [sampleList addObject: @"GSM358645"];
  [sampleList addObject: @"GSM358646"];
  [sampleList addObject: @"GSM358647"];
  [sampleList addObject: @"GSM358648"];
  [sampleList addObject: @"GSM358649"];
  [sampleList addObject: @"GSM358650"];
  [sampleList addObject: @"GSM358651"];
  [sampleList addObject: @"GSM358652"];
  [sampleList addObject: @"GSM358653"];
  [sampleList addObject: @"GSM358654"];
  [sampleList addObject: @"GSM358655"];
  [sampleList addObject: @"GSM358656"];
  [sampleList addObject: @"GSM358657"];
  [sampleList addObject: @"GSM358658"];
  [sampleList addObject: @"GSM358659"];
  [sampleList addObject: @"GSM358660"];
  [sampleList addObject: @"GSM358661"];
  [sampleList addObject: @"GSM358662"];
  [sampleList addObject: @"GSM358663"];
  [sampleList addObject: @"GSM358664"];
  [sampleList addObject: @"GSM358665"];
  [sampleList addObject: @"GSM358666"];
  [sampleList addObject: @"GSM358667"];
  [sampleList addObject: @"GSM358668"];
  
  // extract the data set
  BCDataMatrix *dMatrix = [eData extractDatasetForGenes:geneList fromSamples:sampleList probesUsed:nil platformTag: @"Gene Symbol"];

  // write data set to output file
  int numOfObservations = [dMatrix numberOfRows];
  int numOfVariables = [dMatrix numberOfColumns];
  double (*DM)[numOfObservations][numOfVariables];
	DM = [dMatrix dataMatrix];

  NSMutableString *dataString = [NSMutableString new];
  [dataString appendFormat: @"%@", [geneList objectAtIndex: 0]];
  for (i = 1; i < [geneList count]; ++i) {
    [dataString appendFormat: @" %@", [geneList objectAtIndex: i]];
  }
  [dataString appendString: @"\n"];
  
  for (i = 0; i < numOfObservations; ++i) {
    [dataString appendFormat: @"%f", (*DM)[i][0]];
    for (j = 1; j < numOfVariables; ++j) {
      [dataString appendFormat: @" %f", (*DM)[i][j]];
    }
    [dataString appendString: @"\n"];
  }
  [dataString writeToFile: outputFile atomically: YES encoding: NSUTF8StringEncoding error: NULL];
  
  [pool drain];  
}

int analyzeLungNetwork(NSString *descFile, NSString *networkFile, NSString *edgeFile, NSString *sifFile, NSString *edaFile)
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];
  int i, j;
  
  // parse the file
  NSString *softFile = [NSString stringWithFormat:
                        @"%@/Projects/research/lung/data/GSE14334_family.soft",
                        NSHomeDirectory()];
  
  BCParseSOFT *parser1 = [BCParseSOFT new];
  printf("Parsing file: %s\n", [softFile UTF8String]);
  [parser1 parseFile: softFile];
  
  // Gene list
  NSDictionary *dataDesc = [NSDictionary dictionaryWithContentsOfFile: descFile];
  NSArray *geneList = [dataDesc objectForKey: @"parseGenes"];

  // network
  NSString *lungNetwork = [NSString stringWithContentsOfFile: networkFile encoding: NSUTF8StringEncoding error: NULL];

  // edge attributes
  NSString *lungEdges = [NSString stringWithContentsOfFile: edgeFile encoding: NSUTF8StringEncoding error: NULL];
  
  // 1 platform
  BCPlatform *p1 = [BCPlatform platformWithId: @"GPL570"];
  NSArray *probeSet = [p1 valuesForKey: @"Probe Set"];
  
  //
  // map probes to genes
  //
  NSMutableDictionary *probeMap = [NSMutableDictionary dictionary];
  for (j = 0; j < [geneList count]; ++j) {
    NSString *geneID = [geneList objectAtIndex: j];
    for (i = 0; i < [probeSet count]; ++i) {
      NSArray *a1 = [probeSet objectAtIndex: i];
      NSString *probeGene = [a1 objectAtIndex: 10];
      NSRange r = [probeGene rangeOfString: @" "];
      if (r.location != NSNotFound) {
        r.length = r.location; r.location = 0;
        probeGene = [probeGene substringWithRange: r];
      }
      if ([geneID isEqualToString: probeGene]) {
        [probeMap setObject:geneID forKey:[a1 objectAtIndex: 0]];
      }
    }
  }

  // Add gene names to network
  NSMutableString *sifText = [NSMutableString string];
  NSArray *lines = [lungNetwork componentsSeparatedByString: @"\n"];
  for (i = 0; i < [lines count]; ++i) {
    NSString *s = [lines objectAtIndex: i];
    if ([s length] == 0) continue;
    NSArray *line = [s componentsSeparatedByString: @" "];
#if 0
    printf("%s(%s) %s %s(%s)\n", [[line objectAtIndex: 0] UTF8String], [[probeMap objectForKey:[line objectAtIndex: 0]] UTF8String],
           [[line objectAtIndex:1] UTF8String], [[line objectAtIndex: 2] UTF8String],
           [[probeMap objectForKey:[line objectAtIndex: 2]] UTF8String]);
#else
    printf("%s %s %s\n", [[probeMap objectForKey:[line objectAtIndex: 0]] UTF8String],
           [[line objectAtIndex:1] UTF8String],
           [[probeMap objectForKey:[line objectAtIndex: 2]] UTF8String]);
    [sifText appendFormat: @"%@ %@ %@\n", [probeMap objectForKey:[line objectAtIndex: 0]],
     [line objectAtIndex:1], [probeMap objectForKey:[line objectAtIndex: 2]]];
#endif
  }
  [sifText writeToFile: sifFile atomically: YES encoding: NSUTF8StringEncoding error: NULL];

  // Add gene names to edge attributes
  NSMutableString *edaText = [NSMutableString string];
  [edaText appendString: @"InteractionStrength\n"];
  printf("\nInteractionStrength\n");
  lines = [lungEdges componentsSeparatedByString: @"\n"];
  for (i = 1; i < [lines count]; ++i) {
    NSString *s = [lines objectAtIndex: i];
    if ([s length] == 0) continue;
    NSArray *line = [s componentsSeparatedByString: @" "];
#if 0
    printf("%s(%s) %s %s(%s) = %s\n", [[line objectAtIndex: 0] UTF8String], [[probeMap objectForKey:[line objectAtIndex: 0]] UTF8String],
           [[line objectAtIndex:1] UTF8String], [[line objectAtIndex: 2] UTF8String],
           [[probeMap objectForKey:[line objectAtIndex: 2]] UTF8String], [[line objectAtIndex: 4] UTF8String]);
#else
    printf("%s %s %s = %s\n", [[probeMap objectForKey:[line objectAtIndex: 0]] UTF8String],
           [[line objectAtIndex:1] UTF8String],
           [[probeMap objectForKey:[line objectAtIndex: 2]] UTF8String], [[line objectAtIndex: 4] UTF8String]);
    [edaText appendFormat: @"%@ %@ %@ = %@\n", [probeMap objectForKey:[line objectAtIndex: 0]],
     [line objectAtIndex:1], [probeMap objectForKey:[line objectAtIndex: 2]], [line objectAtIndex: 4]];
#endif
  }
  [edaText writeToFile: edaFile atomically: YES encoding: NSUTF8StringEncoding error: NULL];

  [pool drain];
  return 0;
}

void lungNetwork(NSString *descFile, NSString *networkFile, NSString *edgeFile)
{
	int i, j, k, l;

  // construct model
	BCOptimizationDataHandler *lungData = [[BCOptimizationDataHandler alloc] initWithDescriptorFile: descFile];
  
  NSArray *geneNames = [lungData geneNames];
	int numOfObservations = [lungData numOfObservations];
	int numOfVariables = [lungData numOfVariables];

  print_matrix("raw Lung = \n", numOfObservations, numOfVariables, [[lungData dataMatrix] dataMatrix]);

  double (*lungGrid)[numOfObservations][numOfVariables];
  lungGrid = [[lungData dataMatrix] dataMatrix];
  for (i = 0; i < numOfObservations; ++i)
		for (j = 0; j < numOfVariables; ++j)
      (*lungGrid)[i][j] = exp2((*lungGrid)[i][j]);

	printf("%d observations\n", numOfObservations);
	printf("%d variables\n", numOfVariables);
  print_matrix("Lung = ", numOfObservations, numOfVariables, [[lungData dataMatrix] dataMatrix]);

  BCOptimizationDataHandler *lungModel = [[BCOptimizationDataHandler alloc] initWithDataMatrix: [lungData dataMatrix] andResponse: [lungData dataMatrix]];
  print_matrix("Lung = ", numOfObservations, numOfVariables, [[lungModel dataMatrix] dataMatrix]);

#if USE_PRIOR
	// constraint matrix, prior information
	NSString *priorFile = [NSString stringWithFormat:
                         @"%@/Projects/research/stem/data/xenopus/Nanostring/Prior.txt",
                         NSHomeDirectory()];
	BCDataMatrix *xenopusPrior = [BCDataMatrix dataMatrixWithContentsOfFile: priorFile andEncode: BCdoubleEncode
                                                                andFormat: [NSDictionary dictionaryWithObjectsAndKeys: BCListFormat, BCDataLayout, geneNames, BCRowNames, geneNames, BCColumnNames, nil]];
  
	double (*grid)[numOfVariables][numOfVariables];
	grid = [xenopusPrior dataMatrix];
  print_matrix("Xenopus prior = ", numOfVariables, numOfVariables, grid);
  
	// complement the matrix
	for (i = 0; i < numOfVariables; ++i)
		for (j = 0; j < numOfVariables; ++j)
			if ((*grid)[i][j] == 0) (*grid)[i][j] = 1.0;
			else (*grid)[i][j] = 0.0;
  
  print_matrix("prior matrix = ", numOfVariables, numOfVariables, grid);
#endif
  
  
  // minimum error
	BCOptimization *bco = [BCOptimization new];
	[bco setAllowSelfConnections: NO];
	//[bco addDataHandler: lungData];
	[bco addDataHandler: lungModel];
	[bco loadData: numOfVariables];
  
	//[bco outputCytoscapeGraph: grid withGeneNames: geneNames];
  
  BCDataMatrix *meA = [bco optimizeWithConstraints: 0.0];
  double (*minErrorA)[numOfVariables][numOfVariables] = [meA dataMatrix];
  double (*minErrorB)[numOfVariables][numOfVariables];
  
  print_matrix("\nA = ", numOfVariables, numOfVariables, minErrorA);
  
  double minError = [bco calculateError: minErrorA];
	printf("minError = %lf\n", minError);
  
  for (j = 0; j < numOfVariables; ++j) {
    printf("optimizeVariable: %d\n", j);
    BCDataMatrix *meB = [bco optimizeVariable: j withConstraints: 0.0];
    minErrorB = [meB dataMatrix];
    //print_matrix("\nB = ", numOfVariables, numOfVariables, minErrorB);
    for (i = 0; i < numOfVariables; ++i) (*minErrorA)[i][j] = (*minErrorB)[i][j];
  }
  
  print_matrix("\nA = ", numOfVariables, numOfVariables, minErrorA);
  minError = [bco calculateError: minErrorA];
	printf("minError = %lf\n", minError);
  
  
	// perform cross-validation optimization
	double alpha = 0.0, beta = 0.0;
	double (*A)[numOfVariables][numOfVariables];
	double (*B)[numOfVariables][numOfVariables];
  
  A = malloc(sizeof(double) * numOfVariables * numOfVariables);
  for (i = 0; i < numOfVariables; ++i)
    for (j = 0; j < numOfVariables; ++j)
      (*A)[i][j] = 0.0;
  
#if !USE_PRIOR
  // no prior network
	BCOptimizationController *bcoc = [[BCOptimizationController alloc] initWithConstraintArray: nil];  
	[bcoc setAllowSelfConnections: NO];
  
  // optimize each gene separately
  for (j = 0; j < numOfVariables; ++j) {
    [bcoc crossValidateOptimizeVariable: j withData: lungModel optimalAlpha: &alpha];
    //[bcoc crossValidateOptimizeVariable: j withData: lungData optimalAlpha: &alpha];
    
    BCDataMatrix *rMatrix = [bco optimizeVariable: j withConstraints: alpha];
    B = [rMatrix dataMatrix];
    //print_matrix("B = ", numOfVariables, numOfVariables, B);
    
    if (alpha > 0.001) {
      [bco outputCytoscapeGraph: B withGeneNames: geneNames];
      for (i = 0; i < numOfVariables; ++i) (*A)[i][j] = (*B)[i][j];
      for (i = 0; i < numOfVariables; ++i) printf("%lf ", (*A)[i][j]);
      printf("\n");
    }
    
    printf("variable: %d alpha = %lf beta = %lf\n\n", j, alpha, beta);
  }
  
  print_matrix("A = ", numOfVariables, numOfVariables, A);
  [bco outputCytoscapeGraph: A withGeneNames: geneNames];
  [bco outputCytoscapeEdgeAttributes: A withGeneNames: geneNames];
  
#else
  
  // use prior network
	BCOptimizationController *bcoc = [[BCOptimizationController alloc] initWithConstraintArray: nil defaultConstraintMatrix: xenopusPrior];
	[bcoc setAllowSelfConnections: NO];
  
  // constraint matrix
	BCDataMatrix *cMatrix = [BCDataMatrix emptyDataMatrixWithRows: numOfVariables andColumns: numOfVariables andEncode: BCdoubleEncode];
	double (*CM)[numOfVariables][numOfVariables];
	CM = [cMatrix dataMatrix];
  
  // optimize each gene separately
  for (j = 0; j < numOfVariables; ++j) {
    [bcoc crossValidateMatrixOptimizeVariable: j withData: lungData optimalAlpha: &alpha optimalBeta: &beta];
    
    for (k = 0; k < numOfVariables; ++k)
      for (l = 0; l < numOfVariables; ++l) {
        (*CM)[k][l] = alpha;
        if ((*grid)[k][l] != 0.0) (*CM)[k][l] += beta;
      }
    
    B = [bco optimizeVariable: j withConstraintMatrix: cMatrix];
    
    //print_matrix("B = ", numOfVariables, numOfVariables, B);
    
    if ((alpha > 0.001) || (beta > 0.001)) {
      [bco outputCytoscapeGraph: B withGeneNames: geneNames];
      for (i = 0; i < numOfVariables; ++i) (*A)[i][j] = (*B)[i][j];
      for (i = 0; i < numOfVariables; ++i) printf("%lf ", (*A)[i][j]);
      printf("\n");
    }
    
    printf("variable: %d alpha = %lf beta = %lf\n\n", j, alpha, beta);
  }
  
  print_matrix("A = ", numOfVariables, numOfVariables, A);
  [bco outputCytoscapeGraph: A withGeneNames: geneNames];
  [bco outputCytoscapeEdgeAttributes: A withGeneNames: geneNames];  
#endif

  print_matrix("A = ", numOfVariables, numOfVariables, A);
  print_SIF(A, geneNames, numOfVariables);
  print_EDA(A, geneNames, numOfVariables);
  
  if (networkFile) {
    NSString *s = strprint_SIF(A, geneNames, numOfVariables);
    [s writeToFile: networkFile atomically: YES encoding: NSUTF8StringEncoding error: NULL];
  }
  
  if (edgeFile) {
    NSString *s = strprint_EDA(A, geneNames, numOfVariables);
    [s writeToFile: edgeFile atomically: YES encoding: NSUTF8StringEncoding error: NULL];
  }

}

int main (int argc, const char * argv[]) {
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

  NSString *softFile = [NSString stringWithFormat:
                        @"%@/Projects/research/lung/data/GSE14334_family.soft",
                        NSHomeDirectory()];
  printf("%s\n", [softFile UTF8String]);
  
  NSString *descFile = [NSString stringWithFormat:
                        @"%@/Projects/research/lung/data/epi2Lung.txt",
                        NSHomeDirectory()];
  printf("%s\n", [descFile UTF8String]);

  NSString *outputFile = [NSString stringWithFormat:
                          @"%@/Projects/research/lung/data/GSE14334_2.txt",
                          NSHomeDirectory()];
  printf("%s\n", [outputFile UTF8String]);
  
  NSString *networkFile = [NSString stringWithFormat:
                           @"%@/Projects/research/lung/data/epi2LungNetwork.txt",
                           NSHomeDirectory()];
  printf("%s\n", [networkFile UTF8String]);

  NSString *edgeFile = [NSString stringWithFormat:
                        @"%@/Projects/research/lung/data/epi2LungEdges.txt",
                        NSHomeDirectory()];
  printf("%s\n", [edgeFile UTF8String]);
  
  NSString *sifFile = [NSString stringWithFormat:
                           @"%@/Projects/research/lung/data/epi2LungNetwork.sif",
                           NSHomeDirectory()];
  printf("%s\n", [sifFile UTF8String]);
  
  NSString *edaFile = [NSString stringWithFormat:
                        @"%@/Projects/research/lung/data/epi2LungNetwork.eda",
                        NSHomeDirectory()];
  printf("%s\n", [edaFile UTF8String]);
  
  //parseLungData(descFile, outputFile);
  //newParseLungData(softFile, descFile, outputFile);

  lungNetwork(descFile, networkFile, edgeFile);

  //analyzeLungNetwork(descFile, networkFile, edgeFile, sifFile, edaFile);
  
    [pool drain];
    return 0;
}
