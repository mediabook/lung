#include <BioSwarm/GPU_sem.h>
#include <BioSwarm/GPU_MPI.h>
#include "duct_2_defines.cu"

#include "duct_2_kernel.cu"

extern void BC_MPI_record_time(void *model, const char *msg);

// Data Transfer
void duct_2_transferGPUKernel(void *model, void *g, int aFlag, float *hostParameters, int *cellNumber, int numOfCells, int totalElements, void *hostX, void *hostY, void *hostZ, void *hostType, void *hostSector, void *hostElementNeighborTable, void *EFT, void *EFTT, void *ETT, void *EFFT, void *hostData, void *meshElements, void *hostSectorCoordinates, void *hostNumSectors, void *hostSectorSize, void *hostSectorElementTable, void *hostSectorNeighborTable, void *hostReadOnlySector)
{

 BC_SEM_transferGPUKernel(model, g, aFlag, hostParameters, cellNumber, numOfCells, totalElements, hostX, hostY, hostZ, hostType, hostSector, hostElementNeighborTable, EFT, EFTT, ETT, EFFT, hostData, meshElements, hostSectorCoordinates, hostNumSectors, hostSectorSize, hostSectorElementTable, hostSectorNeighborTable, hostReadOnlySector);

 if (aFlag) {
   // Copy data structure
   cudaMemcpyToSymbol(duct_2_sem_grids, g, sizeof(BC_SEM_GPUgrids), 0, cudaMemcpyHostToDevice);
 }
}

void duct_2_MPI_transferGPUKernel(void *model, void *g, int aFlag, void *sem, void *incomingElements, void *outgoingElements, int totalElements)
{
 BC_MPI_SEM_GPUdata *mpi_data = (BC_MPI_SEM_GPUdata *)g;
 BC_SEM_GPUgrids *sem_data = (BC_SEM_GPUgrids *)sem;

 BC_MPI_SEM_transferGPUKernel(model, g, aFlag, sem, incomingElements, outgoingElements, totalElements);

 if (sem_data->totalElements != totalElements) {
   // Copy data structure
   sem_data->totalElements = totalElements;
   cudaMemcpyToSymbol(duct_2_sem_grids, sem, sizeof(BC_SEM_GPUgrids), 0, cudaMemcpyHostToDevice);
 }
}

void *duct_2_MPI_allocGPUKernel(void *model, void *g, int boundarySize, int sectorsInDomain, void *incomingSectors, void *outgoingSectors)
{
  void *mpi_data = BC_MPI_SEM_allocGPUKernel(model, g, boundarySize, sectorsInDomain, incomingSectors, outgoingSectors);
  // Copy data structure
  cudaMemcpyToSymbol(duct_2_mpi_sem_data, mpi_data, sizeof(BC_MPI_SEM_GPUdata), 0, cudaMemcpyHostToDevice);
  cudacheck("transfer MPI data structure");
  return mpi_data;
}

// Execution
void duct_2_invokeGPUKernel(void *model, void *duct_2_data, double startTime, double nextTime)
{
 BC_SEM_GPUgrids *duct_2_ptrs = (BC_SEM_GPUgrids *)duct_2_data;
 BC_MPI_SEM_GPUdata *duct_2_mpi_ptrs = (BC_MPI_SEM_GPUdata *)duct_2_ptrs->mpiData;
 int duct_2_x = 256;
 int duct_2_y = 1;
 if (duct_2_x > duct_2_ptrs->totalElements) duct_2_x = duct_2_ptrs->totalElements;
 if (duct_2_y > duct_2_ptrs->numOfModels) duct_2_y = duct_2_ptrs->numOfModels;
 dim3 duct_2_threadsPerBlock(duct_2_x, duct_2_y);
 dim3 duct_2_threadsPerGrid((duct_2_ptrs->totalElements + duct_2_threadsPerBlock.x - 1) / duct_2_threadsPerBlock.x, (duct_2_ptrs->numOfModels + duct_2_threadsPerBlock.y - 1) / duct_2_threadsPerBlock.y);

 int duct_2_xx = 256;
 if (duct_2_xx > duct_2_ptrs->numOfCells) duct_2_xx = duct_2_ptrs->numOfCells;
 dim3 duct_2_cell_threadsPerBlock(duct_2_xx, duct_2_y);
 dim3 duct_2_cell_blocksPerGrid((duct_2_ptrs->numOfCells + duct_2_cell_threadsPerBlock.x - 1) / duct_2_cell_threadsPerBlock.x, (duct_2_ptrs->numOfModels + duct_2_cell_threadsPerBlock.y - 1) / duct_2_cell_threadsPerBlock.y);

 int duct_2_nx = 256;
 int duct_2_ny = 1;
 int duct_2_maxy = duct_2_ptrs->numOfModels * duct_2_ptrs->sectorNeighborSize * duct_2_ptrs->maxElementsPerSector;
 if (duct_2_nx > duct_2_ptrs->totalElements) duct_2_nx = duct_2_ptrs->totalElements;
 if (duct_2_ny > duct_2_maxy) duct_2_ny = duct_2_maxy;
 dim3 duct_2n_threadsPerBlock(duct_2_nx, duct_2_ny);
 dim3 duct_2n_threadsPerGrid((duct_2_ptrs->totalElements + duct_2n_threadsPerBlock.x - 1) / duct_2n_threadsPerBlock.x, (duct_2_maxy + duct_2n_threadsPerBlock.y - 1) / duct_2n_threadsPerBlock.y);

 int duct_2_sx = 256;
 int duct_2_sy = 1;
 if (duct_2_sx > duct_2_ptrs->maxSectors) duct_2_sx = duct_2_ptrs->maxSectors;
 if (duct_2_sy > duct_2_ptrs->numOfModels) duct_2_sy = duct_2_ptrs->numOfModels;
 dim3 duct_2s_threadsPerBlock(duct_2_sx, duct_2_sy);
 dim3 duct_2s_threadsPerGrid((duct_2_ptrs->maxSectors + duct_2s_threadsPerBlock.x - 1) / duct_2s_threadsPerBlock.x, (duct_2_ptrs->numOfModels + duct_2s_threadsPerBlock.y - 1) / duct_2s_threadsPerBlock.y);

 int duct_2_mx = 256;
 int duct_2_my = 1;
 if (duct_2_mx > duct_2_ptrs->numOfModels) duct_2_mx = duct_2_ptrs->numOfModels;
 dim3 duct_2m_threadsPerBlock(duct_2_mx, duct_2_my);
 dim3 duct_2m_threadsPerGrid((duct_2_ptrs->numOfModels + duct_2m_threadsPerBlock.x - 1) / duct_2m_threadsPerBlock.x, 1);

 int duct_2_mpi_x = 256;
 int duct_2_mpi_maxx = duct_2_mpi_ptrs->boundarySize * duct_2_ptrs->maxElementsPerSector;
 if (duct_2_mpi_x > duct_2_mpi_maxx) duct_2_mpi_x = duct_2_mpi_maxx;
 dim3 duct_2_mpi_threadsPerBlock(duct_2_mpi_x, 1);
 dim3 duct_2_mpi_blocksPerGrid((duct_2_mpi_maxx + duct_2_mpi_threadsPerBlock.x - 1) / duct_2_mpi_threadsPerBlock.x, 1);
 int duct_2s_mpi_x = 256;
 int duct_2s_mpi_maxx = duct_2_mpi_ptrs->boundarySize;
 if (duct_2s_mpi_x > duct_2s_mpi_maxx) duct_2s_mpi_x = duct_2s_mpi_maxx;
 dim3 duct_2s_mpi_threadsPerBlock(duct_2s_mpi_x, 1);
 dim3 duct_2s_mpi_blocksPerGrid((duct_2s_mpi_maxx + duct_2s_mpi_threadsPerBlock.x - 1) / duct_2s_mpi_threadsPerBlock.x, 1);

   // Invoke kernel
   double currentTime = startTime;
   int done = 0;
   while (!done) {
      if (duct_2_ptrs->calcCellCenters) duct_2_SEM_center_kernel<<< duct_2_cell_blocksPerGrid, duct_2_cell_threadsPerBlock >>>();
      if (duct_2_ptrs->calcElementCenters) duct_2_SEM_element_center_kernel<<< duct_2_cell_blocksPerGrid, duct_2_cell_threadsPerBlock >>>();

      BC_MPI_record_time(model, "start SEM_pairwise_kernel1_2rk");
      duct_2_SEM_pairwise_kernel1_2rk<<< duct_2_threadsPerGrid, duct_2_threadsPerBlock >>>(currentTime, nextTime);
      cudacheck("invoke duct_2_SEM_pairwise_kernel1_2rk");
      BC_MPI_record_time(model, "end SEM_pairwise_kernel1_2rk");

      BC_MPI_record_time(model, "start BC_MPI_SEM_MODE_F1");
      duct_2_MPI_SEM_gather<<< duct_2_mpi_blocksPerGrid, duct_2_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_F1);
      BC_MPI_initiate_transfer(model, BC_MPI_SEM_MODE_F1, currentTime, nextTime);
      duct_2_MPI_SEM_scatter<<< duct_2_mpi_blocksPerGrid, duct_2_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_F1);
      BC_MPI_record_time(model, "end BC_MPI_SEM_MODE_F1");

      BC_MPI_record_time(model, "start SEM_pairwise_kernel2_2rk");
      duct_2_SEM_pairwise_kernel2_2rk<<< duct_2_threadsPerGrid, duct_2_threadsPerBlock >>>(currentTime, nextTime);
      cudacheck("invoke duct_2_SEM_pairwise_kernel2_2rk");
      BC_MPI_record_time(model, "end SEM_pairwise_kernel2_2rk");

      BC_MPI_record_time(model, "start sector_kernels");
      duct_2_SEM_sector_count_kernel<<< duct_2s_threadsPerGrid, duct_2s_threadsPerBlock >>>(currentTime, nextTime);
      cudacheck("invoke duct_2_SEM_sector_count_kernel");

      duct_2_SEM_create_sectors_kernel<<< duct_2m_threadsPerGrid, duct_2m_threadsPerBlock >>>(currentTime, nextTime);
      cudacheck("invoke duct_2_SEM_create_sectors_kernel");

      duct_2_SEM_build_sector_neighbors_kernel<<< duct_2s_threadsPerGrid, duct_2s_threadsPerBlock >>>(currentTime, nextTime);
      cudacheck("invoke duct_2_SEM_build_sector_neighbors_kernel");

      duct_2_SEM_move_elements_kernel<<< duct_2s_threadsPerGrid, duct_2s_threadsPerBlock >>>(currentTime, nextTime);
      cudacheck("invoke duct_2_SEM_move_elements_kernel");
      BC_MPI_record_time(model, "end sector_kernels");

      BC_MPI_record_time(model, "end BC_MPI_SEM_MODE_MOVE_READ_ONLY");
      duct_2_MPI_SEM_gather<<< duct_2_mpi_blocksPerGrid, duct_2_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE_READ_ONLY);
      BC_MPI_initiate_transfer(model, BC_MPI_SEM_MODE_MOVE_READ_ONLY, currentTime, nextTime);
      duct_2_MPI_SEM_scatter<<< duct_2_mpi_blocksPerGrid, duct_2_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE_READ_ONLY);
      duct_2_MPI_SEM_sector_scatter<<< duct_2s_mpi_blocksPerGrid, duct_2s_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE_READ_ONLY);
      BC_MPI_record_time(model, "end BC_MPI_SEM_MODE_MOVE_READ_ONLY");

      BC_MPI_record_time(model, "end BC_MPI_SEM_MODE_MOVE");
      duct_2_MPI_SEM_gather<<< duct_2_mpi_blocksPerGrid, duct_2_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE);
      BC_MPI_initiate_transfer(model, BC_MPI_SEM_MODE_MOVE, currentTime, nextTime);
      duct_2_MPI_SEM_scatter<<< duct_2_mpi_blocksPerGrid, duct_2_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE);
      duct_2_MPI_SEM_sector_scatter<<< duct_2s_mpi_blocksPerGrid, duct_2s_mpi_threadsPerBlock >>>(currentTime, nextTime, BC_MPI_SEM_MODE_MOVE);
      BC_MPI_record_time(model, "end BC_MPI_SEM_MODE_MOVE");

      duct_2_SEM_update_element_sector_kernel<<< duct_2s_threadsPerGrid, duct_2s_threadsPerBlock >>>(currentTime, nextTime);
      cudacheck("invoke duct_2_SEM_update_element_sector_kernel");


      currentTime += duct_2_ptrs->dt;
      if (TIME_EQUAL(currentTime, nextTime) || currentTime > nextTime) done = 1;
   }
}

void duct_2_MPI_invokeGPUKernel(void *model, void *duct_2_data, double startTime, double nextTime)
{
 BC_MPI_SEM_GPUdata *duct_2_ptrs = (BC_MPI_SEM_GPUdata *)duct_2_data;

}

// GPU function pointers
SEMfunctions duct_2_gpuFunctions = {BC_SEM_allocGPUKernel, duct_2_transferGPUKernel, duct_2_invokeGPUKernel, BC_SEM_releaseGPUKernel, duct_2_MPI_allocGPUKernel, duct_2_MPI_transferGPUKernel, duct_2_MPI_invokeGPUKernel, BC_MPI_SEM_releaseGPUKernel};
