


gpuAlveolis for Physical Biology paper
--------------------------------------

I have a hack for the GPU kernel function in order to integrate the
pressure function as an input into the force calculation. At the
time, BioSwarm does not support generate functions for calculation,
and I needed to multiply the force interaction by the experimental
data for the pressure values for Control and Tween-injured. So as a
hack, I check the index for the interaction in the force table and
add the multiplier. This code is in custom_alveolis_2_kernel.cu, so
in order to compile, you first let the makefile generate the GPU code
using modelToGPU, then copy the custom kernel code over the generated
code, and relink the executable.
