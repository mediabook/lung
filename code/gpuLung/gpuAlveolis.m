//
// gpuAlveolis.m
//
// 3D model of alveolis sac
//

#include <Foundation/Foundation.h>
#import <BioSwarm/BioSwarmManager.h>
#import <BioSwarm/SimulationEvent.h>
#import <BioSwarm/EvolutionaryStrategy.h>
#import <BioSwarm/SubcellularElementModel.h>
#import <BioCocoa/BCDataMatrix.h>

extern void *getGPUFunctionForModel(int aModel, int value);

@interface MyDelegate : NSObject
{
  int totalElements;
  float *initX;
  float *initY;
  float *initZ;
  int *initT;
  int *initC;
  
  BCDataMatrix *controlData;
  int controlCols;
  int controlRows;
  
  BCDataMatrix *tweenData;
  int tweenCols;
  int tweenRows;

  BCDataMatrix *severeData;
  int severeCols;
  int severeRows;

  int totalParameters;
  NSArray *parameterNames;
  NSArray *parameterIndexes;
  NSArray *parameterModelNumbers;
  NSArray *parameterModelKeys;
  double *parameterMinRange;
  double *parameterMaxRange;
  void *acceptParametersValues;
  void *rejectParametersValues;

  NSString *expectationFileName;
}

- (void)didInitializeSimulation: theController;
//- (void)willRunSimulation: theController;
- (void)performAssignPressure: theControlller;
- (void)recordDiameter: theControlller;
- (double)calculateFitness: theController forIndividual: (int)modelIndex;
- (void)bestFitness: (double)aFitness forGeneration: (int)aGen controller: theController individual: (int)modelIndex;

// MCMC delegate
- (void)parameterInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes
	 modelNumbers: (NSArray *)modelNumbers modelKeys: (NSArray *)modelKeys
            minRanges: (double *)minRange maxRange: (double *)maxRange acceptValues: (void *)acceptValues rejectValues:(void *)rejectValues;
- (void)willPerformMCMC: theController setNumber: (int)setNum;
- (void)didPerformMCMC: theController setNumber: (int)setNum;
- (void)allocatePosterior: theController;
- (void)willCalculatePosteriorDensity: theController forIndividual: (int)modelIndex;
- (double)calculatePosteriorDensity: theController forIndividual: (int)modelIndex;
- (void)initExpectation: theController;
- (void)calculateExpectation: theController weights: (double *)particleWeights accept: (BOOL *)acceptFlags;
- (void)finishExpectation: theController;
@end

@implementation MyDelegate
- init
{
	int i, j;
	
  [super init];

  FILE *initFile = fopen("initial_alveolis_cells.dat", "r");
  if (!initFile) printf("ERROR: could not open file: initial_alveolis_cells.dat\n");
  else {
    int i;
    float ct;
    fscanf(initFile, "%f %d", &ct, &totalElements);
    initX = (float *)malloc(sizeof(float) * totalElements);
    initY = (float *)malloc(sizeof(float) * totalElements);
    initZ = (float *)malloc(sizeof(float) * totalElements);
    initT = (int *)malloc(sizeof(int) * totalElements);
    initC = (int *)malloc(sizeof(int) * totalElements);

    for (i = 0; i < totalElements; ++i) {
      fscanf(initFile, "%f", &(initX[i]));
      fscanf(initFile, "%f", &(initY[i]));
      fscanf(initFile, "%f", &(initZ[i]));
      fscanf(initFile, "%d", &(initT[i]));
      fscanf(initFile, "%d", &(initC[i]));
    }

#if 0
    printf("%d", totalElements);
    for (i = 0; i < totalElements; ++i) printf(" %f %f %f %d %d", initX[i], initY[i], initZ[i], initT[i], initC[i]);
    printf("\n");
#endif
  }

	controlData = [BCDataMatrix dataMatrixWithContentsOfFile: @"lungControl.txt"
		andEncode: BCdoubleEncode andFormat: [NSDictionary dictionaryWithObjectsAndKeys:
		[NSNumber numberWithBool: YES], BCParseColumnNames, nil]];
  if (!controlData) printf("ERROR: could not open file: lungControl.txt\n");
  controlCols = [controlData numberOfColumns];
  controlRows = [controlData numberOfRows];
  printf("%d %d\n", controlCols, controlRows);
	double (*CD)[controlRows][controlCols] = [controlData dataMatrix];
	
	// rescale the time
	for (i = 0; i < controlRows; ++i) {
		(*CD)[i][0] = 60.0 * (*CD)[i][0] + 20.0;
	}

	for (i = 0; i < controlRows; ++i) {
		for (j = 0; j < controlCols; ++j)	printf("%f ", (*CD)[i][j]);
		printf("\n");
	}

	tweenData = [BCDataMatrix dataMatrixWithContentsOfFile: @"lungTween.txt"
		andEncode: BCdoubleEncode andFormat: [NSDictionary dictionaryWithObjectsAndKeys:
		[NSNumber numberWithBool: YES], BCParseColumnNames, nil]];
  if (!tweenData) printf("ERROR: could not open file: lungTween.txt\n");
  tweenCols = [tweenData numberOfColumns];
  tweenRows = [tweenData numberOfRows];
  printf("%d %d\n", tweenCols, tweenRows);
	double (*TD)[tweenRows][tweenCols] = [tweenData dataMatrix];

	// rescale the time
	for (i = 0; i < tweenRows; ++i) {
		(*TD)[i][0] = 60.0 * (*TD)[i][0] + 20.0;
	}

	for (i = 0; i < tweenRows; ++i) {
		for (j = 0; j < tweenCols; ++j)	printf("%f ", (*TD)[i][j]);
		printf("\n");
	}

	severeData = [BCDataMatrix dataMatrixWithContentsOfFile: @"severeTween.txt"
		andEncode: BCdoubleEncode andFormat: [NSDictionary dictionaryWithObjectsAndKeys:
		[NSNumber numberWithBool: YES], BCParseColumnNames, nil]];
  if (!tweenData) printf("ERROR: could not open file: severeTween.txt\n");
  severeCols = [severeData numberOfColumns];
  severeRows = [severeData numberOfRows];
  printf("%d %d\n", severeCols, severeRows);
	double (*SD)[severeRows][severeCols] = [severeData dataMatrix];

	// rescale the time
	double offset = (*SD)[0][0];
	for (i = 0; i < severeRows; ++i) {
		(*SD)[i][0] = 60.0 * ((*SD)[i][0] - offset) + 20.0;
	}

	for (i = 0; i < severeRows; ++i) {
		for (j = 0; j < severeCols; ++j)	printf("%f ", (*SD)[i][j]);
		printf("\n");
	}

  return self;
}

- (void)didInitializeSimulation: theController
{
	int i;
	printf("didInitializeSimulation:\n");

	// setup simulation events for pressure input	
	NSInvocation *anInv = [NSInvocation invocationWithMethodSignature: [self methodSignatureForSelector: @selector(performAssignPressure:)]];
  [anInv setTarget: self];
  [anInv setSelector: @selector(performAssignPressure:)];
  [anInv setArgument: &theController atIndex: 2];

	if (![theController isPerturbation]) {
		double (*CD)[controlRows][controlCols] = [controlData dataMatrix];
		for (i = 0; i < controlRows; ++i) {
	  	SimulationEvent *anEvent = [SimulationEvent eventWithInvocation: anInv atTime: (*CD)[i][0]];
  		[anEvent setRequireTransfer: YES];
  		[theController addSimulationEvent: anEvent];
  	}
  } else {
		printf("perturb: %s\n", [[theController perturbationName] UTF8String]);
		if ([[theController perturbationName] isEqualToString: @"tween"]) {
			double (*TD)[tweenRows][tweenCols] = [tweenData dataMatrix];
			for (i = 0; i < tweenRows; ++i) {
	  		SimulationEvent *anEvent = [SimulationEvent eventWithInvocation: anInv atTime: (*TD)[i][0]];
  			[anEvent setRequireTransfer: YES];
  			[theController addSimulationEvent: anEvent];
  		}
  	} else if ([[theController perturbationName] isEqualToString: @"severe"]) {
			double (*SD)[severeRows][severeCols] = [severeData dataMatrix];
			for (i = 0; i < severeRows; ++i) {
	  		SimulationEvent *anEvent = [SimulationEvent eventWithInvocation: anInv atTime: (*SD)[i][0]];
  			[anEvent setRequireTransfer: YES];
  			[theController addSimulationEvent: anEvent];
  		}
  	} else {
  		printf("ERROR: unknown perturbation: %s\n", [[theController perturbationName] UTF8String]);
  		exit(1);
  	}
  }

	// setup simulation event to record diameter
	anInv = [NSInvocation invocationWithMethodSignature: [self methodSignatureForSelector: @selector(recordDiameter:)]];
  [anInv setTarget: self];
  [anInv setSelector: @selector(recordDiameter:)];
  [anInv setArgument: &theController atIndex: 2];

  SimulationEvent *anEvent = [SimulationEvent eventWithInvocation: anInv atTime: 20.0 repeat: 2.0];
 	[anEvent setRequireTransfer: YES];
 	[theController addSimulationEvent: anEvent];
}

- (void)performAssignPressure: theController
{
	int i;
	double (*CD)[controlRows][controlCols] = [controlData dataMatrix];
	double (*TD)[tweenRows][tweenCols] = [tweenData dataMatrix];
	double (*SD)[severeRows][severeCols] = [severeData dataMatrix];

	double ct = [theController currentTime];
	printf("performAssignPressure: %f\n", ct);
	
	int modelIndex;
	for (modelIndex = 0; modelIndex < [theController numModelInstances]; ++modelIndex) {
		SubcellularElementModel *sem = [theController modelOfTypeClass: [SubcellularElementModel class] forModel: modelIndex];
		
		if (![theController isPerturbation]) {
			for (i = 0; i < controlRows; ++i) {
				if (TIME_EQUAL((*CD)[i][0], ct)) {
					NSNumber *n = [sem valueOfSpecies: 0];
					printf("Set pressure: %f old: %f\n", (*CD)[i][1], [n doubleValue]);
					[sem setValue: (*CD)[i][1] forSpecies: @"P"];
				}
			}
		} else {
			printf("perturb: %s\n", [[theController perturbationName] UTF8String]);
			if ([[theController perturbationName] isEqualToString: @"tween"]) {
				for (i = 0; i < tweenRows; ++i) {
					if (TIME_EQUAL((*TD)[i][0], ct)) {
						NSNumber *n = [sem valueOfSpecies: 0];
						printf("Set pressure: %f old: %f\n", (*TD)[i][1], [n doubleValue]);
						[sem setValue: (*TD)[i][1] forSpecies: @"P"];
					}
				}
			} else if ([[theController perturbationName] isEqualToString: @"severe"]) {
				for (i = 0; i < severeRows; ++i) {
					if (TIME_EQUAL((*SD)[i][0], ct)) {
						NSNumber *n = [sem valueOfSpecies: 0];
						printf("Set pressure: %f old: %f\n", (*SD)[i][6], [n doubleValue]);
						[sem setValue: (*SD)[i][1] forSpecies: @"P"];
					}
				}
			} else {
				printf("ERROR: unknown perturbation: %s\n", [[theController perturbationName] UTF8String]);
				exit(1);
			}
		}
	}
}

#define DIST(X1, Y1, Z1, X2, Y2, Z2) sqrt( ((X1) - (X2))*((X1) - (X2)) + ((Y1) - (Y2))*((Y1) - (Y2)) + ((Z1) - (Z2))*((Z1) - (Z2)) )

- (void)recordDiameter: theController
{
	int i, j;
  NSArray *modelInstances = [theController modelInstances];
	int modelIndex;
	for (modelIndex = 0; modelIndex < [modelInstances count]; ++modelIndex) {
	  id modelObject = [modelInstances objectAtIndex: modelIndex];
  	SubcellularElementModel *sem = [modelObject modelOfTypeClass: [SubcellularElementModel class]];

	  int maxElements = [sem maxElements];
  	float (*xPos)[1][maxElements] = [sem elementX];
  	float (*yPos)[1][maxElements] = [sem elementY];
  	float (*zPos)[1][maxElements] = [sem elementZ];
  	int (*eType)[1][maxElements] = [sem elementType];

		double d = 0.0;
		int c = 0;
  	for (i = 0; i < totalElements; ++i) {
	  	for (j = i; j < totalElements; ++j) {
				d += DIST((*xPos)[0][i], (*yPos)[0][i], (*zPos)[0][i], (*xPos)[0][j], (*yPos)[0][j], (*zPos)[0][j]);
				//printf("(%d %d %d): %f %f %f, %f %f %f\n", modelIndex, i, j, (*xPos)[0][i], (*yPos)[0][i], (*zPos)[0][i], (*xPos)[0][j], (*yPos)[0][j], (*zPos)[0][j]);
				++c;
	  	}
		}
		d = d / (double)c;

		// scale
		d *= 250;

		printf("recordDiameter (%d): %f\n", modelIndex, d);
		NSNumber *n = [sem valueOfSpecies: 1];
		printf("Set diameter: %f old: %f\n", d, [n doubleValue]);
		[sem setValue: d forSpecies: @"Diameter"];

#if 1
		if (modelIndex == 0) {
			FILE *f = fopen("diameter.txt", "a");
  		if (!f) printf("ERROR: could not open file: diameter.txt\n");
			fprintf(f, "%f\n", d);
			fclose(f);
		}
#endif
	}

}

- (double)calculateFitness: theController forIndividual: (int)modelIndex
{
  NSArray *modelInstances = [theController modelInstances];
  id modelObject = [modelInstances objectAtIndex: modelIndex];
  SubcellularElementModel *sem = [modelObject modelOfTypeClass: [SubcellularElementModel class]];

  int maxElements = [sem maxElements];
  float (*xPos)[0][maxElements] = [sem elementX];
  float (*yPos)[0][maxElements] = [sem elementY];
  float (*zPos)[0][maxElements] = [sem elementZ];
  int (*eType)[0][maxElements] = [sem elementType];

  double error = 0.0;
  int i;
  for (i = 0; i < totalElements; ++i) {
    if ((*eType)[0][i] != initT[i]) printf("WARNING: element types do not match\n");
    error += (initX[i] - (*xPos)[0][i]) * (initX[i] - (*xPos)[0][i]);
    error += (initY[i] - (*yPos)[0][i]) * (initY[i] - (*yPos)[0][i]);
    error += (initZ[i] - (*zPos)[0][i]) * (initZ[i] - (*zPos)[0][i]);
  }

  return error;
}

- (void)bestFitness: (double)aFitness forGeneration: (int)aGen controller: theController individual: (int)modelIndex
{
  printf("New best individual: (%d)%f at generation %d\n", modelIndex, aFitness, aGen);
}

//
// MCMC delegate
//
- (void)parameterInfo: theController names: (NSArray *)names indexes: (NSArray *)indexes
	 modelNumbers: (NSArray *)modelNumbers modelKeys: (NSArray *)modelKeys
            minRanges: (double *)minRange maxRange: (double *)maxRange acceptValues: (void *)acceptValues rejectValues:(void *)rejectValues
{
  parameterNames = names;
  parameterIndexes = indexes;
  parameterModelNumbers = modelNumbers;
  parameterModelKeys = modelKeys;
  parameterMinRange = minRange;
  parameterMaxRange = maxRange;
  totalParameters = [parameterNames count];
  acceptParametersValues = acceptValues;
  rejectParametersValues = rejectValues;
  printf("parameterInfo: %d\n", totalParameters);
}

- (void)willPerformMCMC: theController setNumber: (int)setNum
{
  printf("willPerformMCMC:setNumber:\n");
  int i;

  expectationFileName = [[NSString stringWithFormat: @"smc_exp_%d.dat", setNum] retain];

  // zero output file
  FILE *out = fopen([expectationFileName UTF8String], "w");
  for (i = 0; i < totalParameters; ++i) {
    if (i != 0) fprintf(out, " ");
    NSString *pn = [parameterNames objectAtIndex: i];
    fprintf(out, "%s", [pn UTF8String]);
  }
  fprintf(out, " weight\n");
  fclose(out);
}

- (void)didPerformMCMC: theController setNumber: (int)setNum
{
  [expectationFileName release];
  expectationFileName = nil;
}

- (void)allocatePosterior: theController
{
}

- (void)willCalculatePosteriorDensity: theController forIndividual: (int)modelIndex
{
}

- (double)calculatePosteriorDensity: theController forIndividual: (int)modelIndex
{
}

- (void)initExpectation: theController
{
  printf("initExpectation:\n");
}

- (void)calculateExpectation: theController weights: (double *)particleWeights accept: (BOOL *)acceptFlags
{
  int i, j;
  //if (DEBUG) printf("calculateExpectation:weights:accept\n");
  int numOfParticles = [[theController modelInstances] count];
  printf("calculateExpectation:weights:accept:  %d %d\n", numOfParticles, totalParameters);

  double (*acceptParameters)[numOfParticles][totalParameters] = acceptParametersValues;
  double (*rejectParameters)[numOfParticles][totalParameters] = rejectParametersValues;

  FILE *out = fopen([expectationFileName UTF8String], "a");
  for (i = 0; i < numOfParticles; ++i) {
    for (j = 0; j < totalParameters; ++j) {
      if (j != 0) fprintf(out, " ");
      if (acceptFlags[i]) fprintf(out, "%f", (*acceptParameters)[i][j]);
      else fprintf(out, "%f", (*rejectParameters)[i][j]);
    }
    if (particleWeights) fprintf(out, " %f\n", particleWeights[i]);
    else fprintf(out, " 1\n");
  }
  fclose(out);
}

- (void)finishExpectation: theController
{
}

@end

int crypt2d()
{
  NSMutableDictionary *runParameters;
  NSAutoreleasePool *pool;
  NSString *paramFile;
  id s;

  pool = [NSAutoreleasePool new];

  BioSwarmManager *simManager = [[BioSwarmManager alloc] initWithDefaultModelFile: MODEL_FILE];
  id delegate = [MyDelegate new];
  [simManager setDelegate: delegate];

  if ([simManager processArguments: [[NSProcessInfo processInfo] arguments]]) {
    [simManager setGPUFunctions: &getGPUFunctionForModel];
    [simManager startSimulation];
  }

  [simManager release];
  [delegate release];

  [pool release];
  return 0;
}

int
main( int argc, char** argv) 
{
  return crypt2d();
}
