#include <stdio.h>

main()
{
  double d = 12.0;
  float t3 = 0.66666662;
  //float t3 = 0.3333337;

  // moving down, so want to round down
  // but normal float rounds up
  printf("\nmoving down, but error rounds up\n");
  d += t3;
  float f = 12.0 + t3;
  float t4 = f - 12.0;
  //if (f == 10.0) printf("yes\n");
  printf("double %.15lf, float 12.0 + %.15f = %.15f, %.15f\n", d, t3, f, t4);

  if (t4 > t3) printf("precision lost\n");
  f -= 0.000001;
  printf("adjusted float 12.0 + %.15f = %.15f\n", t3, f);
  t4 = f - 12.0;
  if (t4 < t3) printf("corrected inequality\n");

  // moving up, so want to round up
  // but normal float rounds down
  printf("\nmoving up, but error rounds down\n");
  t3 = -0.3333334;
  d = 12.0 + t3;
  f = 12.0 + t3;
  t4 = f - 12.0;

  printf("double %.15lf, float 12.0 + %.15f = %.15f, %.15f\n", d, t3, f, t4);
  if (t4 < t3) printf("yes\n");
  f += 0.000001;
  printf("adjusted float 12.0 + %.15f = %.15f\n", t3, f);
  t4 = f - 12.0;
  if (t4 > t3) printf("yes\n");

  // 
  printf("\nbig number + small number\n");
  t3 = -0.0000001;
  d = 12.0 + t3;
  f = 12.0 + t3;
  t4 = f - 12.0;

  printf("double %.15lf, float 12.0 + %.15f = %.15f, %.15f\n", d, t3, f, t4);
  if (t4 > t3) printf("precision lost\n");
  f -= 0.000001;
  printf("adjusted float 12.0 + %.15f = %.15f\n", t3, f);
  t4 = f - 12.0;
  if (t4 < t3) printf("corrected inequality\n");

  printf("\nbig number + small number\n");
  t3 = 0.0000001;
  d = 12.0 + t3;
  f = 12.0 + t3;
  t4 = f - 12.0;

  printf("double %.15lf, float 12.0 + %.15f = %.15f, %.15f\n", d, t3, f, t4);
  if (t4 < t3) printf("precision lost\n");
  f += 0.000001;
  printf("adjusted float 12.0 + %.15f = %.15f\n", t3, f);
  t4 = f - 12.0;
  if (t4 > t3) printf("corrected inequality\n");


  t3 = -0.000001649883529;
  f = 24.0 + t3;
  t4 = f - 24.0;
  printf("float 24.0 + %.15f = %.15f, %.15f\n", t3, f, t4);

}
