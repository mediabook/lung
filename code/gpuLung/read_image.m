//
//  read_image.m
//
//  Written by: Scott Christley <schristley@mac.com>
//  Copyright (c) 2014-2015 Scott Christley
//  All rights reserved.
//
//  This program takes the image stack initially processed
//  by processImageSamples, cleans up noise, reconstitutes
//  the lung microstructure, and saves the data for use by model.
//

#import <Foundation/Foundation.h>
#import <AppKit/NSImage.h>
#import <BioSwarm/GPUDefines.h>
#import <AppKit/NSGraphics.h>

//
// We hard-code much of the processing as each sample has specific
// processing requirements, and attempting to generalize is too much effort.
//
// LUNG_SAMPLE is defined in this file, change it to process desired sample.
//
#include "lung_sample.h"


typedef struct _TISSUE {
  int bytesPerPixel;
  int numCols;
  int numRows;
  int numDepth;
  void *data;
  NSBitmapImageRep *rep;
} TISSUE;

// helper class
@interface TissuePoint : NSObject {
@public
  int k, i, j;
}
+ newWithPoints: (int)kp :(int)ki :(int)kj;
@end
@implementation TissuePoint
+ newWithPoints: (int)kp :(int)ip :(int)jp
{
  TissuePoint *p = [TissuePoint new];
  p->k = kp; p->i = ip; p->j = jp;
  return p;
}
@end

// main processing routine
void process_sample();

int main()
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];

  process_sample();

  [pool release];
  return 0;
}

//
// Instead of writing a generalized routine that attempts
// to handle all the different sample data, we write a function
// specialized for each one.
//

// LUNG_SAMPLE = 1
void process_R120_LS1();
// LUNG_SAMPLE = 2
void process_R120_LS2();
// LUNG_SAMPLE = 3
void process_R120_LS3();
// LUNG_SAMPLE = 4
void process_R75_LS4();
// LUNG_SAMPLE = 5
void process_R75_LS5();
// LUNG_SAMPLE = 6
void process_R79_LS6();
// LUNG_SAMPLE = 7
void process_R79_LS7();
// LUNG_SAMPLE = 8
void process_R83_LS8();
// LUNG_SAMPLE = 9
void process_R83_LS9();
// LUNG_SAMPLE = 10
void process_R84_LS10();

// This just calls the sample specific processing function
void process_sample()
{
  printf("Processing sample:\n%s\n", SAMPLE_DESC);

#if LUNG_SAMPLE == 1
  process_R120_LS1();
#elif LUNG_SAMPLE == 2
  process_R120_LS2();
#elif LUNG_SAMPLE == 3
  process_R120_LS3();
#elif LUNG_SAMPLE == 4
  process_R75_LS4();
#elif LUNG_SAMPLE == 5
  process_R75_LS5();
#elif LUNG_SAMPLE == 6
  process_R79_LS6();
#elif LUNG_SAMPLE == 7
  process_R79_LS7();
#elif LUNG_SAMPLE == 8
  process_R83_LS8();
#elif LUNG_SAMPLE == 9
  process_R83_LS9();
#elif LUNG_SAMPLE == 10
  process_R84_LS10();
#else
  process_APRV75_Inspiration_Sample1(tissue);
#endif
}

//
// Generic routines
//

void read_image_data(TISSUE *tissue)
{
  int currentImage;
  NSString *prefixName = [NSString stringWithFormat: @"%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_DIR];
	
  tissue->numDepth = MESH_END_IMAGE - MESH_START_IMAGE + 1;

  printf("%d representations\n", tissue->numDepth);

  for (currentImage = MESH_START_IMAGE; currentImage <= MESH_END_IMAGE; ++currentImage) {
    NSMutableString *imageName = [NSMutableString stringWithFormat: @"%@%s", prefixName, OUTPUT_NAME];
    if (currentImage < 100) [imageName appendString: @"000"];
    else if (currentImage < 1000) [imageName appendString: @"00"];
    else [imageName appendString: @"0"];
    [imageName appendFormat: @"%d", currentImage];
    [imageName appendString: @".png"];

    printf("Reading image file:\n%s\n", [imageName UTF8String]);
    NSImage* tempImage = [[NSImage alloc] initWithContentsOfFile:imageName];
    if (!tempImage) {
      printf("Could not open image file.\n");
      abort();
    }
    NSArray* reps = [tempImage representations];
    NSBitmapImageRep *rep = [reps objectAtIndex: 0];

    int i, j, k;
    if (currentImage == MESH_START_IMAGE) {
      printf("bitsPerPixel = %ld\n", (long)[rep bitsPerPixel]);
      printf("bytesPerPlane = %ld\n", (long)[rep bytesPerPlane]);
      printf("bytesPerRow = %ld\n", (long)[rep bytesPerRow]);
      printf("numberOfPlanes = %ld\n", (long)[rep numberOfPlanes]);

      tissue->bytesPerPixel = [rep bitsPerPixel] / 8; // should be 3
      tissue->numCols = [rep bytesPerRow] / tissue->bytesPerPixel;
      tissue->numRows = [rep bytesPerPlane] / [rep bytesPerRow];
      printf("size = (%d, %d)\n", tissue->numCols, tissue->numRows);

      tissue->data = malloc(sizeof(char) * tissue->numDepth * tissue->numRows * tissue->numCols);
      char (*bitmapData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
      for (k = 0; k < tissue->numDepth; ++k)
	for (i = 0; i < tissue->numRows; ++i)
	  for (j = 0; j < tissue->numCols; ++j)
	    (*bitmapData)[k][i][j] = -1;
    }
    char (*bitmapData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;

    k = currentImage - MESH_START_IMAGE;
    unsigned char *bd = [rep bitmapData];
    int pos = 0;
    for (i = tissue->numRows-1; i >= 0; --i) // flip coordinates
      for (j = 0; j < tissue->numCols; ++j) {
	(*bitmapData)[k][i][j] = bd[pos];
	++pos;
      }

  }

}

void set_image_rep(TISSUE *tissue)
{
  int currentImage;
  NSString *prefixName = [NSString stringWithFormat: @"%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_DIR];

  currentImage = MESH_START_IMAGE;
  NSMutableString *imageName = [NSMutableString stringWithFormat: @"%@%s", prefixName, OUTPUT_NAME];
  if (currentImage < 100) [imageName appendString: @"000"];
  else if (currentImage < 1000) [imageName appendString: @"00"];
  else [imageName appendString: @"0"];
  [imageName appendFormat: @"%d", currentImage];
  [imageName appendString: @".png"];

  printf("Reading image file:\n%s\n", [imageName UTF8String]);
  NSImage* tempImage = [[NSImage alloc] initWithContentsOfFile:imageName];
  if (!tempImage) {
    printf("Could not open image file.\n");
    abort();
  }
  NSArray* reps = [tempImage representations];
  NSBitmapImageRep *rep = [reps objectAtIndex: 0];

  NSData *d = [rep representationUsingType: NSPNGFileType properties: nil];
  tissue->rep = [[NSBitmapImageRep imageRepWithData: d] retain];
}

void read_segment_data(TISSUE *tissue, NSString *filename)
{
  FILE *segmentFile = fopen([filename UTF8String], "r");
  if (!segmentFile) {
    printf("Could not open file: %s\n", [filename UTF8String]);
    abort();
  }

  int numVertex, numFace;
  char line[1024];
  BOOL done = NO;
  while (!done) {
    int i = 0;
    char c = 0;
    while ((c != '\n') & !feof(segmentFile)) { c = fgetc(segmentFile); line[i++] = c; }
    if (c == '\n') --i;
    line[i] = 0;
		
    printf("%s\n", line);

    if (strncmp(line, "element vertex", 14) == 0) {
      numVertex = atoi(&(line[14]));
      printf("vertices: %d\n", numVertex);
    }

    if (strncmp(line, "element face", 12) == 0) {
      numFace = atoi(&(line[12]));
      printf("faces: %d\n", numFace);
    }
		
    if (strcmp(line, "end_header") == 0) done = YES;
  }

}

void trace_airway_xy(TISSUE *tissue, TISSUE *airway, NSMutableArray *points, int level, int maxLevel)
{
  if ((maxLevel != -1) & (level == maxLevel)) return;

  int i, j, k, p;

  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
	
  //printf("%d: %lu points\n", level, (unsigned long)[points count]);
  NSMutableArray *newPoints = [NSMutableArray new];
  for (p = 0; p < [points count]; ++p) {
    TissuePoint *tp = [points objectAtIndex: p];
    k = tp->k; i = tp->i; j = tp->j;

    // already visited?
    if ((*airwayData)[k][i][j] != -1) continue;

    // visit
    (*airwayData)[k][i][j] = TISSUE_VALUE;
		
    // stop at tissue or border
    if ((*tissueData)[k][i][j]) continue;

    // 6 directions
    int up = i + 1;
    int down = i - 1;
    int right = j + 1;
    int left = j - 1;
    //int back = k + 1;
    //int front = k - 1;

    if (up < tissue->numRows) [newPoints addObject: [TissuePoint newWithPoints: k : up : j]];
    if (down >= 0) [newPoints addObject: [TissuePoint newWithPoints: k : down : j]];
    if (right < tissue->numCols) [newPoints addObject: [TissuePoint newWithPoints: k : i : right]];
    if (left >= 0) [newPoints addObject: [TissuePoint newWithPoints: k : i : left]];
    //if (back < tissue->numDepth) [newPoints addObject: [TissuePoint newWithPoints: back : i : j]];
    //if (front >= 0) [newPoints addObject: [TissuePoint newWithPoints: front : i : j]];
  }
	
  [points release];
  if ([newPoints count] == 0) return;
  trace_airway_xy(tissue, airway, newPoints, level + 1, maxLevel);
}

void subtrace_airway_xy(TISSUE *tissue, TISSUE *airway, NSMutableArray *points, int level, int maxLevel)
{
  if ((maxLevel != -1) & (level == maxLevel)) return;

  int i, j, k, p;

  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
	
  //printf("%d: %lu points\n", level, (unsigned long)[points count]);
  NSMutableArray *newPoints = [NSMutableArray new];
  for (p = 0; p < [points count]; ++p) {
    TissuePoint *tp = [points objectAtIndex: p];
    k = tp->k; i = tp->i; j = tp->j;

    // already visited?
    if ((*airwayData)[k][i][j] != -1) continue;

    (*airwayData)[k][i][j] = 0;

    // stop at tissue or border
    if ((*tissueData)[k][i][j]) continue;

    // 6 directions
    int up = i + 1;
    int down = i - 1;
    int right = j + 1;
    int left = j - 1;

    TissuePoint *upp = nil, *downp = nil, *rightp = nil, *leftp = nil;
    if (up < tissue->numRows) upp = [TissuePoint newWithPoints: k : up : j];
    if (down >= 0) downp = [TissuePoint newWithPoints: k : down : j];
    if (right < tissue->numCols) rightp = [TissuePoint newWithPoints: k : i : right];
    if (left >= 0) leftp = [TissuePoint newWithPoints: k : i : left];

    BOOL full = YES;
    if (upp) {
      [newPoints addObject: upp];
      [upp release];
      if ((*tissueData)[k][up][j]) full = NO;
    }
    if (downp) {
      [newPoints addObject: downp];
      [downp release];
      if ((*tissueData)[k][down][j]) full = NO;
    }
    if (rightp) {
      [newPoints addObject: rightp];
      [rightp release];
      if ((*tissueData)[k][i][right]) full = NO;
    }
    if (leftp) {
      [newPoints addObject: leftp];
      [leftp release];
      if ((*tissueData)[k][i][left]) full = NO;
    }

    // visit
    if (full) (*airwayData)[k][i][j] = TISSUE_VALUE;
  }
	
  [points release];
  if ([newPoints count] == 0) return;
  subtrace_airway_xy(tissue, airway, newPoints, level + 1, maxLevel);
}

void do_trace_airway(TISSUE *tissue, TISSUE *airway)
{
  int i, j, k;

  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;

  // use airway matrix to record visited
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j) {
	(*airwayData)[k][i][j] = -1;
      }

  k = START_IMAGE - MESH_START_IMAGE + SEED_Z;
  i = airway->numRows - SEED_Y; j = SEED_X;
  printf("(%d, %d, %d) %d\n", k, i, j, (*tissueData)[k][i][j]);
  BOOL done = NO;
  while (!done) {
    NSMutableArray *points = [NSMutableArray new];
    [points addObject: [TissuePoint newWithPoints: k : i : j]];
    //trace_airway_xy(tissue, airway, points, 0, -1);
    subtrace_airway_xy(tissue, airway, points, 0, -1);

    ++k;
    if (k == tissue->numDepth) { done = YES; break; }
    if ((*tissueData)[k][i][j]) { done = YES; break; }

#if 0
    if ((*tissueData)[k][i][j]) {
      int up = i + 1;
      int down = i - 1;
      int right = j + 1;
      int left = j - 1;
      if ((up < tissue->numRows) & !(*tissueData)[k][up][j]) i = up;
      else if ((down >= 0) & !(*tissueData)[k][down][j]) i = down;
      else if ((right < tissue->numCols) & !(*tissueData)[k][i][right]) j = right;
      else if ((left >= 0) & !(*tissueData)[k][i][left]) j = left;
      else done = YES;
    }
#endif
  }

  k = START_IMAGE - MESH_START_IMAGE + SEED_Z;
  i = airway->numRows - SEED_Y; j = SEED_X;
  done = NO;
  while (!done) {
    NSMutableArray *points = [NSMutableArray new];
    [points addObject: [TissuePoint newWithPoints: k : i : j]];
    //trace_airway_xy(tissue, airway, points, 0, 600);
    subtrace_airway_xy(tissue, airway, points, 0, -1);

    --k;
    if (k < 0) { done = YES; break; }
    if ((*tissueData)[k][i][j]) { done = YES; break; }

#if 0
    if ((*tissueData)[k][i][j]) {
      int up = i + 1;
      int down = i - 1;
      int right = j + 1;
      int left = j - 1;
      if ((up < tissue->numRows) & !(*tissueData)[k][up][j]) i = up;
      else if ((down >= 0) & !(*tissueData)[k][down][j]) i = down;
      else if ((right < tissue->numCols) & !(*tissueData)[k][i][right]) j = right;
      else if ((left >= 0) & !(*tissueData)[k][i][left]) j = left;
      else done = YES;
    }
#endif
  }
}

void do_subtrace_airway_xy(TISSUE *tissue, TISSUE *airway)
{
  int i, j, k;

  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;

  // use airway matrix to record visited
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j) {
	(*airwayData)[k][i][j] = -1;
      }

  for (k = 0; k < tissue->numDepth; ++k) {
    for (i = 0; i < tissue->numRows; ++i) {
      for (j = 0; j < tissue->numCols; ++j) {
	if ((*tissueData)[k][i][j]) continue;
	if ((*airwayData)[k][i][j] != -1) continue;

	//printf("airway trace (%d): %d %d %d\n", tag, k, i, j);
	NSMutableArray *points = [NSMutableArray new];
	TissuePoint *tp = [TissuePoint newWithPoints: k : i : j];
	[points addObject: tp];
	[tp release];
	subtrace_airway_xy(tissue, airway, points, 0, -1);
      }
    }
  }
}

void trim_airway_xy(TISSUE *tissue, TISSUE *airway)
{
  int i, j, k;

  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;

  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j) {
	int up = i + 1;
	int down = i - 1;
	int right = j + 1;
	int left = j - 1;

	BOOL full = YES;
	if ((up == tissue->numRows) | ((*airwayData)[k][up][j] == TISSUE_VALUE)) full = NO;
	if ((down == 0) | ((*airwayData)[k][down][j] == TISSUE_VALUE)) full = NO;
	if ((right == tissue->numCols) | ((*airwayData)[k][i][right] == TISSUE_VALUE)) full = NO;
	if ((left == 0) | ((*airwayData)[k][i][left] == TISSUE_VALUE)) full = NO;

	if (full) (*tissueData)[k][i][j] = TISSUE_VALUE;
      }
}

void trace_airway(TISSUE *tissue, TISSUE *trace, NSMutableArray *points, int level, int maxLevel, int tag)
{
  if ((maxLevel != -1) & (level == maxLevel)) return;

  int i, j, k, p;

  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
	
  printf("%d: %lu points\n", level, (unsigned long)[points count]);
  NSMutableArray *newPoints = [NSMutableArray new];
  for (p = 0; p < [points count]; ++p) {
    TissuePoint *tp = [points objectAtIndex: p];
    k = tp->k; i = tp->i; j = tp->j;

    // already visited?
    if ((*traceData)[k][i][j] != -1) continue;

    // stop at tissue
    if ((*tissueData)[k][i][j]) continue;

    // visit
    (*traceData)[k][i][j] = tag;
		
    // 6 directions
    int up = i + 1;
    int down = i - 1;
    int right = j + 1;
    int left = j - 1;
    int back = k + 1;
    int front = k - 1;

    if (up < tissue->numRows) {
      TissuePoint *np = [TissuePoint newWithPoints: k : up : j];
      [newPoints addObject: np];
      [np release];
    }
    if (down >= 0) {
      TissuePoint *np = [TissuePoint newWithPoints: k : down : j];
      [newPoints addObject: np];
      [np release];
    }
    if (right < tissue->numCols) {
      TissuePoint *np = [TissuePoint newWithPoints: k : i : right];
      [newPoints addObject: np];
      [np release];
    }
    if (left >= 0) {
      TissuePoint *np = [TissuePoint newWithPoints: k : i : left];
      [newPoints addObject: np];
      [np release];
    }
    if (back < tissue->numDepth) {
      TissuePoint *np = [TissuePoint newWithPoints: back : i : j];
      [newPoints addObject: np];
      [np release];
    }
    if (front >= 0) {
      TissuePoint *np = [TissuePoint newWithPoints: front : i : j];
      [newPoints addObject: np];
      [np release];
    }
  }

  [points release];
  if ([newPoints count] == 0) return;
  trace_airway(tissue, trace, newPoints, level + 1, maxLevel, tag);
}

NSMutableDictionary *airway_metrics(TISSUE *tissue, TISSUE *trace, TISSUE *airway, BOOL clean, NSRange cleanSize)
{
  int i, j, k, t;
  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;

  NSMutableDictionary *metrics = [NSMutableDictionary dictionary];
	
  // use airway matrix to record visited
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j) {
	(*traceData)[k][i][j] = -1;
	(*airwayData)[k][i][j] = -1;
      }

  //BOOL done = NO;
  int tag = 1;
  for (k = 0; k < tissue->numDepth; ++k) {
    for (i = 0; i < tissue->numRows; ++i) {
      for (j = 0; j < tissue->numCols; ++j) {
	if ((*tissueData)[k][i][j]) continue;
	if ((*traceData)[k][i][j] != -1) continue;

	printf("airway trace (%d): %d %d %d\n", tag, k, i, j);
	NSMutableArray *points = [NSMutableArray new];
	TissuePoint *tp = [TissuePoint newWithPoints: k : i : j];
	[points addObject: tp];
	[tp release];
	trace_airway(tissue, trace, points, 0, -1, tag);
	++tag;
      }
    }
  }

  // count up size of each tagged tissue segment
  int *size = malloc(sizeof(int) * tag);
  for (t = 0; t < tag; ++t) size[t] = 0;
  for (k = 0; k < tissue->numDepth; ++k)
    for (i = 0; i < tissue->numRows; ++i)
      for (j = 0; j < tissue->numCols; ++j)
	if ((*traceData)[k][i][j] > 0) ++size[(*traceData)[k][i][j]];

  for (t = 1; t < tag; ++t) {
    NSNumber *n = [NSNumber numberWithInt: size[t]];
    NSNumber *value = [metrics objectForKey: n];
    if (!value) value = [NSNumber numberWithInt: 1];
    else value = [NSNumber numberWithInt: [value intValue] + 1];
    [metrics setObject: value forKey: n];
    //printf("%d size = %d\n", t, size[t]);
  }

  for (k = 0; k < tissue->numDepth; ++k)
    for (i = 0; i < tissue->numRows; ++i)
      for (j = 0; j < tissue->numCols; ++j)
	if ((*traceData)[k][i][j] > 0)
	  if ((size[(*traceData)[k][i][j]] >= cleanSize.location) & (size[(*traceData)[k][i][j]] <= cleanSize.length)) {
	    (*airwayData)[k][i][j] = 1;
	    if (clean) (*tissueData)[k][i][j] = 0;
	  }

  return metrics;
}

void trace_tissue(TISSUE *tissue, TISSUE *trace, NSMutableArray *points, int level, int maxLevel, int tag)
{
  if ((maxLevel != -1) & (level == maxLevel)) return;

  int i, j, k, p;

  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
	
  //printf("%d: %lu points\n", level, (unsigned long)[points count]);
  NSMutableArray *newPoints = [NSMutableArray new];
  for (p = 0; p < [points count]; ++p) {
    TissuePoint *tp = [points objectAtIndex: p];
    k = tp->k; i = tp->i; j = tp->j;

    // already visited?
    if ((*traceData)[k][i][j] != -1) continue;

    // stop at airway
    if ((*tissueData)[k][i][j] != TISSUE_VALUE) continue;

    // visit
    (*traceData)[k][i][j] = tag;
		
    // 6 directions
    int up = i + 1;
    int down = i - 1;
    int right = j + 1;
    int left = j - 1;
    int back = k + 1;
    int front = k - 1;

    if (up < tissue->numRows) [newPoints addObject: [TissuePoint newWithPoints: k : up : j]];
    if (down >= 0) [newPoints addObject: [TissuePoint newWithPoints: k : down : j]];
    if (right < tissue->numCols) [newPoints addObject: [TissuePoint newWithPoints: k : i : right]];
    if (left >= 0) [newPoints addObject: [TissuePoint newWithPoints: k : i : left]];
    if (back < tissue->numDepth) [newPoints addObject: [TissuePoint newWithPoints: back : i : j]];
    if (front >= 0) [newPoints addObject: [TissuePoint newWithPoints: front : i : j]];
  }

  for (p = 0; p < [newPoints count]; ++p) {
    id obj = [newPoints objectAtIndex: p];
    [obj release];
  }

  [points release];
  if ([newPoints count] == 0) return;
  trace_tissue(tissue, trace, newPoints, level + 1, maxLevel, tag);
}

void do_trace_tissue(TISSUE *tissue, TISSUE *trace)
{
  int i, j, k;

  //char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;

  // use trace matrix to record visited
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j)
	(*traceData)[k][i][j] = -1;

  k = 21;
  i = 176; j = 164;
  NSMutableArray *points = [NSMutableArray new];
  [points addObject: [TissuePoint newWithPoints: k : i : j]];
  trace_tissue(tissue, trace, points, 0, -1, 1);
}

NSMutableDictionary *tissue_metrics(TISSUE *tissue, TISSUE *trace, TISSUE *airway, BOOL clean, int cleanSize)
{
  int i, j, k, t;
  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;

  NSMutableDictionary *metrics = [NSMutableDictionary dictionary];
	
  // use airway matrix to record visited
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j) {
	(*traceData)[k][i][j] = -1;
	(*airwayData)[k][i][j] = -1;
      }

  //BOOL done = NO;
  int tag = 1;
  for (k = 0; k < tissue->numDepth; ++k) {
    for (i = 0; i < tissue->numRows; ++i) {
      for (j = 0; j < tissue->numCols; ++j) {
	if ((*tissueData)[k][i][j] != TISSUE_VALUE) continue;
	if ((*traceData)[k][i][j] != -1) continue;

	printf("trace (%d): %d %d %d\n", tag, k, i, j);
	NSMutableArray *points = [NSMutableArray new];
	[points addObject: [TissuePoint newWithPoints: k : i : j]];
	trace_tissue(tissue, trace, points, 0, -1, tag);
	++tag;
      }
    }
  }

  // count up size of each tagged tissue segment
  int *size = malloc(sizeof(int) * tag);
  for (t = 0; t < tag; ++t) size[t] = 0;
  for (k = 0; k < tissue->numDepth; ++k)
    for (i = 0; i < tissue->numRows; ++i)
      for (j = 0; j < tissue->numCols; ++j)
	if ((*traceData)[k][i][j] > 0) ++size[(*traceData)[k][i][j]];

  for (t = 1; t < tag; ++t) {
    NSNumber *n = [NSNumber numberWithInt: size[t]];
    NSNumber *value = [metrics objectForKey: n];
    if (!value) value = [NSNumber numberWithInt: 1];
    else value = [NSNumber numberWithInt: [value intValue] + 1];
    [metrics setObject: value forKey: n];
    //printf("%d size = %d\n", t, size[t]);
  }

  for (k = 0; k < tissue->numDepth; ++k)
    for (i = 0; i < tissue->numRows; ++i)
      for (j = 0; j < tissue->numCols; ++j)
	if ((*traceData)[k][i][j] > 0)
	  if (size[(*traceData)[k][i][j]] < cleanSize) {
	    (*airwayData)[k][i][j] = 1;
	    if (clean) (*tissueData)[k][i][j] = 0;
	  }

  return metrics;
}

NSMutableDictionary *length_metrics_k(TISSUE *tissue, TISSUE *airway, TISSUE *trace,
				      BOOL tissueFlag, int fill, NSRange markRange)
{
  int i, j, k, p;
  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;

  NSMutableDictionary *metrics = [NSMutableDictionary dictionary];
	
  // use airway matrix to record visited
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j) {
	(*airwayData)[k][i][j] = -1;
	(*traceData)[k][i][j] = -1;
      }

  // trace internal distances
  for (k = 0; k < tissue->numDepth; ++k)
    for (i = 0; i < tissue->numRows; ++i)
      for (j = 0; j < tissue->numCols; ++j) {
	// skip tissue/air
	if (tissueFlag) { if ((*tissueData)[k][i][j] == TISSUE_VALUE) continue; }
	else { if ((*tissueData)[k][i][j] != TISSUE_VALUE) continue; }

	// already visited?
	if ((*airwayData)[k][i][j] != -1) continue;

	// extend to max
	int maxK = k;
	BOOL valid = YES;
	BOOL done = NO;
	while (!done) {
	  ++maxK;
	  if (maxK == tissue->numDepth) { done = YES; valid = NO; }
	  else {
	    (*airwayData)[maxK][i][j] = 0;
	    if (tissueFlag) { if ((*tissueData)[maxK][i][j] == TISSUE_VALUE) done = YES; }
	    else { if ((*tissueData)[maxK][i][j] != TISSUE_VALUE) done = YES; }
	  }
	}
	--maxK;

	// extend to min
	int minK = k;
	done = NO;
	while (!done) {
	  --minK;
	  if (minK < 0) { done = YES; valid = NO; }
	  else {
	    (*airwayData)[minK][i][j] = 0;
	    if (tissueFlag) { if ((*tissueData)[minK][i][j] == TISSUE_VALUE) done = YES; }
	    else { if (!(*tissueData)[minK][i][j]) done = YES; }
	  }
	}
	++minK;
				
	// do not count those which reach edge
	if (valid) {
	  NSNumber *n = [NSNumber numberWithInt: (maxK - minK + 1)];
	  NSNumber *value = [metrics objectForKey: n];
	  if (!value) value = [NSNumber numberWithInt: 1];
	  else value = [NSNumber numberWithInt: [value intValue] + 1];
	  [metrics setObject: value forKey: n];

	  // fill in smaller spaces
	  if ([n intValue] <= fill) {
	    if (tissueFlag)
	      for (p = minK; p <= maxK; ++p) (*tissueData)[p][i][j] = TISSUE_VALUE;
	    else
	      for (p = minK; p <= maxK; ++p) (*tissueData)[p][i][j] = 0;
	  }

	  // record length
	  for (p = minK; p <= maxK; ++p) (*traceData)[p][i][j] = [n intValue];
					
	  // mark by size range
	  if (([n intValue] < markRange.length) & ([n intValue] > markRange.location)) {
	    for (p = minK; p <= maxK; ++p) (*airwayData)[p][i][j] = 1;
	  }
	}
      }

  return metrics;
}

NSMutableDictionary *length_metrics_i(TISSUE *tissue, TISSUE *airway, BOOL tissueFlag, int fill, NSRange markRange)
{
  int i, j, k, p;
  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;

  NSMutableDictionary *metrics = [NSMutableDictionary dictionary];
	
  // use airway matrix to record visited
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*airwayData)[k][i][j] = -1;

  // trace internal distances
  for (i = 0; i < tissue->numRows; ++i)
    for (k = 0; k < tissue->numDepth; ++k)
      for (j = 0; j < tissue->numCols; ++j) {
	//printf("%d %d %d\n", k, i, j);
	// skip tissue
	if (tissueFlag) { if ((*tissueData)[k][i][j] == TISSUE_VALUE) continue; }
	else { if ((*tissueData)[k][i][j] != TISSUE_VALUE) continue; }

	// already visited?
	if ((*airwayData)[k][i][j] != -1) continue;

	// extend to max
	int maxI = i;
	BOOL valid = YES;
	BOOL done = NO;
	while (!done) {
	  ++maxI;
	  if (maxI == tissue->numRows) { done = YES; valid = NO; }
	  else {
	    (*airwayData)[k][maxI][j] = 0;
	    if (tissueFlag) { if ((*tissueData)[k][maxI][j] == TISSUE_VALUE) done = YES; }
	    else { if ((*tissueData)[k][maxI][j] != TISSUE_VALUE) done = YES; }
	  }
	}
	--maxI;

	// extend to min
	int minI = i;
	done = NO;
	while (!done) {
	  --minI;
	  if (minI < 0) { done = YES; valid = NO; }
	  else {
	    (*airwayData)[k][minI][j] = 0;
	    if (tissueFlag) { if ((*tissueData)[k][minI][j] == TISSUE_VALUE) done = YES; }
	    else { if ((*tissueData)[k][minI][j] != TISSUE_VALUE) done = YES; }
	  }
	}
	++minI;
				
	// do not count those which reach edge
	if (valid) {
	  NSNumber *n = [NSNumber numberWithInt: (maxI - minI + 1)];
	  NSNumber *value = [metrics objectForKey: n];
	  if (!value) value = [NSNumber numberWithInt: 1];
	  else value = [NSNumber numberWithInt: [value intValue] + 1];
	  [metrics setObject: value forKey: n];

	  // fill in smaller spaces
	  if ([n intValue] <= fill) {
	    if (tissueFlag)
	      for (p = minI; p <= maxI; ++p) (*tissueData)[k][p][j] = TISSUE_VALUE;
	    else
	      for (p = minI; p <= maxI; ++p) (*tissueData)[k][p][j] = 0;

	  }

	  if (([n intValue] < markRange.length) & ([n intValue] > markRange.location)) {
	    for (p = minI; p <= maxI; ++p) (*airwayData)[k][p][j] = 1;
	  }
	}
      }

  return metrics;
}

NSMutableDictionary *length_metrics_j(TISSUE *tissue, TISSUE *airway, BOOL tissueFlag, int fill, NSRange markRange)
{
  int i, j, k, p;
  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;

  NSMutableDictionary *metrics = [NSMutableDictionary dictionary];
	
  // use airway matrix to record visited
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*airwayData)[k][i][j] = -1;

  // trace internal distances
  for (j = 0; j < tissue->numCols; ++j)
    for (i = 0; i < tissue->numRows; ++i)
      for (k = 0; k < tissue->numDepth; ++k) {
	//printf("%d %d %d ", k, i, j);
	// skip tissue
	if (tissueFlag) { if ((*tissueData)[k][i][j] == TISSUE_VALUE) continue; }
	else { if ((*tissueData)[k][i][j] != TISSUE_VALUE) continue; }

	// already visited?
	if ((*airwayData)[k][i][j] != -1) continue;

	// extend to max
	int maxJ = j;
	BOOL valid = YES;
	BOOL done = NO;
	while (!done) {
	  ++maxJ;
	  if (maxJ == tissue->numCols) { done = YES; valid = NO; }
	  else {
	    (*airwayData)[k][i][maxJ] = 0;
	    if (tissueFlag) { if ((*tissueData)[k][i][maxJ] == TISSUE_VALUE) done = YES; }
	    else { if ((*tissueData)[k][i][maxJ] != TISSUE_VALUE) done = YES; }
	  }
	}
	--maxJ;
	//printf("%d ", maxJ);

	// extend to min
	int minJ = j;
	done = NO;
	while (!done) {
	  --minJ;
	  if (minJ < 0) { done = YES; valid = NO; }
	  else {
	    (*airwayData)[k][i][minJ] = 0;
	    if (tissueFlag) { if ((*tissueData)[k][i][minJ] == TISSUE_VALUE) done = YES; }
	    else { if ((*tissueData)[k][i][minJ] != TISSUE_VALUE) done = YES; }
	  }
	}
	++minJ;
	//printf("%d %d", minJ, valid);

	// do not count those which reach edge
	if (valid) {
	  NSNumber *n = [NSNumber numberWithInt: (maxJ - minJ + 1)];
	  NSNumber *value = [metrics objectForKey: n];
	  if (!value) value = [NSNumber numberWithInt: 1];
	  else value = [NSNumber numberWithInt: [value intValue] + 1];
	  [metrics setObject: value forKey: n];

	  // fill in smaller spaces
	  if ([n intValue] <= fill) {
	    if (tissueFlag)
	      for (p = minJ; p <= maxJ; ++p) (*tissueData)[k][i][p] = TISSUE_VALUE;
	    else
	      for (p = minJ; p <= maxJ; ++p) (*tissueData)[k][i][p] = 0;
	  }

	  if (([n intValue] < markRange.length) & ([n intValue] > markRange.location)) {
	    for (p = minJ; p <= maxJ; ++p) (*airwayData)[k][i][p] = 1;
	  }
	}
      }

  return metrics;
}

NSMutableDictionary *length_neighbors(TISSUE *trace)
{
  int i, j, k;
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;

  NSMutableDictionary *metrics = [NSMutableDictionary dictionary];

  // compare neighbor distances
  for (i = 0; i < trace->numRows; ++i)
    for (j = 0; j < trace->numCols; ++j) {
      int prevLength = 0;
      for (k = 0; k < trace->numDepth; ++k) {
	// no length
	if ((*traceData)[k][i][j] == -1) { prevLength = 0; continue; }
	if ((*traceData)[k][i][j] == prevLength) continue;

	prevLength = (*traceData)[k][i][j];
				
	// 4 neighbors
	int up = i + 1;
	int down = i - 1;
	int right = j + 1;
	int left = j - 1;

	if (up < trace->numRows)
	  if ((*traceData)[k][up][j] != -1) {
	    NSNumber *n = [NSNumber numberWithInt: abs((*traceData)[k][up][j] - (*traceData)[k][i][j])];
	    NSNumber *value = [metrics objectForKey: n];
	    if (!value) value = [NSNumber numberWithInt: 1];
	    else value = [NSNumber numberWithInt: [value intValue] + 1];
	    [metrics setObject: value forKey: n];
	  }

	if (down >= 0)
	  if ((*traceData)[k][down][j] != -1) {
	    NSNumber *n = [NSNumber numberWithInt: abs((*traceData)[k][down][j] - (*traceData)[k][i][j])];
	    NSNumber *value = [metrics objectForKey: n];
	    if (!value) value = [NSNumber numberWithInt: 1];
	    else value = [NSNumber numberWithInt: [value intValue] + 1];
	    [metrics setObject: value forKey: n];
	  }
	  		
	if (right < trace->numCols)
	  if ((*traceData)[k][i][right] != -1) {
	    NSNumber *n = [NSNumber numberWithInt: abs((*traceData)[k][i][right] - (*traceData)[k][i][j])];
	    NSNumber *value = [metrics objectForKey: n];
	    if (!value) value = [NSNumber numberWithInt: 1];
	    else value = [NSNumber numberWithInt: [value intValue] + 1];
	    [metrics setObject: value forKey: n];
	  }

	if (left >= 0)
	  if ((*traceData)[k][i][left] != -1) {
	    NSNumber *n = [NSNumber numberWithInt: abs((*traceData)[k][i][left] - (*traceData)[k][i][j])];
	    NSNumber *value = [metrics objectForKey: n];
	    if (!value) value = [NSNumber numberWithInt: 1];
	    else value = [NSNumber numberWithInt: [value intValue] + 1];
	    [metrics setObject: value forKey: n];
	  }

      }
    }
		
  return metrics;
}

void write_df3(TISSUE *tissue, const char *fileName)
{
  char (*bitmapData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  FILE *fptr;
  int i,j,k;
  int nx = tissue->numCols, ny = tissue->numRows;
  int nz = tissue->numDepth;

  if (nx == 0) {
    printf("ERROR: data has no size.\n");
    abort();
  }

  if ((fptr = fopen(fileName,"w")) == NULL)
    abort();
  printf("df3 file size (%d, %d, %d)\n", nx, ny, nz);
  fputc(nx >> 8,fptr);
  fputc(nx & 0xff,fptr);
  fputc(ny >> 8,fptr);
  fputc(ny & 0xff,fptr);
  fputc(nz >> 8,fptr);
  fputc(nz & 0xff,fptr);
  for (k=0;k<nz;k++) {
    for (i=0;i<ny;i++) {
      for (j=0;j<nx;j++) {
	fputc((*bitmapData)[k][i][j],fptr);
      }
    }
  }
  fclose(fptr);
}

void read_df3(TISSUE *tissue, const char *fileName)
{
  FILE *fptr;
  int i,j,k;

  int nx, ny, nz;

  if ((fptr = fopen(fileName,"r")) == NULL) {
    printf("Could not open file: %s\n", fileName);
    abort();
  }

  unsigned int s;
  s = fgetc(fptr);
  nx = s << 8;
  s = fgetc(fptr);
  nx = nx | s;
  tissue->numCols = nx;

  s = fgetc(fptr);
  ny = s << 8;
  s = fgetc(fptr);
  ny = ny | s;
  tissue->numRows = ny;

  s = fgetc(fptr);
  nz = s << 8;
  s = fgetc(fptr);
  nz = nz | s;
  tissue->numDepth = nz;

  printf("Reading df3 data (%d, %d, %d).\n", nx, ny, nz);
  tissue->data = malloc(sizeof(char) * tissue->numDepth * tissue->numRows * tissue->numCols);

  char (*bitmapData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;

  for (k = 0; k < nz; ++k) {
    for (i = 0; i < ny; ++i) {
      for (j = 0; j < nx; ++j) {
	(*bitmapData)[k][i][j] = fgetc(fptr);
      }
    }
  }

  fclose(fptr);
}

void write_raw(TISSUE *tissue, char *fileName, int size)
{
  FILE *fptr;
  int nx = tissue->numCols, ny = tissue->numRows;
  int nz = tissue->numDepth;

  if (nx == 0) {
    printf("ERROR: data has no size.\n");
    abort();
  }

  if ((fptr = fopen(fileName,"w")) == NULL)
    abort();
  printf("raw file size (%d, %d, %d, %d)\n", nx, ny, nz, size);
  fwrite(&nx, sizeof(int), 1, fptr);
  fwrite(&ny, sizeof(int), 1, fptr);
  fwrite(&nz, sizeof(int), 1, fptr);
  fwrite(&size, sizeof(int), 1, fptr);
  fwrite(tissue->data, size, nx * ny * nz, fptr);
  fclose(fptr);
}

void read_raw(TISSUE *tissue, char *fileName)
{
  FILE *fptr;
  int nx, ny, nz;
  int size;

  if ((fptr = fopen(fileName,"r")) == NULL) {
    printf("Could not open file: %s\n", fileName);
    abort();
  }

  size_t res = fread(&nx, sizeof(int), 1, fptr);
  res = fread(&ny, sizeof(int), 1, fptr);
  res = fread(&nz, sizeof(int), 1, fptr);
  res = fread(&size, sizeof(int), 1, fptr);

  tissue->numCols = nx;
  tissue->numRows = ny;
  tissue->numDepth = nz;

  printf("Reading raw data (%d, %d, %d, %d).\n", nx, ny, nz, size);
  tissue->data = malloc(size * tissue->numDepth * tissue->numRows * tissue->numCols);

  fread(tissue->data, size, nx * ny * nz, fptr);
  fclose(fptr);
}

void write_povray(TISSUE *tissue, char *fileName, int start, int end)
{
  int i, j, k;

  // copy header
  FILE *header = fopen("header.pov", "r");
  if (!header) {
    printf("Could not open header.pov\n");
    abort();
  }
  FILE *povrayFile = fopen(fileName, "w");
  while (!feof(header)) {
    int c;
    c = fgetc(header);
    if (!feof(header)) fputc(c, povrayFile);
  }
  fclose(header);

  fprintf(povrayFile, "union {\n");
	
  char (*bitmapData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  float maxX = 0, maxY = 0, maxZ = 0;
  float minX = 0, minY = 0, minZ = 0;
  for (k = start; k < end; ++k) {
    for (i = 0; i < tissue->numRows; ++i)
      for (j = 0; j < tissue->numCols; ++j) {
	float y = i;
	float x = j;
	float z = k;
	x = x / 80;
	y = y / 80;
	z = z / 80;
	if ((*bitmapData)[k][i][j] == TISSUE_VALUE) {
	  fprintf(povrayFile, "sphere { \n");
	  fprintf(povrayFile, "<%f, %f, %f>, 0.01000\n", x, y, z);
	  //fprintf(povrayFile, "<%f, %f, %f>, 0.012500 strength 1\n", x, y, z);
	  fprintf(povrayFile, "texture {\n");
	  fprintf(povrayFile, "pigment { rgb <1.000000, 0.5, 0.5> }\n");
	  fprintf(povrayFile, "}\n");
	  fprintf(povrayFile, "}\n");

	  if (x < minX) minX = x;
	  if (x > maxX) maxX = x;
	  if (y < minY) minY = y;
	  if (y > maxY) maxY = y;
	  if (z < minZ) minZ = z;
	  if (z > maxZ) maxZ = z;
	}
      }
  }
  fprintf(povrayFile, "translate <-%f, -%f, -%f>\n", (maxX - minX)/2.0, (maxY - minY)/2.0, (maxZ - minZ)/2.0);
  fprintf(povrayFile, "}\n");

  fclose(povrayFile);
}

void write_metrics(NSDictionary *metrics, char *fileName)
{
  int i;

  FILE *metricsFile = fopen(fileName, "w");

  NSArray *keys = [metrics allKeys];
  for (i = 0; i < [keys count]; ++i) {
    NSNumber *key = [keys objectAtIndex: i];
    NSNumber *value = [metrics objectForKey: key];
    fprintf(metricsFile, "%d\t%d\n", [key intValue], [value intValue]);
  }
  fclose(metricsFile);
}

void write_both_povray(TISSUE *tissue, int tissueStart, int tissueEnd,
		       TISSUE *airway, int airwayStart, int airwayEnd, char *fileName)
{
  int i, j, k;

  // copy header
  FILE *header = fopen("header.pov", "r");
  if (!header) {
    printf("Could not open header.pov\n");
    abort();
  }
  FILE *povrayFile = fopen(fileName, "w");
  while (!feof(header)) {
    int c;
    c = fgetc(header);
    if (!feof(header)) fputc(c, povrayFile);
  }
  fclose(header);
	
  // tissue
  char (*bitmapData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  //for (k = 0; k < tissue->numDepth; ++k) {
  for (k = tissueStart; k < tissueEnd; ++k) {
    for (i = 0; i < tissue->numRows; ++i)
      for (j = 0; j < tissue->numCols; ++j) {
	float y = i;
	float x = j;
	float z = k;
	x = x / 80;
	y = y / 80;
	z = z / 80;
	if ((*bitmapData)[k][i][j] == TISSUE_VALUE) {
	  fprintf(povrayFile, "sphere {\n");
	  fprintf(povrayFile, "<%f, %f, %f>, 0.01000\n", x, y, z);
	  //fprintf(povrayFile, "<%f, %f, %f>, 0.012500 strength 1\n", x, y, z);
	  fprintf(povrayFile, "texture {\n");
	  fprintf(povrayFile, "pigment { rgb <1.000000, 0.5, 0.5> }\n");
	  fprintf(povrayFile, "}\n");
	  fprintf(povrayFile, "}\n");
	}
      }
  }
	
  // airway
  bitmapData = airway->data;
  for (k = airwayStart; k < airwayEnd; ++k) {
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j) {
	float y = i;
	float x = j;
	float z = k;
	x = x / 80;
	y = y / 80;
	z = z / 80;
	if ((*bitmapData)[k][i][j] == TISSUE_VALUE) {
	  fprintf(povrayFile, "sphere {\n");
	  fprintf(povrayFile, "<%f, %f, %f>, 0.01000\n", x, y, z);
	  //fprintf(povrayFile, "<%f, %f, %f>, 0.012500 strength 1\n", x, y, z);
	  fprintf(povrayFile, "texture {\n");
	  fprintf(povrayFile, "pigment { rgb <0.500000, 0.5, 1.0> }\n");
	  fprintf(povrayFile, "}\n");
	  fprintf(povrayFile, "}\n");
	}
      }
  }

  fclose(povrayFile);
}

void write_mesh(TISSUE *tissue, TISSUE *airway, char *fileName, NSRange kr, NSRange ir, NSRange jr)
{
  int i, j, k;

  FILE *meshFile = fopen(fileName, "w");
  FILE *initFile = fopen("initial_duct_cells.dat", "w");
	
  char (*bitmapData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*modelData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  int *meshElements[MESH_XYZ_TOT];
	
  // count elements
  int numElements = 0;
  for (k = kr.location; k < kr.length; ++k)
    for (i = ir.location; i < ir.length; ++i)
      for (j = jr.location; j < jr.length; ++j)
	if ((*bitmapData)[k][i][j] == 1) {
	  ++numElements;
	  (*modelData)[k][i][j] = 1;
	}

  printf("%d elements in mesh.\n", numElements);

  // allocate mesh table
  for (k = MESH_CURRENT; k < MESH_XYZ_TOT; ++k)
    meshElements[k] = (int *)malloc(sizeof(int) * numElements);

  void *mesh = malloc(sizeof(int) * tissue->numDepth * tissue->numRows * tissue->numCols);
  int (*meshData)[tissue->numDepth][tissue->numRows][tissue->numCols] = mesh;
  for (k = 0; k < tissue->numDepth; ++k)
    for (i = 0; i < tissue->numRows; ++i)
      for (j = 0; j < tissue->numCols; ++j)
	(*meshData)[k][i][j] = -1;

  // give each element an index
  int elemID = 0;
  for (k = kr.location; k < kr.length; ++k)
    for (i = ir.location; i < ir.length; ++i)
      for (j = jr.location; j < jr.length; ++j)
	if ((*bitmapData)[k][i][j] == 1) {
	  (*meshData)[k][i][j] = elemID;
	  ++elemID;
	}

  // write out mesh
  fprintf(initFile, "0.0 %d", numElements);
  for (k = kr.location; k < kr.length; ++k)
    for (i = ir.location; i < ir.length; ++i)
      for (j = jr.location; j < jr.length; ++j) {
	int up = i + 1;
	int down = i - 1;
	int right = j + 1;
	int left = j - 1;
	int back = k + 1;
	int front = k - 1;

	if ((*bitmapData)[k][i][j] != 1) continue;
	fprintf(meshFile, "%d", (*meshData)[k][i][j]);

	// MESH_LEFT 1
	if (left < 0) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[k][i][left] == 1) fprintf(meshFile, " %d", (*meshData)[k][i][left]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_RIGHT 2
	if (right == tissue->numCols) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[k][i][right] == 1) fprintf(meshFile, " %d", (*meshData)[k][i][right]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_UP 3
	if (up == tissue->numRows) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[k][up][j] == 1) fprintf(meshFile, " %d", (*meshData)[k][up][j]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_DOWN 4
	if (down < 0) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[k][down][j] == 1) fprintf(meshFile, " %d", (*meshData)[k][down][j]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_LEFT_UP 5
	if ((left < 0) | (up == tissue->numRows)) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[k][up][left] == 1) fprintf(meshFile, " %d", (*meshData)[k][up][left]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_RIGHT_UP 6
	if ((right == tissue->numCols) | (up == tissue->numRows)) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[k][up][right] == 1) fprintf(meshFile, " %d", (*meshData)[k][up][right]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_LEFT_DOWN 7
	if ((left < 0) | (down < 0)) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[k][down][left] == 1) fprintf(meshFile, " %d", (*meshData)[k][down][left]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_RIGHT_DOWN 8
	if ((right == tissue->numCols) | (down < 0)) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[k][down][right] == 1) fprintf(meshFile, " %d", (*meshData)[k][down][right]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_FRONT 9
	if (front < 0) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[front][i][j] == 1) fprintf(meshFile, " %d", (*meshData)[front][i][j]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_BACK 10
	if (back == tissue->numDepth) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[back][i][j] == 1) fprintf(meshFile, " %d", (*meshData)[back][i][j]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_LEFT_FRONT 11
	if ((left < 0) | (front < 0)) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[front][i][left] == 1) fprintf(meshFile, " %d", (*meshData)[front][i][left]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_RIGHT_FRONT 12
	if ((right == tissue->numCols) | (front < 0)) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[front][i][right] == 1) fprintf(meshFile, " %d", (*meshData)[front][i][right]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_LEFT_BACK 13
	if ((left < 0) | (back == tissue->numDepth)) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[back][i][left] == 1) fprintf(meshFile, " %d", (*meshData)[back][i][left]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_RIGHT_BACK 14
	if ((right == tissue->numCols) | (back == tissue->numDepth)) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[back][i][right] == 1) fprintf(meshFile, " %d", (*meshData)[back][i][right]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_UP_FRONT 15
	if ((front < 0) | (up == tissue->numRows)) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[front][up][j] == 1) fprintf(meshFile, " %d", (*meshData)[front][up][j]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_DOWN_FRONT 16
	if ((front < 0) | (down < 0)) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[front][down][j] == 1) fprintf(meshFile, " %d", (*meshData)[front][down][j]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_UP_BACK 17
	if ((back == tissue->numDepth) | (up == tissue->numRows)) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[back][up][j] == 1) fprintf(meshFile, " %d", (*meshData)[back][up][j]);
	else fprintf(meshFile, " %d", MESH_NONE);

	// MESH_DOWN_BACK 18
	if ((back == tissue->numDepth) | (down < 0)) fprintf(meshFile, " %d", MESH_NONE);
	else if ((*bitmapData)[back][down][j] == 1) fprintf(meshFile, " %d", (*meshData)[back][down][j]);
	else fprintf(meshFile, " %d", MESH_NONE);

	fprintf(meshFile, "\n");

	float y = i;
	float x = j;
	float z = k;
	x = x / 80;
	y = y / 80;
	z = z / 80;

	fprintf(initFile, " %f %f %f 0 0", x, y, z);
      }

  fprintf(initFile, "\n");

  fclose(meshFile);
  fclose(initFile);
  free(mesh);
}

void write_scem(TISSUE *tissue, TISSUE *airway, NSRange kr, NSRange ir, NSRange jr)
{
  int i, j, k;

  FILE *initFile = fopen("initial_duct_cells.dat", "w");
	
  char (*bitmapData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*modelData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;

  // count elements
  int numElements = 0;
  for (k = kr.location; k < kr.length; ++k)
    for (i = ir.location; i < ir.length; ++i)
      for (j = jr.location; j < jr.length; ++j)
				if ((*bitmapData)[k][i][j] == TISSUE_VALUE) {
	  			++numElements;
			  	(*modelData)[k][i][j] = TISSUE_VALUE;
	  		}

  printf("%d elements.\n", numElements);

  // write out elements
  fprintf(initFile, "0.0 %d", numElements);
  for (k = kr.location; k < kr.length; ++k)
    for (i = ir.location; i < ir.length; ++i)
      for (j = jr.location; j < jr.length; ++j) {
				if ((*bitmapData)[k][i][j] == TISSUE_VALUE) {
					float y = Y_START + (float)i * Y_OFFSET;
					float x = X_START + (float)j * X_OFFSET;
					float z = Z_START + (float)k * Z_OFFSET;

					fprintf(initFile, " %f %f %f 0 0", x, y, z);
				}
      }

  fprintf(initFile, "\n");

  fclose(initFile);
}

void generate_image_stack(TISSUE *tissue, NSRange stackRange, NSString *filePrefix)
{
  int i, j, k;

  printf("Generating image stack.\n");
  set_image_rep(tissue);
  unsigned char *bd = [tissue->rep bitmapData];
  unsigned char (*repData)[tissue->numRows][tissue->numCols] = (void *)bd;
  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;

  for (k = stackRange.location; k < (stackRange.location + stackRange.length); ++k) {
    for (i = 0; i < tissue->numRows; ++i)
      for (j = 0; j < tissue->numCols; ++j)
	(*repData)[tissue->numRows - 1 - i][j] = (*tissueData)[k][i][j];

    NSMutableString *imageName = [NSMutableString stringWithFormat: @"%@", filePrefix];
    if (k < 10) [imageName appendString: @"0000"];
    else if (k < 100) [imageName appendString: @"000"];
    else if (k < 1000) [imageName appendString: @"00"];
    else [imageName appendString: @"0"];
    [imageName appendFormat: @"%d", k];
    [imageName appendString: @".png"];

    NSData *d = [tissue->rep representationUsingType: NSPNGFileType properties: nil];
    [d writeToFile: imageName atomically: YES];
  }

}

//
// Lung sample specific processing functions
//
// Each sample is different and some processing steps
// need to be explored to figure out the best way to 
// clean up the image. The main procedure involves
// these steps.
//

// 1) Read image stack and save in df3 format

// 2) Generate tissue metrics to see how many connected
//    components exist. In theory all of the tissue should
//    be connected, but typically there is one large
//    component and many small ones.

// 3) Fill small airway gaps. These are small gaps
//    that are converted from airway space to tissue.
//    Have to decide upon how big of gaps to close, to
//    big and it closes small alveoli, and too small
//    and it leaves small holes in tissue. So far
//    a gap of 3 seems to be decent.

// 4) Save the airway filled data in df3 format.

// 5) Generate X,Y,Z airway metrics to see the distribution
//    of spacing. There shouldn't be anything smaller than
//    the threshold for airway gap filling above. Plotting
//    this distribution can reveal insights. It would be interesting
//    to plot inspiration vs. expiration.

// airway fix?

// 6) Generate an image stack to see how it looks.

// 7) Generate tissue metrics again to see the connected
//    components. Determine the range of component sizes
//    to be deleted.

// 8) Using the range of component sizes, do tissue clean.

// 9) Can generate tissue metrics again to verify that you
//    are left with expected number of components, and
//    generate an image stack to see how the cleaned data looks.

// 10) Need to evaluate the microstructure of the tissue, by
//     comparing the cleaned images with the raw images to see
//     well the original microstructure is preserved. If there
//     are too many gaps then may need to go back to the
//     beginning with processImageSamples and use a different
//     threshold value to adjust the amount of tissue.

// 11) Generate data for model.

//
// *** ALTERNATIVE ***
//
// I have found with some of the injured tissue that filling the
// small airway gaps tends to remove the fine structure of the 
// tissue and cause it to become "thick". I was able to obtain
// better results by first doing a tissue clean and then filling
// the airway gaps, but only for a gap of 1.
//


//
// LUNG_SAMPLE = 1
//

void process_R120_LS1()
{
  NSMutableDictionary *metrics = nil;
  int i, j, k;
  NSRange nullRange = {0, 0};
  BOOL clean = NO;
  int cleanTissueSize = 0;

  TISSUE *tissue = malloc(sizeof(TISSUE));

#if 0
  printf("Reading image data.\n");
  read_image_data(tissue);
#endif

  NSString *rawName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "raw.df3"];
  NSString *airwayFillName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "airway_fill.df3"];
  NSString *tissueCleanName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "tissue_clean.df3"];

#if 0
  printf("Saving raw data.\n%s\n", [rawName UTF8String]);
  write_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading raw data.\n%s\n", [rawName UTF8String]);
  read_df3(tissue, [rawName UTF8String]);
#endif

#if 1
  printf("Reading airway fill data.\n%s\n", [airwayFillName UTF8String]);
  read_df3(tissue, [airwayFillName UTF8String]);
#endif

#if 0
  printf("Reading r120_airway_fix.df3.\n");
  read_df3(tissue, "r120_airway_fix.df3");
#endif

#if 0
  printf("Reading r120_tissue_fill.df3.\n");
  read_df3(tissue, "r120_tissue_fill.df3");
#endif

#if 0
  printf("Reading tissue clean data.\n%s\n", [tissueCleanName UTF8String]);
  read_df3(tissue, [tissueCleanName UTF8String]);
#endif

#if 0
  printf("Reading r120_airway.df3.\n");
  read_df3(tissue, "r120_airway.df3");
#endif

#if 0
  printf("Reading r120_airway_trace.df3.\n");
  read_df3(tissue, "r120_airway_trace.df3");
#endif

#if 1
  TISSUE *airway = malloc(sizeof(TISSUE));
  airway->bytesPerPixel = tissue->bytesPerPixel;
  airway->numDepth = tissue->numDepth;
  airway->numRows = tissue->numRows;
  airway->numCols = tissue->numCols;
	
  airway->data = malloc(sizeof(char) * airway->numDepth * airway->numRows * airway->numCols);
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*airwayData)[k][i][j] = -1;
#endif

#if 1
  TISSUE *trace = malloc(sizeof(TISSUE));
  trace->bytesPerPixel = tissue->bytesPerPixel;
  trace->numDepth = tissue->numDepth;
  trace->numRows = tissue->numRows;
  trace->numCols = tissue->numCols;
	
  trace->data = malloc(sizeof(int) * trace->numDepth * trace->numRows * trace->numCols);
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j)
	(*traceData)[k][i][j] = -1;
#endif

#if 0
  printf("Connected tissue metrics.\n");	
  clean = NO;
  cleanTissueSize = 0;
  metrics = tissue_metrics(tissue, trace, airway, NO, 0);
  write_metrics(metrics, "metrics_tissue_R120.txt");
#endif

#if 0
  printf("Write povray.\n");
  write_povray(tissue, "bitmap.pov", 0, tissue->numDepth);
  //write_povray(tissue, "bitmap_R120.pov", 0, 20);
  //k = START_IMAGE - MESH_START_IMAGE + SEED_Z;
  //write_povray(tissue, "bitmap_R120.pov", k, k + 20);
  //write_df3(tissue, "bitmap.df3");
#endif

#if 0
  printf("Filling small airway gaps.\n");
  BOOL done = NO;
  int fill = 3;
  int pass = 1;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  NSNumber *three = [NSNumber numberWithInt: 3];
  NSNumber *four = [NSNumber numberWithInt: 4];
  NSNumber *five = [NSNumber numberWithInt: 5];
  NSNumber *six = [NSNumber numberWithInt: 6];
  NSNumber *seven = [NSNumber numberWithInt: 7];
  NSNumber *eight = [NSNumber numberWithInt: 8];
  NSNumber *nine = [NSNumber numberWithInt: 9];
  NSNumber *ten = [NSNumber numberWithInt: 10];
  while (!done) {
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    done = YES;
    printf("pass = %d\n", pass);
    metrics = length_metrics_k(tissue, airway, trace, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_i(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_j(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    ++pass;
    [pool release];
  }
  //write_povray(tissue, "fill.pov", 200, 250);
  write_df3(tissue, [airwayFillName UTF8String]);

  printf("Calculating XYZ airway metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, trace, YES, fill, markRange);
  write_metrics(metrics, "metrics_k.txt");
  metrics = length_metrics_i(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_i.txt");
  metrics = length_metrics_j(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_j.txt");
  //write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 200, 250);
#endif

#if 0
  set_image_rep(tissue);
  unsigned char *bd = [tissue->rep bitmapData];
  unsigned char (*repData)[tissue->numRows][tissue->numCols] = bd;
  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  k = START_IMAGE - MESH_START_IMAGE + SEED_Z;
  for (i = 0; i < tissue->numRows; ++i)
    for (j = 0; j < tissue->numCols; ++j)
      (*repData)[tissue->numRows - 1 - i][j] = (*tissueData)[k][i][j];

  NSData *d = [tissue->rep representationUsingType: NSPNGFileType properties: nil];
  [d writeToFile: @"test_tissue.png" atomically: YES];
#endif

#if 0
  // the border gets messed up filling the airway gaps, so copy it
  TISSUE *airway = malloc(sizeof(TISSUE));
  printf("Reading r120_raw.df3.\n");
  read_df3(airway, "r120_raw.df3");

  //printf("Reading r120_airway_fill.df3.\n");
  //read_df3(tissue, "r120_airway_fill.df3");
  printf("Reading r120_tissue_clean.df3.\n");
  read_df3(tissue, "r120_tissue_clean.df3");

  char (*tissueData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	if ((*airwayData)[k][i][j] == BORDER_VALUE) (*tissueData)[k][i][j] = BORDER_VALUE;

  //write_df3(tissue, "r120_airway_fix.df3");	
  write_df3(tissue, "r120_tissue_fix.df3");	
#endif

#if 0
  //
  // NOTE: this seems to create small little breaks in the tissue
  // which is likely not what we want
  //
  printf("Filling small tissue gaps.\n");
  int fill = 2;
  int pass = 1;
  BOOL done = NO;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  //NSNumber *three = [NSNumber numberWithInt: 3];
  while (!done) {
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    done = YES;
    printf("pass = %d\n", pass);
    metrics = length_metrics_k(tissue, airway, trace, NO, fill, nullRange);
    write_metrics(metrics, "metrics_tissue_k.txt");
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    metrics = length_metrics_i(tissue, airway, NO, fill, nullRange);
    write_metrics(metrics, "metrics_tissue_i.txt");
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    metrics = length_metrics_j(tissue, airway, NO, fill, nullRange);
    write_metrics(metrics, "metrics_tissue_j.txt");
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    ++pass;
    [pool release];
  }
  //write_povray(tissue, "fill.pov", 100, 250);
  write_df3(tissue, "r120_tissue_fill.df3");

  printf("Calculating metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, trace, NO, fill, markRange);
  write_metrics(metrics, "metrics_tissue_k.txt");
  metrics = length_metrics_i(tissue, airway, NO, fill, markRange);
  write_metrics(metrics, "metrics_tissue_i.txt");
  metrics = length_metrics_j(tissue, airway, NO, fill, markRange);
  write_metrics(metrics, "metrics_tissue_j.txt");
  //write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 200, 250);
#endif

#if 0
  printf("Cleaning tissue.\n");
  //do_trace_tissue(tissue, airway);
	
  clean = YES;
  cleanTissueSize = 1000000;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanTissueSize);
  write_metrics(metrics, "metrics_tissue.txt");
  write_df3(tissue, [tissueCleanName UTF8String]);

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 0
  printf("Tracing airway.\n");
  do_subtrace_airway_xy(tissue, airway);
  trim_airway_xy(tissue, airway);

  printf("Saving r120_airway.df3.\n");
  write_df3(tissue, "r120_airway.df3");

  //printf("Write povray.\n");
  //write_both_povray(tissue, 100, 250, airway, 0, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Tracing airway.\n");
  do_trace_airway(tissue, airway);

  printf("Saving r120_airway_trace.df3.\n");
  write_df3(airway, "r120_airway_trace.df3");

  //printf("Write povray.\n");
  //write_both_povray(tissue, 100, 250, airway, 0, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Tissue metrics.\n");
	
  clean = NO;
  cleanTissueSize = 0;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanTissueSize);
  write_metrics(metrics, "metrics_tissue_R120.txt");

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 0
  metrics = length_metrics_k(tissue, airway, trace, YES, 0, nullRange);
  metrics = length_neighbors(trace);
  write_metrics(metrics, "metrics_length.txt");
#endif


#if 0
  printf("Airway metrics.\n");
  clean = NO;
  NSRange cleanSize = {100000, 2000000};
  metrics = airway_metrics(tissue, trace, airway, clean, cleanSize);
  write_metrics(metrics, "metrics_airway_R120.txt");
  write_raw(trace, "raw_airway_trace.dat", sizeof(int));

  //printf("Write povray.\n");
  //write_povray(airway, "airway_trace.pov", 0, airway->numDepth);
#endif

#if 0
  k = START_IMAGE - MESH_START_IMAGE + SEED_Z;
  i = airway->numRows - SEED_Y; j = SEED_X;
  NSMutableArray *points = [NSMutableArray new];
  [points addObject: [TissuePoint newWithPoints: k : i : j]];

  printf("Tracing airway.\n");
  trace_airway(tissue, trace, points, 0, 200, 1);

  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j) {
	if ((*traceData)[k][i][j] == 1) (*airwayData)[k][i][j] = TISSUE_VALUE;
	else (*airwayData)[k][i][j] = 0;
      }

  printf("Write povray.\n");
  write_povray(airway, "airway.pov", 0, airway->numDepth);
#endif

#if 0
  set_image_rep(tissue);
  unsigned char *bd = [tissue->rep bitmapData];
  unsigned char (*repData)[tissue->numRows][tissue->numCols] = bd;
  for (i = 0; i < airway->numRows; ++i)
    for (j = 0; j < airway->numCols; ++j)
      (*repData)[airway->numRows - 1 - i][j] = (*airwayData)[START_IMAGE - MESH_START_IMAGE + SEED_Z][i][j];

  NSData *d = [tissue->rep representationUsingType: NSPNGFileType properties: nil];
  [d writeToFile: @"airway_trace.png" atomically: YES];
#endif

#if 0
  NSRange stackRange = {0, tissue->numDepth};
  generate_image_stack(tissue, stackRange, @"stack/airway");
#endif

#if 1
  printf("Write subcellular element model data.\n");

  //NSRange kr = {0, tissue->numDepth};
  //NSRange ir = {0, tissue->numRows};
  //NSRange jr = {0, tissue->numCols};
  NSRange kr = {200, 800};
  NSRange ir = {200, 1100};
  NSRange jr = {200, 800};
  //NSRange kr = {220, 290};
  //NSRange ir = {220, 290};
  //NSRange jr = {220, 290};
  write_scem(tissue, airway, kr, ir, jr);
  write_povray(airway, "scem_lung.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Write mesh.\n");
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  NSRange kr = {200, 250};
  NSRange ir = {200, 250};
  NSRange jr = {200, 250};
  write_mesh(tissue, airway, "initial_duct_mesh.dat", kr, ir, jr);
  write_povray(airway, "mesh.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Reading segment data.\n");
  read_segment_data(tissue, segmentFile);
#endif

}

void read_image_data_R120_LS1(TISSUE *tissue)
{
  int currentImage;
  NSString *prefixName = [NSString stringWithFormat: @"%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_DIR];
	
  tissue->numDepth = MESH_END_IMAGE - MESH_START_IMAGE + 1;

  printf("%d representations\n", tissue->numDepth);

  for (currentImage = MESH_START_IMAGE; currentImage <= MESH_END_IMAGE; ++currentImage) {
    NSMutableString *imageName = [NSMutableString stringWithFormat: @"%@%s", prefixName, OUTPUT_NAME];
    if (currentImage < 100) [imageName appendString: @"000"];
    else if (currentImage < 1000) [imageName appendString: @"00"];
    else [imageName appendString: @"0"];
    [imageName appendFormat: @"%d", currentImage];
    [imageName appendString: @".png"];

    printf("Reading image file:\n%s\n", [imageName UTF8String]);
    NSImage* tempImage = [[NSImage alloc] initWithContentsOfFile:imageName];
    if (!tempImage) {
      printf("Could not open image file.\n");
      abort();
    }
    NSArray* reps = [tempImage representations];
    NSBitmapImageRep *rep = [reps objectAtIndex: 0];

    int i, j, k;
    if (currentImage == MESH_START_IMAGE) {
      printf("bitsPerPixel = %ld\n", (long)[rep bitsPerPixel]);
      printf("bytesPerPlane = %ld\n", (long)[rep bytesPerPlane]);
      printf("bytesPerRow = %ld\n", (long)[rep bytesPerRow]);
      printf("numberOfPlanes = %ld\n", (long)[rep numberOfPlanes]);

      tissue->bytesPerPixel = [rep bitsPerPixel] / 8; // should be 3
      tissue->numCols = [rep bytesPerRow] / tissue->bytesPerPixel;
      tissue->numRows = [rep bytesPerPlane] / [rep bytesPerRow];
      printf("size = (%d, %d)\n", tissue->numCols, tissue->numRows);

      tissue->data = malloc(sizeof(char) * tissue->numDepth * tissue->numRows * tissue->numCols);
      char (*bitmapData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
      for (k = 0; k < tissue->numDepth; ++k)
	for (i = 0; i < tissue->numRows; ++i)
	  for (j = 0; j < tissue->numCols; ++j)
	    (*bitmapData)[k][i][j] = -1;
    }
    char (*bitmapData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;

    k = currentImage - MESH_START_IMAGE;
    unsigned char *bd = [rep bitmapData];
    int pos = 0;
    for (i = tissue->numRows-1; i >= 0; --i) // flip coordinates
      for (j = 0; j < tissue->numCols; ++j) {
	(*bitmapData)[k][i][j] = bd[pos];
	//if (bd[pos] == 0x80) (*bitmapData)[k][i][j] = TISSUE_VALUE;
	//else if (bd[pos] == 0x40) (*bitmapData)[k][i][j] = BORDER_VALUE;
	//else if (bd[pos] == (0x40 | 0x80)) (*bitmapData)[k][i][j] = BORDER_VALUE;
	//else (*bitmapData)[k][i][j] = bd[pos];
	++pos;
      }

  }

}

#if 0
void image_rep_R120_LS1(TISSUE *tissue)
{
  int currentImage;
  NSString *prefixName = [NSString stringWithFormat: @"%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_DIR];

  currentImage = MESH_START_IMAGE;
  NSMutableString *imageName = [NSMutableString stringWithFormat: @"%@%s", prefixName, OUTPUT_NAME];
  if (currentImage < 100) [imageName appendString: @"000"];
  else if (currentImage < 1000) [imageName appendString: @"00"];
  else [imageName appendString: @"0"];
  [imageName appendFormat: @"%d", currentImage];
  [imageName appendString: @".png"];

  printf("Reading image file:\n%s\n", [imageName UTF8String]);
  NSImage* tempImage = [[NSImage alloc] initWithContentsOfFile:imageName];
  if (!tempImage) {
    printf("Could not open image file.\n");
    abort();
  }
  NSArray* reps = [tempImage representations];
  NSBitmapImageRep *rep = [reps objectAtIndex: 0];

  NSData *d = [rep representationUsingType: NSPNGFileType properties: nil];
  tissue->rep = [[NSBitmapImageRep imageRepWithData: d] retain];
}
#endif

//
// LUNG_SAMPLE = 2
//

void process_R120_LS2()
{
  NSMutableDictionary *metrics = nil;
  int i, j, k;
  NSRange nullRange = {0, 0};
  BOOL clean = NO;
  int cleanTissueSize = 0;

  TISSUE *tissue = malloc(sizeof(TISSUE));

#if 0
  printf("Reading image data.\n");
  read_image_data(tissue);
#endif

  NSString *rawName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "raw.df3"];
  NSString *airwayFillName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "airway_fill.df3"];
  NSString *tissueCleanName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "tissue_clean.df3"];

#if 0
  printf("Saving raw data.\n%s\n", [rawName UTF8String]);
  write_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading raw data.\n%s\n", [rawName UTF8String]);
  read_df3(tissue, [rawName UTF8String]);
#endif

#if 1
  printf("Reading airway fill data.\n%s\n", [airwayFillName UTF8String]);
  read_df3(tissue, [airwayFillName UTF8String]);
#endif

#if 0
  printf("Reading r120_airway_fix.df3.\n");
  read_df3(tissue, "r120_airway_fix.df3");
#endif

#if 0
  printf("Reading r120_tissue_fill.df3.\n");
  read_df3(tissue, "r120_tissue_fill.df3");
#endif

#if 0
  printf("Reading tissue clean data.\n%s\n", [tissueCleanName UTF8String]);
  read_df3(tissue, [tissueCleanName UTF8String]);
#endif

#if 0
  printf("Reading r120_airway.df3.\n");
  read_df3(tissue, "r120_airway.df3");
#endif

#if 0
  printf("Reading r120_airway_trace.df3.\n");
  read_df3(tissue, "r120_airway_trace.df3");
#endif

#if 1
  TISSUE *airway = malloc(sizeof(TISSUE));
  airway->bytesPerPixel = tissue->bytesPerPixel;
  airway->numDepth = tissue->numDepth;
  airway->numRows = tissue->numRows;
  airway->numCols = tissue->numCols;
	
  airway->data = malloc(sizeof(char) * airway->numDepth * airway->numRows * airway->numCols);
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*airwayData)[k][i][j] = -1;
#endif

#if 1
  TISSUE *trace = malloc(sizeof(TISSUE));
  trace->bytesPerPixel = tissue->bytesPerPixel;
  trace->numDepth = tissue->numDepth;
  trace->numRows = tissue->numRows;
  trace->numCols = tissue->numCols;
	
  trace->data = malloc(sizeof(int) * trace->numDepth * trace->numRows * trace->numCols);
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j)
	(*traceData)[k][i][j] = -1;
#endif

#if 0
  printf("Connected tissue metrics.\n");	
  clean = NO;
  cleanTissueSize = 0;
  metrics = tissue_metrics(tissue, trace, airway, NO, 0);
  write_metrics(metrics, "metrics_tissue_R120.txt");
#endif

#if 0
  printf("Write povray.\n");
  write_povray(tissue, "bitmap.pov", 0, tissue->numDepth);
  //write_povray(tissue, "bitmap_R120.pov", 0, 20);
  //k = START_IMAGE - MESH_START_IMAGE + SEED_Z;
  //write_povray(tissue, "bitmap_R120.pov", k, k + 20);
  //write_df3(tissue, "bitmap.df3");
#endif

#if 0
  printf("Filling small airway gaps.\n");
  BOOL done = NO;
  int fill = 3;
  int pass = 1;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  NSNumber *three = [NSNumber numberWithInt: 3];
  NSNumber *four = [NSNumber numberWithInt: 4];
  NSNumber *five = [NSNumber numberWithInt: 5];
  NSNumber *six = [NSNumber numberWithInt: 6];
  NSNumber *seven = [NSNumber numberWithInt: 7];
  NSNumber *eight = [NSNumber numberWithInt: 8];
  NSNumber *nine = [NSNumber numberWithInt: 9];
  NSNumber *ten = [NSNumber numberWithInt: 10];
  while (!done) {
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    done = YES;
    printf("pass = %d\n", pass);
    metrics = length_metrics_k(tissue, airway, trace, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_i(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_j(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    ++pass;
    [pool release];
  }
  //write_povray(tissue, "fill.pov", 200, 250);
  write_df3(tissue, [airwayFillName UTF8String]);

  printf("Calculating XYZ airway metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, trace, YES, fill, markRange);
  write_metrics(metrics, "metrics_k.txt");
  metrics = length_metrics_i(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_i.txt");
  metrics = length_metrics_j(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_j.txt");
  //write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 200, 250);
#endif

#if 1
  printf("Cleaning tissue.\n");
  //do_trace_tissue(tissue, airway);
	
  clean = YES;
  cleanTissueSize = 1000000;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanTissueSize);
  write_metrics(metrics, "metrics_tissue.txt");
  write_df3(tissue, [tissueCleanName UTF8String]);

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 0
  printf("Tracing airway.\n");
  do_subtrace_airway_xy(tissue, airway);
  trim_airway_xy(tissue, airway);

  printf("Saving r120_airway.df3.\n");
  write_df3(tissue, "r120_airway.df3");

  //printf("Write povray.\n");
  //write_both_povray(tissue, 100, 250, airway, 0, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Tracing airway.\n");
  do_trace_airway(tissue, airway);

  printf("Saving r120_airway_trace.df3.\n");
  write_df3(airway, "r120_airway_trace.df3");

  //printf("Write povray.\n");
  //write_both_povray(tissue, 100, 250, airway, 0, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Tissue metrics.\n");
	
  clean = NO;
  cleanTissueSize = 0;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanTissueSize);
  write_metrics(metrics, "metrics_tissue_R120.txt");

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 0
  metrics = length_metrics_k(tissue, airway, trace, YES, 0, nullRange);
  metrics = length_neighbors(trace);
  write_metrics(metrics, "metrics_length.txt");
#endif


#if 0
  printf("Airway metrics.\n");
  clean = NO;
  NSRange cleanSize = {100000, 2000000};
  metrics = airway_metrics(tissue, trace, airway, clean, cleanSize);
  write_metrics(metrics, "metrics_airway_R120.txt");
  write_raw(trace, "raw_airway_trace.dat", sizeof(int));

  //printf("Write povray.\n");
  //write_povray(airway, "airway_trace.pov", 0, airway->numDepth);
#endif

#if 0
  k = START_IMAGE - MESH_START_IMAGE + SEED_Z;
  i = airway->numRows - SEED_Y; j = SEED_X;
  NSMutableArray *points = [NSMutableArray new];
  [points addObject: [TissuePoint newWithPoints: k : i : j]];

  printf("Tracing airway.\n");
  trace_airway(tissue, trace, points, 0, 200, 1);

  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j) {
	if ((*traceData)[k][i][j] == 1) (*airwayData)[k][i][j] = TISSUE_VALUE;
	else (*airwayData)[k][i][j] = 0;
      }

  printf("Write povray.\n");
  write_povray(airway, "airway.pov", 0, airway->numDepth);
#endif

#if 0
  set_image_rep(tissue);
  unsigned char *bd = [tissue->rep bitmapData];
  unsigned char (*repData)[tissue->numRows][tissue->numCols] = bd;
  for (i = 0; i < airway->numRows; ++i)
    for (j = 0; j < airway->numCols; ++j)
      (*repData)[airway->numRows - 1 - i][j] = (*airwayData)[START_IMAGE - MESH_START_IMAGE + SEED_Z][i][j];

  NSData *d = [tissue->rep representationUsingType: NSPNGFileType properties: nil];
  [d writeToFile: @"airway_trace.png" atomically: YES];
#endif

#if 1
  NSRange stackRange = {0, tissue->numDepth};
  generate_image_stack(tissue, stackRange, @"stack/airway");
#endif

#if 0
  printf("Write mesh.\n");
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  NSRange kr = {200, 250};
  NSRange ir = {200, 250};
  NSRange jr = {200, 250};
  write_mesh(tissue, airway, "initial_duct_mesh.dat", kr, ir, jr);
  write_povray(airway, "mesh.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Reading segment data.\n");
  read_segment_data(tissue, segmentFile);
#endif

}

//
// LUNG_SAMPLE = 3
//

void process_R120_LS3()
{
  NSMutableDictionary *metrics = nil;
  int i, j, k;
  NSRange nullRange = {0, 0};
  BOOL clean = NO;
  int cleanTissueSize = 0;

  TISSUE *tissue = malloc(sizeof(TISSUE));

#if 0
  printf("Reading image data.\n");
  read_image_data(tissue);
#endif

  NSString *rawName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "raw.df3"];
  NSString *airwayFillName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "airway_fill.df3"];
  NSString *tissueCleanName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "tissue_clean.df3"];

#if 0
  printf("Saving raw data.\n%s\n", [rawName UTF8String]);
  write_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading raw data.\n%s\n", [rawName UTF8String]);
  read_df3(tissue, [rawName UTF8String]);
#endif

#if 1
  printf("Reading airway fill data.\n%s\n", [airwayFillName UTF8String]);
  read_df3(tissue, [airwayFillName UTF8String]);
#endif

#if 0
  printf("Reading tissue clean data.\n%s\n", [tissueCleanName UTF8String]);
  read_df3(tissue, [tissueCleanName UTF8String]);
#endif

#if 1
  TISSUE *airway = malloc(sizeof(TISSUE));
  airway->bytesPerPixel = tissue->bytesPerPixel;
  airway->numDepth = tissue->numDepth;
  airway->numRows = tissue->numRows;
  airway->numCols = tissue->numCols;
	
  airway->data = malloc(sizeof(char) * airway->numDepth * airway->numRows * airway->numCols);
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*airwayData)[k][i][j] = -1;
#endif

#if 1
  TISSUE *trace = malloc(sizeof(TISSUE));
  trace->bytesPerPixel = tissue->bytesPerPixel;
  trace->numDepth = tissue->numDepth;
  trace->numRows = tissue->numRows;
  trace->numCols = tissue->numCols;
	
  trace->data = malloc(sizeof(int) * trace->numDepth * trace->numRows * trace->numCols);
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j)
	(*traceData)[k][i][j] = -1;
#endif

#if 0
  printf("Connected tissue metrics.\n");	
  clean = NO;
  cleanTissueSize = 0;
  metrics = tissue_metrics(tissue, trace, airway, NO, 0);
  write_metrics(metrics, "metrics_tissue.txt");
#endif

#if 0
  printf("Filling small airway gaps.\n");
  BOOL done = NO;
  int fill = 3;
  int pass = 1;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  NSNumber *three = [NSNumber numberWithInt: 3];
  NSNumber *four = [NSNumber numberWithInt: 4];
  NSNumber *five = [NSNumber numberWithInt: 5];
  NSNumber *six = [NSNumber numberWithInt: 6];
  NSNumber *seven = [NSNumber numberWithInt: 7];
  NSNumber *eight = [NSNumber numberWithInt: 8];
  NSNumber *nine = [NSNumber numberWithInt: 9];
  NSNumber *ten = [NSNumber numberWithInt: 10];
  while (!done) {
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    done = YES;
    printf("pass = %d\n", pass);
    metrics = length_metrics_k(tissue, airway, trace, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_i(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_j(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    ++pass;
    [pool release];
  }
  //write_povray(tissue, "fill.pov", 200, 250);
  write_df3(tissue, [airwayFillName UTF8String]);

  printf("Calculating XYZ airway metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, trace, YES, fill, markRange);
  write_metrics(metrics, "metrics_k.txt");
  metrics = length_metrics_i(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_i.txt");
  metrics = length_metrics_j(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_j.txt");
  //write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 200, 250);
#endif

#if 1
  printf("Cleaning tissue.\n");
  //do_trace_tissue(tissue, airway);
	
  clean = YES;
  cleanTissueSize = 4000000;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanTissueSize);
  write_metrics(metrics, "metrics_tissue.txt");
  write_df3(tissue, [tissueCleanName UTF8String]);

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 1
  NSRange stackRange = {0, tissue->numDepth};
  generate_image_stack(tissue, stackRange, @"stack/tissue");
#endif

#if 0
  printf("Write mesh.\n");
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  NSRange kr = {200, 250};
  NSRange ir = {200, 250};
  NSRange jr = {200, 250};
  write_mesh(tissue, airway, "initial_duct_mesh.dat", kr, ir, jr);
  write_povray(airway, "mesh.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Reading segment data.\n");
  read_segment_data(tissue, segmentFile);
#endif

}

//
// LUNG_SAMPLE = 4
//

void process_R75_LS4()
{
  NSMutableDictionary *metrics = nil;
  int i, j, k;
  NSRange nullRange = {0, 0};
  BOOL clean = NO;
  int cleanTissueSize = 0;

  TISSUE *tissue = malloc(sizeof(TISSUE));

#if 0
  printf("Reading image data.\n");
  read_image_data(tissue);
#endif

  NSString *rawName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "raw.df3"];
  NSString *airwayFillName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "airway_fill.df3"];
  NSString *tissueCleanName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "tissue_clean.df3"];

#if 0
  printf("Saving raw data.\n%s\n", [rawName UTF8String]);
  write_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading raw data.\n%s\n", [rawName UTF8String]);
  read_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading airway fill data.\n%s\n", [airwayFillName UTF8String]);
  read_df3(tissue, [airwayFillName UTF8String]);
#endif

#if 1
  printf("Reading tissue clean data.\n%s\n", [tissueCleanName UTF8String]);
  read_df3(tissue, [tissueCleanName UTF8String]);
#endif

#if 1
  TISSUE *airway = malloc(sizeof(TISSUE));
  airway->bytesPerPixel = tissue->bytesPerPixel;
  airway->numDepth = tissue->numDepth;
  airway->numRows = tissue->numRows;
  airway->numCols = tissue->numCols;
	
  airway->data = malloc(sizeof(char) * airway->numDepth * airway->numRows * airway->numCols);
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*airwayData)[k][i][j] = -1;
#endif

#if 1
  TISSUE *trace = malloc(sizeof(TISSUE));
  trace->bytesPerPixel = tissue->bytesPerPixel;
  trace->numDepth = tissue->numDepth;
  trace->numRows = tissue->numRows;
  trace->numCols = tissue->numCols;
	
  trace->data = malloc(sizeof(int) * trace->numDepth * trace->numRows * trace->numCols);
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j)
	(*traceData)[k][i][j] = -1;
#endif

#if 0
  printf("Connected tissue metrics.\n");	
  clean = NO;
  cleanTissueSize = 0;
  metrics = tissue_metrics(tissue, trace, airway, NO, 0);
  write_metrics(metrics, "metrics_tissue.txt");
#endif

#if 1
  printf("Filling small airway gaps.\n");
  BOOL done = NO;
  int fill = 3;
  int pass = 1;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  NSNumber *three = [NSNumber numberWithInt: 3];
  NSNumber *four = [NSNumber numberWithInt: 4];
  NSNumber *five = [NSNumber numberWithInt: 5];
  NSNumber *six = [NSNumber numberWithInt: 6];
  NSNumber *seven = [NSNumber numberWithInt: 7];
  NSNumber *eight = [NSNumber numberWithInt: 8];
  NSNumber *nine = [NSNumber numberWithInt: 9];
  NSNumber *ten = [NSNumber numberWithInt: 10];
  while (!done) {
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    done = YES;
    printf("pass = %d\n", pass);
    metrics = length_metrics_k(tissue, airway, trace, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_i(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_j(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    ++pass;
    [pool release];
  }
  //write_povray(tissue, "fill.pov", 200, 250);
  write_df3(tissue, [airwayFillName UTF8String]);

  printf("Calculating XYZ airway metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, trace, YES, fill, markRange);
  write_metrics(metrics, "metrics_k.txt");
  metrics = length_metrics_i(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_i.txt");
  metrics = length_metrics_j(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_j.txt");
  //write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 200, 250);
#endif

#if 0
  printf("Cleaning tissue.\n");
  //do_trace_tissue(tissue, airway);
	
  clean = YES;
  cleanTissueSize = 4000000;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanTissueSize);
  write_metrics(metrics, "metrics_tissue.txt");
  write_df3(tissue, [tissueCleanName UTF8String]);

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 1
  NSRange stackRange = {0, tissue->numDepth};
  generate_image_stack(tissue, stackRange, @"stack/tissue");
#endif

#if 0
  printf("Write mesh.\n");
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  NSRange kr = {200, 250};
  NSRange ir = {200, 250};
  NSRange jr = {200, 250};
  write_mesh(tissue, airway, "initial_duct_mesh.dat", kr, ir, jr);
  write_povray(airway, "mesh.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Reading segment data.\n");
  read_segment_data(tissue, segmentFile);
#endif

}


//
// LUNG_SAMPLE = 5
//

void process_R75_LS5()
{
  NSMutableDictionary *metrics = nil;
  int i, j, k;
  NSRange nullRange = {0, 0};
  BOOL clean = NO;
  int cleanTissueSize = 0;

  TISSUE *tissue = malloc(sizeof(TISSUE));

#if 0
  printf("Reading image data.\n");
  read_image_data(tissue);
#endif

  NSString *rawName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "raw.df3"];
  NSString *airwayFillName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "airway_fill.df3"];
  NSString *tissueCleanName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "tissue_clean.df3"];

#if 0
  printf("Saving raw data.\n%s\n", [rawName UTF8String]);
  write_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading raw data.\n%s\n", [rawName UTF8String]);
  read_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading airway fill data.\n%s\n", [airwayFillName UTF8String]);
  read_df3(tissue, [airwayFillName UTF8String]);
#endif

#if 1
  printf("Reading tissue clean data.\n%s\n", [tissueCleanName UTF8String]);
  read_df3(tissue, [tissueCleanName UTF8String]);
#endif

#if 1
  TISSUE *airway = malloc(sizeof(TISSUE));
  airway->bytesPerPixel = tissue->bytesPerPixel;
  airway->numDepth = tissue->numDepth;
  airway->numRows = tissue->numRows;
  airway->numCols = tissue->numCols;
	
  airway->data = malloc(sizeof(char) * airway->numDepth * airway->numRows * airway->numCols);
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*airwayData)[k][i][j] = -1;
#endif

#if 1
  TISSUE *trace = malloc(sizeof(TISSUE));
  trace->bytesPerPixel = tissue->bytesPerPixel;
  trace->numDepth = tissue->numDepth;
  trace->numRows = tissue->numRows;
  trace->numCols = tissue->numCols;
	
  trace->data = malloc(sizeof(int) * trace->numDepth * trace->numRows * trace->numCols);
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j)
	(*traceData)[k][i][j] = -1;
#endif

#if 0
  printf("Connected tissue metrics.\n");	
  clean = NO;
  cleanTissueSize = 0;
  metrics = tissue_metrics(tissue, trace, airway, NO, 0);
  write_metrics(metrics, "metrics_tissue.txt");
#endif

#if 1
  printf("Filling small airway gaps.\n");
  BOOL done = NO;
  int fill = 3;
  int pass = 1;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  NSNumber *three = [NSNumber numberWithInt: 3];
  NSNumber *four = [NSNumber numberWithInt: 4];
  NSNumber *five = [NSNumber numberWithInt: 5];
  NSNumber *six = [NSNumber numberWithInt: 6];
  NSNumber *seven = [NSNumber numberWithInt: 7];
  NSNumber *eight = [NSNumber numberWithInt: 8];
  NSNumber *nine = [NSNumber numberWithInt: 9];
  NSNumber *ten = [NSNumber numberWithInt: 10];
  while (!done) {
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    done = YES;
    printf("pass = %d\n", pass);
    metrics = length_metrics_k(tissue, airway, trace, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_i(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_j(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    ++pass;
    [pool release];
  }
  //write_povray(tissue, "fill.pov", 200, 250);
  write_df3(tissue, [airwayFillName UTF8String]);

  printf("Calculating XYZ airway metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, trace, YES, fill, markRange);
  write_metrics(metrics, "metrics_k.txt");
  metrics = length_metrics_i(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_i.txt");
  metrics = length_metrics_j(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_j.txt");
  //write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 200, 250);
#endif

#if 0
  printf("Cleaning tissue.\n");
  //do_trace_tissue(tissue, airway);
	
  clean = YES;
  cleanTissueSize = 4000000;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanTissueSize);
  write_metrics(metrics, "metrics_tissue.txt");
  write_df3(tissue, [tissueCleanName UTF8String]);

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 1
  NSRange stackRange = {0, tissue->numDepth};
  generate_image_stack(tissue, stackRange, @"stack/tissue");
#endif

#if 0
  printf("Write mesh.\n");
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  NSRange kr = {200, 250};
  NSRange ir = {200, 250};
  NSRange jr = {200, 250};
  write_mesh(tissue, airway, "initial_duct_mesh.dat", kr, ir, jr);
  write_povray(airway, "mesh.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Reading segment data.\n");
  read_segment_data(tissue, segmentFile);
#endif

}

//
// LUNG_SAMPLE = 6
//

void process_R79_LS6()
{
  NSMutableDictionary *metrics = nil;
  int i, j, k;
  NSRange nullRange = {0, 0};
  BOOL clean = NO;
  int cleanTissueSize = 0;

  TISSUE *tissue = malloc(sizeof(TISSUE));

#if 0
  printf("Reading image data.\n");
  read_image_data(tissue);
#endif

  NSString *rawName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "raw.df3"];
  NSString *airwayFillName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "airway_fill.df3"];
  NSString *tissueCleanName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "tissue_clean.df3"];

#if 0
  printf("Saving raw data.\n%s\n", [rawName UTF8String]);
  write_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading raw data.\n%s\n", [rawName UTF8String]);
  read_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading airway fill data.\n%s\n", [airwayFillName UTF8String]);
  read_df3(tissue, [airwayFillName UTF8String]);
#endif

#if 1
  printf("Reading tissue clean data.\n%s\n", [tissueCleanName UTF8String]);
  read_df3(tissue, [tissueCleanName UTF8String]);
#endif

#if 1
  TISSUE *airway = malloc(sizeof(TISSUE));
  airway->bytesPerPixel = tissue->bytesPerPixel;
  airway->numDepth = tissue->numDepth;
  airway->numRows = tissue->numRows;
  airway->numCols = tissue->numCols;
	
  airway->data = malloc(sizeof(char) * airway->numDepth * airway->numRows * airway->numCols);
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*airwayData)[k][i][j] = -1;
#endif

#if 1
  TISSUE *trace = malloc(sizeof(TISSUE));
  trace->bytesPerPixel = tissue->bytesPerPixel;
  trace->numDepth = tissue->numDepth;
  trace->numRows = tissue->numRows;
  trace->numCols = tissue->numCols;
	
  trace->data = malloc(sizeof(int) * trace->numDepth * trace->numRows * trace->numCols);
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j)
	(*traceData)[k][i][j] = -1;
#endif

#if 1
  printf("Connected tissue metrics.\n");	
  clean = NO;
  cleanTissueSize = 0;
  metrics = tissue_metrics(tissue, trace, airway, NO, 0);
  write_metrics(metrics, "metrics_tissue.txt");
#endif

#if 1
  printf("Filling small airway gaps.\n");
  BOOL done = NO;
  int fill = 3;
  int pass = 1;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  NSNumber *three = [NSNumber numberWithInt: 3];
  NSNumber *four = [NSNumber numberWithInt: 4];
  NSNumber *five = [NSNumber numberWithInt: 5];
  NSNumber *six = [NSNumber numberWithInt: 6];
  NSNumber *seven = [NSNumber numberWithInt: 7];
  NSNumber *eight = [NSNumber numberWithInt: 8];
  NSNumber *nine = [NSNumber numberWithInt: 9];
  NSNumber *ten = [NSNumber numberWithInt: 10];
  while (!done) {
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    done = YES;
    printf("pass = %d\n", pass);
    metrics = length_metrics_k(tissue, airway, trace, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_i(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_j(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    ++pass;
    [pool release];
  }
  //write_povray(tissue, "fill.pov", 200, 250);
  write_df3(tissue, [airwayFillName UTF8String]);

  printf("Calculating XYZ airway metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, trace, YES, fill, markRange);
  write_metrics(metrics, "metrics_k.txt");
  metrics = length_metrics_i(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_i.txt");
  metrics = length_metrics_j(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_j.txt");
  //write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 200, 250);
#endif

#if 0
  printf("Cleaning tissue.\n");
  //do_trace_tissue(tissue, airway);
	
  clean = YES;
  cleanTissueSize = 4000000;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanTissueSize);
  write_metrics(metrics, "metrics_tissue.txt");
  write_df3(tissue, [tissueCleanName UTF8String]);

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 1
  NSRange stackRange = {0, tissue->numDepth};
  generate_image_stack(tissue, stackRange, @"stack/tissue");
#endif

#if 0
  printf("Write mesh.\n");
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  NSRange kr = {200, 250};
  NSRange ir = {200, 250};
  NSRange jr = {200, 250};
  write_mesh(tissue, airway, "initial_duct_mesh.dat", kr, ir, jr);
  write_povray(airway, "mesh.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Reading segment data.\n");
  read_segment_data(tissue, segmentFile);
#endif

}


//
// LUNG_SAMPLE = 7
//

void process_R79_LS7()
{
  NSMutableDictionary *metrics = nil;
  int i, j, k;
  NSRange nullRange = {0, 0};
  BOOL clean = NO;
  int cleanTissueSize = 0;

  TISSUE *tissue = malloc(sizeof(TISSUE));

#if 0
  printf("Reading image data.\n");
  read_image_data(tissue);
#endif

  NSString *rawName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "raw.df3"];
  NSString *airwayFillName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "airway_fill.df3"];
  NSString *tissueCleanName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "tissue_clean.df3"];

#if 0
  printf("Saving raw data.\n%s\n", [rawName UTF8String]);
  write_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading raw data.\n%s\n", [rawName UTF8String]);
  read_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading airway fill data.\n%s\n", [airwayFillName UTF8String]);
  read_df3(tissue, [airwayFillName UTF8String]);
#endif

#if 1
  printf("Reading tissue clean data.\n%s\n", [tissueCleanName UTF8String]);
  read_df3(tissue, [tissueCleanName UTF8String]);
#endif

#if 1
  TISSUE *airway = malloc(sizeof(TISSUE));
  airway->bytesPerPixel = tissue->bytesPerPixel;
  airway->numDepth = tissue->numDepth;
  airway->numRows = tissue->numRows;
  airway->numCols = tissue->numCols;
	
  airway->data = malloc(sizeof(char) * airway->numDepth * airway->numRows * airway->numCols);
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*airwayData)[k][i][j] = -1;
#endif

#if 1
  TISSUE *trace = malloc(sizeof(TISSUE));
  trace->bytesPerPixel = tissue->bytesPerPixel;
  trace->numDepth = tissue->numDepth;
  trace->numRows = tissue->numRows;
  trace->numCols = tissue->numCols;
	
  trace->data = malloc(sizeof(int) * trace->numDepth * trace->numRows * trace->numCols);
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j)
	(*traceData)[k][i][j] = -1;
#endif

#if 0
  printf("Connected tissue metrics.\n");	
  clean = NO;
  cleanTissueSize = 0;
  metrics = tissue_metrics(tissue, trace, airway, NO, 0);
  write_metrics(metrics, "metrics_tissue.txt");
#endif

#if 1
  printf("Filling small airway gaps.\n");
  BOOL done = NO;
  int fill = 3;
  int pass = 1;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  NSNumber *three = [NSNumber numberWithInt: 3];
  NSNumber *four = [NSNumber numberWithInt: 4];
  NSNumber *five = [NSNumber numberWithInt: 5];
  NSNumber *six = [NSNumber numberWithInt: 6];
  NSNumber *seven = [NSNumber numberWithInt: 7];
  NSNumber *eight = [NSNumber numberWithInt: 8];
  NSNumber *nine = [NSNumber numberWithInt: 9];
  NSNumber *ten = [NSNumber numberWithInt: 10];
  while (!done) {
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    done = YES;
    printf("pass = %d\n", pass);
    metrics = length_metrics_k(tissue, airway, trace, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_i(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_j(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    ++pass;
    [pool release];
  }
  //write_povray(tissue, "fill.pov", 200, 250);
  write_df3(tissue, [airwayFillName UTF8String]);

  printf("Calculating XYZ airway metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, trace, YES, fill, markRange);
  write_metrics(metrics, "metrics_k.txt");
  metrics = length_metrics_i(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_i.txt");
  metrics = length_metrics_j(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_j.txt");
  //write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 200, 250);
#endif

#if 0
  printf("Cleaning tissue.\n");
  //do_trace_tissue(tissue, airway);
	
  clean = YES;
  cleanTissueSize = 4000000;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanTissueSize);
  write_metrics(metrics, "metrics_tissue.txt");
  write_df3(tissue, [tissueCleanName UTF8String]);

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 1
  NSRange stackRange = {0, tissue->numDepth};
  generate_image_stack(tissue, stackRange, @"stack/tissue");
#endif

#if 0
  printf("Write mesh.\n");
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  NSRange kr = {200, 250};
  NSRange ir = {200, 250};
  NSRange jr = {200, 250};
  write_mesh(tissue, airway, "initial_duct_mesh.dat", kr, ir, jr);
  write_povray(airway, "mesh.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Reading segment data.\n");
  read_segment_data(tissue, segmentFile);
#endif

}


//
// LUNG_SAMPLE = 8
//

void process_R83_LS8()
{
  NSMutableDictionary *metrics = nil;
  int i, j, k;
  NSRange nullRange = {0, 0};
  BOOL clean = NO;
  int cleanTissueSize = 0;

  TISSUE *tissue = malloc(sizeof(TISSUE));

#if 0
  printf("Reading image data.\n");
  read_image_data(tissue);
#endif

  NSString *rawName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "raw.df3"];
  NSString *airwayFillName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "airway_fill.df3"];
  NSString *tissueCleanName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "tissue_clean.df3"];

#if 0
  printf("Saving raw data.\n%s\n", [rawName UTF8String]);
  write_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading raw data.\n%s\n", [rawName UTF8String]);
  read_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading airway fill data.\n%s\n", [airwayFillName UTF8String]);
  read_df3(tissue, [airwayFillName UTF8String]);
#endif

#if 1
  printf("Reading tissue clean data.\n%s\n", [tissueCleanName UTF8String]);
  read_df3(tissue, [tissueCleanName UTF8String]);
#endif

#if 1
  TISSUE *airway = malloc(sizeof(TISSUE));
  airway->bytesPerPixel = tissue->bytesPerPixel;
  airway->numDepth = tissue->numDepth;
  airway->numRows = tissue->numRows;
  airway->numCols = tissue->numCols;
	
  airway->data = malloc(sizeof(char) * airway->numDepth * airway->numRows * airway->numCols);
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*airwayData)[k][i][j] = -1;
#endif

#if 1
  TISSUE *trace = malloc(sizeof(TISSUE));
  trace->bytesPerPixel = tissue->bytesPerPixel;
  trace->numDepth = tissue->numDepth;
  trace->numRows = tissue->numRows;
  trace->numCols = tissue->numCols;
	
  trace->data = malloc(sizeof(int) * trace->numDepth * trace->numRows * trace->numCols);
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j)
	(*traceData)[k][i][j] = -1;
#endif

#if 0
  printf("Connected tissue metrics.\n");	
  clean = NO;
  cleanTissueSize = 0;
  metrics = tissue_metrics(tissue, trace, airway, NO, 0);
  write_metrics(metrics, "metrics_tissue.txt");
#endif

#if 1
  printf("Filling small airway gaps.\n");
  BOOL done = NO;
  int fill = 3;
  int pass = 1;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  NSNumber *three = [NSNumber numberWithInt: 3];
  NSNumber *four = [NSNumber numberWithInt: 4];
  NSNumber *five = [NSNumber numberWithInt: 5];
  NSNumber *six = [NSNumber numberWithInt: 6];
  NSNumber *seven = [NSNumber numberWithInt: 7];
  NSNumber *eight = [NSNumber numberWithInt: 8];
  NSNumber *nine = [NSNumber numberWithInt: 9];
  NSNumber *ten = [NSNumber numberWithInt: 10];
  while (!done) {
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    done = YES;
    printf("pass = %d\n", pass);
    metrics = length_metrics_k(tissue, airway, trace, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_i(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_j(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    ++pass;
    [pool release];
  }
  //write_povray(tissue, "fill.pov", 200, 250);
  write_df3(tissue, [airwayFillName UTF8String]);

  printf("Calculating XYZ airway metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, trace, YES, fill, markRange);
  write_metrics(metrics, "metrics_k.txt");
  metrics = length_metrics_i(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_i.txt");
  metrics = length_metrics_j(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_j.txt");
  //write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 200, 250);
#endif

#if 0
  printf("Cleaning tissue.\n");
  //do_trace_tissue(tissue, airway);
	
  clean = YES;
  cleanTissueSize = 4000000;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanTissueSize);
  write_metrics(metrics, "metrics_tissue.txt");
  write_df3(tissue, [tissueCleanName UTF8String]);

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 1
  NSRange stackRange = {0, tissue->numDepth};
  generate_image_stack(tissue, stackRange, @"stack/tissue");
#endif

#if 0
  printf("Write mesh.\n");
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  NSRange kr = {200, 250};
  NSRange ir = {200, 250};
  NSRange jr = {200, 250};
  write_mesh(tissue, airway, "initial_duct_mesh.dat", kr, ir, jr);
  write_povray(airway, "mesh.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Reading segment data.\n");
  read_segment_data(tissue, segmentFile);
#endif

}

//
// LUNG_SAMPLE = 9
//

void process_R83_LS9()
{
  NSMutableDictionary *metrics = nil;
  int i, j, k;
  NSRange nullRange = {0, 0};
  BOOL clean = NO;
  int cleanTissueSize = 0;

  TISSUE *tissue = malloc(sizeof(TISSUE));

#if 0
  printf("Reading image data.\n");
  read_image_data(tissue);
#endif

  NSString *rawName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "raw.df3"];
  NSString *airwayFillName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "airway_fill.df3"];
  NSString *tissueCleanName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "tissue_clean.df3"];

#if 0
  printf("Saving raw data.\n%s\n", [rawName UTF8String]);
  write_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading raw data.\n%s\n", [rawName UTF8String]);
  read_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading airway fill data.\n%s\n", [airwayFillName UTF8String]);
  read_df3(tissue, [airwayFillName UTF8String]);
#endif

#if 1
  printf("Reading tissue clean data.\n%s\n", [tissueCleanName UTF8String]);
  read_df3(tissue, [tissueCleanName UTF8String]);
#endif

#if 1
  TISSUE *airway = malloc(sizeof(TISSUE));
  airway->bytesPerPixel = tissue->bytesPerPixel;
  airway->numDepth = tissue->numDepth;
  airway->numRows = tissue->numRows;
  airway->numCols = tissue->numCols;
	
  airway->data = malloc(sizeof(char) * airway->numDepth * airway->numRows * airway->numCols);
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*airwayData)[k][i][j] = -1;
#endif

#if 1
  TISSUE *trace = malloc(sizeof(TISSUE));
  trace->bytesPerPixel = tissue->bytesPerPixel;
  trace->numDepth = tissue->numDepth;
  trace->numRows = tissue->numRows;
  trace->numCols = tissue->numCols;
	
  trace->data = malloc(sizeof(int) * trace->numDepth * trace->numRows * trace->numCols);
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j)
	(*traceData)[k][i][j] = -1;
#endif

#if 0
  printf("Connected tissue metrics.\n");	
  clean = NO;
  cleanTissueSize = 0;
  metrics = tissue_metrics(tissue, trace, airway, NO, 0);
  write_metrics(metrics, "metrics_tissue.txt");
#endif

#if 1
  printf("Filling small airway gaps.\n");
  BOOL done = NO;
  int fill = 3;
  int pass = 1;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  NSNumber *three = [NSNumber numberWithInt: 3];
  NSNumber *four = [NSNumber numberWithInt: 4];
  NSNumber *five = [NSNumber numberWithInt: 5];
  NSNumber *six = [NSNumber numberWithInt: 6];
  NSNumber *seven = [NSNumber numberWithInt: 7];
  NSNumber *eight = [NSNumber numberWithInt: 8];
  NSNumber *nine = [NSNumber numberWithInt: 9];
  NSNumber *ten = [NSNumber numberWithInt: 10];
  while (!done) {
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    done = YES;
    printf("pass = %d\n", pass);
    metrics = length_metrics_k(tissue, airway, trace, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_i(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_j(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    ++pass;
    [pool release];
  }
  //write_povray(tissue, "fill.pov", 200, 250);
  write_df3(tissue, [airwayFillName UTF8String]);

  printf("Calculating XYZ airway metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, trace, YES, fill, markRange);
  write_metrics(metrics, "metrics_k.txt");
  metrics = length_metrics_i(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_i.txt");
  metrics = length_metrics_j(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_j.txt");
  //write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 200, 250);
#endif

#if 0
  printf("Cleaning tissue.\n");
  //do_trace_tissue(tissue, airway);
	
  clean = YES;
  cleanTissueSize = 4000000;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanTissueSize);
  write_metrics(metrics, "metrics_tissue.txt");
  write_df3(tissue, [tissueCleanName UTF8String]);

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 1
  NSRange stackRange = {0, tissue->numDepth};
  generate_image_stack(tissue, stackRange, @"stack/tissue");
#endif

#if 0
  printf("Write mesh.\n");
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  NSRange kr = {200, 250};
  NSRange ir = {200, 250};
  NSRange jr = {200, 250};
  write_mesh(tissue, airway, "initial_duct_mesh.dat", kr, ir, jr);
  write_povray(airway, "mesh.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Reading segment data.\n");
  read_segment_data(tissue, segmentFile);
#endif

}


//
// LUNG_SAMPLE = 10
//

void process_R84_LS10()
{
  NSMutableDictionary *metrics = nil;
  int i, j, k;
  NSRange nullRange = {0, 0};
  BOOL clean = NO;
  int cleanTissueSize = 0;

  TISSUE *tissue = malloc(sizeof(TISSUE));

#if 0
  printf("Reading image data.\n");
  read_image_data(tissue);
#endif

  NSString *rawName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "raw.df3"];
  NSString *airwayFillName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "airway_fill.df3"];
  NSString *tissueCleanName = [NSString stringWithFormat: @"%s%s%s%s", DATA_DIR, SAMPLE_DIR, OUTPUT_NAME, "tissue_clean.df3"];

#if 0
  printf("Saving raw data.\n%s\n", [rawName UTF8String]);
  write_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading raw data.\n%s\n", [rawName UTF8String]);
  read_df3(tissue, [rawName UTF8String]);
#endif

#if 0
  printf("Reading airway fill data.\n%s\n", [airwayFillName UTF8String]);
  read_df3(tissue, [airwayFillName UTF8String]);
#endif

#if 1
  printf("Reading tissue clean data.\n%s\n", [tissueCleanName UTF8String]);
  read_df3(tissue, [tissueCleanName UTF8String]);
#endif

#if 1
  TISSUE *airway = malloc(sizeof(TISSUE));
  airway->bytesPerPixel = tissue->bytesPerPixel;
  airway->numDepth = tissue->numDepth;
  airway->numRows = tissue->numRows;
  airway->numCols = tissue->numCols;
	
  airway->data = malloc(sizeof(char) * airway->numDepth * airway->numRows * airway->numCols);
  char (*airwayData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*airwayData)[k][i][j] = -1;
#endif

#if 1
  TISSUE *trace = malloc(sizeof(TISSUE));
  trace->bytesPerPixel = tissue->bytesPerPixel;
  trace->numDepth = tissue->numDepth;
  trace->numRows = tissue->numRows;
  trace->numCols = tissue->numCols;
	
  trace->data = malloc(sizeof(int) * trace->numDepth * trace->numRows * trace->numCols);
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
  for (k = 0; k < trace->numDepth; ++k)
    for (i = 0; i < trace->numRows; ++i)
      for (j = 0; j < trace->numCols; ++j)
	(*traceData)[k][i][j] = -1;
#endif

#if 0
  printf("Connected tissue metrics.\n");	
  clean = NO;
  cleanTissueSize = 0;
  metrics = tissue_metrics(tissue, trace, airway, NO, 0);
  write_metrics(metrics, "metrics_tissue.txt");
#endif

#if 1
  printf("Filling small airway gaps.\n");
  BOOL done = NO;
  int fill = 3;
  int pass = 1;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  NSNumber *three = [NSNumber numberWithInt: 3];
  NSNumber *four = [NSNumber numberWithInt: 4];
  NSNumber *five = [NSNumber numberWithInt: 5];
  NSNumber *six = [NSNumber numberWithInt: 6];
  NSNumber *seven = [NSNumber numberWithInt: 7];
  NSNumber *eight = [NSNumber numberWithInt: 8];
  NSNumber *nine = [NSNumber numberWithInt: 9];
  NSNumber *ten = [NSNumber numberWithInt: 10];
  while (!done) {
    NSAutoreleasePool *pool = [NSAutoreleasePool new];
    done = YES;
    printf("pass = %d\n", pass);
    metrics = length_metrics_k(tissue, airway, trace, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_i(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_j(tissue, airway, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    //if ([metrics objectForKey: two]) done = NO;
    //if ([metrics objectForKey: three]) done = NO;
    //if ([metrics objectForKey: four]) done = NO;
    //if ([metrics objectForKey: five]) done = NO;
    //if ([metrics objectForKey: six]) done = NO;
    //if ([metrics objectForKey: seven]) done = NO;
    //if ([metrics objectForKey: eight]) done = NO;
    //if ([metrics objectForKey: nine]) done = NO;
    //if ([metrics objectForKey: ten]) done = NO;
    ++pass;
    [pool release];
  }
  //write_povray(tissue, "fill.pov", 200, 250);
  write_df3(tissue, [airwayFillName UTF8String]);

  printf("Calculating XYZ airway metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, trace, YES, fill, markRange);
  write_metrics(metrics, "metrics_k.txt");
  metrics = length_metrics_i(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_i.txt");
  metrics = length_metrics_j(tissue, airway, YES, fill, markRange);
  write_metrics(metrics, "metrics_j.txt");
  //write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  //write_povray(airway, "airway.pov", 200, 250);
#endif

#if 0
  printf("Cleaning tissue.\n");
  //do_trace_tissue(tissue, airway);
	
  clean = YES;
  cleanTissueSize = 4000000;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanTissueSize);
  write_metrics(metrics, "metrics_tissue.txt");
  write_df3(tissue, [tissueCleanName UTF8String]);

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 1
  NSRange stackRange = {0, tissue->numDepth};
  generate_image_stack(tissue, stackRange, @"stack/tissue");
#endif

#if 0
  printf("Write mesh.\n");
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  NSRange kr = {200, 250};
  NSRange ir = {200, 250};
  NSRange jr = {200, 250};
  write_mesh(tissue, airway, "initial_duct_mesh.dat", kr, ir, jr);
  write_povray(airway, "mesh.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Reading segment data.\n");
  read_segment_data(tissue, segmentFile);
#endif

}


//
// Old sample code
//

#if 0
void process_APRV75_Inspiration_Sample1()
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];

  //NSString *segmentFile = @"/Volumes/Data/Projects/lung/recon_proj_1-600-1000-filtered.surf.ply";
  NSMutableDictionary *metrics = nil;

  NSRange nullRange = {0, 0};

  printf("Processing sample:\n%s\n", SAMPLE_DESC);

  printf("Reading image data.\n");
  TISSUE *tissue = malloc(sizeof(TISSUE));
  read_image_data(tissue);

  TISSUE *airway = malloc(sizeof(TISSUE));
  airway->bytesPerPixel = tissue->bytesPerPixel;
  airway->numDepth = tissue->numDepth;
  airway->numRows = tissue->numRows;
  airway->numCols = tissue->numCols;
	
  airway->data = malloc(sizeof(char) * airway->numDepth * airway->numRows * airway->numCols);
  char (*bitmapData)[airway->numDepth][airway->numRows][airway->numCols] = airway->data;
  int i, j, k;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  TISSUE *trace = malloc(sizeof(TISSUE));
  trace->bytesPerPixel = tissue->bytesPerPixel;
  trace->numDepth = tissue->numDepth;
  trace->numRows = tissue->numRows;
  trace->numCols = tissue->numCols;
	
  trace->data = malloc(sizeof(int) * trace->numDepth * trace->numRows * trace->numCols);
  int (*traceData)[trace->numDepth][trace->numRows][trace->numCols] = trace->data;
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*traceData)[k][i][j] = -1;

#if 0
  printf("Write povray.\n");
  //write_povray(tissue, "bitmap.pov", 0, tissue->numDepth);
  //write_povray(tissue, "bitmap.pov", 0, 100);
  //write_df3(tissue, "bitmap.df3");
#endif

#if 0
  printf("Filling small airway gaps.\n");
  BOOL done = NO;
  int fill = 10;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  NSNumber *three = [NSNumber numberWithInt: 3];
  NSNumber *four = [NSNumber numberWithInt: 4];
  NSNumber *five = [NSNumber numberWithInt: 5];
  NSNumber *six = [NSNumber numberWithInt: 6];
  NSNumber *seven = [NSNumber numberWithInt: 7];
  NSNumber *eight = [NSNumber numberWithInt: 8];
  NSNumber *nine = [NSNumber numberWithInt: 9];
  NSNumber *ten = [NSNumber numberWithInt: 10];
  while (!done) {
    done = YES;
    metrics = length_metrics_k(tissue, airway, trace, YES, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    if ([metrics objectForKey: three]) done = NO;
    if ([metrics objectForKey: four]) done = NO;
    if ([metrics objectForKey: five]) done = NO;
    if ([metrics objectForKey: six]) done = NO;
    if ([metrics objectForKey: seven]) done = NO;
    if ([metrics objectForKey: eight]) done = NO;
    if ([metrics objectForKey: nine]) done = NO;
    if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_i(tissue, airway, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    if ([metrics objectForKey: three]) done = NO;
    if ([metrics objectForKey: four]) done = NO;
    if ([metrics objectForKey: five]) done = NO;
    if ([metrics objectForKey: six]) done = NO;
    if ([metrics objectForKey: seven]) done = NO;
    if ([metrics objectForKey: eight]) done = NO;
    if ([metrics objectForKey: nine]) done = NO;
    if ([metrics objectForKey: ten]) done = NO;
    metrics = length_metrics_j(tissue, airway, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    if ([metrics objectForKey: three]) done = NO;
    if ([metrics objectForKey: four]) done = NO;
    if ([metrics objectForKey: five]) done = NO;
    if ([metrics objectForKey: six]) done = NO;
    if ([metrics objectForKey: seven]) done = NO;
    if ([metrics objectForKey: eight]) done = NO;
    if ([metrics objectForKey: nine]) done = NO;
    if ([metrics objectForKey: ten]) done = NO;
  }
  write_povray(tissue, "fill.pov", 200, 250);

  printf("Calculating metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, trace, YES, fill, markRange);
  write_metrics(metrics, "metrics_k.txt");
  metrics = length_metrics_i(tissue, airway, fill, markRange);
  write_metrics(metrics, "metrics_i.txt");
  metrics = length_metrics_j(tissue, airway, fill, markRange);
  write_metrics(metrics, "metrics_j.txt");
  write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  write_povray(airway, "airway.pov", 200, 250);
#endif

#if 0
  int fill = 2;
#if 0
  printf("Filling small tissue gaps.\n");
  NSRange nullRange = {0, 0};
  BOOL done = NO;
  NSNumber *one = [NSNumber numberWithInt: 1];
  NSNumber *two = [NSNumber numberWithInt: 2];
  while (!done) {
    done = YES;
    metrics = length_metrics_k(tissue, airway, NO, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    metrics = length_metrics_i(tissue, airway, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
    metrics = length_metrics_j(tissue, airway, fill, nullRange);
    if ([metrics objectForKey: one]) done = NO;
    if ([metrics objectForKey: two]) done = NO;
  }
  write_povray(tissue, "fill.pov", 100, 250);
#endif

  printf("Calculating metrics.\n");
  NSRange markRange = {0, 50};
  metrics = length_metrics_k(tissue, airway, NO, fill, markRange);
  write_metrics(metrics, "metrics_tissue_k.txt");
  metrics = length_metrics_i(tissue, airway, fill, markRange);
  write_metrics(metrics, "metrics_tissue_i.txt");
  metrics = length_metrics_j(tissue, airway, fill, markRange);
  write_metrics(metrics, "metrics_tissue_j.txt");
  write_both_povray(tissue, 200, 250, airway, 200, 250 /* airway->numDepth */, "both.pov");
  write_povray(airway, "airway.pov", 200, 250);
#endif

#if 0
  printf("Cleaning tissue.\n");
  //do_trace_tissue(tissue, airway);
	
  BOOL clean = YES;
  int cleanSize = 1000;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanSize);
  write_metrics(metrics, "metrics_tissue.txt");

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 0
  printf("Tissue metrics.\n");
	
  BOOL clean = NO;
  int cleanSize = 1000;
  metrics = tissue_metrics(tissue, trace, airway, clean, cleanSize);
  write_metrics(metrics, "metrics_tissue.txt");

  //printf("Write povray.\n");
  //write_povray(airway, "tissue_trace.pov", 0, tissue->numDepth);
  //write_povray(tissue, "tissue.pov", 100, 250);
#endif

#if 0
  metrics = length_metrics_k(tissue, airway, trace, YES, 0, nullRange);
  metrics = length_neighbors(trace);
  write_metrics(metrics, "metrics_length.txt");
#endif


#if 0
  printf("Airway metrics.\n");
  NSRange cleanSize = {100000, 2000000};
  metrics = airway_metrics(tissue, trace, airway, NO, cleanSize);
  write_metrics(metrics, "metrics_airway.txt");

  printf("Write povray.\n");
  write_povray(airway, "airway_trace.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Tracing airway.\n");
  do_trace_airway(tissue, airway);

  printf("Write povray.\n");
  write_both_povray(tissue, 100, 250, airway, 0, 250 /* airway->numDepth */, "both.pov");
  write_povray(airway, "airway.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Write mesh.\n");
  for (k = 0; k < airway->numDepth; ++k)
    for (i = 0; i < airway->numRows; ++i)
      for (j = 0; j < airway->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  NSRange kr = {200, 250};
  NSRange ir = {200, 250};
  NSRange jr = {200, 250};
  write_mesh(tissue, airway, "initial_duct_mesh.dat", kr, ir, jr);
  write_povray(airway, "mesh.pov", 0, airway->numDepth);
#endif

#if 0
  printf("Reading segment data.\n");
  read_segment_data(tissue, segmentFile);
#endif

  [pool release];
}

//
// read image functions
//
void read_image_data_APRV75_Inspiration_Sample1(TISSUE *tissue)
{
  NSString *imageName = @"/Volumes/Data/Projects/lung/recon_proj_1-600-1000-filtered.labels.tif";
  NSString *segmentFile = @"/Volumes/Data/Projects/lung/recon_proj_1-600-1000-filtered.surf.ply";

  NSImage* tempImage = [[NSImage alloc] initWithContentsOfFile:imageName];
  if (!tempImage) {
    printf("Could not open image file: %s.\n", [imageName UTF8String]);
    abort();
  }

  NSArray* reps = [tempImage representations];
  tissue->numDepth = [reps count];
  printf("%d representations\n", tissue->numDepth);
	
  NSBitmapImageRep *rep = [reps objectAtIndex: 0];
  printf("bitsPerPixel = %ld\n", (long)[rep bitsPerPixel]);
  printf("bytesPerPlane = %ld\n", (long)[rep bytesPerPlane]);
  printf("bytesPerRow = %ld\n", (long)[rep bytesPerRow]);
  printf("numberOfPlanes = %ld\n", (long)[rep numberOfPlanes]);
	
  tissue->bytesPerPixel = [rep bitsPerPixel] / 8; // should be 3
  tissue->numCols = [rep bytesPerRow] / tissue->bytesPerPixel;
  tissue->numRows = [rep bytesPerPlane] / [rep bytesPerRow];
  printf("size = (%d, %d)\n", tissue->numCols, tissue->numRows);
	
  tissue->data = malloc(sizeof(char) * tissue->numDepth * tissue->numRows * tissue->numCols);
  char (*bitmapData)[tissue->numDepth][tissue->numRows][tissue->numCols] = tissue->data;
  int i, j, k;
  for (k = 0; k < tissue->numDepth; ++k)
    for (i = 0; i < tissue->numRows; ++i)
      for (j = 0; j < tissue->numCols; ++j)
	(*bitmapData)[k][i][j] = -1;

  for (k = 0; k < tissue->numDepth; ++k) {
    rep = [reps objectAtIndex: k];
    unsigned char *bd = [rep bitmapData];
    int pos = 0;
    for (i = tissue->numRows-1; i >= 0; --i) // flip coordinates
      for (j = 0; j < tissue->numCols; ++j) {
	int r = bd[pos++];
	int g = bd[pos++];
	int b = bd[pos++];
			
	if ((r == 164) & (g == 0) & (b == 205)) (*bitmapData)[k][i][j] = 1;
	if ((r == 179) & (g == 205) & (b == 205)) (*bitmapData)[k][i][j] = 0;
      }
  }
}
#endif
