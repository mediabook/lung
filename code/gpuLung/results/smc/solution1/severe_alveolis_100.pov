global_settings {
  max_trace_level 10
  assumed_gamma 1
}

camera {
  location    <-1.300000, -1.300000, 0.000000>
  look_at     <0.000000, 0.000000, 0>
}

light_source {
  <-5.000000, 0.500000, -2.000000>*500
  color rgb <2.0, 1.5, 1.0>*1.8
  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient
}

light_source {
  <5.000000, 0.500000, 2.000000>*500
  color rgb <2.0, 1.5, 1.0>*1.8
  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient
}

blob {
  threshold 0.5
  sphere {
    <0.000000, 0.000000, 0.000000>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000980, 0.025458, 0.016199>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000982, 0.019621, 0.009305>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.004288, -0.037738, 0.020093>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001313, -0.014183, 0.000794>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000025, 0.031450, 0.034547>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000958, 0.020662, 0.010272>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006042, -0.025562, 0.066210>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000596, 0.031169, 0.041315>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002452, 0.024232, 0.058161>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003610, -0.031609, 0.011089>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006411, -0.028830, 0.063706>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003721, 0.015899, 0.066197>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003902, -0.032859, 0.012479>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006192, -0.040791, 0.042937>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000716, 0.017977, 0.007774>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003106, -0.026980, 0.006901>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000368, -0.000361, -0.000151>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001853, -0.015961, 0.001208>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002986, -0.025008, 0.005470>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006937, -0.029853, 0.062824>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002380, -0.019596, 0.002535>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003933, 0.016885, 0.065570>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002894, 0.023685, 0.058986>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000199, 0.031356, 0.031934>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.005760, -0.040332, 0.026805>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006460, -0.009522, 0.072455>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007091, -0.038582, 0.050352>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003600, 0.020237, 0.062788>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001524, -0.010536, -0.000106>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002497, -0.018955, 0.002218>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007052, -0.017859, 0.070361>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001377, 0.030775, 0.044268>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002657, 0.026280, 0.055368>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001515, 0.030518, 0.045373>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006041, -0.040155, 0.026009>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007386, -0.040142, 0.046031>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.005135, 0.011046, 0.069190>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007483, -0.040190, 0.045882>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000317, 0.021865, 0.011224>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006036, 0.003288, 0.071906>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007262, -0.041427, 0.039024>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008091, -0.033650, 0.058791>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007993, -0.039002, 0.049460>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007549, -0.041321, 0.040254>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002456, 0.028790, 0.050697>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006974, -0.004364, 0.072834>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.005681, -0.036247, 0.016881>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003278, -0.020167, 0.002598>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.004091, 0.022140, 0.061042>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.005000, 0.016694, 0.065913>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000021, 0.023070, 0.012447>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000249, 0.013857, 0.004572>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008556, -0.022837, 0.068108>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000369, 0.029298, 0.022859>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000096, 0.024102, 0.013667>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000737, 0.031090, 0.028907>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008881, -0.038973, 0.049638>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008447, -0.013433, 0.071852>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000453, 0.013921, 0.004538>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000642, 0.029983, 0.024580>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003596, -0.019172, 0.002062>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007029, -0.038821, 0.021763>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000530, 0.028392, 0.020562>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003829, -0.020361, 0.002574>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.009491, -0.032727, 0.059981>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.005687, 0.016424, 0.066215>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003859, 0.026406, 0.055623>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008040, -0.041066, 0.029306>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007701, 0.001621, 0.072388>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.004126, -0.021085, 0.002876>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000557, 0.025552, 0.015466>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008769, -0.009481, 0.072618>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008190, -0.002376, 0.072859>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007526, -0.038957, 0.021987>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.004559, 0.024502, 0.058475>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.004404, 0.025208, 0.057499>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002534, -0.007075, -0.000801>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000713, 0.019836, 0.008781>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.009969, -0.039174, 0.049202>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002225, 0.031906, 0.038982>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010310, -0.028939, 0.063827>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.005440, 0.021630, 0.061883>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010370, -0.038243, 0.051422>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006417, -0.031869, 0.010752>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006430, -0.031819, 0.010694>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008782, -0.040764, 0.027555>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008143, 0.005572, 0.071548>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008503, -0.039703, 0.023819>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010664, -0.038902, 0.049900>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001121, 0.027013, 0.017481>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.009939, -0.041611, 0.038253>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010950, -0.034949, 0.057142>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003561, 0.030298, 0.047624>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001234, 0.028168, 0.019604>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001635, 0.008697, 0.001659>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003713, -0.012255, -0.000269>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008531, 0.004002, 0.071971>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006942, 0.015904, 0.066746>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010674, -0.019189, 0.069973>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001215, 0.024562, 0.013720>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.001234, 0.021349, 0.010003>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004445, -0.016707, 0.000889>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007414, -0.033991, 0.013151>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007987, -0.036432, 0.016720>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009088, -0.040075, 0.024864>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006138, 0.021561, 0.062096>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.001798, 0.030678, 0.025786>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004015, 0.030081, 0.048494>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.001685, 0.012356, 0.003271>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009271, -0.040215, 0.025305>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008016, 0.011416, 0.069374>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011051, -0.017763, 0.070542>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004308, -0.013974, 0.000044>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006981, 0.018163, 0.065168>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.001722, 0.029193, 0.021542>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005482, 0.025498, 0.057414>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009211, -0.039679, 0.023615>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011735, -0.031069, 0.061801>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003511, 0.031512, 0.043322>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004094, -0.011753, -0.000453>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006551, 0.020924, 0.062782>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009922, -0.002553, 0.072954>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002556, 0.032065, 0.032689>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005201, 0.027322, 0.054625>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010898, -0.011399, 0.072355>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009385, 0.003560, 0.072117>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003495, -0.005401, -0.001112>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002298, 0.031339, 0.027992>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.001697, 0.022968, 0.011559>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002254, 0.030987, 0.026503>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005738, 0.025799, 0.057070>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005522, 0.026792, 0.055565>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004467, -0.012462, -0.000372>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010758, -0.007393, 0.072900>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005039, 0.028599, 0.052256>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003219, 0.032286, 0.036993>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007860, -0.032367, 0.011082>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007958, 0.015969, 0.066824>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006717, -0.026073, 0.005478>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003128, 0.032298, 0.034834>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012581, -0.033604, 0.058876>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003843, -0.005622, -0.001176>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004442, -0.010511, -0.000769>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012009, -0.040743, 0.044091>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004376, 0.031027, 0.045920>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011393, -0.041705, 0.036329>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002867, 0.004456, 0.000025>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012327, -0.023212, 0.067908>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004351, -0.009335, -0.000938>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006886, -0.026262, 0.005592>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002414, 0.030151, 0.023546>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004355, -0.009228, -0.000954>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012037, -0.041009, 0.042847>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011958, -0.041344, 0.040849>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003204, 0.002097, -0.000548>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002365, 0.014058, 0.004003>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002359, 0.014740, 0.004407>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012895, -0.034600, 0.057546>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002490, 0.029690, 0.022235>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011210, -0.005892, 0.072988>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009745, -0.038148, 0.019733>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003355, 0.032332, 0.034086>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003968, -0.004489, -0.001237>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005254, -0.014337, -0.000025>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003659, -0.001013, -0.001047>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013111, -0.034052, 0.058265>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002335, 0.020610, 0.008874>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012833, -0.022271, 0.068422>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009961, -0.038022, 0.019448>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011044, -0.001482, 0.072909>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007358, 0.022631, 0.061223>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002449, 0.023385, 0.011729>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002866, 0.011768, 0.002640>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003323, 0.031903, 0.029729>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011868, -0.007637, 0.072877>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002693, 0.027212, 0.016984>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003073, 0.010042, 0.001812>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002658, 0.021574, 0.009689>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008876, -0.032583, 0.011207>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006960, -0.022375, 0.003061>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013627, -0.029718, 0.063000>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006047, -0.016343, 0.000472>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005948, 0.029294, 0.051201>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002903, 0.017147, 0.005843>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006512, 0.027649, 0.054472>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010446, -0.037936, 0.019226>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011991, -0.005266, 0.073008>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003382, 0.008911, 0.001269>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004322, 0.032502, 0.037458>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007186, -0.022531, 0.003117>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012022, -0.004995, 0.073014>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013944, -0.029239, 0.063417>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006140, 0.029299, 0.051262>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004295, 0.032552, 0.036122>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013609, -0.021343, 0.068867>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012434, -0.041591, 0.032712>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013998, -0.027615, 0.064787>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004021, 0.032333, 0.031829>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003953, 0.032194, 0.030748>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003872, 0.005636, 0.000082>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005766, 0.030778, 0.047502>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006681, 0.028190, 0.053578>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004138, 0.032417, 0.032537>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003549, 0.010754, 0.001994>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008693, -0.029013, 0.007581>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003168, 0.021863, 0.009805>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003226, 0.021765, 0.009682>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014185, -0.025900, 0.066067>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014022, -0.022375, 0.068296>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003320, 0.026403, 0.015391>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009356, 0.017399, 0.065996>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011742, -0.039996, 0.024389>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013933, -0.040352, 0.045338>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003835, 0.008938, 0.001169>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003865, 0.008661, 0.001059>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003875, 0.009146, 0.001239>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013328, -0.041652, 0.036808>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010768, -0.036771, 0.016999>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007363, -0.020084, 0.001819>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005605, 0.031795, 0.043902>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004853, -0.001221, -0.001311>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010949, 0.008875, 0.070613>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003747, 0.029857, 0.021819>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004217, 0.005931, 0.000087>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003470, 0.020942, 0.008794>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011884, 0.002175, 0.072455>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010825, 0.009937, 0.070181>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012359, -0.040764, 0.027238>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014652, -0.029843, 0.062787>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004813, -0.000160, -0.001211>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007177, -0.018265, 0.001038>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014338, -0.022537, 0.068183>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007029, -0.017018, 0.000568>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004272, 0.031911, 0.028511>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010272, 0.014075, 0.068163>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003929, 0.011804, 0.002378>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007171, -0.017672, 0.000806>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013633, -0.041602, 0.037580>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012818, -0.004805, 0.073002>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004957, -0.000460, -0.001269>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009149, -0.028728, 0.007279>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004468, 0.032189, 0.029900>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014487, -0.039878, 0.046817>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006091, -0.009537, -0.001226>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014449, -0.040188, 0.045801>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007099, 0.028912, 0.052343>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003757, 0.025868, 0.014423>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013865, -0.041569, 0.038018>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004610, 0.005209, -0.000194>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009844, -0.030786, 0.009154>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004261, 0.030460, 0.023093>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012085, 0.004287, 0.072016>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006293, -0.009220, -0.001303>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009674, -0.029730, 0.008144>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008229, 0.025901, 0.057474>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015241, -0.034035, 0.058046>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004164, 0.028989, 0.019615>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014675, -0.040571, 0.044273>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013641, -0.007810, 0.072812>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015090, -0.038542, 0.050298>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011326, 0.010589, 0.069926>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004097, 0.019296, 0.007150>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012514, 0.002382, 0.072414>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004126, 0.024885, 0.012924>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012725, -0.040117, 0.024805>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015034, -0.039619, 0.047478>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013158, -0.002502, 0.072950>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014323, -0.013501, 0.071789>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011037, 0.013186, 0.068670>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010301, -0.031583, 0.009949>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004337, 0.015613, 0.004394>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013896, -0.008649, 0.072704>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011942, -0.037620, 0.018524>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013553, -0.041193, 0.029632>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009754, -0.028253, 0.006816>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014754, -0.041205, 0.040942>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011088, -0.034112, 0.012923>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011982, 0.008729, 0.070685>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008731, 0.026026, 0.057390>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015305, -0.021238, 0.068785>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005373, 0.004714, -0.000472>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007215, 0.031091, 0.047270>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004590, 0.022796, 0.010317>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015460, -0.021648, 0.068561>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009808, 0.022255, 0.061927>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005759, 0.002308, -0.001049>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009581, -0.025256, 0.004546>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010703, -0.030928, 0.009235>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016079, -0.031874, 0.060541>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006104, 0.032901, 0.036036>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014667, -0.010411, 0.072417>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005942, 0.032863, 0.034136>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012447, -0.037542, 0.018371>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015871, -0.025402, 0.066247>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007188, 0.031644, 0.045474>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010766, 0.018471, 0.065304>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009064, -0.021457, 0.002304>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009445, 0.024820, 0.059064>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008364, 0.028940, 0.052625>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014508, -0.007448, 0.072801>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007426, 0.031520, 0.046025>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009921, -0.025781, 0.004883>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013022, 0.005362, 0.071743>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006252, 0.000584, -0.001388>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013350, 0.003168, 0.072245>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012426, -0.036662, 0.016760>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007119, -0.007013, -0.001680>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005439, 0.030251, 0.021829>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005172, 0.021367, 0.008676>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015683, -0.040995, 0.041815>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010972, 0.019326, 0.064636>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016404, -0.027635, 0.064503>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011893, -0.033921, 0.012642>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012501, 0.010685, 0.069897>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005412, 0.020612, 0.007912>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005565, 0.015259, 0.003870>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014361, -0.040809, 0.027754>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006569, 0.032967, 0.034046>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008482, 0.030220, 0.049989>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009218, -0.018782, 0.001006>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014736, -0.004939, 0.072915>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015759, -0.014869, 0.071292>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009870, -0.022587, 0.002844>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014848, -0.041278, 0.030662>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013649, -0.038907, 0.021419>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006974, -0.000800, -0.001653>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009579, 0.027175, 0.055878>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015776, -0.041436, 0.038025>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007124, -0.001785, -0.001740>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011777, 0.017352, 0.066162>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012620, 0.012491, 0.069061>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006305, 0.031921, 0.026641>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016815, -0.037734, 0.051672>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010514, 0.023872, 0.060291>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006565, 0.005217, -0.000571>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014176, -0.039727, 0.023722>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007851, 0.032418, 0.042691>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007004, 0.001115, -0.001437>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006689, 0.032505, 0.029191>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014954, -0.002824, 0.072885>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016476, -0.018048, 0.070112>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006848, 0.005089, -0.000651>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017153, -0.036854, 0.053291>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013461, -0.036961, 0.017310>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006928, 0.032709, 0.030257>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007993, 0.032639, 0.041636>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015170, -0.040994, 0.028940>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012574, -0.033375, 0.011941>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017051, -0.038728, 0.049333>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014779, -0.040290, 0.025687>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006556, 0.031018, 0.023320>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008625, 0.031652, 0.046104>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017079, -0.038844, 0.049023>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007621, -0.000720, -0.001744>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012031, 0.018999, 0.064945>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013442, 0.010679, 0.069890>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009432, 0.029897, 0.050947>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014846, 0.000546, 0.072609>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015781, -0.041400, 0.032467>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012422, -0.031871, 0.010164>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016167, -0.011155, 0.072158>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015986, -0.009307, 0.072478>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016662, -0.016537, 0.070665>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017202, -0.023556, 0.067263>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011941, 0.020139, 0.064012>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013593, 0.010757, 0.069854>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014335, 0.005460, 0.071677>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017609, -0.029354, 0.062829>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016408, -0.041449, 0.036476>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016567, -0.041380, 0.037725>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017743, -0.032736, 0.059236>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007877, 0.033189, 0.035066>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006760, 0.022258, 0.009127>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017651, -0.037543, 0.051826>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017831, -0.031951, 0.060139>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017450, -0.039462, 0.047154>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006940, 0.028045, 0.016622>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011528, -0.025203, 0.004374>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017879, -0.033900, 0.057730>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010091, -0.015909, -0.000178>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007004, 0.024819, 0.011918>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009357, 0.031771, 0.045981>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017573, -0.039671, 0.046456>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017432, -0.019970, 0.069164>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007083, 0.024423, 0.011415>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007162, 0.025818, 0.013178>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013614, -0.034223, 0.013044>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007312, 0.015376, 0.003575>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017644, -0.039873, 0.045755>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015314, -0.039574, 0.023456>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015107, 0.003830, 0.072022>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010982, -0.019953, 0.001392>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018081, -0.029023, 0.063047>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007828, 0.032223, 0.026840>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017620, -0.020488, 0.068884>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007922, 0.032546, 0.028309>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007519, 0.029979, 0.020221>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018096, -0.037006, 0.052747>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018069, -0.026614, 0.065039>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013130, -0.031172, 0.009409>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012382, -0.027301, 0.005897>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007528, 0.017158, 0.004699>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016911, -0.041410, 0.034972>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012463, 0.021888, 0.062455>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010492, -0.014970, -0.000509>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008474, 0.003118, -0.001334>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014130, -0.034736, 0.013770>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013486, 0.017066, 0.066372>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012236, -0.025297, 0.004409>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007904, 0.014137, 0.002751>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007748, 0.023560, 0.010264>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010034, -0.009943, -0.001664>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009292, -0.003707, -0.002084>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017253, -0.011753, 0.071919>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018511, -0.029062, 0.062926>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010212, 0.031641, 0.046670>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013104, 0.020288, 0.063909>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007955, 0.021887, 0.008485>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009799, -0.006459, -0.002049>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016485, -0.040692, 0.027906>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010004, 0.032414, 0.043831>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011273, 0.028907, 0.053207>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010220, 0.032054, 0.045292>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011073, -0.015654, -0.000336>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008465, 0.031612, 0.024191>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008718, 0.032670, 0.028359>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012398, -0.023898, 0.003495>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014394, -0.033935, 0.012689>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010835, 0.030906, 0.048888>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008715, 0.031929, 0.025160>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010038, 0.033025, 0.040725>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008979, 0.032286, 0.026409>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016877, -0.040693, 0.028095>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015062, 0.011361, 0.069535>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009931, 0.033310, 0.038223>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019068, -0.030671, 0.061245>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008648, 0.017912, 0.005021>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018964, -0.027345, 0.064297>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009900, -0.001288, -0.002063>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009210, 0.032447, 0.026972>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010684, -0.008295, -0.001944>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012254, -0.019173, 0.000965>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014857, 0.014474, 0.067957>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010333, 0.033251, 0.039229>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010044, -0.000426, -0.002014>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013915, -0.028663, 0.007013>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010899, 0.032426, 0.044104>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019096, -0.024586, 0.066296>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011907, 0.029941, 0.051266>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015473, 0.011687, 0.069366>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009196, 0.026005, 0.012919>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016293, -0.037936, 0.019520>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019537, -0.032021, 0.059666>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019059, -0.021280, 0.068255>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012771, 0.027760, 0.055292>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009395, 0.018338, 0.005208>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015485, -0.034419, 0.013417>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009438, 0.025458, 0.012140>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017823, -0.005738, 0.072607>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014568, 0.019633, 0.064427>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018961, -0.040777, 0.040705>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014298, 0.021623, 0.062712>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016597, 0.006907, 0.071110>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013261, 0.027311, 0.055999>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010090, 0.030925, 0.021585>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016362, -0.036589, 0.016928>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019100, -0.040797, 0.040411>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018007, -0.040740, 0.029024>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011006, 0.033537, 0.035496>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013145, -0.018520, 0.000655>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011995, -0.009446, -0.001883>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012056, -0.009552, -0.001870>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011455, -0.004095, -0.002279>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012448, 0.031139, 0.048534>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017416, -0.039321, 0.023247>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011765, -0.006687, -0.002189>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017530, 0.001458, 0.072262>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010744, 0.032898, 0.028324>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017977, -0.002171, 0.072572>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019430, -0.040627, 0.041222>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010322, 0.022633, 0.008788>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010376, 0.026683, 0.013615>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010674, 0.009930, 0.000381>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011521, 0.033545, 0.036717>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013705, 0.027792, 0.055273>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019744, -0.040081, 0.043732>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015040, -0.027862, 0.006362>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020031, -0.024266, 0.066315>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018420, -0.004759, 0.072568>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019490, -0.040819, 0.039766>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014635, 0.023964, 0.060317>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014360, 0.025701, 0.058236>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010749, 0.024000, 0.010169>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011309, 0.006761, -0.000765>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020274, -0.024399, 0.066179>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016243, 0.015830, 0.067069>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015487, 0.020710, 0.063492>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011079, 0.013132, 0.001780>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011159, 0.022235, 0.008270>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019590, -0.011866, 0.071571>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011339, 0.023157, 0.009187>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018432, 0.000961, 0.072224>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020503, -0.022921, 0.067063>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011450, 0.026159, 0.012712>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018355, 0.002058, 0.072076>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012082, 0.003980, -0.001519>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011542, 0.015410, 0.002985>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016332, -0.030633, 0.008991>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013133, 0.032864, 0.042762>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011948, 0.007409, -0.000614>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012058, 0.032716, 0.026902>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015208, 0.025377, 0.058625>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019218, -0.004277, 0.072461>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019162, -0.040376, 0.027902>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011896, 0.024290, 0.010337>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012002, 0.021445, 0.007427>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015361, -0.021955, 0.002336>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014553, -0.015763, -0.000411>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020625, -0.040315, 0.041843>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015320, -0.021117, 0.001891>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015778, 0.024904, 0.059185>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017066, 0.017020, 0.066221>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013502, -0.004704, -0.002369>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019466, -0.040365, 0.028066>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018081, 0.009634, 0.070016>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017159, 0.017551, 0.065852>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016221, -0.025286, 0.004450>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021498, -0.032401, 0.058646>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019313, 0.000473, 0.072176>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013795, -0.003338, -0.002389>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013158, 0.032812, 0.027023>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014314, -0.008013, -0.002144>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013156, 0.007202, -0.000755>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021695, -0.033250, 0.057489>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020756, -0.040794, 0.037681>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017275, 0.019696, 0.064208>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018855, -0.036632, 0.017499>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018551, 0.011250, 0.069282>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018236, -0.033685, 0.012779>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014129, 0.000198, -0.002207>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019867, -0.000242, 0.072178>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022050, -0.031566, 0.059452>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022056, -0.029355, 0.061768>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020530, -0.006177, 0.072195>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014191, 0.033451, 0.030542>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020691, -0.040586, 0.030629>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018381, -0.032693, 0.011538>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022245, -0.031657, 0.059287>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022235, -0.029876, 0.061194>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014254, 0.032339, 0.024923>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013989, 0.015868, 0.003085>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016849, -0.021590, 0.002203>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017556, 0.022547, 0.061615>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014885, 0.033725, 0.034393>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021623, -0.013361, 0.070846>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021574, -0.040718, 0.035759>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016241, -0.012075, -0.001431>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021832, -0.015363, 0.070209>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021460, -0.009694, 0.071648>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022116, -0.040111, 0.041314>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021301, -0.040431, 0.030281>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022839, -0.034310, 0.055579>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016191, 0.032851, 0.042932>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016966, -0.015688, -0.000376>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015209, 0.016743, 0.003603>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019155, -0.031714, 0.010506>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015434, 0.029457, 0.017566>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022873, -0.038048, 0.048318>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017946, 0.027118, 0.056061>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022889, -0.023332, 0.066228>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022994, -0.037942, 0.048515>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022953, -0.024538, 0.065422>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022112, -0.011175, 0.071245>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016161, 0.005487, -0.001299>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021433, -0.001157, 0.071994>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023164, -0.037825, 0.048701>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019677, -0.031213, 0.010039>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017013, 0.033496, 0.038884>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021793, -0.003288, 0.072028>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021191, -0.037687, 0.020409>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021060, -0.037096, 0.019074>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016783, 0.006980, -0.000887>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017001, 0.008556, -0.000382>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019722, -0.026633, 0.005806>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016957, 0.021766, 0.007494>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021536, 0.004931, 0.071003>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022840, -0.040434, 0.033893>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017870, -0.004969, -0.002317>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022267, -0.039452, 0.026221>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018762, 0.030215, 0.050455>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023711, -0.022012, 0.066797>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023055, -0.011485, 0.070970>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022005, -0.038499, 0.022864>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023954, -0.026540, 0.063639>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017669, 0.028332, 0.015572>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017952, 0.033137, 0.028497>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023879, -0.039355, 0.043090>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022274, 0.002044, 0.071456>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017825, 0.011937, 0.000982>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021828, -0.036420, 0.018040>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024293, -0.036695, 0.050608>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019392, -0.014302, -0.000643>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020132, -0.021399, 0.002405>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024320, -0.023653, 0.065603>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023212, -0.039838, 0.028965>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020668, 0.023638, 0.060026>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021344, 0.017813, 0.065088>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021815, -0.033662, 0.013552>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024535, -0.037843, 0.047706>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021385, -0.029687, 0.008793>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020614, -0.020514, 0.002008>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024570, -0.038916, 0.044198>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024172, -0.014066, 0.070054>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021371, 0.021651, 0.061917>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023001, -0.037884, 0.021766>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022696, 0.007046, 0.070191>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023235, 0.004265, 0.070818>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020410, 0.032716, 0.042403>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019597, 0.013542, 0.001860>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020594, 0.032180, 0.044564>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020576, -0.006265, -0.002048>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021761, -0.022360, 0.003191>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019872, 0.013386, 0.001798>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020094, 0.004893, -0.001259>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022992, -0.033300, 0.013419>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020865, 0.033156, 0.039593>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022465, -0.026978, 0.006576>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025668, -0.027525, 0.062213>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022779, 0.019484, 0.063512>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020605, 0.007490, -0.000521>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023237, 0.016209, 0.065790>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021667, 0.031756, 0.045608>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025973, -0.036023, 0.050941>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020983, 0.030102, 0.019321>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025710, -0.020664, 0.066924>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022069, -0.014930, -0.000133>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021027, 0.026799, 0.013570>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021183, 0.031797, 0.023792>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025190, -0.009461, 0.070824>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023077, 0.022283, 0.060920>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023934, -0.033559, 0.014102>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021852, 0.032795, 0.041169>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026286, -0.030183, 0.059351>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021923, 0.033284, 0.032240>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026171, -0.039013, 0.041715>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025309, -0.004382, 0.071233>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025754, -0.010902, 0.070404>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021935, 0.031957, 0.024595>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021763, 0.023999, 0.010151>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024345, 0.015249, 0.066133>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023694, -0.023592, 0.004347>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021973, 0.019752, 0.006146>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023279, -0.016488, 0.000614>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022239, 0.026786, 0.013764>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023829, 0.028149, 0.053078>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027253, -0.030111, 0.058984>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024664, 0.023186, 0.059535>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025329, -0.031708, 0.012145>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027489, -0.034793, 0.052311>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023743, -0.006184, -0.001640>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026832, -0.039321, 0.032317>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026272, 0.002172, 0.070491>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026378, -0.037521, 0.023094>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025311, 0.021562, 0.061012>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023764, 0.026546, 0.013760>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023935, 0.032086, 0.026112>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023906, 0.002062, -0.001335>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023967, 0.028815, 0.017500>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024452, 0.032815, 0.038300>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025584, -0.025046, 0.005825>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024987, -0.013556, -0.000048>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027288, -0.006293, 0.070587>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025275, 0.029737, 0.049331>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024702, 0.032712, 0.030353>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024557, 0.029865, 0.019866>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028152, -0.018221, 0.067286>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027632, -0.004059, 0.070574>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027818, -0.003418, 0.070516>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028836, -0.030391, 0.057859>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027723, 0.001615, 0.070150>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026510, 0.025203, 0.056512>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027204, -0.031902, 0.013161>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026241, 0.028634, 0.051182>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025393, 0.005500, -0.000404>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029048, -0.031055, 0.056945>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028695, -0.038745, 0.037636>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025411, 0.021616, 0.008412>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027078, -0.027088, 0.007955>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026470, 0.031036, 0.045168>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026082, 0.032474, 0.030465>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026614, 0.030898, 0.045500>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026848, 0.029881, 0.048131>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028786, -0.037952, 0.027219>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027239, 0.027553, 0.052670>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029854, -0.037346, 0.043668>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029330, -0.038061, 0.028681>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029562, -0.038508, 0.033432>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028768, -0.033182, 0.015796>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029447, -0.000408, 0.069837>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027562, 0.029190, 0.019754>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030106, -0.038073, 0.030571>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028477, -0.016532, 0.001854>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027884, 0.000292, -0.000762>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030469, -0.037888, 0.030178>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028224, 0.019472, 0.007284>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031158, -0.033752, 0.051427>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028754, 0.031515, 0.028046>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028736, 0.029811, 0.021957>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031254, -0.016764, 0.066670>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031606, -0.037012, 0.042092>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030599, 0.014282, 0.064673>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031408, -0.010340, 0.068570>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031898, -0.031891, 0.053911>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029472, 0.000949, -0.000279>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029548, 0.008107, 0.001342>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029678, 0.025619, 0.014511>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032000, -0.021297, 0.064016>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031265, 0.007084, 0.067647>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030097, 0.031716, 0.035662>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032274, -0.032602, 0.052480>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029858, 0.002621, 0.000064>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030437, 0.030208, 0.044309>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030275, 0.022772, 0.011187>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032214, -0.037302, 0.031000>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031570, 0.012468, 0.065289>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031348, 0.020169, 0.059948>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030585, -0.004205, -0.000159>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031073, 0.027477, 0.050540>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032707, -0.027773, 0.058450>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030583, 0.028566, 0.020234>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032667, -0.036824, 0.040701>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030834, -0.000188, 0.000011>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030854, 0.000451, 0.000073>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032259, 0.001414, 0.068550>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033104, -0.036809, 0.039563>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031926, -0.022447, 0.006254>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033434, -0.023655, 0.061635>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032727, 0.005301, 0.067571>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031852, 0.009614, 0.002615>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033582, -0.036907, 0.033703>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032882, 0.016365, 0.062313>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032946, -0.028085, 0.011612>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033947, -0.026667, 0.058735>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032473, 0.009396, 0.002753>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034051, -0.028580, 0.012860>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033640, 0.029947, 0.040737>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034193, 0.009811, 0.065360>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034845, -0.018655, 0.064007>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035032, -0.036194, 0.036507>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034731, 0.022258, 0.055850>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034688, 0.028403, 0.044956>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035662, -0.016504, 0.064578>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035588, -0.030115, 0.015999>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.036095, 0.010396, 0.064123>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.036235, -0.005053, 0.002033>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.036686, 0.020632, 0.056326>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037769, -0.015056, 0.005137>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038064, 0.019838, 0.012439>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038246, -0.027792, 0.015146>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038872, -0.014788, 0.005666>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.039260, -0.005013, 0.065287>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.039597, -0.026724, 0.015064>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041302, 0.001783, 0.005034>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041199, 0.025459, 0.043159>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042088, -0.024377, 0.014802>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041972, -0.028403, 0.048697>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042041, -0.004382, 0.063463>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.043572, 0.021955, 0.020895>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042279, -0.010523, 0.062332>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042602, -0.012224, 0.061578>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042791, -0.022197, 0.055537>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.043077, -0.030657, 0.040364>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.044877, 0.001423, 0.007526>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045660, 0.003335, 0.008534>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.043899, -0.025095, 0.051051>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.043992, -0.008981, 0.061354>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.046817, 0.015257, 0.016220>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045239, 0.011494, 0.056776>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045898, -0.017742, 0.012876>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045340, -0.028139, 0.026285>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045684, -0.002443, 0.060573>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045719, -0.019238, 0.054825>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047881, 0.018976, 0.045084>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047559, 0.001202, 0.058462>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.049558, 0.015400, 0.047489>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.051367, -0.005775, 0.014149>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.050616, 0.017065, 0.041979>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052418, 0.014949, 0.027274>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.048124, -0.011412, 0.056610>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.050797, -0.014222, 0.016715>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052146, -0.005697, 0.015235>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.049440, -0.025002, 0.030993>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.049822, -0.022761, 0.043506>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.053044, -0.018559, 0.026563>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052487, -0.020868, 0.030847>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.051301, -0.019809, 0.044934>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057897, -0.001076, 0.030343>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055240, -0.015262, 0.038606>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056832, -0.006433, 0.042275>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
}

