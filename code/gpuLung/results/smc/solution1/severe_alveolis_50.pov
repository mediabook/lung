global_settings {
  max_trace_level 10
  assumed_gamma 1
}

camera {
  location    <-1.300000, -1.300000, 0.000000>
  look_at     <0.000000, 0.000000, 0>
}

light_source {
  <-5.000000, 0.500000, -2.000000>*500
  color rgb <2.0, 1.5, 1.0>*1.8
  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient
}

light_source {
  <5.000000, 0.500000, 2.000000>*500
  color rgb <2.0, 1.5, 1.0>*1.8
  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient
}

blob {
  threshold 0.1
  sphere {
    <0.000000, 0.000000, 0.000000>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.008503, 0.230432, 0.146580>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.008582, 0.177978, 0.084859>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.036442, -0.342371, 0.179350>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.011184, -0.127824, 0.006446>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000075, 0.285334, 0.311876>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.008381, 0.187189, 0.093567>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.051108, -0.232576, 0.600682>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.005383, 0.283115, 0.373374>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.021332, 0.221608, 0.525673>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.030526, -0.285578, 0.097980>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.054165, -0.262453, 0.577614>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.031879, 0.147193, 0.598653>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.032981, -0.297102, 0.110550>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.052356, -0.371291, 0.387859>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.006422, 0.162986, 0.071222>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.026187, -0.243359, 0.060602>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002998, -0.003477, -0.001094>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.015605, -0.143632, 0.010318>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.025103, -0.225271, 0.047966>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.058577, -0.271709, 0.569310>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.019983, -0.176362, 0.022065>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.033568, 0.155762, 0.592727>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.024920, 0.216516, 0.532989>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001555, 0.283927, 0.288246>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.048692, -0.366009, 0.240796>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.054896, -0.084994, 0.656954>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.059775, -0.351040, 0.455368>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.030757, 0.185717, 0.567311>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.012736, -0.094732, -0.000969>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.020894, -0.170556, 0.019408>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.059775, -0.161712, 0.637912>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.011846, 0.279241, 0.400155>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.022826, 0.239399, 0.500308>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.013029, 0.276937, 0.410169>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.050958, -0.364140, 0.233629>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.062215, -0.364814, 0.415919>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.043498, 0.102781, 0.625730>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.063011, -0.365188, 0.414556>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.003273, 0.197640, 0.102514>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.051135, 0.032271, 0.650952>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.061158, -0.376185, 0.352158>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.068078, -0.305763, 0.532031>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.067196, -0.354344, 0.447052>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.063506, -0.375099, 0.363351>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.020904, 0.261397, 0.458166>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.059041, -0.037628, 0.659643>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.047588, -0.327630, 0.150893>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.027159, -0.181396, 0.023217>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.034543, 0.202350, 0.551158>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.042113, 0.153572, 0.595293>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000913, 0.208230, 0.113615>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001375, 0.125479, 0.042851>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.072097, -0.206779, 0.616661>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002427, 0.264196, 0.206631>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000018, 0.217353, 0.124560>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.005636, 0.280649, 0.261040>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.074396, -0.353571, 0.448478>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.071197, -0.120614, 0.650444>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002993, 0.125954, 0.042703>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.004643, 0.270249, 0.222122>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.029602, -0.172270, 0.018811>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.058647, -0.350951, 0.195442>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003597, 0.255800, 0.186184>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.031512, -0.183066, 0.023380>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.079491, -0.296757, 0.542349>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.047561, 0.150876, 0.597709>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.032322, 0.239682, 0.502234>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.067109, -0.371560, 0.264003>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.064604, 0.016465, 0.654496>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.033880, -0.189530, 0.026181>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003669, 0.230150, 0.140817>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.073699, -0.084519, 0.656959>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.068733, -0.019087, 0.658884>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.062580, -0.351940, 0.197633>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.038003, 0.222655, 0.527704>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.036727, 0.228997, 0.518958>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.020561, -0.063324, -0.005929>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.004870, 0.178853, 0.081259>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.083065, -0.354753, 0.444307>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.018063, 0.288043, 0.352247>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.086153, -0.261904, 0.576945>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.045169, 0.197009, 0.558204>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.086295, -0.346124, 0.464349>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.052815, -0.286685, 0.096547>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.052907, -0.286200, 0.096022>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.072825, -0.368188, 0.248310>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.067780, 0.052211, 0.646115>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.070375, -0.358371, 0.214503>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.088531, -0.351930, 0.450506>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008110, 0.242742, 0.159023>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.082454, -0.376153, 0.345172>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.091012, -0.316126, 0.516072>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.029270, 0.273570, 0.430251>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.009055, 0.253166, 0.177950>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.012548, 0.078273, 0.017198>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.030086, -0.110214, -0.000872>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.070962, 0.038152, 0.649893>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.057427, 0.145689, 0.601931>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.089078, -0.172926, 0.632330>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008785, 0.220677, 0.125649>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008930, 0.191947, 0.092536>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.036056, -0.150011, 0.009388>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060852, -0.305760, 0.118365>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065715, -0.328110, 0.150491>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075029, -0.361487, 0.224093>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.050597, 0.195964, 0.559830>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013749, 0.275685, 0.233325>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032878, 0.271350, 0.438027>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012756, 0.111223, 0.032100>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076481, -0.362698, 0.228136>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066224, 0.104964, 0.625705>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.092018, -0.160118, 0.637223>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034804, -0.125582, 0.002141>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057482, 0.165582, 0.587420>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012928, 0.262082, 0.195404>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045046, 0.230825, 0.517797>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075869, -0.357718, 0.212891>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097403, -0.280650, 0.557990>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028471, 0.283992, 0.391487>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033001, -0.105868, -0.002159>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.053851, 0.190104, 0.565870>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082396, -0.021192, 0.658675>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020093, 0.288377, 0.295536>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042601, 0.246722, 0.492839>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090579, -0.102158, 0.653408>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077646, 0.033973, 0.650783>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027952, -0.049687, -0.007726>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017724, 0.281502, 0.253229>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012468, 0.206181, 0.106681>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017276, 0.278192, 0.239879>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.046937, 0.233228, 0.514663>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045131, 0.241924, 0.501208>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035901, -0.111978, -0.001259>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089240, -0.065150, 0.658150>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041089, 0.257993, 0.471634>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025560, 0.290346, 0.334446>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063994, -0.290653, 0.100240>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065209, 0.145784, 0.602160>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.054374, -0.233787, 0.050395>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024652, 0.290260, 0.315321>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.103595, -0.303322, 0.531262>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030629, -0.051314, -0.008067>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035564, -0.094462, -0.004550>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098740, -0.367493, 0.397870>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035338, 0.279204, 0.414841>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.093582, -0.376044, 0.327869>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022402, 0.039875, 0.003042>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.102002, -0.209217, 0.612922>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034780, -0.083984, -0.005987>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055672, -0.235473, 0.051497>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018323, 0.270256, 0.213513>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034806, -0.083065, -0.006109>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098894, -0.369800, 0.386641>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098200, -0.372760, 0.368637>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025164, 0.018286, -0.002093>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017901, 0.126242, 0.039276>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017818, 0.132282, 0.042928>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.106082, -0.312103, 0.519156>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018834, 0.265969, 0.201848>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.092614, -0.052496, 0.658565>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079529, -0.343109, 0.178192>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026322, 0.290274, 0.308293>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031499, -0.038853, -0.008351>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042063, -0.128897, 0.002177>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028888, -0.009858, -0.006536>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.107945, -0.307087, 0.525564>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017392, 0.184718, 0.083118>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.106062, -0.200516, 0.617283>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081134, -0.341840, 0.175704>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090938, -0.012318, 0.657561>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.059738, 0.204692, 0.551465>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018226, 0.209386, 0.108632>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021887, 0.105407, 0.027213>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025687, 0.285983, 0.269066>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097825, -0.067545, 0.657334>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020167, 0.243377, 0.155344>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023593, 0.090028, 0.019794>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019841, 0.193087, 0.090586>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.071744, -0.292278, 0.101888>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055762, -0.200381, 0.029629>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111991, -0.267837, 0.568127>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.048260, -0.146531, 0.006901>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047836, 0.263287, 0.462003>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021859, 0.153572, 0.056298>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052483, 0.248678, 0.491157>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.084773, -0.340813, 0.173921>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098547, -0.046214, 0.658249>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026037, 0.079519, 0.015045>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034000, 0.291436, 0.338651>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057488, -0.201756, 0.030263>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098743, -0.042333, 0.658265>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114338, -0.263351, 0.571784>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.049296, 0.263192, 0.462516>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033651, 0.291714, 0.326703>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111774, -0.192038, 0.620936>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.101212, -0.374148, 0.295492>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114782, -0.248684, 0.584092>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031156, 0.289462, 0.288072>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030547, 0.288171, 0.278306>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030019, 0.049925, 0.004451>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.046033, 0.276108, 0.428916>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.053633, 0.253274, 0.483132>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032106, 0.290227, 0.294454>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027174, 0.096135, 0.021833>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069730, -0.259810, 0.069921>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023708, 0.195391, 0.091917>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024142, 0.194454, 0.090858>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.116269, -0.233073, 0.595537>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114944, -0.201209, 0.615626>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024866, 0.235731, 0.141534>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075588, 0.157628, 0.593952>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095176, -0.359412, 0.220616>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113402, -0.363072, 0.408969>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029479, 0.079598, 0.014481>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029736, 0.077104, 0.013481>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029772, 0.081450, 0.015158>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.108198, -0.374576, 0.332290>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.086867, -0.329960, 0.154169>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.058542, -0.179866, 0.019229>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.044388, 0.284916, 0.396662>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037999, -0.011799, -0.007982>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088886, 0.081063, 0.635774>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028324, 0.266460, 0.198638>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032614, 0.052460, 0.004774>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026006, 0.186974, 0.083096>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096878, 0.020449, 0.652658>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087774, 0.090850, 0.631818>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100095, -0.366269, 0.246317>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119737, -0.268657, 0.565853>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037614, -0.002580, -0.007008>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056945, -0.163686, 0.012450>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117381, -0.202593, 0.614472>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055709, -0.152532, 0.008357>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032745, 0.285141, 0.258386>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082987, 0.127795, 0.613412>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029977, 0.105253, 0.025630>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056843, -0.158538, 0.010472>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110628, -0.374050, 0.339267>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104746, -0.040011, 0.657711>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038732, -0.005301, -0.007451>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073094, -0.257084, 0.067543>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034336, 0.287621, 0.270843>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117643, -0.358677, 0.422215>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047999, -0.085752, -0.007235>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117297, -0.361401, 0.413100>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056593, 0.259280, 0.472011>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028125, 0.230713, 0.133177>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.112316, -0.373638, 0.343273>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035639, 0.045898, 0.002489>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078646, -0.275566, 0.084335>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032257, 0.271549, 0.210132>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098077, 0.039685, 0.648380>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.049488, -0.083037, -0.007737>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077182, -0.266094, 0.075401>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065681, 0.232573, 0.517554>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.123961, -0.306114, 0.523054>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031342, 0.258376, 0.179270>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.118871, -0.364669, 0.399379>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111243, -0.069340, 0.655820>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.122378, -0.346513, 0.453446>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091421, 0.096372, 0.629214>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030785, 0.172082, 0.068806>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.101566, 0.022155, 0.651959>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030870, 0.221617, 0.120112>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.102573, -0.360070, 0.224606>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.121773, -0.356147, 0.428116>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.107091, -0.021457, 0.656893>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.116844, -0.120843, 0.646627>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088838, 0.119713, 0.617755>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082207, -0.282642, 0.091544>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032783, 0.139119, 0.044215>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113200, -0.077535, 0.654787>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095875, -0.337248, 0.168227>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.109293, -0.369803, 0.268051>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077510, -0.252658, 0.063851>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119211, -0.370143, 0.369527>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088600, -0.305423, 0.118152>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096640, 0.079381, 0.635969>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069371, 0.233341, 0.516681>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.124643, -0.190820, 0.619458>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041429, 0.041360, 0.000521>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056779, 0.277793, 0.426717>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034345, 0.202907, 0.097270>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125781, -0.194416, 0.617402>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078097, 0.200032, 0.556990>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.044525, 0.019504, -0.004595>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075718, -0.225725, 0.044037>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085036, -0.276605, 0.085500>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130330, -0.286395, 0.545258>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047143, 0.293379, 0.326121>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119198, -0.092888, 0.651935>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045744, 0.292947, 0.309213>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.099599, -0.336343, 0.167060>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.128914, -0.228161, 0.596517>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056339, 0.282543, 0.410693>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085875, 0.166384, 0.587132>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.071360, -0.191885, 0.024436>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074906, 0.222429, 0.531447>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065967, 0.258694, 0.474323>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117675, -0.065949, 0.655279>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.058170, 0.281310, 0.415587>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078315, -0.230375, 0.047160>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104920, 0.048846, 0.645393>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.048355, 0.004184, -0.007431>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.107709, 0.029134, 0.649968>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.099174, -0.328324, 0.152735>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055453, -0.063104, -0.010221>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.040916, 0.268739, 0.199346>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038702, 0.189760, 0.083032>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.126168, -0.367885, 0.377370>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087233, 0.173900, 0.581014>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.132868, -0.248223, 0.580710>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094499, -0.303424, 0.116038>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100136, 0.096764, 0.628464>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.040513, 0.182968, 0.076376>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041979, 0.135351, 0.040307>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.115084, -0.365852, 0.251400>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.050353, 0.293405, 0.308499>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066455, 0.269491, 0.450847>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072147, -0.167985, 0.013456>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119117, -0.042416, 0.656028>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127676, -0.133425, 0.641559>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077452, -0.201788, 0.029552>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.118955, -0.370080, 0.277530>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.108946, -0.348515, 0.194650>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.053838, -0.008656, -0.009421>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075359, 0.242657, 0.503038>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.126515, -0.371604, 0.343470>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055025, -0.016511, -0.010168>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.093529, 0.156064, 0.594575>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100677, 0.112744, 0.620717>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047656, 0.283505, 0.242288>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.135293, -0.338605, 0.465567>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082945, 0.213592, 0.542155>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.050259, 0.045378, 0.000525>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113196, -0.355915, 0.215353>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060832, 0.288627, 0.385804>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.053882, 0.009178, -0.007269>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.050710, 0.288688, 0.265078>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.120479, -0.025940, 0.655520>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.133210, -0.161946, 0.630809>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052361, 0.044178, -0.000006>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.137891, -0.330701, 0.480030>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.106891, -0.330698, 0.158031>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052575, 0.290420, 0.274653>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.061729, 0.290349, 0.376416>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.121080, -0.367273, 0.262207>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.099354, -0.298341, 0.110194>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.136848, -0.347397, 0.444604>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117774, -0.360880, 0.233063>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.049221, 0.274903, 0.212926>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066944, 0.281655, 0.416261>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.137031, -0.348443, 0.441820>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.058609, -0.008080, -0.009765>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095059, 0.170441, 0.583480>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.107033, 0.096377, 0.628039>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073518, 0.266143, 0.459282>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119123, 0.005469, 0.652824>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.126000, -0.370927, 0.293762>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097897, -0.284644, 0.094506>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130408, -0.099878, 0.648993>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.128862, -0.083165, 0.651850>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.134485, -0.148165, 0.635643>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.138774, -0.211400, 0.605143>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094149, 0.180463, 0.575102>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.108163, 0.096998, 0.627650>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114725, 0.049325, 0.644231>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141758, -0.263360, 0.565371>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.131000, -0.371404, 0.329689>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.132303, -0.370784, 0.340840>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.142603, -0.293685, 0.533186>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060037, 0.294518, 0.317301>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.050338, 0.196779, 0.087888>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141449, -0.336629, 0.466873>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.143299, -0.286584, 0.541245>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.139616, -0.353821, 0.425107>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.051656, 0.247901, 0.153999>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089994, -0.224922, 0.043661>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.143519, -0.304070, 0.519665>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078187, -0.142390, 0.003951>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052045, 0.219260, 0.112634>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072235, 0.282208, 0.415146>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.140455, -0.355602, 0.418864>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.140370, -0.179033, 0.622012>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052625, 0.215715, 0.108246>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.053193, 0.228090, 0.123803>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.107178, -0.305689, 0.120359>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.054831, 0.135588, 0.038746>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.140911, -0.357337, 0.412599>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.121380, -0.354142, 0.213314>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.120533, 0.034889, 0.647191>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085201, -0.178261, 0.017772>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.145236, -0.260250, 0.567204>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.058779, 0.285166, 0.244487>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141764, -0.183764, 0.619461>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.059610, 0.288105, 0.257538>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056019, 0.264867, 0.185898>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.144781, -0.331733, 0.475060>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.145186, -0.238713, 0.585010>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.102887, -0.278182, 0.088231>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096605, -0.243633, 0.057282>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056253, 0.151367, 0.048940>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.134516, -0.370780, 0.316598>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097549, 0.195395, 0.561008>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081003, -0.134211, 0.001399>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.064506, 0.026043, -0.005182>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111100, -0.310185, 0.126978>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.106063, 0.152830, 0.595951>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095165, -0.225651, 0.044347>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.059257, 0.124414, 0.031688>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057478, 0.207755, 0.098400>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077125, -0.089485, -0.008444>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.071119, -0.033487, -0.011953>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.138458, -0.105438, 0.646462>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.148393, -0.260535, 0.566010>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078514, 0.280570, 0.421243>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.102562, 0.181189, 0.573876>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.059044, 0.192866, 0.082809>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075072, -0.058485, -0.011608>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130569, -0.364020, 0.253261>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076596, 0.287245, 0.396041>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087171, 0.256622, 0.479110>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078373, 0.284088, 0.409023>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085293, -0.140113, 0.003181>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063148, 0.279028, 0.221222>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065361, 0.288667, 0.258187>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096105, -0.213307, 0.036542>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.112819, -0.302883, 0.117566>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.083332, 0.273884, 0.440903>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065037, 0.281789, 0.229851>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076401, 0.292263, 0.368427>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067049, 0.284931, 0.240978>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.133438, -0.363916, 0.255031>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.118847, 0.101733, 0.624260>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075295, 0.294570, 0.346118>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152412, -0.274866, 0.550884>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.064327, 0.157434, 0.052429>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151746, -0.245118, 0.578154>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075302, -0.013097, -0.011147>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.068768, 0.286268, 0.246028>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081683, -0.075553, -0.010342>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094307, -0.171207, 0.014843>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.116589, 0.129385, 0.609952>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078324, 0.293897, 0.355140>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076273, -0.006003, -0.010551>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.107992, -0.255502, 0.067725>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.083062, 0.286809, 0.398482>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152702, -0.220321, 0.595946>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091388, 0.265062, 0.461882>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.121600, 0.104451, 0.622607>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067875, 0.228585, 0.122385>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127874, -0.338767, 0.178635>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.155771, -0.286789, 0.536701>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152354, -0.190740, 0.613391>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098275, 0.245895, 0.497367>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069704, 0.160832, 0.054483>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.120766, -0.307029, 0.124417>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069621, 0.223568, 0.115657>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141909, -0.051791, 0.652105>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113254, 0.174906, 0.578197>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.150027, -0.364741, 0.367589>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110845, 0.192202, 0.562926>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130760, 0.061811, 0.638273>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.101913, 0.241813, 0.503549>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074607, 0.271736, 0.198752>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127870, -0.326575, 0.155708>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151008, -0.364869, 0.364978>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141744, -0.364074, 0.263534>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082641, 0.295624, 0.321954>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100638, -0.165420, 0.012674>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091259, -0.085244, -0.009153>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091710, -0.086151, -0.009019>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.086814, -0.036401, -0.012375>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094820, 0.274963, 0.437722>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.136596, -0.351248, 0.212046>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089320, -0.060645, -0.011679>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.138599, 0.012946, 0.648684>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079905, 0.289425, 0.258369>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.142464, -0.018683, 0.651530>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.153482, -0.363283, 0.372216>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076059, 0.198302, 0.086572>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076330, 0.233952, 0.128965>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079760, 0.086022, 0.011915>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.086477, 0.295499, 0.332957>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104936, 0.245686, 0.497105>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.156016, -0.358470, 0.394571>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.115917, -0.248217, 0.062593>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.159534, -0.217377, 0.595888>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.146079, -0.041056, 0.651493>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.153767, -0.364926, 0.359255>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.112640, 0.212322, 0.541575>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110190, 0.227487, 0.523188>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079043, 0.210090, 0.098882>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.084673, 0.057753, 0.001864>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.161293, -0.218521, 0.594608>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.126197, 0.140784, 0.601604>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119558, 0.183772, 0.569677>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082332, 0.114229, 0.024727>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082077, 0.194381, 0.082369>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.155515, -0.106501, 0.642566>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.083309, 0.202499, 0.090492>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.145205, 0.008674, 0.648103>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162939, -0.205206, 0.602405>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.084005, 0.228811, 0.121449>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.144463, 0.017876, 0.646754>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090538, 0.033037, -0.004613>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085427, 0.134107, 0.035738>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125882, -0.272851, 0.086003>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098800, 0.289223, 0.386682>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089205, 0.063500, 0.003592>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089125, 0.286990, 0.246120>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.116320, 0.224299, 0.526538>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151752, -0.036450, 0.650292>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.149886, -0.360478, 0.253792>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087194, 0.212095, 0.100795>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088134, 0.187070, 0.075283>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117062, -0.195692, 0.028060>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110349, -0.141057, 0.004363>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162170, -0.360243, 0.377832>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.116651, -0.188373, 0.024234>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.120493, 0.219922, 0.531423>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.131761, 0.151003, 0.593838>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.101571, -0.043503, -0.012144>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152094, -0.360323, 0.255318>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.140866, 0.085435, 0.627932>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.132287, 0.155544, 0.590514>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.123716, -0.225168, 0.046579>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.169968, -0.289806, 0.527335>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151649, 0.003615, 0.647490>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.103542, -0.031664, -0.012046>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096924, 0.287275, 0.247436>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.107747, -0.073314, -0.010098>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097876, 0.061260, 0.002925>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171311, -0.297434, 0.517017>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162655, -0.364320, 0.340877>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.132548, 0.174323, 0.575808>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.145888, -0.326510, 0.161547>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.143842, 0.099600, 0.621197>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.140443, -0.300030, 0.119822>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.105606, -0.001237, -0.009970>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.155787, -0.003216, 0.647412>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.174017, -0.282269, 0.534449>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.174174, -0.262546, 0.555037>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.161573, -0.055505, 0.647648>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104670, 0.292665, 0.278678>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.161285, -0.362160, 0.278279>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141205, -0.291114, 0.109013>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.175419, -0.283018, 0.532958>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.175442, -0.267245, 0.549913>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104523, 0.282398, 0.229174>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.102900, 0.137236, 0.037698>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127596, -0.192448, 0.027564>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.133765, 0.198888, 0.552744>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110084, 0.295081, 0.312929>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.170390, -0.119696, 0.635631>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.168314, -0.363373, 0.323794>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.122018, -0.108748, -0.003381>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.172104, -0.137685, 0.629964>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.168789, -0.086940, 0.642686>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.172883, -0.358098, 0.373220>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.165626, -0.360656, 0.275322>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179490, -0.306563, 0.499951>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.120518, 0.287648, 0.388405>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127818, -0.140433, 0.005762>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111562, 0.144615, 0.042767>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.146491, -0.282224, 0.100267>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.112449, 0.256127, 0.165077>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179095, -0.339739, 0.435437>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.135195, 0.238037, 0.503780>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.180231, -0.208771, 0.594510>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179986, -0.338786, 0.437181>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.180716, -0.219495, 0.587358>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.173687, -0.100507, 0.639004>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119643, 0.045244, -0.000630>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.167246, -0.011401, 0.645460>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.181243, -0.337723, 0.438854>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.150125, -0.277705, 0.096369>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125799, 0.292541, 0.352771>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.170241, -0.031747, 0.645768>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.163237, -0.335687, 0.187898>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162049, -0.330378, 0.176085>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.123974, 0.058255, 0.003364>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125416, 0.072065, 0.007994>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.149336, -0.237071, 0.059659>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.123627, 0.187933, 0.077607>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.166768, 0.042749, 0.636355>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.177216, -0.360577, 0.307529>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.133145, -0.047476, -0.009722>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171984, -0.351670, 0.239533>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.139977, 0.264155, 0.454614>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.186155, -0.196887, 0.599409>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.180552, -0.103492, 0.636372>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.169544, -0.342968, 0.209789>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.187973, -0.237282, 0.571399>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.128478, 0.245228, 0.148263>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.131367, 0.288099, 0.261370>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.185846, -0.351161, 0.389122>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.172696, 0.016870, 0.640379>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130979, 0.101500, 0.020490>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.167376, -0.324312, 0.167175>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.189615, -0.327583, 0.455771>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.144937, -0.128637, 0.004525>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151306, -0.190758, 0.030605>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.190609, -0.211610, 0.588752>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179225, -0.355055, 0.263989>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.155629, 0.207140, 0.538543>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162132, 0.156331, 0.583281>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.166332, -0.299513, 0.127763>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.191082, -0.337637, 0.430054>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162105, -0.264106, 0.086099>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.154647, -0.183132, 0.027400>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.190963, -0.347154, 0.398977>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.188927, -0.126294, 0.628083>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.161243, 0.189668, 0.555178>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.176539, -0.337316, 0.200332>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.174640, 0.061270, 0.628865>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179146, 0.036716, 0.634478>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.150642, 0.284717, 0.384029>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.143740, 0.115177, 0.028915>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152254, 0.280107, 0.403027>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152740, -0.057821, -0.006435>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.163315, -0.199197, 0.037909>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.145696, 0.113697, 0.028455>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.148246, 0.039451, 0.001256>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.174821, -0.296258, 0.126928>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.153595, 0.288107, 0.359383>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.169353, -0.240078, 0.067250>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.200412, -0.245895, 0.558575>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171977, 0.170437, 0.569237>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151755, 0.062012, 0.008100>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.176199, 0.141713, 0.589445>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.160162, 0.276068, 0.412221>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.201868, -0.321392, 0.458767>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152707, 0.259685, 0.181671>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.200602, -0.185048, 0.600221>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.164515, -0.134019, 0.009858>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152967, 0.230607, 0.131677>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.154421, 0.274691, 0.220705>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.195770, -0.085296, 0.634631>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.173326, 0.194590, 0.546341>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.181808, -0.298497, 0.133156>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.160963, 0.284769, 0.373298>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.204801, -0.269515, 0.533212>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.160562, 0.288163, 0.294887>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.202350, -0.347760, 0.377127>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.195849, -0.038635, 0.638117>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.200069, -0.098346, 0.630862>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.160014, 0.275863, 0.227866>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.158624, 0.205835, 0.102159>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.184490, 0.132925, 0.592436>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.177716, -0.210195, 0.048475>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.160569, 0.168711, 0.067330>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.173606, -0.147651, 0.016601>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162009, 0.230076, 0.133659>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.177069, 0.244621, 0.477601>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.211846, -0.268803, 0.529900>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.184587, 0.201949, 0.534115>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.191598, -0.281865, 0.116453>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.213091, -0.310233, 0.470890>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.176113, -0.057352, -0.001663>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.206044, -0.350195, 0.294213>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.201673, 0.017089, 0.631276>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.201357, -0.333830, 0.212763>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.189780, 0.187594, 0.547081>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.173486, 0.227515, 0.133960>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.175040, 0.276391, 0.241441>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.176770, 0.014059, 0.001863>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.174979, 0.247275, 0.166457>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179868, 0.283699, 0.348256>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.192009, -0.222808, 0.061798>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.186019, -0.122072, 0.011775>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.210626, -0.057341, 0.632158>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.187196, 0.257672, 0.444832>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.181118, 0.281995, 0.278650>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179497, 0.256403, 0.187116>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.218321, -0.163258, 0.603068>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.212783, -0.035939, 0.631933>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.214062, -0.032984, 0.631401>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.223438, -0.271095, 0.519902>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.212443, 0.012174, 0.628082>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.197605, 0.218520, 0.507525>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.205673, -0.283498, 0.125826>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.194672, 0.247886, 0.460961>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.187800, 0.043622, 0.010781>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.224950, -0.276903, 0.511815>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.220447, -0.344972, 0.341377>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.186471, 0.183874, 0.087908>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.203635, -0.240840, 0.080625>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.195684, 0.268012, 0.408439>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.191653, 0.279440, 0.279784>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.196812, 0.266798, 0.411336>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.198861, 0.258209, 0.434323>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.219803, -0.337561, 0.249537>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.202417, 0.238299, 0.473906>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.229742, -0.332495, 0.394647>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.224061, -0.338524, 0.262513>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.226394, -0.342652, 0.304408>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.217872, -0.294854, 0.149244>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.225618, -0.006621, 0.625109>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.202759, 0.249628, 0.186588>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.230127, -0.338592, 0.279258>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.212861, -0.147931, 0.029237>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.207364, -0.002304, 0.008167>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.232812, -0.336874, 0.275868>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.208617, 0.164470, 0.078796>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.240256, -0.300563, 0.463089>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.212338, 0.269992, 0.258896>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.212029, 0.254737, 0.205905>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.241240, -0.150427, 0.597138>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.242741, -0.329251, 0.380865>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.231149, 0.122682, 0.578985>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.241780, -0.093719, 0.613733>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.246029, -0.283978, 0.484950>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.219826, 0.003447, 0.012983>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.220056, 0.065709, 0.027528>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.219602, 0.217445, 0.141678>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.247072, -0.190352, 0.573752>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.237829, 0.059363, 0.605299>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.223253, 0.271868, 0.325441>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.248760, -0.290270, 0.472332>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.222810, 0.018022, 0.016252>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.226350, 0.259420, 0.400840>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.224595, 0.192394, 0.113203>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.246175, -0.331370, 0.283337>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.238990, 0.106396, 0.584264>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.235481, 0.173580, 0.537123>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.228824, -0.040600, 0.014025>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.231820, 0.236059, 0.455010>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.252407, -0.247426, 0.524802>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.226734, 0.242888, 0.191330>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.250668, -0.327352, 0.368670>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.230684, -0.006536, 0.016010>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.230815, -0.001175, 0.016611>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.246478, 0.009731, 0.613147>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.253905, -0.327080, 0.358672>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.240480, -0.199276, 0.067967>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.257971, -0.211124, 0.552610>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.249357, 0.043310, 0.604305>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.238364, 0.077926, 0.039805>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.256995, -0.327630, 0.307291>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.248265, 0.139989, 0.557643>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.249307, -0.248726, 0.114277>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.261844, -0.237607, 0.527097>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.243388, 0.075882, 0.041377>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.258239, -0.252836, 0.125473>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.251850, 0.255267, 0.369489>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.259870, 0.082371, 0.584242>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.268610, -0.166820, 0.572968>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.268535, -0.320976, 0.331950>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.261834, 0.190035, 0.500587>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.260525, 0.241823, 0.405920>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.274788, -0.147916, 0.577641>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.270909, -0.265951, 0.153130>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.274855, 0.086994, 0.572633>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.274553, -0.047880, 0.036595>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.277911, 0.175138, 0.504059>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.287542, -0.133170, 0.062632>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.288503, 0.161781, 0.128629>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.292503, -0.244196, 0.146897>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.296620, -0.130388, 0.068227>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.302245, -0.047977, 0.581568>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.303553, -0.233972, 0.146994>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.315780, 0.009822, 0.069513>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.314142, 0.209987, 0.388005>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.323880, -0.211306, 0.146773>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.324937, -0.249093, 0.437177>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.324779, -0.042389, 0.563559>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.333134, 0.172663, 0.203304>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.327138, -0.095023, 0.553900>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.329840, -0.109209, 0.547194>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.331701, -0.194961, 0.495532>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.333658, -0.267239, 0.364957>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.344140, 0.005833, 0.095958>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.350031, 0.020701, 0.105863>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.340667, -0.218932, 0.456324>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.340858, -0.080968, 0.543628>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.358159, 0.113568, 0.169597>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.348737, 0.091248, 0.500757>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.353894, -0.150142, 0.136202>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.351140, -0.241136, 0.245106>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.353870, -0.026505, 0.534085>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.355168, -0.167430, 0.486676>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.367607, 0.146785, 0.398815>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.368132, 0.004567, 0.512613>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.380524, 0.116316, 0.416502>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.391944, -0.049124, 0.158220>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.387145, 0.126155, 0.371119>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.397281, 0.103667, 0.258914>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.373539, -0.099594, 0.497769>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.389814, -0.115568, 0.174719>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.397076, -0.048183, 0.167766>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.383039, -0.208150, 0.285333>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.386637, -0.190777, 0.387727>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.406764, -0.147906, 0.251919>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.404056, -0.168040, 0.284675>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.397212, -0.164134, 0.397712>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.431084, -0.014122, 0.282500>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.421597, -0.120559, 0.343670>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.429464, -0.053113, 0.368808>, 0.110000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
}

