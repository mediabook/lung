global_settings {
  max_trace_level 10
  assumed_gamma 1
}

camera {
  location    <-1.300000, -1.300000, 0.000000>
  look_at     <0.000000, 0.000000, 0>
}

light_source {
  <-5.000000, 0.500000, -2.000000>*500
  color rgb <2.0, 1.5, 1.0>*1.8
  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient
}

light_source {
  <5.000000, 0.500000, 2.000000>*500
  color rgb <2.0, 1.5, 1.0>*1.8
  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient
}

blob {
  threshold 0.5
  sphere {
    <0.000000, 0.000000, 0.000000>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.007661, 0.193449, 0.122526>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.007876, 0.149173, 0.070892>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.039910, -0.289336, 0.152517>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010715, -0.108195, 0.005450>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000371, 0.240534, 0.262079>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.007784, 0.156928, 0.078165>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.061291, -0.195489, 0.505404>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.005451, 0.238763, 0.314612>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.021643, 0.185730, 0.444674>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.030678, -0.241782, 0.083414>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.062751, -0.220295, 0.486235>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.033111, 0.121431, 0.506439>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.032904, -0.251563, 0.094207>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.058182, -0.312056, 0.328256>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.006499, 0.136614, 0.059500>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.025075, -0.206120, 0.051586>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001718, -0.002345, -0.000863>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.013743, -0.121783, 0.008799>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.023248, -0.190916, 0.040874>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.064228, -0.228075, 0.479317>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.017692, -0.149591, 0.018842>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.033210, 0.128662, 0.501585>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.023917, 0.181236, 0.451005>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000712, 0.239324, 0.242242>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.049095, -0.308989, 0.205054>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.057075, -0.074401, 0.552789>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.063112, -0.294606, 0.384777>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.029462, 0.154532, 0.480185>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010117, -0.080468, -0.000823>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.017840, -0.144713, 0.016577>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.061984, -0.137500, 0.536393>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010079, 0.235425, 0.337791>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.020734, 0.200966, 0.423323>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.011133, 0.233434, 0.346388>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.049613, -0.307552, 0.199126>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.062713, -0.306447, 0.352032>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.041674, 0.082873, 0.529192>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.062963, -0.306791, 0.350918>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.004601, 0.165910, 0.085620>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.049498, 0.022925, 0.549464>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.059859, -0.316572, 0.298842>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.067446, -0.256602, 0.448584>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.065566, -0.297563, 0.378064>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.061277, -0.315596, 0.308332>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.017582, 0.219984, 0.387561>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.056585, -0.035699, 0.555747>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.042811, -0.277694, 0.129172>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.021799, -0.154306, 0.019905>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.030134, 0.168723, 0.466856>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.037560, 0.126445, 0.504115>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.003007, 0.174888, 0.094793>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.001180, 0.105010, 0.035450>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.068024, -0.174916, 0.518814>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000011, 0.222618, 0.173272>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.002317, 0.182680, 0.103981>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002844, 0.236726, 0.219368>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.068530, -0.297123, 0.379486>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.065169, -0.104193, 0.547382>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000046, 0.105420, 0.035215>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001692, 0.227883, 0.186378>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.022855, -0.146887, 0.016118>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.050952, -0.297369, 0.167361>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000665, 0.215626, 0.156061>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.024426, -0.156072, 0.020031>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.072078, -0.249514, 0.457432>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.040633, 0.124008, 0.506417>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.026240, 0.201137, 0.425547>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.058541, -0.314086, 0.225422>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.056697, 0.008161, 0.552834>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.026168, -0.161884, 0.022518>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000434, 0.193828, 0.117637>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.065050, -0.074711, 0.553347>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.060175, -0.020688, 0.555837>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.053072, -0.298415, 0.169362>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.030828, 0.186328, 0.447386>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.029666, 0.191840, 0.439957>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.014303, -0.055297, -0.005429>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001274, 0.150591, 0.067570>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.072249, -0.298545, 0.376316>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.012982, 0.243406, 0.297567>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.074928, -0.220850, 0.486380>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.036695, 0.163992, 0.473474>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.074172, -0.291359, 0.393075>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.042260, -0.244368, 0.083067>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.042287, -0.243968, 0.082607>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.060776, -0.311895, 0.212434>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.056707, 0.039244, 0.546601>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.058056, -0.304078, 0.183850>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.074987, -0.296461, 0.381675>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.004043, 0.204973, 0.132987>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.069254, -0.317609, 0.293793>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.077143, -0.266254, 0.436178>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.022312, 0.230879, 0.364542>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.004881, 0.213905, 0.149030>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007431, 0.065207, 0.013244>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.021704, -0.094684, -0.001264>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.059054, 0.027208, 0.549691>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.046711, 0.119410, 0.510588>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.074977, -0.147750, 0.532774>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.004539, 0.186354, 0.104728>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.004593, 0.161939, 0.076720>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026657, -0.128966, 0.007722>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.048321, -0.260808, 0.101864>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052695, -0.279496, 0.129433>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.061079, -0.306873, 0.192059>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.040311, 0.163167, 0.475224>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008953, 0.233399, 0.196145>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025122, 0.229077, 0.371392>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007668, 0.093198, 0.025501>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.062075, -0.307929, 0.195492>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.053796, 0.084276, 0.530497>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076272, -0.137452, 0.537109>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025502, -0.108481, 0.001301>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.046029, 0.136778, 0.498624>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008225, 0.221906, 0.163832>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035318, 0.193647, 0.439627>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.061200, -0.303999, 0.182593>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080537, -0.237049, 0.471236>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021386, 0.240308, 0.331505>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023937, -0.091242, -0.002645>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042775, 0.158144, 0.480482>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067576, -0.022728, 0.556500>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014403, 0.244455, 0.249233>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033145, 0.207658, 0.418386>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074286, -0.089795, 0.551184>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063189, 0.023601, 0.550838>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019933, -0.043839, -0.007802>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012422, 0.238701, 0.213107>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.007781, 0.174417, 0.088411>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012051, 0.235930, 0.201725>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.036716, 0.195902, 0.437086>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035202, 0.203498, 0.425677>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026379, -0.097120, -0.001930>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072712, -0.059171, 0.555685>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031847, 0.217628, 0.400397>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019087, 0.246304, 0.282743>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.049902, -0.248700, 0.086184>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.051988, 0.119672, 0.511363>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041611, -0.200550, 0.042953>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018403, 0.246390, 0.266377>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.083929, -0.256424, 0.449383>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022266, -0.045513, -0.008362>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026185, -0.082186, -0.005064>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079550, -0.310871, 0.338209>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027191, 0.236467, 0.351771>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075116, -0.318687, 0.279672>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015822, 0.032600, 0.000531>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082627, -0.178294, 0.517283>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025602, -0.073380, -0.006403>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042654, -0.202092, 0.043868>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013152, 0.229495, 0.179178>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025634, -0.072576, -0.006533>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079463, -0.312978, 0.328846>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078737, -0.315669, 0.313795>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018138, 0.014030, -0.003886>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012489, 0.106463, 0.030911>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012469, 0.111647, 0.033979>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085351, -0.263988, 0.439461>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013708, 0.225985, 0.169180>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074593, -0.048603, 0.556586>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.062760, -0.292802, 0.153075>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020027, 0.246717, 0.260346>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023207, -0.034695, -0.008968>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031511, -0.111389, 0.000637>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021275, -0.010544, -0.007709>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.086527, -0.259947, 0.444880>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012788, 0.156689, 0.067868>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085056, -0.171349, 0.521286>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063907, -0.291938, 0.150909>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072995, -0.016123, 0.556561>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047203, 0.171327, 0.468930>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014193, 0.178037, 0.089381>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016866, 0.088884, 0.020108>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020219, 0.243586, 0.226717>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078336, -0.061522, 0.555697>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016697, 0.207471, 0.129064>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018655, 0.075793, 0.013652>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016845, 0.164461, 0.073586>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055995, -0.250781, 0.087325>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042929, -0.172972, 0.024536>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089214, -0.227426, 0.480589>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.036888, -0.126494, 0.004332>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037947, 0.223066, 0.392592>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019411, 0.130780, 0.044045>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041666, 0.210229, 0.417595>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066735, -0.291479, 0.149298>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078706, -0.043848, 0.557003>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022095, 0.067040, 0.009113>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027812, 0.248466, 0.286662>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.044481, -0.174204, 0.024918>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078824, -0.040477, 0.557131>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090833, -0.223891, 0.483798>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.039338, 0.223095, 0.393200>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028326, 0.249047, 0.276367>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088891, -0.164563, 0.524919>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080010, -0.318453, 0.252379>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091138, -0.211696, 0.494150>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027970, 0.247770, 0.242908>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027841, 0.246801, 0.234459>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026809, 0.041688, -0.000390>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037416, 0.234779, 0.364440>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042992, 0.214493, 0.410948>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029105, 0.248536, 0.248397>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025847, 0.081750, 0.013979>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.054647, -0.223802, 0.059356>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024536, 0.167691, 0.073624>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025389, 0.167011, 0.072601>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.092221, -0.198860, 0.503857>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091234, -0.172384, 0.520684>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026380, 0.202676, 0.116045>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060479, 0.130792, 0.505401>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075199, -0.307112, 0.189005>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089671, -0.308440, 0.347833>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028864, 0.067572, 0.007335>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029124, 0.065444, 0.006476>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029627, 0.069217, 0.007744>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085544, -0.318825, 0.283417>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.068631, -0.282972, 0.132121>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.046663, -0.155845, 0.014659>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038790, 0.243437, 0.336758>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034648, -0.010956, -0.011190>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.071170, 0.064293, 0.540398>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030453, 0.229542, 0.165112>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032214, 0.044284, -0.001131>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028955, 0.160999, 0.065401>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077475, 0.012144, 0.553883>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070342, 0.072537, 0.537277>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079184, -0.312790, 0.210754>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094810, -0.228716, 0.479259>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035217, -0.003741, -0.010729>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.046183, -0.142131, 0.008483>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.093104, -0.173639, 0.519946>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045746, -0.132673, 0.004739>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034277, 0.245670, 0.216812>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066668, 0.104653, 0.522127>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032194, 0.090290, 0.016053>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.046740, -0.137782, 0.006563>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087509, -0.318514, 0.289299>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.083586, -0.038305, 0.557348>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037039, -0.005929, -0.011355>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.058215, -0.221951, 0.057026>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.036168, 0.247956, 0.227555>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.093031, -0.305018, 0.359011>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042745, -0.075413, -0.010075>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.092761, -0.307401, 0.351349>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.048398, 0.220739, 0.401708>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033136, 0.199428, 0.108077>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088902, -0.318277, 0.292655>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037451, 0.038987, -0.003842>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063273, -0.237725, 0.071407>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037503, 0.235000, 0.174555>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078770, 0.029050, 0.550847>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.046123, -0.073042, -0.011124>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.062493, -0.229782, 0.063581>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055762, 0.197281, 0.440900>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098128, -0.260607, 0.443677>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037745, 0.223858, 0.147645>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094144, -0.310520, 0.339852>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088897, -0.063121, 0.555767>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096889, -0.294926, 0.385315>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073832, 0.077971, 0.535394>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038185, 0.149258, 0.051943>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081647, 0.014063, 0.553771>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.039087, 0.192448, 0.096170>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081745, -0.308335, 0.192135>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096479, -0.303270, 0.364023>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085923, -0.023145, 0.557372>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.093160, -0.105881, 0.547717>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072262, 0.098096, 0.526092>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067230, -0.243964, 0.077482>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.040489, 0.120652, 0.030532>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090501, -0.069782, 0.554960>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077094, -0.289681, 0.143749>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087160, -0.316253, 0.228979>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065541, -0.218588, 0.052885>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094700, -0.315638, 0.314743>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072650, -0.263294, 0.100501>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078710, 0.063253, 0.541290>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063059, 0.198975, 0.440320>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.099300, -0.164219, 0.524911>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.048599, 0.035582, -0.007154>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057753, 0.239426, 0.362679>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.046354, 0.177060, 0.075588>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100296, -0.167297, 0.523246>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069367, 0.169527, 0.474940>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052048, 0.016752, -0.011628>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.068559, -0.195681, 0.034574>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073610, -0.238981, 0.071246>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.103764, -0.244544, 0.462689>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055178, 0.254875, 0.274883>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095849, -0.082497, 0.552922>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.054653, 0.254722, 0.260092>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082373, -0.289320, 0.142391>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.102827, -0.195582, 0.505797>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.059887, 0.244256, 0.348676>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075212, 0.139870, 0.500741>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067730, -0.166757, 0.016786>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070052, 0.189934, 0.453067>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066126, 0.222657, 0.403817>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095179, -0.059981, 0.556099>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.062655, 0.243432, 0.352824>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072461, -0.199815, 0.037049>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087026, 0.037220, 0.549303>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057651, 0.003660, -0.014485>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088973, 0.020559, 0.552880>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.084317, -0.282779, 0.129709>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.061346, -0.055081, -0.015914>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056077, 0.235096, 0.163733>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055044, 0.166580, 0.062486>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.101707, -0.314366, 0.321233>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079703, 0.147338, 0.495480>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.106613, -0.212670, 0.492714>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.083808, -0.262039, 0.097581>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.086636, 0.079356, 0.535561>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.058583, 0.160970, 0.056374>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.059026, 0.119229, 0.025196>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095931, -0.313834, 0.214327>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.064000, 0.256230, 0.258977>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072171, 0.233415, 0.383270>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074357, -0.145817, 0.005760>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098755, -0.040279, 0.557363>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104039, -0.116403, 0.544403>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077314, -0.175040, 0.020336>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.099068, -0.317218, 0.236455>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094285, -0.299750, 0.165434>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066701, -0.007166, -0.016944>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077986, 0.209322, 0.428469>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104353, -0.318042, 0.292408>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067882, -0.013572, -0.017535>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087223, 0.132119, 0.507120>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090968, 0.093925, 0.529220>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066155, 0.248676, 0.200645>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110106, -0.289498, 0.395665>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082707, 0.183381, 0.462221>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066905, 0.040582, -0.009203>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098399, -0.305848, 0.182898>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073212, 0.251659, 0.326327>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069375, 0.008938, -0.015627>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069947, 0.253331, 0.220402>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.103387, -0.026174, 0.557378>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110308, -0.140311, 0.535466>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070929, 0.039893, -0.010009>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113484, -0.282877, 0.407931>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097625, -0.284901, 0.133021>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072588, 0.254991, 0.228636>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076574, 0.253669, 0.317958>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.105280, -0.315162, 0.222810>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094263, -0.257545, 0.091115>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113509, -0.297190, 0.377908>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.103862, -0.309986, 0.197805>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.071825, 0.241997, 0.174447>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079825, 0.245608, 0.352656>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114015, -0.298120, 0.375502>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076256, -0.005790, -0.018129>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.093749, 0.145735, 0.497554>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.099518, 0.080165, 0.535449>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.084136, 0.231564, 0.390033>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.105718, 0.001396, 0.555568>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.109709, -0.318067, 0.249626>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095610, -0.245848, 0.077056>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111741, -0.088106, 0.551256>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110973, -0.074019, 0.553802>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113868, -0.128689, 0.539708>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.116331, -0.181886, 0.513788>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094648, 0.154942, 0.490220>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.101730, 0.080912, 0.535125>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104958, 0.039287, 0.548816>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119218, -0.225797, 0.480131>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114132, -0.318270, 0.280037>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.115154, -0.317686, 0.289474>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.120421, -0.251555, 0.452836>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081991, 0.258854, 0.265414>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078960, 0.174802, 0.064786>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.120535, -0.288169, 0.396527>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.121698, -0.245543, 0.459650>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.120041, -0.302923, 0.361010>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080626, 0.219418, 0.122267>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095917, -0.194141, 0.031252>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.122290, -0.260411, 0.441333>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091106, -0.122442, -0.004743>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082674, 0.194659, 0.086106>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090254, 0.247072, 0.351094>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.122090, -0.304480, 0.355651>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.121821, -0.154530, 0.528175>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.083964, 0.191665, 0.082237>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.084940, 0.202491, 0.095727>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.106642, -0.263562, 0.099091>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085178, 0.121602, 0.021901>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.123435, -0.306004, 0.350209>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113914, -0.304357, 0.179773>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113247, 0.027383, 0.551183>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097171, -0.153283, 0.007530>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125745, -0.223220, 0.481606>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088301, 0.251885, 0.200987>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.123933, -0.158533, 0.525991>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088561, 0.254368, 0.212446>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087887, 0.234493, 0.149696>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.126211, -0.284018, 0.403369>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.126512, -0.204963, 0.496715>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.106611, -0.239934, 0.070553>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.103805, -0.210162, 0.043053>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089409, 0.135760, 0.030552>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.122169, -0.317862, 0.268227>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.105060, 0.169431, 0.477489>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097875, -0.114809, -0.007669>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.092676, 0.025655, -0.015772>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.112295, -0.267089, 0.104325>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110061, 0.131812, 0.507579>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.105373, -0.194351, 0.031228>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.093604, 0.112353, 0.015494>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094398, 0.185409, 0.073165>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098688, -0.075455, -0.017197>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096677, -0.025322, -0.021051>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125791, -0.092046, 0.549268>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.131496, -0.223357, 0.480573>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.101010, 0.246421, 0.355599>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110549, 0.157235, 0.488339>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097418, 0.172660, 0.059488>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100227, -0.047880, -0.020569>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.124138, -0.312344, 0.213482>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.101987, 0.252735, 0.333312>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.105661, 0.224784, 0.405938>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.103065, 0.249837, 0.344667>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104689, -0.119326, -0.006538>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.099972, 0.247287, 0.179782>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100544, 0.255418, 0.212137>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.109746, -0.183296, 0.023848>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117534, -0.260446, 0.095487>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.106363, 0.240614, 0.372399>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.103014, 0.249798, 0.187111>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.106630, 0.257698, 0.308664>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.105942, 0.252598, 0.196648>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.129051, -0.312084, 0.214577>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.122440, 0.087682, 0.531349>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.108206, 0.260038, 0.288840>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.138478, -0.235487, 0.467486>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.106422, 0.142273, 0.032869>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.138162, -0.210103, 0.490771>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.108465, -0.007712, -0.021038>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.108812, 0.253839, 0.200866>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110083, -0.062084, -0.019607>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.115247, -0.145884, 0.003633>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.124035, 0.112335, 0.518840>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.112800, 0.259592, 0.296332>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111592, -0.000943, -0.020755>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.121343, -0.219069, 0.050951>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114280, 0.252885, 0.334322>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141250, -0.188868, 0.505848>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117334, 0.233222, 0.389815>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127952, 0.090601, 0.529575>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114447, 0.204337, 0.093218>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.131923, -0.290371, 0.147820>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.144565, -0.245519, 0.455129>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.142666, -0.163557, 0.520709>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.122195, 0.216256, 0.420483>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.116494, 0.145683, 0.034568>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130130, -0.262956, 0.100312>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117636, 0.200051, 0.087386>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.138808, -0.045138, 0.554131>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.128186, 0.153207, 0.490844>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.144223, -0.312113, 0.310244>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.128287, 0.168670, 0.477450>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.136331, 0.053830, 0.542572>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127491, 0.212902, 0.425358>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.123064, 0.241493, 0.158974>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.136146, -0.279476, 0.127382>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.146179, -0.312144, 0.307837>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.142432, -0.311528, 0.220711>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125698, 0.261820, 0.265896>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.128253, -0.139599, 0.001173>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.126691, -0.068692, -0.019070>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127358, -0.069403, -0.018966>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.126605, -0.025245, -0.022362>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.129095, 0.242800, 0.367365>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141333, -0.300483, 0.176151>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127339, -0.047460, -0.021514>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.142143, 0.011604, 0.551170>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127603, 0.256618, 0.210425>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.144273, -0.015893, 0.553478>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.149763, -0.310642, 0.313745>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.128216, 0.178246, 0.062439>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.128537, 0.208868, 0.098828>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.128510, 0.081003, -0.001896>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130920, 0.261635, 0.275284>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.134675, 0.216744, 0.418834>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152095, -0.306458, 0.332785>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.137047, -0.210993, 0.045133>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.153523, -0.185824, 0.505343>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.147502, -0.034783, 0.553237>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151560, -0.311930, 0.302450>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.137413, 0.187280, 0.457649>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.137805, 0.200711, 0.441419>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.132933, 0.188299, 0.073153>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.133947, 0.056704, -0.010302>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.156504, -0.186636, 0.504106>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.143338, 0.124245, 0.509993>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141838, 0.162258, 0.481944>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.134530, 0.105739, 0.009461>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.137265, 0.174730, 0.059189>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.155954, -0.090696, 0.545073>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.139132, 0.181595, 0.066205>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152692, 0.009004, 0.549940>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.160165, -0.174973, 0.510590>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.140072, 0.204007, 0.092637>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152755, 0.017182, 0.548733>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.140749, 0.035454, -0.015545>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.140152, 0.122991, 0.019333>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.148252, -0.231145, 0.065037>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.144139, 0.255801, 0.321065>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141302, 0.062006, -0.008386>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.142736, 0.253771, 0.199093>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.147937, 0.198662, 0.443080>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.157636, -0.030249, 0.551548>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.157452, -0.307261, 0.210852>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.144451, 0.189532, 0.075247>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.145423, 0.168226, 0.053617>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151006, -0.163135, 0.013831>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.149747, -0.115665, -0.006983>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.163985, -0.307208, 0.317345>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151718, -0.156704, 0.010472>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.153836, 0.195171, 0.446635>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.155538, 0.134292, 0.501669>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.149369, -0.030020, -0.021536>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.160928, -0.306837, 0.211866>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.157839, 0.076804, 0.531711>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.157495, 0.138468, 0.498424>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.156025, -0.188181, 0.029859>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.169291, -0.246993, 0.445602>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162845, 0.005690, 0.548629>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.153404, -0.020366, -0.021173>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.153049, 0.253148, 0.199849>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.154951, -0.056097, -0.019443>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152877, 0.060590, -0.007958>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171595, -0.253353, 0.436581>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.168209, -0.310186, 0.285118>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162419, 0.155677, 0.484656>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.165988, -0.276217, 0.129873>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.165706, 0.089887, 0.524784>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.165283, -0.252923, 0.093697>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.158558, 0.006838, -0.018822>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.168856, 0.000774, 0.548023>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.175911, -0.240042, 0.451280>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.176376, -0.223141, 0.468895>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171675, -0.045024, 0.548278>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.160872, 0.257168, 0.226247>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.173281, -0.307413, 0.230518>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.169003, -0.244606, 0.083961>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.178194, -0.240505, 0.449846>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.178366, -0.226974, 0.464391>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162519, 0.247885, 0.184231>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.161642, 0.125304, 0.022978>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.167703, -0.157817, 0.013109>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.169891, 0.177748, 0.463065>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.165385, 0.259050, 0.255277>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179311, -0.099482, 0.537683>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179420, -0.308269, 0.269309>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.170091, -0.084980, -0.012850>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.180450, -0.115091, 0.532759>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.180041, -0.071268, 0.543471>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.182006, -0.303930, 0.311728>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.180839, -0.305233, 0.227251>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.185346, -0.260112, 0.420830>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.172749, 0.252785, 0.319457>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.174256, -0.111699, -0.005047>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.170895, 0.131030, 0.028297>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179157, -0.235199, 0.075635>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.172100, 0.224277, 0.130770>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.187536, -0.288134, 0.364942>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179777, 0.211251, 0.418546>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.188555, -0.175783, 0.501874>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.188888, -0.287206, 0.366332>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.188770, -0.185039, 0.495713>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.186687, -0.082262, 0.539778>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.176284, 0.047538, -0.008336>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.186817, -0.005030, 0.544419>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.190854, -0.286123, 0.367590>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.185152, -0.230328, 0.072115>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.180336, 0.255679, 0.288300>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.189032, -0.023064, 0.544655>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.189692, -0.281372, 0.150872>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.189683, -0.276590, 0.140632>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.181045, 0.058382, -0.004204>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.182871, 0.069963, 0.000110>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.190042, -0.193645, 0.040758>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.183184, 0.166345, 0.059150>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.193688, 0.042848, 0.535125>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.195747, -0.303698, 0.253673>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.186031, -0.030328, -0.015986>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.195859, -0.295111, 0.194976>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.189342, 0.232373, 0.374507>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.198939, -0.164511, 0.505076>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.197549, -0.083556, 0.536364>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.196090, -0.287127, 0.169310>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.199858, -0.199189, 0.481111>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.187695, 0.213096, 0.117749>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.188707, 0.249487, 0.211045>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.201701, -0.295984, 0.323507>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.199237, 0.021142, 0.538202>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.188893, 0.094183, 0.011693>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.198606, -0.269877, 0.132427>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.203532, -0.276207, 0.380929>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.193690, -0.099270, -0.004381>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.196286, -0.152506, 0.016921>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.205284, -0.176426, 0.495374>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.203847, -0.297355, 0.215389>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.199215, 0.184905, 0.446052>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.201415, 0.141768, 0.485657>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.202015, -0.247059, 0.098501>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.207174, -0.284176, 0.358330>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.201567, -0.215866, 0.063369>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.200250, -0.145251, 0.014541>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.209051, -0.291806, 0.331287>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.208697, -0.102314, 0.528203>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.203553, 0.170232, 0.460056>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.207067, -0.280398, 0.160318>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.206957, 0.060571, 0.526263>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.210381, 0.039260, 0.531198>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.203209, 0.246772, 0.312826>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.200669, 0.105347, 0.020831>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.203985, 0.243259, 0.328768>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.204394, -0.038647, -0.010885>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.207846, -0.158309, 0.023763>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.202398, 0.103984, 0.020678>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.203262, 0.042764, -0.002800>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.211820, -0.242752, 0.097761>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.206642, 0.248567, 0.291781>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.211177, -0.193249, 0.047893>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.219276, -0.204553, 0.468070>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.213276, 0.154062, 0.470984>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.206893, 0.061166, 0.003796>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.215563, 0.129885, 0.488800>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.210817, 0.239293, 0.335708>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.222365, -0.268844, 0.381600>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.208920, 0.221970, 0.146336>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.222164, -0.151574, 0.503382>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.212317, -0.101713, 0.002006>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.209435, 0.198029, 0.106112>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.209984, 0.234433, 0.177894>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.221849, -0.065068, 0.531929>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.216533, 0.173816, 0.450327>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.218912, -0.243568, 0.102862>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.212791, 0.245236, 0.302881>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.225376, -0.224264, 0.445603>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.214279, 0.245936, 0.238077>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.226812, -0.289946, 0.310701>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.225117, -0.023836, 0.533690>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.226558, -0.075846, 0.528296>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.214722, 0.234637, 0.183749>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.214298, 0.177283, 0.082838>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.223562, 0.122539, 0.490261>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.221087, -0.165766, 0.033289>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.215739, 0.147476, 0.054971>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.220124, -0.112143, 0.008132>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.217011, 0.196534, 0.108531>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.223693, 0.213541, 0.389462>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.234958, -0.222536, 0.441802>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.227811, 0.179444, 0.437551>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.229935, -0.227077, 0.089147>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.237025, -0.257642, 0.390651>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.225279, -0.036633, -0.003660>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.235404, -0.289245, 0.238791>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.234863, 0.024352, 0.525095>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.234444, -0.273378, 0.169664>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.231895, 0.167630, 0.448242>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.226472, 0.192934, 0.109737>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.227112, 0.233009, 0.194921>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.227467, 0.022455, 0.001493>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.227603, 0.208635, 0.135519>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.229695, 0.240954, 0.280821>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.233634, -0.174799, 0.045042>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.232066, -0.089029, 0.006436>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.240874, -0.039421, 0.526706>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.233611, 0.222205, 0.360250>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.231795, 0.237337, 0.224588>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.231127, 0.215455, 0.152100>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.245206, -0.130328, 0.502945>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.244122, -0.021510, 0.525550>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.245567, -0.018454, 0.524797>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.248828, -0.222931, 0.431685>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.245753, 0.020977, 0.520656>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.240622, 0.191537, 0.412299>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.243112, -0.226221, 0.097133>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.239636, 0.213974, 0.372987>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.237063, 0.046498, 0.011413>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.250578, -0.227603, 0.424605>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.249058, -0.283792, 0.278118>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.237031, 0.156776, 0.074815>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.243285, -0.188956, 0.060894>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.241761, 0.228157, 0.329008>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.240247, 0.233601, 0.225394>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.242627, 0.227155, 0.331253>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.243847, 0.220864, 0.350093>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.251619, -0.274605, 0.199983>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.245944, 0.205823, 0.383005>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.257356, -0.272999, 0.323031>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.255303, -0.275183, 0.210784>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.256283, -0.279730, 0.246086>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.253548, -0.234643, 0.116616>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.258173, 0.005607, 0.516480>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.249737, 0.206155, 0.153123>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.260798, -0.274706, 0.224586>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.254292, -0.107795, 0.023038>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.252365, 0.009854, 0.011315>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.263513, -0.272617, 0.221559>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.254564, 0.139191, 0.070804>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.267208, -0.245331, 0.380794>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.256814, 0.221772, 0.209022>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.257033, 0.208675, 0.168381>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.269725, -0.116532, 0.494252>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.270802, -0.267752, 0.310052>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.267067, 0.113603, 0.470581>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.271504, -0.067706, 0.506874>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.272996, -0.230480, 0.398923>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.262386, 0.014543, 0.017750>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.263090, 0.063077, 0.031334>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.263052, 0.178138, 0.120372>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.274839, -0.150167, 0.474144>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.271533, 0.061376, 0.494600>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.265085, 0.223281, 0.260156>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.275885, -0.235165, 0.387867>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.264773, 0.027020, 0.021606>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.266737, 0.216387, 0.319675>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.266859, 0.158484, 0.099533>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.276005, -0.265705, 0.227199>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.273598, 0.100186, 0.474284>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.272028, 0.153463, 0.432353>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.268971, -0.019381, 0.018488>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.270306, 0.199999, 0.363057>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.279494, -0.198438, 0.432143>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.268377, 0.196767, 0.158278>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.278726, -0.264372, 0.299054>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.270805, 0.007257, 0.021888>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.270991, 0.011011, 0.022561>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.278281, 0.020160, 0.501724>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.281933, -0.263264, 0.290270>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.276087, -0.147188, 0.055110>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.285241, -0.166579, 0.454738>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.281540, 0.048349, 0.492124>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.277381, 0.071070, 0.044497>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.285565, -0.261352, 0.246748>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.282095, 0.126734, 0.448336>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.282273, -0.188629, 0.090889>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.288767, -0.188623, 0.432671>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.281143, 0.069463, 0.046653>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.289745, -0.190272, 0.100067>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.286898, 0.207115, 0.292397>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.291206, 0.080547, 0.470996>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.295757, -0.127535, 0.469381>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.295921, -0.254185, 0.266230>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.293717, 0.162559, 0.396383>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.293047, 0.197467, 0.319831>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.301707, -0.110398, 0.471789>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.299798, -0.199771, 0.122345>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.303702, 0.083877, 0.457560>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.303889, -0.023320, 0.044478>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.306234, 0.149582, 0.396396>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.313365, -0.085677, 0.061459>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.313149, 0.127339, 0.118795>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.317127, -0.176260, 0.119798>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.320011, -0.082169, 0.067323>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.325977, -0.025259, 0.465174>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.325637, -0.165119, 0.121188>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.331928, 0.021226, 0.078849>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.331612, 0.163871, 0.300125>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.340460, -0.141846, 0.123904>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.344218, -0.184333, 0.347395>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.343645, -0.018523, 0.443924>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.341476, 0.129411, 0.173376>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.345889, -0.058924, 0.438032>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.347993, -0.070842, 0.432362>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.349711, -0.139546, 0.392772>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.350334, -0.194427, 0.288032>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.349366, 0.019206, 0.101539>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.352661, 0.028294, 0.109433>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.356400, -0.156261, 0.359643>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.355975, -0.046483, 0.425215>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.356148, 0.087634, 0.153145>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.357777, 0.081710, 0.380908>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.359163, -0.088277, 0.122751>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.360977, -0.162654, 0.195922>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.364198, -0.003489, 0.411145>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.366924, -0.112061, 0.379492>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.365246, 0.114334, 0.301814>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.372363, 0.020306, 0.389004>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.373779, 0.093523, 0.312750>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.377494, -0.014924, 0.145697>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.375702, 0.097288, 0.281261>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.377925, 0.079506, 0.210272>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.378275, -0.056059, 0.381102>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.379151, -0.059368, 0.153539>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.380173, -0.013573, 0.152135>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.381628, -0.129991, 0.224935>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.386203, -0.120017, 0.298297>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.391267, -0.080948, 0.203203>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.391891, -0.095976, 0.224429>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.391865, -0.098136, 0.303533>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.397984, 0.007599, 0.223877>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.400886, -0.061576, 0.263041>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.402545, -0.017592, 0.278022>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
}

