global_settings {
  max_trace_level 10
  assumed_gamma 1
}

camera {
  location    <-1.300000, -1.300000, 0.000000>
  look_at     <0.000000, 0.000000, 0>
}

light_source {
  <-5.000000, 0.500000, -2.000000>*500
  color rgb <2.0, 1.5, 1.0>*1.8
  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient
}

light_source {
  <5.000000, 0.500000, 2.000000>*500
  color rgb <2.0, 1.5, 1.0>*1.8
  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient
}

blob {
  threshold 0.5
  sphere {
    <0.000000, 0.000000, 0.000000>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.007546, 0.157278, 0.100163>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.007631, 0.120967, 0.057769>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.037546, -0.234577, 0.123383>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010020, -0.087812, 0.004204>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000382, 0.195461, 0.213652>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.007634, 0.127321, 0.063737>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.057076, -0.157140, 0.409943>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.004113, 0.194047, 0.255925>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.019130, 0.151208, 0.361291>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.028624, -0.196546, 0.067717>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.058267, -0.177408, 0.394354>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.029983, 0.098854, 0.411450>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.030582, -0.204414, 0.076460>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.054078, -0.252577, 0.265373>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.006686, 0.110677, 0.048480>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.023207, -0.167758, 0.041868>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.001091, -0.001920, -0.000658>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.012398, -0.098971, 0.006972>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.021316, -0.155424, 0.033156>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.059238, -0.183798, 0.388744>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.015981, -0.121721, 0.015165>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.029685, 0.104680, 0.407556>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.020854, 0.147504, 0.366483>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000650, 0.194441, 0.197714>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.045148, -0.250307, 0.165690>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.051948, -0.059097, 0.448408>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.058123, -0.238324, 0.311529>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.025949, 0.125722, 0.390234>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008621, -0.065292, -0.000777>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.015859, -0.117758, 0.013344>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.056486, -0.110063, 0.435137>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007645, 0.191323, 0.274652>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.017511, 0.163505, 0.343960>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008569, 0.189725, 0.281582>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.045284, -0.249196, 0.160944>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.057365, -0.248062, 0.284824>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.037106, 0.067563, 0.429803>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.057504, -0.248349, 0.283923>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.005785, 0.134580, 0.069941>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.044283, 0.019181, 0.445961>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.054489, -0.256368, 0.241534>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.061337, -0.207223, 0.363728>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.059571, -0.240806, 0.306108>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.055597, -0.255584, 0.249265>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.014026, 0.178857, 0.314937>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.050571, -0.028003, 0.450906>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.038422, -0.225447, 0.104764>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.018707, -0.125672, 0.016135>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.025563, 0.137201, 0.379476>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.032537, 0.102763, 0.409696>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.004846, 0.141904, 0.077515>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.003048, 0.084822, 0.028977>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.060962, -0.140572, 0.420964>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.002395, 0.180894, 0.141873>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.004421, 0.148283, 0.085079>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000151, 0.192272, 0.179331>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.061467, -0.240524, 0.307338>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.057744, -0.083256, 0.444155>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.002366, 0.085126, 0.028810>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.001154, 0.185130, 0.152579>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.019039, -0.119677, 0.013103>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.045054, -0.241212, 0.135534>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.002217, 0.175182, 0.127842>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.020414, -0.127185, 0.016308>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.064271, -0.201532, 0.371042>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.034611, 0.100708, 0.411620>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.021007, 0.163491, 0.345919>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.051749, -0.254578, 0.182259>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.049306, 0.007117, 0.448762>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.021754, -0.131956, 0.018362>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.002696, 0.157359, 0.096350>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.056952, -0.059565, 0.449034>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.052388, -0.015857, 0.451116>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.046482, -0.242101, 0.137182>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.024937, 0.151423, 0.363739>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.023836, 0.155907, 0.357689>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010331, -0.045018, -0.004297>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.002120, 0.121873, 0.055263>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.063801, -0.241797, 0.304844>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008225, 0.197558, 0.242446>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.065925, -0.178179, 0.394718>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.030032, 0.133187, 0.385011>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.065244, -0.235928, 0.318555>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.035976, -0.198950, 0.067723>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.035968, -0.198629, 0.067351>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.052805, -0.252922, 0.171884>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.048290, 0.032121, 0.443918>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.050167, -0.246709, 0.148894>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.065632, -0.240148, 0.309277>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000342, 0.166423, 0.109006>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.060373, -0.257478, 0.237674>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.067480, -0.215402, 0.353815>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.016098, 0.187488, 0.296505>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000337, 0.173712, 0.122169>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002970, 0.052357, 0.011053>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.016272, -0.077193, -0.000932>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.050193, 0.022396, 0.446392>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.038726, 0.096837, 0.415128>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.064890, -0.118729, 0.432485>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.000167, 0.151167, 0.085809>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <-0.000103, 0.131122, 0.062800>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.020679, -0.105196, 0.006385>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.040785, -0.212223, 0.082975>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.044775, -0.227157, 0.105198>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052385, -0.249001, 0.155539>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032416, 0.132436, 0.386532>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.003559, 0.189509, 0.160649>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018108, 0.186013, 0.302109>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002632, 0.075046, 0.021008>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.053137, -0.249860, 0.158312>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.044777, 0.068349, 0.431169>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065525, -0.110425, 0.436040>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019167, -0.088494, 0.001173>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037477, 0.110906, 0.405536>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.002662, 0.180207, 0.134316>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027383, 0.157242, 0.357594>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052166, -0.246746, 0.147953>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069655, -0.191571, 0.382499>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014464, 0.195035, 0.269901>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017559, -0.074491, -0.002010>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034289, 0.128308, 0.390848>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057184, -0.017618, 0.451828>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008021, 0.198381, 0.203643>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025040, 0.168624, 0.340327>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063226, -0.071955, 0.447475>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052976, 0.019408, 0.447403>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013506, -0.035946, -0.006106>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006102, 0.193795, 0.174447>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.001956, 0.141360, 0.072401>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005689, 0.191574, 0.165216>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028159, 0.159045, 0.355585>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026675, 0.165223, 0.346298>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019316, -0.079328, -0.001416>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.061464, -0.047259, 0.451163>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023474, 0.176711, 0.325733>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011771, 0.199847, 0.230686>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041098, -0.202643, 0.070382>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042147, 0.096929, 0.415910>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033308, -0.163766, 0.035252>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010997, 0.199947, 0.217491>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.071913, -0.207530, 0.364748>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015052, -0.037380, -0.006554>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018686, -0.067217, -0.003937>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067961, -0.252156, 0.273972>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018807, 0.191938, 0.286361>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063888, -0.258561, 0.226380>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009018, 0.025936, 0.000746>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070359, -0.143737, 0.420062>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018034, -0.060074, -0.005008>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034053, -0.165045, 0.036010>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006184, 0.186451, 0.146861>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018028, -0.059420, -0.005112>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067747, -0.253896, 0.266355>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066984, -0.256111, 0.254109>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010921, 0.010898, -0.002851>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005675, 0.085759, 0.025319>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005615, 0.089996, 0.027815>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072843, -0.213777, 0.356686>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006495, 0.183659, 0.138686>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.062423, -0.038819, 0.451979>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052221, -0.237981, 0.124332>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.012021, 0.200283, 0.212714>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015381, -0.028544, -0.007036>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023122, -0.091069, 0.000686>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013532, -0.009166, -0.005992>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073618, -0.210492, 0.361132>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.005547, 0.126921, 0.055407>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.071839, -0.138155, 0.423381>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052904, -0.237341, 0.122615>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060528, -0.012670, 0.452065>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.036515, 0.138933, 0.381686>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.006562, 0.144609, 0.072962>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.009010, 0.071524, 0.016385>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.011792, 0.198013, 0.185494>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065188, -0.049301, 0.451319>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008566, 0.169038, 0.105518>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010460, 0.060942, 0.011080>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.008645, 0.133662, 0.059700>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045181, -0.204510, 0.071396>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032944, -0.141498, 0.020260>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075272, -0.183968, 0.390337>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027185, -0.103542, 0.003715>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027224, 0.181110, 0.319625>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.010778, 0.106139, 0.035273>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030580, 0.170651, 0.339976>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.054732, -0.237080, 0.121370>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065000, -0.035086, 0.452454>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.013178, 0.053975, 0.007050>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018057, 0.201975, 0.233973>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034020, -0.142554, 0.020586>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.064999, -0.032371, 0.452575>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076325, -0.181127, 0.392991>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028206, 0.181147, 0.320164>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018403, 0.202709, 0.225659>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074265, -0.132760, 0.426448>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066513, -0.258637, 0.204428>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076401, -0.171184, 0.401436>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018011, 0.202277, 0.198531>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017886, 0.201626, 0.191630>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.017058, 0.033382, -0.001004>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026188, 0.190709, 0.296868>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031270, 0.174142, 0.334642>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018951, 0.202979, 0.202962>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016143, 0.066258, 0.010289>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042935, -0.182916, 0.048739>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.014887, 0.137222, 0.058876>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.015594, 0.136757, 0.057937>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076985, -0.160757, 0.409369>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075955, -0.139179, 0.423052>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.016438, 0.166389, 0.093914>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047314, 0.105793, 0.411456>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.061629, -0.249652, 0.153357>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074853, -0.250456, 0.282043>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018657, 0.054763, 0.004616>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018877, 0.053024, 0.003905>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019283, 0.056157, 0.004815>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070923, -0.258995, 0.229626>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055413, -0.230454, 0.107614>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034838, -0.127729, 0.012144>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027041, 0.198433, 0.274614>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023633, -0.009546, -0.010079>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056936, 0.051922, 0.439531>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.019932, 0.188858, 0.134445>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021478, 0.035789, -0.002457>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.018576, 0.132141, 0.051588>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.062715, 0.009881, 0.450209>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056110, 0.058550, 0.437060>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.064914, -0.254235, 0.170898>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079017, -0.185193, 0.389387>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024081, -0.003551, -0.009926>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034112, -0.116572, 0.007036>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077206, -0.140253, 0.422502>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033569, -0.108854, 0.003913>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.023228, 0.202018, 0.177009>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052516, 0.084524, 0.424979>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.021381, 0.073708, 0.010990>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034419, -0.113045, 0.005406>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072368, -0.258784, 0.234441>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.068106, -0.030713, 0.452906>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025599, -0.005352, -0.010679>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045255, -0.181568, 0.046862>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024836, 0.204004, 0.185775>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077284, -0.247739, 0.291229>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030562, -0.062162, -0.009028>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077003, -0.249696, 0.284979>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035117, 0.179603, 0.327417>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022127, 0.164612, 0.086680>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073376, -0.258625, 0.237193>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025870, 0.031695, -0.005369>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.049407, -0.194384, 0.058572>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.025940, 0.194271, 0.141898>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063112, 0.023443, 0.447931>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033376, -0.060292, -0.010462>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.048534, -0.188006, 0.052198>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041397, 0.160259, 0.359404>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081385, -0.211388, 0.360430>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026113, 0.185335, 0.119433>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077768, -0.252320, 0.275643>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072251, -0.050840, 0.451687>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080209, -0.239539, 0.312765>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.058293, 0.062848, 0.435721>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026406, 0.123224, 0.039416>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065467, 0.011359, 0.450273>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.027201, 0.159508, 0.076193>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066154, -0.250853, 0.155978>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079686, -0.246406, 0.295394>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069309, -0.018533, 0.453082>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076037, -0.085417, 0.445161>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056559, 0.079134, 0.428337>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052432, -0.199517, 0.063509>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028388, 0.099536, 0.021554>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073478, -0.056277, 0.451054>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.061561, -0.236060, 0.117054>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070802, -0.257212, 0.185701>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.050391, -0.179189, 0.043374>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077739, -0.256591, 0.255240>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057067, -0.215081, 0.082178>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.062082, 0.050955, 0.440514>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047439, 0.162500, 0.359551>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081232, -0.132782, 0.426717>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035392, 0.029300, -0.009627>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.043161, 0.197365, 0.296358>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.033443, 0.147361, 0.058289>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081957, -0.135312, 0.425390>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052743, 0.137868, 0.387676>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038378, 0.013737, -0.013502>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052709, -0.161211, 0.027475>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057067, -0.195884, 0.058233>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085166, -0.198407, 0.376093>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041203, 0.211643, 0.224413>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077385, -0.066654, 0.449535>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.040771, 0.211712, 0.212182>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065207, -0.235994, 0.116006>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.084089, -0.158404, 0.411235>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045100, 0.202000, 0.284998>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057687, 0.113299, 0.408467>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052042, -0.137828, 0.012018>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.053454, 0.155489, 0.370372>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.050321, 0.183570, 0.330297>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076434, -0.048433, 0.452182>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047522, 0.201609, 0.288488>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056086, -0.164836, 0.029156>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.068541, 0.029886, 0.447061>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.043243, 0.002990, -0.016413>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070309, 0.016466, 0.449876>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066316, -0.230947, 0.105767>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.046512, -0.045731, -0.017040>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042032, 0.196811, 0.131925>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.040978, 0.139309, 0.046544>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082688, -0.255775, 0.260670>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.061529, 0.119987, 0.404888>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.086892, -0.172455, 0.400664>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065607, -0.214827, 0.079422>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067347, 0.063882, 0.436402>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.044059, 0.134831, 0.041133>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.044403, 0.099582, 0.015123>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076563, -0.255692, 0.174036>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.048978, 0.214170, 0.211211>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055824, 0.193798, 0.313831>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057960, -0.121083, 0.001403>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078477, -0.032602, 0.453442>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.083650, -0.094226, 0.442793>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060496, -0.145177, 0.013982>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079253, -0.258413, 0.191925>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074466, -0.244695, 0.134583>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.051156, -0.005700, -0.019412>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060728, 0.173280, 0.351136>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.084056, -0.258988, 0.237309>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.052201, -0.011010, -0.019948>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067986, 0.107812, 0.414761>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070834, 0.076081, 0.432030>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.050920, 0.208851, 0.162583>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089425, -0.235508, 0.321527>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.064633, 0.151230, 0.378828>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.051285, 0.034003, -0.013605>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077861, -0.249637, 0.148676>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056983, 0.210311, 0.266976>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.053475, 0.007640, -0.018842>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.054269, 0.212884, 0.179041>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081476, -0.021476, 0.453731>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088425, -0.113753, 0.435682>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.054807, 0.033507, -0.014653>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091858, -0.230180, 0.331620>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077364, -0.233863, 0.107792>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056600, 0.214413, 0.185881>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060001, 0.212510, 0.260104>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.083617, -0.257128, 0.180961>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074884, -0.212466, 0.073039>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091662, -0.241977, 0.307117>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082338, -0.253226, 0.160679>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055929, 0.203968, 0.140409>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.062797, 0.205538, 0.288891>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091998, -0.242763, 0.305165>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.059545, -0.004507, -0.021423>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074049, 0.119960, 0.407969>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078193, 0.065174, 0.437717>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066509, 0.193518, 0.319935>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082894, 0.000875, 0.452659>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087304, -0.259483, 0.202718>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076291, -0.203424, 0.061017>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088529, -0.071574, 0.448739>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087759, -0.060165, 0.450848>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090732, -0.104449, 0.439287>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.093276, -0.147678, 0.418163>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075013, 0.127915, 0.402252>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080187, 0.065929, 0.437740>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082447, 0.031727, 0.448060>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095816, -0.183594, 0.390750>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090995, -0.259643, 0.227434>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091885, -0.259158, 0.235118>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096825, -0.204716, 0.368484>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.064922, 0.218003, 0.216488>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.062057, 0.147737, 0.047031>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096813, -0.234800, 0.322483>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097712, -0.199835, 0.374085>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096204, -0.246967, 0.293474>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063694, 0.185750, 0.095888>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077123, -0.161900, 0.021564>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098183, -0.212047, 0.359137>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072946, -0.102377, -0.009802>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065410, 0.164846, 0.064998>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072139, 0.207752, 0.287843>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097693, -0.248345, 0.289141>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096835, -0.125631, 0.430122>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066539, 0.162354, 0.061653>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067445, 0.171625, 0.073119>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085998, -0.218330, 0.078714>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067390, 0.102770, 0.010560>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098687, -0.249678, 0.284731>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091404, -0.250307, 0.145439>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089674, 0.022308, 0.450695>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078379, -0.128296, 0.000599>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100480, -0.181715, 0.392150>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070600, 0.213216, 0.162442>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098443, -0.128946, 0.428412>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070832, 0.215227, 0.172086>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070178, 0.198836, 0.119028>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.101014, -0.231605, 0.328211>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100872, -0.166830, 0.404529>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.086370, -0.199618, 0.054512>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.084141, -0.175426, 0.031109>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.071134, 0.114931, 0.017615>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097789, -0.260358, 0.217830>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.084702, 0.141243, 0.392868>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078972, -0.096174, -0.012947>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073972, 0.021946, -0.020952>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.091074, -0.221575, 0.082701>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088667, 0.109380, 0.417340>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085649, -0.162597, 0.020709>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074756, 0.095175, 0.004804>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075690, 0.157336, 0.053677>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079572, -0.063125, -0.021557>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077631, -0.021141, -0.025197>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.099531, -0.075113, 0.448063>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104825, -0.182052, 0.391497>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081780, 0.207880, 0.291791>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089519, 0.131148, 0.402010>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078271, 0.146473, 0.042013>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080846, -0.040007, -0.024791>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100278, -0.257210, 0.172804>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082703, 0.213501, 0.273191>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085821, 0.189280, 0.333904>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.083649, 0.211002, 0.282698>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085049, -0.100127, -0.012364>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080882, 0.209906, 0.144414>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081428, 0.216662, 0.171718>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089618, -0.153685, 0.013967>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095976, -0.216742, 0.074873>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.086556, 0.203109, 0.305918>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.083563, 0.212026, 0.150606>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.086851, 0.218159, 0.252641>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.086134, 0.214376, 0.158659>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104791, -0.257505, 0.173547>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.099177, 0.072903, 0.436947>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088236, 0.220336, 0.236061>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110755, -0.192516, 0.381272>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085942, 0.120657, 0.019371>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110332, -0.171712, 0.400386>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087898, -0.006041, -0.025920>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088634, 0.215421, 0.162214>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089604, -0.052003, -0.024379>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094514, -0.122671, -0.003974>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.101068, 0.093785, 0.427303>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.092285, 0.219996, 0.242336>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090583, -0.000342, -0.025725>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.099872, -0.183590, 0.036687>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.093635, 0.214222, 0.274180>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113070, -0.154574, 0.413157>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096353, 0.197219, 0.320701>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104260, 0.075649, 0.436003>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.093073, 0.173611, 0.070782>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.108383, -0.241311, 0.117995>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.116347, -0.201328, 0.371583>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114376, -0.133955, 0.425692>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100630, 0.182714, 0.346468>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094507, 0.123589, 0.020802>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.107338, -0.219538, 0.078206>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095736, 0.169962, 0.065811>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111884, -0.036860, 0.454407>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.105426, 0.128743, 0.404982>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117538, -0.257450, 0.252323>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.105693, 0.141965, 0.393987>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111227, 0.045159, 0.446619>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.105334, 0.180019, 0.350665>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100639, 0.205125, 0.126825>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.112464, -0.233028, 0.100696>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119388, -0.257706, 0.250345>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117054, -0.258219, 0.178243>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.103334, 0.221936, 0.216828>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.105970, -0.117679, -0.006675>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104064, -0.057666, -0.024205>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104648, -0.058270, -0.024115>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.103594, -0.021432, -0.027082>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.106696, 0.205714, 0.301972>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.116613, -0.249817, 0.141239>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104420, -0.039829, -0.026321>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.115830, 0.010163, 0.453365>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104684, 0.217726, 0.170329>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117333, -0.012398, 0.454937>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.122645, -0.256731, 0.255247>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104380, 0.151372, 0.044558>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104869, 0.177496, 0.075630>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104496, 0.068712, -0.010005>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.107804, 0.221736, 0.224781>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111694, 0.183535, 0.345312>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.124581, -0.253249, 0.271024>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113867, -0.177421, 0.031055>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.124376, -0.152827, 0.413860>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.120015, -0.028282, 0.454702>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.124444, -0.258107, 0.245901>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114047, 0.158256, 0.377827>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114425, 0.169828, 0.364296>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.108310, 0.159967, 0.053722>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.109083, 0.048101, -0.017071>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127137, -0.153659, 0.413075>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.118618, 0.104684, 0.421111>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117727, 0.136966, 0.398075>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.109375, 0.089675, -0.000439>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111755, 0.148393, 0.041817>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127098, -0.074446, 0.447869>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113281, 0.154259, 0.047812>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125345, 0.008290, 0.453103>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130531, -0.144218, 0.418800>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114213, 0.173374, 0.070398>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125518, 0.015079, 0.452233>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114748, 0.030106, -0.021467>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113908, 0.104318, 0.007908>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.123650, -0.194273, 0.047720>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119169, 0.216636, 0.263183>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.115008, 0.052568, -0.015472>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117057, 0.215313, 0.160871>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.123200, 0.168099, 0.365698>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.129290, -0.024471, 0.454197>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130812, -0.255937, 0.169722>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117596, 0.161048, 0.055553>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.118244, 0.142866, 0.037082>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125636, -0.137656, 0.004151>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.123996, -0.097681, -0.013675>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.135689, -0.255042, 0.258317>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.126162, -0.132260, 0.001291>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.128231, 0.165097, 0.368661>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.129506, 0.113527, 0.414605>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.122452, -0.025527, -0.026339>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.133911, -0.255752, 0.170508>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130857, 0.065064, 0.439079>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.131235, 0.117104, 0.411969>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130088, -0.158633, 0.017836>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.139265, -0.204452, 0.365119>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.134378, 0.005740, 0.452515>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125547, -0.017152, -0.026070>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125317, 0.214795, 0.161589>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.127326, -0.047403, -0.024462>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.124236, 0.051315, -0.015148>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141448, -0.209907, 0.357705>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.139809, -0.258122, 0.231470>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.135513, 0.131595, 0.400461>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.138850, -0.231433, 0.102003>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.137894, 0.076249, 0.433664>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.138285, -0.212435, 0.071674>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.129254, 0.005806, -0.024202>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.139636, 0.001699, 0.452208>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.145240, -0.199037, 0.370146>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.145523, -0.184970, 0.384889>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141683, -0.036594, 0.452104>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.131708, 0.218084, 0.183886>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.144690, -0.256511, 0.185915>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141362, -0.205543, 0.063552>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.147309, -0.199550, 0.369033>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.147351, -0.188258, 0.381208>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.132590, 0.210464, 0.148472>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130794, 0.106299, 0.010933>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.139241, -0.133308, 0.003688>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141676, 0.150216, 0.382375>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.135533, 0.219540, 0.208283>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.148077, -0.082040, 0.443143>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.149832, -0.257115, 0.218233>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.139799, -0.072058, -0.018700>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.149026, -0.095092, 0.438966>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.148860, -0.058443, 0.448221>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151831, -0.253309, 0.253683>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151290, -0.254958, 0.183132>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.153926, -0.216337, 0.344965>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.142111, 0.213972, 0.262136>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.143570, -0.094706, -0.011906>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.137843, 0.111249, 0.015371>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.149594, -0.197741, 0.056680>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.139427, 0.190764, 0.103073>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.156294, -0.240037, 0.298217>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.149132, 0.178486, 0.345174>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.156154, -0.145891, 0.413096>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.157464, -0.239301, 0.299388>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.156372, -0.153625, 0.407904>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.154613, -0.067696, 0.445300>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.142363, 0.040100, -0.015695>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.155178, -0.003035, 0.449737>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.159164, -0.238456, 0.300459>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.154315, -0.193736, 0.053786>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.147469, 0.216596, 0.236165>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.156953, -0.018375, 0.449870>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.158678, -0.235666, 0.119515>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.158611, -0.231746, 0.111001>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.145816, 0.049325, -0.012333>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.147064, 0.059210, -0.008751>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.157464, -0.163488, 0.027376>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.147122, 0.141658, 0.041596>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.161075, 0.037002, 0.442098>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.163877, -0.253864, 0.205173>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.150883, -0.026438, -0.021903>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.163868, -0.246873, 0.156224>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.155787, 0.196505, 0.308482>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.165146, -0.136665, 0.416135>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.163951, -0.068854, 0.442777>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.163931, -0.240338, 0.134889>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.166078, -0.165765, 0.395905>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.150901, 0.181618, 0.091887>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152663, 0.212037, 0.171343>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.168692, -0.247219, 0.263585>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.165532, 0.018912, 0.444602>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151338, 0.079915, 0.000880>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.165619, -0.226238, 0.104258>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.169944, -0.230497, 0.311744>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.158092, -0.084696, -0.011542>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.161294, -0.129382, 0.006987>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.170603, -0.146805, 0.408094>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.170373, -0.248634, 0.173255>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.164603, 0.156082, 0.368375>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.166896, 0.119731, 0.401247>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.167796, -0.207615, 0.076015>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.173112, -0.237337, 0.292800>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.166755, -0.182018, 0.046536>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.164079, -0.123454, 0.004880>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.174737, -0.243792, 0.270110>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.173230, -0.084646, 0.435985>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.168134, 0.143675, 0.380084>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.172479, -0.234823, 0.127511>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171622, 0.051782, 0.434765>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.174345, 0.033997, 0.438837>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.164934, 0.209244, 0.257187>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.159841, 0.089595, 0.008262>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.165818, 0.206155, 0.270565>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.164554, -0.033781, -0.018002>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.170019, -0.134574, 0.012739>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.161093, 0.088455, 0.008064>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162269, 0.035914, -0.011806>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.175146, -0.204260, 0.075446>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.167032, 0.211055, 0.239618>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.173379, -0.163669, 0.033391>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.182313, -0.170594, 0.385161>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.175630, 0.130073, 0.389323>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.164651, 0.051723, -0.006453>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.177663, 0.109761, 0.404058>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.170936, 0.202910, 0.276578>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.185267, -0.224551, 0.312334>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.166508, 0.189875, 0.116230>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.184274, -0.126108, 0.415016>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171799, -0.087362, -0.006498>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.166454, 0.169574, 0.081537>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.167644, 0.200321, 0.143272>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.183609, -0.053430, 0.439253>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.177627, 0.146791, 0.372328>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.180461, -0.205096, 0.079727>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171684, 0.208342, 0.249101>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.187318, -0.187181, 0.366214>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171571, 0.209697, 0.194491>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.188713, -0.242321, 0.252893>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.185943, -0.018751, 0.440909>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.187311, -0.062515, 0.436237>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171079, 0.200694, 0.148270>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.169699, 0.152054, 0.061229>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.183628, 0.103630, 0.405460>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179841, -0.141310, 0.020666>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.170558, 0.126422, 0.036996>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.177673, -0.096481, -0.001400>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171831, 0.168675, 0.083428>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.181432, 0.180945, 0.322006>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.194753, -0.185883, 0.363077>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.185535, 0.151786, 0.362165>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.188016, -0.192079, 0.068152>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.196533, -0.215353, 0.320024>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179417, -0.032509, -0.012913>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.194640, -0.242227, 0.192977>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.192909, 0.021489, 0.434216>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.192975, -0.229615, 0.135467>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.188723, 0.141810, 0.371186>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.178469, 0.166165, 0.084161>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179948, 0.199985, 0.157816>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179767, 0.018301, -0.009421>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.179523, 0.179647, 0.106547>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.183239, 0.205823, 0.230997>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.189050, -0.149428, 0.030410>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.185500, -0.077474, -0.003812>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.197860, -0.031985, 0.435442>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.187771, 0.189006, 0.298025>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.183678, 0.203663, 0.183239>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.182170, 0.185663, 0.120843>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.201795, -0.108629, 0.415131>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.200086, -0.017665, 0.434715>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.201104, -0.014789, 0.434170>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.205167, -0.186596, 0.354794>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.200834, 0.018618, 0.430982>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.194012, 0.162626, 0.341867>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.197410, -0.192056, 0.074686>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.192319, 0.182129, 0.309003>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.186104, 0.039239, -0.001912>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.206478, -0.190580, 0.348867>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.204925, -0.237992, 0.225927>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.185541, 0.135651, 0.053079>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.196287, -0.161680, 0.043721>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.192730, 0.195006, 0.272153>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.189559, 0.201167, 0.183999>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.193384, 0.194177, 0.274087>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.194689, 0.188591, 0.290020>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.205646, -0.231226, 0.160805>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.196984, 0.175392, 0.317756>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.211196, -0.229185, 0.263644>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.208395, -0.231832, 0.169840>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.209634, -0.235286, 0.199318>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.205093, -0.199521, 0.090939>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.209819, 0.005691, 0.428158>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.195051, 0.179333, 0.121206>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.212424, -0.231687, 0.181406>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.201473, -0.094683, 0.009229>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.197322, 0.007138, -0.003162>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.214235, -0.230216, 0.178906>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.197683, 0.121560, 0.048106>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.218371, -0.206403, 0.312483>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.200716, 0.192986, 0.169908>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.200268, 0.182202, 0.134356>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.219344, -0.097820, 0.409201>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.220445, -0.225858, 0.253129>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.214130, 0.096961, 0.391857>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.220052, -0.056597, 0.420476>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.222411, -0.194280, 0.328065>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.204174, 0.011307, 0.001107>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.203972, 0.054443, 0.012363>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.203873, 0.156688, 0.091506>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.223244, -0.126573, 0.392287>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.218188, 0.052718, 0.411701>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.207328, 0.194404, 0.214389>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.224389, -0.198496, 0.318869>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.205627, 0.022302, 0.003952>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.209775, 0.187357, 0.265597>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.206342, 0.139850, 0.072528>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.222797, -0.225637, 0.183758>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.218817, 0.085755, 0.395478>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.216397, 0.131539, 0.360912>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.209398, -0.018976, 0.001467>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.213316, 0.172647, 0.302734>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.226808, -0.167757, 0.356689>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.207958, 0.173456, 0.124963>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.225723, -0.223898, 0.244152>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.210147, 0.004795, 0.003637>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.210210, 0.008128, 0.004130>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.223483, 0.017744, 0.417898>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.227779, -0.223422, 0.236874>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.217654, -0.129817, 0.035940>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.230563, -0.141208, 0.376556>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.225194, 0.041799, 0.410635>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.213906, 0.062299, 0.021783>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.229541, -0.222878, 0.200327>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.223853, 0.109033, 0.375179>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.223284, -0.164903, 0.067294>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.233104, -0.160264, 0.357944>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.216599, 0.061089, 0.023047>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.228516, -0.167176, 0.074694>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.223077, 0.182766, 0.243313>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.231032, 0.069572, 0.394792>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.237393, -0.108844, 0.390601>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.236749, -0.218046, 0.217041>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.230364, 0.141956, 0.333343>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.227999, 0.174570, 0.267654>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.241253, -0.094632, 0.393729>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.235884, -0.176326, 0.093434>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.239300, 0.073188, 0.385641>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.234320, -0.023562, 0.017994>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.239189, 0.132176, 0.335156>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.242467, -0.081048, 0.033644>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.240164, 0.119055, 0.083462>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.247417, -0.160030, 0.089000>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.247404, -0.078853, 0.037291>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.256745, -0.022108, 0.394726>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.253414, -0.152460, 0.088773>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.255695, 0.018675, 0.040929>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.256280, 0.153860, 0.254004>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.264402, -0.135845, 0.087959>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.270779, -0.167384, 0.291653>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.269456, -0.016865, 0.381633>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.263587, 0.127874, 0.131731>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.271672, -0.053913, 0.375864>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.273384, -0.065057, 0.371120>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.275046, -0.128161, 0.334212>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.274684, -0.179829, 0.239197>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.270981, 0.017564, 0.057383>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.274048, 0.027356, 0.063999>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.280037, -0.145723, 0.305587>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.279322, -0.043701, 0.368296>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.277271, 0.090191, 0.107791>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.279174, 0.078851, 0.332981>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.280036, -0.091222, 0.079855>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.282016, -0.158984, 0.153823>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.285657, -0.003532, 0.360325>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.288619, -0.107668, 0.327963>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.285714, 0.116424, 0.260357>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.292795, 0.020143, 0.344153>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.294284, 0.097391, 0.272927>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.299651, -0.018279, 0.094120>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.296659, 0.103502, 0.241210>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.300217, 0.087639, 0.164817>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.298736, -0.056320, 0.335982>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.300642, -0.066292, 0.103934>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.302791, -0.016825, 0.100136>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.301664, -0.136023, 0.180610>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.306437, -0.124759, 0.255286>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.313961, -0.091397, 0.155872>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.313951, -0.106484, 0.179252>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.312937, -0.104978, 0.262592>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.325821, 0.008837, 0.177672>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.326144, -0.071368, 0.222223>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.329704, -0.020811, 0.240551>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
}

