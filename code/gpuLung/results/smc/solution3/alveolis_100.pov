global_settings {
  max_trace_level 10
  assumed_gamma 1
}

camera {
  location    <-1.300000, -1.300000, 0.000000>
  look_at     <0.000000, 0.000000, 0>
}

light_source {
  <-5.000000, 0.500000, -2.000000>*500
  color rgb <2.0, 1.5, 1.0>*1.8
  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient
}

light_source {
  <5.000000, 0.500000, 2.000000>*500
  color rgb <2.0, 1.5, 1.0>*1.8
  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient
}

blob {
  threshold 0.5
  sphere {
    <0.000000, 0.000000, 0.000000>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.001644, 0.112259, 0.067513>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.001559, 0.087082, 0.038044>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.009028, -0.156779, 0.094746>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002528, -0.061537, 0.006344>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000495, 0.138028, 0.148995>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <-0.001264, 0.091782, 0.041925>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.014957, -0.104970, 0.277377>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002200, 0.136136, 0.180045>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006369, 0.100839, 0.253595>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008118, -0.134166, 0.053841>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.016239, -0.118007, 0.268469>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.009457, 0.062639, 0.285825>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.009166, -0.139773, 0.059858>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.015325, -0.166160, 0.189086>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.000360, 0.081098, 0.030004>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007410, -0.115844, 0.034110>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.002611, -0.000981, -0.003221>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.005221, -0.069744, 0.006335>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007661, -0.108048, 0.026791>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.018269, -0.123229, 0.266370>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006757, -0.085568, 0.012363>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.011045, 0.067951, 0.284927>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008669, 0.099646, 0.258373>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.003371, 0.141208, 0.136749>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.015083, -0.170104, 0.122968>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.017891, -0.044838, 0.307712>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.018798, -0.158588, 0.218863>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010796, 0.083897, 0.274515>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006043, -0.046839, -0.002567>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007983, -0.083450, 0.009598>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.019627, -0.078114, 0.298363>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.006706, 0.137411, 0.194577>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.009370, 0.113650, 0.244184>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007146, 0.136119, 0.199746>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.016762, -0.171206, 0.119214>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.020379, -0.167061, 0.202579>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.015667, 0.042548, 0.301208>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.020866, -0.167698, 0.202139>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.004290, 0.101355, 0.042044>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.018355, 0.008432, 0.311227>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.020635, -0.174704, 0.174531>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.023177, -0.141251, 0.253625>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.023079, -0.163941, 0.216989>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.021962, -0.174965, 0.179765>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.011238, 0.129092, 0.225138>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.021685, -0.024119, 0.314608>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.017819, -0.159385, 0.076922>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.013148, -0.090891, 0.007485>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.015659, 0.096339, 0.271210>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.017790, 0.070047, 0.291588>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.007174, 0.108971, 0.045599>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008102, 0.066509, 0.009640>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.026674, -0.100396, 0.293755>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008815, 0.138464, 0.092887>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.008290, 0.114500, 0.050441>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.009802, 0.146894, 0.121626>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.027933, -0.167598, 0.219377>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.028054, -0.062856, 0.312485>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010253, 0.067898, 0.007700>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.010928, 0.143355, 0.100441>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.016841, -0.088221, 0.001608>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.024645, -0.174077, 0.098071>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.011373, 0.136712, 0.081077>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.017795, -0.093865, 0.003758>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.030887, -0.142559, 0.262592>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.022907, 0.071088, 0.296576>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.019263, 0.121459, 0.250605>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.028214, -0.183502, 0.132797>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.028776, 0.001033, 0.321388>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.019853, -0.097969, 0.003935>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.013118, 0.125028, 0.056142>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.031641, -0.047248, 0.319907>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.030497, -0.016131, 0.322986>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.028374, -0.177523, 0.098224>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.022643, 0.113146, 0.264818>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.022356, 0.116908, 0.260461>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.018185, -0.032775, -0.018453>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.015074, 0.099055, 0.023498>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.035166, -0.173988, 0.219896>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.018975, 0.156209, 0.171350>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.036852, -0.130192, 0.282026>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.026737, 0.099965, 0.281746>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.037659, -0.171290, 0.230079>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.029002, -0.149975, 0.040921>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.029213, -0.149897, 0.040500>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.035148, -0.188457, 0.123728>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.035098, 0.020972, 0.324472>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.035446, -0.185524, 0.105554>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.040281, -0.176316, 0.224417>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.019621, 0.136772, 0.062751>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.038789, -0.190651, 0.173159>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.041348, -0.158634, 0.255818>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.025604, 0.149582, 0.215821>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.020333, 0.142937, 0.072964>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.021550, 0.046182, -0.014398>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.025965, -0.058911, -0.020072>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.037489, 0.013901, 0.327442>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.034004, 0.073062, 0.306429>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.042617, -0.091286, 0.313535>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.5> }
    }
  }
  sphere {
    <0.021877, 0.126460, 0.043228>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.022235, 0.110798, 0.024850>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.028889, -0.080894, -0.014565>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035977, -0.163469, 0.050454>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037446, -0.174625, 0.068833>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.040162, -0.190205, 0.109792>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034023, 0.103518, 0.286884>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024641, 0.158174, 0.103031>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030273, 0.151501, 0.221431>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.024935, 0.066146, -0.009593>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041606, -0.191703, 0.111715>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.039748, 0.051446, 0.320513>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.046704, -0.086259, 0.319144>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031204, -0.068360, -0.021788>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037773, 0.086730, 0.302270>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.026320, 0.152457, 0.080687>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034711, 0.126320, 0.266095>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042668, -0.190670, 0.102992>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.048330, -0.145310, 0.279347>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030737, 0.161893, 0.195145>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.031656, -0.057485, -0.025662>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037654, 0.101765, 0.291917>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045928, -0.016378, 0.335027>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029694, 0.167891, 0.138739>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.036132, 0.137997, 0.253509>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.049116, -0.058273, 0.330908>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045923, 0.013104, 0.333917>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.032458, -0.026210, -0.032338>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030326, 0.165671, 0.113532>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.029306, 0.123172, 0.028588>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.030938, 0.164519, 0.105462>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038776, 0.130546, 0.266239>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038803, 0.136499, 0.259043>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.035897, -0.061630, -0.028089>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.050824, -0.039755, 0.336087>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038255, 0.147428, 0.242636>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.034893, 0.171715, 0.161814>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.044806, -0.161319, 0.035117>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045961, 0.078185, 0.314134>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042867, -0.130691, 0.003928>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.036195, 0.173409, 0.150233>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056280, -0.161008, 0.269729>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037583, -0.027289, -0.036437>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038940, -0.052118, -0.033231>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055594, -0.196428, 0.201823>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.039720, 0.164568, 0.210387>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.054531, -0.202998, 0.164835>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.036226, 0.027104, -0.032788>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056835, -0.113658, 0.312208>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.039568, -0.046117, -0.035079>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.044878, -0.132413, 0.003455>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.036313, 0.164349, 0.088389>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.039825, -0.045613, -0.035388>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056770, -0.198814, 0.196103>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057383, -0.201448, 0.186718>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038975, 0.014866, -0.037014>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037580, 0.080101, -0.014554>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.037999, 0.083891, -0.012793>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060037, -0.167613, 0.265073>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.038885, 0.163729, 0.080592>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057465, -0.033145, 0.341450>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.054304, -0.192266, 0.079658>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.041451, 0.177619, 0.145571>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042336, -0.019536, -0.040753>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.044849, -0.072794, -0.031621>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.042430, -0.002718, -0.041066>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.062816, -0.166436, 0.269408>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.040857, 0.116971, 0.008848>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063677, -0.111477, 0.318431>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057842, -0.193708, 0.077069>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060592, -0.012199, 0.344900>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.053182, 0.119320, 0.292235>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.043523, 0.133259, 0.022667>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.045399, 0.069875, -0.026387>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047243, 0.180263, 0.120658>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.064358, -0.041597, 0.344485>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.046936, 0.155663, 0.049606>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047807, 0.061029, -0.031995>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.047859, 0.125448, 0.009709>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.059627, -0.169626, 0.029466>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056098, -0.116910, -0.018109>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069883, -0.148867, 0.294686>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055027, -0.084919, -0.034486>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.056552, 0.162941, 0.242139>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.051225, 0.102287, -0.012605>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.058332, 0.152950, 0.260003>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.065897, -0.197662, 0.073635>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069222, -0.030086, 0.348852>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.053015, 0.055751, -0.037815>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.055515, 0.186681, 0.164564>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060092, -0.118791, -0.019898>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070319, -0.027884, 0.349684>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074389, -0.148182, 0.298606>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060392, 0.165017, 0.243346>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.057569, 0.188600, 0.156852>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074698, -0.110076, 0.326375>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073235, -0.215015, 0.146110>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075963, -0.140743, 0.306122>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.059310, 0.189499, 0.131452>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.059606, 0.189141, 0.125064>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.058976, 0.037619, -0.047186>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063223, 0.176927, 0.222965>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.064745, 0.159883, 0.256862>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.060756, 0.190679, 0.135464>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.059497, 0.068140, -0.037601>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069053, -0.155322, 0.003657>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.059665, 0.132280, 0.005339>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.061012, 0.132242, 0.004106>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080112, -0.133813, 0.314261>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080348, -0.116506, 0.325939>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.062204, 0.158997, 0.035775>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072570, 0.095889, 0.323457>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077461, -0.212267, 0.100295>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081152, -0.208623, 0.211197>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063643, 0.058218, -0.044138>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.063964, 0.056652, -0.044882>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.064979, 0.059712, -0.044453>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082078, -0.218432, 0.167223>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.077987, -0.197976, 0.057377>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.071850, -0.108417, -0.034285>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069646, 0.187954, 0.202936>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.068123, -0.000883, -0.057977>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078899, 0.047333, 0.347548>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067323, 0.180847, 0.071374>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.067839, 0.040960, -0.051862>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.066790, 0.129571, -0.003214>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080864, 0.010092, 0.355264>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.079353, 0.053643, 0.345930>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082689, -0.217627, 0.115430>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085809, -0.155296, 0.299702>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.069594, 0.004958, -0.058517>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073734, -0.098733, -0.040463>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085895, -0.118531, 0.327801>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.074478, -0.091878, -0.044063>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.071112, 0.194001, 0.110426>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080413, 0.078119, 0.337173>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.070243, 0.076845, -0.040751>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075555, -0.095763, -0.042934>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.086752, -0.220150, 0.171303>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085079, -0.026286, 0.357359>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.072468, 0.003336, -0.060212>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080177, -0.157286, -0.002738>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073633, 0.196638, 0.118534>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.089631, -0.209845, 0.219649>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.075895, -0.049088, -0.058356>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090145, -0.211917, 0.214467>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078679, 0.171453, 0.252831>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.073607, 0.160781, 0.026767>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090366, -0.221527, 0.173661>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.076419, 0.038377, -0.057348>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.087228, -0.170421, 0.006465>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.078900, 0.189475, 0.076649>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.090371, 0.023209, 0.358146>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.081936, -0.047674, -0.061753>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.088025, -0.164926, -0.000215>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085847, 0.154112, 0.283037>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096306, -0.180338, 0.278643>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.080414, 0.181706, 0.055602>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096730, -0.217130, 0.207125>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095310, -0.043823, 0.359696>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097847, -0.205496, 0.238827>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.092199, 0.060539, 0.349962>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.082345, 0.125002, -0.018188>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094622, 0.012511, 0.361286>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.083935, 0.158808, 0.015221>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096909, -0.221147, 0.099311>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.099833, -0.212536, 0.224326>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096843, -0.014384, 0.363136>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098696, -0.074169, 0.353832>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094577, 0.076788, 0.344833>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.094356, -0.176608, 0.009032>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.085606, 0.103375, -0.035483>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098793, -0.048646, 0.360269>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098937, -0.210024, 0.061276>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.102357, -0.227432, 0.126566>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.096840, -0.158660, -0.011871>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.103903, -0.224354, 0.189666>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100165, -0.191980, 0.026198>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.101428, 0.051053, 0.356781>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.098901, 0.159724, 0.285066>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.107397, -0.116131, 0.339212>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.095920, 0.038003, -0.066731>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100041, 0.195497, 0.225341>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.097161, 0.150258, -0.003745>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110208, -0.118688, 0.338864>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104394, 0.136682, 0.311660>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.100895, 0.023136, -0.071440>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.106238, -0.143348, -0.029781>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.107893, -0.176232, 0.000482>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114565, -0.174342, 0.295982>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104098, 0.211986, 0.154773>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.112997, -0.058173, 0.363462>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.104327, 0.212248, 0.142665>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111958, -0.213542, 0.057578>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.114712, -0.139464, 0.327219>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.105647, 0.201709, 0.214482>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.109815, 0.113733, 0.331510>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.109294, -0.121531, -0.045836>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.109693, 0.155167, 0.296363>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110319, 0.183540, 0.258540>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.116140, -0.041413, 0.367612>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110409, 0.202270, 0.218009>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.112859, -0.147348, -0.029530>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.115899, 0.033070, 0.366130>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.110368, 0.013089, -0.076098>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117131, 0.020201, 0.368540>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.117855, -0.210420, 0.046292>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.111765, -0.034259, -0.076100>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.112976, 0.199713, 0.063718>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.113277, 0.145174, -0.017082>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.124237, -0.229872, 0.194706>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119452, 0.121773, 0.329726>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125110, -0.153550, 0.320164>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.122102, -0.196015, 0.018987>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.121687, 0.066905, 0.358800>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119638, 0.141609, -0.022976>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.119655, 0.107909, -0.047772>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.128032, -0.233889, 0.112639>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.122118, 0.217905, 0.141141>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.124748, 0.196545, 0.243206>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.125322, -0.106504, -0.059355>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.129066, -0.026000, 0.372937>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130051, -0.083659, 0.361100>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.126645, -0.129608, -0.047001>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.131330, -0.236527, 0.129794>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.131488, -0.225364, 0.073176>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.126564, 0.004967, -0.081555>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.129252, 0.176241, 0.279578>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.135122, -0.236280, 0.172883>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.128230, -0.000320, -0.082227>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.131350, 0.111395, 0.340306>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.132260, 0.080170, 0.356573>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.129711, 0.213668, 0.092520>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.137052, -0.212922, 0.250550>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.132532, 0.154610, 0.306282>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.130441, 0.044499, -0.076701>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.137187, -0.230724, 0.086746>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.133521, 0.214974, 0.196828>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.132868, 0.018523, -0.081889>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.135590, 0.218240, 0.108525>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.140242, -0.014676, 0.375992>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.142058, -0.102136, 0.356532>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.137873, 0.044322, -0.078436>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.144240, -0.209232, 0.260227>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.143505, -0.216628, 0.045072>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.140008, 0.220144, 0.115178>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.140657, 0.218000, 0.189770>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.145486, -0.238279, 0.117971>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.144260, -0.195831, 0.009942>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.146272, -0.220836, 0.237845>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.146040, -0.235068, 0.097842>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.141235, 0.209792, 0.070320>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.143194, 0.210876, 0.218672>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.147544, -0.221805, 0.236110>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.144355, 0.006723, -0.085302>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.146608, 0.124997, 0.334983>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.147228, 0.070890, 0.363357>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.146784, 0.198752, 0.249463>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.148477, 0.007589, 0.376974>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.150999, -0.240700, 0.138940>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.149310, -0.187137, -0.002655>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.150141, -0.062547, 0.371372>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.150070, -0.051652, 0.373699>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.150533, -0.093690, 0.361604>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151497, -0.133987, 0.341112>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.149965, 0.133114, 0.329664>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.151833, 0.072029, 0.363685>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.152115, 0.038171, 0.373364>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.155376, -0.167439, 0.315743>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.156443, -0.240878, 0.162701>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.157463, -0.240337, 0.170094>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.157544, -0.187235, 0.295223>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.154717, 0.224570, 0.145405>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.155359, 0.156052, -0.019537>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.159549, -0.215824, 0.252646>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.160153, -0.182946, 0.300641>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.160883, -0.227860, 0.225555>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.157397, 0.192537, 0.027002>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.159532, -0.146463, -0.042793>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.161503, -0.194511, 0.286791>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.160212, -0.088317, -0.073924>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.160984, 0.172444, -0.002442>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162496, 0.214194, 0.217146>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.165480, -0.229767, 0.221450>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.164593, -0.113920, 0.354328>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.162957, 0.170050, -0.005613>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.164302, 0.178914, 0.005306>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.166874, -0.202161, 0.013947>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.164743, 0.112868, -0.054556>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.168626, -0.231412, 0.217289>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.168837, -0.233797, 0.081342>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.167105, 0.029729, 0.376572>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.167571, -0.113399, -0.063818>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.168806, -0.166687, 0.318236>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.167673, 0.219600, 0.091467>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.168642, -0.117113, 0.353040>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.167780, 0.221642, 0.100901>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.167985, 0.205197, 0.049186>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.170830, -0.213989, 0.258122>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171066, -0.152851, 0.330149>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171846, -0.183338, -0.010643>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.171785, -0.159255, -0.033978>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.170996, 0.124532, -0.047547>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.174134, -0.242932, 0.152718>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.172528, 0.147827, 0.320455>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.173258, -0.081657, -0.077218>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.174018, 0.033847, -0.085157>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.176931, -0.205217, 0.017658>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.175733, 0.116291, 0.344329>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.176982, -0.146304, -0.044312>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.176815, 0.105363, -0.059694>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.177693, 0.164823, -0.012862>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.178157, -0.049240, -0.085550>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.178002, -0.008173, -0.089141>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.178412, -0.065453, 0.373217>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.180543, -0.167544, 0.318132>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.180566, 0.214202, 0.220456>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.180652, 0.138070, 0.329061>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.181816, 0.154271, -0.023744>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.182291, -0.026663, -0.088491>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.184437, -0.240204, 0.108066>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.183597, 0.219602, 0.201830>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.183784, 0.195596, 0.262026>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.184564, 0.217066, 0.211252>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.184460, -0.085096, -0.076313>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.184392, 0.215126, 0.073875>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.184888, 0.221981, 0.100499>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.186227, -0.137039, -0.050814>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.188183, -0.199722, 0.009533>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.187910, 0.209013, 0.234224>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.188355, 0.216933, 0.079977>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.191344, 0.223537, 0.180948>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.192119, 0.219015, 0.087880>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.193760, -0.240030, 0.108454>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.192695, 0.080460, 0.362715>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.194029, 0.225399, 0.164364>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.194526, -0.177016, 0.308518>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.193764, 0.128794, -0.044623>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.194564, -0.157310, 0.326700>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.195300, 0.006465, -0.088402>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.195822, 0.219742, 0.091402>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.195954, -0.037912, -0.086980>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.198931, -0.106059, -0.067373>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.198564, 0.101014, 0.352800>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.199859, 0.224530, 0.170534>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.199387, 0.012070, -0.087717>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.201517, -0.165426, -0.028119>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.200992, 0.218850, 0.202065>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.201450, -0.140682, 0.338639>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.203030, 0.202047, 0.247798>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.202877, 0.083194, 0.360908>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.203868, 0.178095, 0.004513>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.206995, -0.222923, 0.052807>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.206230, -0.184725, 0.298877>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.205655, -0.120968, 0.350359>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.208115, 0.187504, 0.272592>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.206697, 0.130381, -0.042189>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.209706, -0.200644, 0.013224>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.207900, 0.174219, 0.000172>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.208217, -0.027266, 0.378279>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.210657, 0.134872, 0.329554>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.212659, -0.238143, 0.184702>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.212389, 0.147621, 0.318531>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.213490, 0.053190, 0.370134>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.215058, 0.184317, 0.275915>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.214243, 0.207132, 0.058074>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.216644, -0.213264, 0.035798>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.216456, -0.237926, 0.182624>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.217546, -0.238336, 0.112507>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.216478, 0.223916, 0.144975>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.217161, -0.099724, -0.068004>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.216979, -0.042924, -0.084091>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.217777, -0.043479, -0.083882>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.217723, -0.007514, -0.086542>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.218879, 0.208429, 0.228214>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.220128, -0.229623, 0.076035>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.218186, -0.025419, -0.085849>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.219436, 0.019031, 0.375929>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.219449, 0.218849, 0.099835>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.220609, -0.003247, 0.377309>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.222458, -0.236132, 0.187101>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.221148, 0.155287, -0.018207>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.221330, 0.179707, 0.010665>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.221445, 0.077364, -0.069420>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.222767, 0.222792, 0.152716>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.224389, 0.186576, 0.269714>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.225222, -0.232363, 0.202092>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.225515, -0.156498, -0.031535>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.224665, -0.137230, 0.337451>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.224542, -0.018264, 0.376485>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.226426, -0.236932, 0.177880>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.226334, 0.162326, 0.300864>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.227515, 0.173218, 0.287600>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.226797, 0.162525, -0.009024>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.227790, 0.058025, -0.074932>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.229915, -0.137550, 0.336016>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.230580, 0.110431, 0.342358>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.230734, 0.141559, 0.319809>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.228825, 0.096570, -0.059100>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.232054, 0.151067, -0.019270>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.234552, -0.062172, 0.368218>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.234206, 0.156134, -0.013447>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.236550, 0.017320, 0.372702>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.236705, -0.127900, 0.340435>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.235191, 0.173633, 0.007324>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.237226, 0.023912, 0.371695>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.235606, 0.040957, -0.077581>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.235522, 0.109798, -0.050027>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.238652, -0.171463, -0.013835>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.237801, 0.215533, 0.189419>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.236582, 0.061203, -0.071854>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.237737, 0.213189, 0.091415>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.239461, 0.169630, 0.287602>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.241447, -0.013960, 0.372858>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.242828, -0.231598, 0.104092>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.240400, 0.161382, -0.005483>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.241617, 0.144501, -0.022117>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.243667, -0.115965, -0.053850>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.243235, -0.078430, -0.070019>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.245869, -0.230442, 0.188715>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.244608, -0.110740, -0.056297>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.246145, 0.165739, 0.289509>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.246511, 0.117658, 0.333055>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.244549, -0.010757, -0.080635>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.247711, -0.230321, 0.104934>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.247448, 0.071152, 0.356701>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.248942, 0.120737, 0.330076>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.248731, -0.135610, -0.040189>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.249857, -0.182390, 0.288289>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.250893, 0.014894, 0.368991>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.249318, -0.003246, -0.079159>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.249578, 0.209991, 0.092654>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.250404, -0.030986, -0.077762>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.249906, 0.059153, -0.068548>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.253500, -0.186791, 0.280884>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.253852, -0.231481, 0.162973>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.254844, 0.133116, 0.318251>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.257554, -0.204135, 0.039967>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.257268, 0.081296, 0.349186>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.257739, -0.185459, 0.011165>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.255541, 0.018022, -0.075597>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.258445, 0.011001, 0.366850>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.259936, -0.175444, 0.291341>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.260744, -0.162256, 0.304780>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.260320, -0.024645, 0.366450>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.258103, 0.211163, 0.113742>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.262835, -0.227527, 0.119827>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.261937, -0.177948, 0.004184>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.263201, -0.175245, 0.289726>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.263536, -0.164761, 0.300869>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.260262, 0.203031, 0.081171>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.259915, 0.108648, -0.042070>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.262274, -0.109759, -0.050226>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.263560, 0.148900, 0.299582>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.262789, 0.211610, 0.136484>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.268074, -0.066348, 0.356293>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.269006, -0.226578, 0.150004>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.266478, -0.052967, -0.068606>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.269018, -0.078241, 0.352267>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.269809, -0.044825, 0.360313>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.270852, -0.222667, 0.182784>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.271863, -0.223566, 0.117413>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.272726, -0.188478, 0.266016>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.269841, 0.205222, 0.186254>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.270365, -0.072887, -0.061853>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.269916, 0.111322, -0.035616>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.272850, -0.168268, -0.000158>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.270909, 0.181757, 0.041502>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.276227, -0.209122, 0.223028>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.275143, 0.172303, 0.262531>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.277346, -0.123555, 0.326650>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.277791, -0.208014, 0.223917>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.277464, -0.130472, 0.322008>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.277245, -0.052726, 0.355537>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.275207, 0.047398, -0.061945>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.278864, 0.006603, 0.358850>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.280061, -0.206594, 0.224626>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.279173, -0.163024, -0.001576>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.278269, 0.204694, 0.161727>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.280926, -0.007394, 0.358412>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.282504, -0.202018, 0.059095>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.282621, -0.198274, 0.051303>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.280199, 0.055391, -0.057186>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.282143, 0.063967, -0.053280>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.284985, -0.133537, -0.023799>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.282705, 0.135148, -0.009515>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.286316, 0.042249, 0.349479>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.287252, -0.217969, 0.137798>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.284162, -0.010514, -0.065117>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.288127, -0.210951, 0.093272>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.285994, 0.185175, 0.227008>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.289146, -0.113146, 0.326182>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.288885, -0.052644, 0.349709>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.288698, -0.204578, 0.073807>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.289719, -0.138880, 0.308178>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.287025, 0.168729, 0.034468>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.287625, 0.196256, 0.103460>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.292354, -0.210550, 0.190241>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.291616, 0.025442, 0.350137>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.288316, 0.081181, -0.042650>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.291725, -0.190329, 0.046343>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.293602, -0.195355, 0.232985>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.290641, -0.061296, -0.054971>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.292087, -0.101050, -0.039180>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.295571, -0.120827, 0.317319>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.295649, -0.210242, 0.109173>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.294238, 0.147580, 0.279563>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.295325, 0.115547, 0.309015>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.295599, -0.171732, 0.021735>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.297475, -0.200110, 0.215658>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.295984, -0.147737, -0.004279>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.296125, -0.094958, -0.039556>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.299439, -0.205157, 0.195424>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.299613, -0.065608, 0.340196>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.298139, 0.135762, 0.289098>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.299497, -0.195896, 0.068377>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.299534, 0.054415, 0.338349>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.302546, 0.038408, 0.340957>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.300622, 0.190487, 0.179038>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.299892, 0.086975, -0.032303>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.301142, 0.187861, 0.190727>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.302060, -0.016522, -0.054885>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.303213, -0.103175, -0.030674>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.301504, 0.085728, -0.031842>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.301963, 0.041890, -0.048584>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.305139, -0.165513, 0.023198>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.304211, 0.190196, 0.163421>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.305651, -0.128481, -0.012708>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.308673, -0.138585, 0.293623>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.307104, 0.121654, 0.294689>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.305433, 0.055026, -0.042361>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.308767, 0.103722, 0.307295>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.307552, 0.182545, 0.195235>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.311550, -0.184080, 0.230626>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.307377, 0.168530, 0.058745>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.311642, -0.099742, 0.318018>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.308462, -0.061041, -0.043837>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.307951, 0.151080, 0.030495>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.308236, 0.177374, 0.081287>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.312082, -0.037787, 0.338103>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.310537, 0.135026, 0.278356>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.311632, -0.164100, 0.028470>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.309822, 0.185637, 0.171105>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.314141, -0.151293, 0.275956>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.311743, 0.184734, 0.124515>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.315965, -0.197549, 0.178971>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.315461, -0.008050, 0.337909>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.316244, -0.044697, 0.333928>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.312467, 0.175895, 0.086006>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.312356, 0.135045, 0.015455>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.316014, 0.096809, 0.305854>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.315303, -0.106032, -0.019701>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.313735, 0.113680, -0.003462>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.315340, -0.067944, -0.036768>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.314840, 0.147565, 0.033817>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.318218, 0.160486, 0.232337>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.322592, -0.147431, 0.270854>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.321024, 0.135554, 0.266169>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.321891, -0.148462, 0.021386>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.324468, -0.171330, 0.234519>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.320990, -0.014288, -0.041750>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.324310, -0.193154, 0.127514>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.324627, 0.025614, 0.327714>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.324499, -0.181216, 0.078601>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.324310, 0.126255, 0.272786>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.323014, 0.142099, 0.036815>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.323268, 0.170213, 0.095087>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.323473, 0.025515, -0.036652>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.323861, 0.152648, 0.054556>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.324891, 0.175835, 0.154764>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.326152, -0.109683, -0.007604>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.326122, -0.049800, -0.033111>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.328999, -0.018623, 0.327088>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.327302, 0.162558, 0.209870>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.326991, 0.171803, 0.115836>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.326784, 0.156201, 0.066415>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.331783, -0.080842, 0.309797>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.331922, -0.005817, 0.324827>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.333146, -0.003426, 0.323698>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.334346, -0.143568, 0.260339>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.333803, 0.022376, 0.320419>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.332314, 0.139750, 0.245058>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.332962, -0.143974, 0.030249>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.332012, 0.155019, 0.217764>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.331502, 0.041222, -0.026025>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.335734, -0.146320, 0.255022>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.335361, -0.184425, 0.154910>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.331671, 0.114523, 0.016202>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.333944, -0.117343, 0.006045>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.334298, 0.163165, 0.187139>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.333929, 0.165896, 0.116854>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.334980, 0.162169, 0.188604>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.335714, 0.157772, 0.201357>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.338379, -0.176024, 0.101489>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.336889, 0.147456, 0.223540>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.341651, -0.174175, 0.184901>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.341227, -0.175066, 0.109083>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.341566, -0.178284, 0.133033>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.341138, -0.146596, 0.045780>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.343530, 0.011750, 0.312759>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.341467, 0.143280, 0.070257>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.345325, -0.172772, 0.118715>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.343593, -0.060152, -0.013820>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.343150, 0.015818, -0.020459>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.347436, -0.170251, 0.116906>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.345074, 0.098231, 0.018737>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.348976, -0.152330, 0.222066>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.346569, 0.151395, 0.107260>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.346733, 0.142498, 0.081265>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.351072, -0.067945, 0.294792>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.352066, -0.165162, 0.175035>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.351520, 0.081501, 0.277588>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.352815, -0.035421, 0.301888>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.353161, -0.140754, 0.232595>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.350386, 0.018088, -0.012397>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.350997, 0.048706, -0.003299>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.351081, 0.120701, 0.052097>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.354684, -0.088222, 0.280318>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.354125, 0.047307, 0.292291>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.352049, 0.150033, 0.140524>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.355175, -0.142918, 0.224754>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.352124, 0.025445, -0.008979>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.352927, 0.145801, 0.178467>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.353712, 0.107082, 0.040131>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.356634, -0.160611, 0.121219>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.356040, 0.071535, 0.277866>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.355623, 0.105460, 0.250633>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.354699, -0.001765, -0.009659>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.355212, 0.134485, 0.206002>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.357847, -0.117947, 0.252236>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.354758, 0.130703, 0.076670>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.357897, -0.159633, 0.167182>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.356087, 0.013284, -0.006604>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.356225, 0.015548, -0.006088>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.358439, 0.020020, 0.294798>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.360252, -0.157434, 0.161497>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.358730, -0.081768, 0.013639>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.361855, -0.096437, 0.264508>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.360801, 0.038444, 0.286965>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.360410, 0.051754, 0.009947>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.363010, -0.154242, 0.133988>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.362244, 0.085912, 0.258193>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.362359, -0.106940, 0.037076>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.363990, -0.109629, 0.249715>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.362783, 0.050064, 0.012507>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.366946, -0.106214, 0.044730>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.366214, 0.131908, 0.160055>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.367640, 0.055675, 0.269860>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.368750, -0.070383, 0.269673>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.369326, -0.146159, 0.145764>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.369924, 0.104094, 0.222521>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.369876, 0.124124, 0.176077>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.372525, -0.058894, 0.268849>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.372871, -0.109282, 0.060371>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.375162, 0.055318, 0.257406>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.375676, -0.004365, 0.018617>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.376907, 0.093003, 0.219787>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.380596, -0.039402, 0.030609>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.380621, 0.076353, 0.062651>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.382522, -0.089930, 0.062977>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.383793, -0.037160, 0.035958>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.386121, -0.007197, 0.255492>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.386565, -0.081910, 0.065822>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.389091, 0.016825, 0.046154>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.389341, 0.092704, 0.162086>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.392839, -0.066849, 0.070721>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.394347, -0.090251, 0.188213>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.394101, -0.003565, 0.238159>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.393238, 0.070936, 0.096475>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.394880, -0.025844, 0.234841>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.395693, -0.032024, 0.231369>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.396370, -0.066512, 0.210798>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.396984, -0.093174, 0.156511>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.396024, 0.014458, 0.062077>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.397194, 0.018999, 0.066694>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.398930, -0.073721, 0.192512>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.398870, -0.018555, 0.225336>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.398359, 0.048298, 0.088638>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.399533, 0.046025, 0.201354>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.399756, -0.036957, 0.074282>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.400600, -0.074374, 0.110144>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.401823, 0.003788, 0.215863>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.402838, -0.048939, 0.200448>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.402251, 0.058765, 0.161300>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.404555, 0.013472, 0.203172>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.404723, 0.048427, 0.165956>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.405331, -0.001286, 0.088522>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.405186, 0.049742, 0.151020>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.405460, 0.041419, 0.118034>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.406322, -0.021952, 0.198741>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.406001, -0.022164, 0.092350>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.406124, -0.000765, 0.091893>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.407236, -0.054388, 0.125478>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.408670, -0.049542, 0.159365>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.409468, -0.032207, 0.116063>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.409860, -0.037980, 0.125657>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.410092, -0.039416, 0.161431>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.411063, 0.006918, 0.125536>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.412088, -0.022599, 0.142987>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
  sphere {
    <0.412410, -0.003163, 0.149117>, 0.100000 strength 1
    texture {
      pigment { rgb <1.000000, 0.5, 0.0> }
    }
  }
}

