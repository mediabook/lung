
// Calculate cell center
__global__ void
alveolis_2_SEM_center_kernel()
{
   int cellNum = blockIdx.x * blockDim.x + threadIdx.x;
   int modelNum = blockIdx.y * blockDim.y + threadIdx.y;

   if (cellNum >= alveolis_2_sem_grids->numOfCells) return;
   if (modelNum >= alveolis_2_sem_grids->numOfModels) return;

   int k;
   float cX = 0.0;
   float cY = 0.0;
   float cZ = 0.0;
   int cidx = modelNum*alveolis_2_sem_grids->idx_cPitch+cellNum;
   float numOfElements = 0.0;

   //printf("%d %d %d\n", modelNum, cellNum, cidx);

   for (k = 0; k < alveolis_2_sem_grids->totalElements; ++k) {
     int idx = modelNum*alveolis_2_sem_grids->idx_pitch+k;
     if (alveolis_2_sem_grids->cellNumber[idx] != cellNum) continue;

     cX += alveolis_2_sem_grids->X[idx];
     cY += alveolis_2_sem_grids->Y[idx];
     cZ += alveolis_2_sem_grids->Z[idx];
     numOfElements += 1.0f;
	   //printf("%d %d %d %d %f %f %f %f\n", modelNum, cellNum, k, idx, cX, cY, cZ, numOfElements);
   }
   //printf("%d %d %d %f %f %f\n", modelNum, cellNum, cidx, cX, cY, cZ);

   cX = cX / numOfElements;
   cY = cY / numOfElements;
   cZ = cZ / numOfElements;
   alveolis_2_sem_grids->cellCenterX[cidx] = cX;
   alveolis_2_sem_grids->cellCenterY[cidx] = cY;
   alveolis_2_sem_grids->cellCenterZ[cidx] = cZ;
   //printf("center: %d %d %d %f %f %f %f\n", modelNum, cellNum, cidx, alveolis_2_sem_grids->cellCenterX[cidx], alveolis_2_sem_grids->cellCenterY[cidx], alveolis_2_sem_grids->cellCenterZ[cidx], numOfElements);
}

// Calculate element centers
__global__ void
alveolis_2_SEM_element_center_kernel()
{
   int cellNum = blockIdx.x * blockDim.x + threadIdx.x;
   int modelNum = blockIdx.y * blockDim.y + threadIdx.y;

   if (cellNum >= alveolis_2_sem_grids->numOfCells) return;
   if (modelNum >= alveolis_2_sem_grids->numOfModels) return;

   int k;

   // zero out element centers
   for (k = 0; k < alveolis_2_sem_grids->maxElementTypes; ++k) {
      int cidx = ((modelNum*alveolis_2_sem_grids->maxElementTypes)+k)*alveolis_2_sem_grids->idx_cPitch+cellNum;
		  //printf("%d %d %d %d\n", modelNum, cellNum, cidx, alveolis_2_sem_grids->maxCells);
      alveolis_2_sem_grids->elementCenterX[cidx] = 0;
      alveolis_2_sem_grids->elementCenterY[cidx] = 0;
      alveolis_2_sem_grids->elementCenterZ[cidx] = 0;
      alveolis_2_sem_grids->elementCenterCount[cidx] = 0;
   }

   // calc element centers
   for (k = 0; k < alveolis_2_sem_grids->totalElements; ++k) {
     int idx = modelNum*alveolis_2_sem_grids->idx_pitch+k;
     if (alveolis_2_sem_grids->cellNumber[idx] != cellNum) continue;
     int cidx = ((modelNum*alveolis_2_sem_grids->maxElementTypes)+alveolis_2_sem_grids->elementType[idx])*alveolis_2_sem_grids->idx_cPitch+cellNum;

		 //printf("%d %d %d %d %d\n", k, modelNum, cellNum, idx, cidx);

     alveolis_2_sem_grids->elementCenterX[cidx] += alveolis_2_sem_grids->X[idx];
     alveolis_2_sem_grids->elementCenterY[cidx] += alveolis_2_sem_grids->Y[idx];
     alveolis_2_sem_grids->elementCenterZ[cidx] += alveolis_2_sem_grids->Z[idx];
     alveolis_2_sem_grids->elementCenterCount[cidx] += 1.0f;
   }
   for (k = 0; k < alveolis_2_sem_grids->maxElementTypes; ++k) {
      int cidx = ((modelNum*alveolis_2_sem_grids->maxElementTypes)+k)*alveolis_2_sem_grids->idx_cPitch+cellNum;
      if (alveolis_2_sem_grids->elementCenterCount[cidx] != 0) {
         alveolis_2_sem_grids->elementCenterX[cidx] /= alveolis_2_sem_grids->elementCenterCount[cidx];
         alveolis_2_sem_grids->elementCenterY[cidx] /= alveolis_2_sem_grids->elementCenterCount[cidx];
         alveolis_2_sem_grids->elementCenterZ[cidx] /= alveolis_2_sem_grids->elementCenterCount[cidx];
      }
		 //printf("elem: %d %d %d %d %f %f %f %f\n", k, modelNum, cellNum, cidx, alveolis_2_sem_grids->elementCenterX[cidx], alveolis_2_sem_grids->elementCenterY[cidx], alveolis_2_sem_grids->elementCenterZ[cidx], alveolis_2_sem_grids->elementCenterCount[cidx]);
   }
}

// SEM movement function used by Runge-Kutta solvers
__device__ void
alveolis_2_SEM_kernel_function(int modelNum, int elemNum, float *X_in, float *Y_in, float *Z_in, float *X_out, float *Y_out, float *Z_out, float mh, float mf)
{
   int idx = modelNum*alveolis_2_sem_grids->idx_pitch+elemNum;
   int j, k, fIndex, fNum;
   float F_X1, F_X2;
   float F_Y1, F_Y2;
   float F_Z1, F_Z2;
   float r, V;
   float forceX = 0.0;
   float forceY = 0.0;
   float forceZ = 0.0;

   // element position
   F_X1 = alveolis_2_sem_grids->X[idx] + mf * X_in[idx];
   F_Y1 = alveolis_2_sem_grids->Y[idx] + mf * Y_in[idx];
   F_Z1 = alveolis_2_sem_grids->Z[idx] + mf * Z_in[idx];
   int eType = alveolis_2_sem_grids->elementType[idx];
   //printf("sem: %d %d %d %f %f %f\n", modelNum, elemNum, idx, X_in[idx], Y_in[idx], Z_in[idx]);

   // forces between elements
   for (k = 0; k < alveolis_2_sem_grids->totalElements; ++k) {
      if (k == elemNum) continue;

      fIndex = 0;
      fNum = alveolis_2_sem_grids->elementForceTable[FINDEX];
      while (fNum >= 0) {
        switch (alveolis_2_sem_grids->elementForceFlagTable[FINDEX]) {
          case FORCE_TYPE_INTRA: {
            // elements of same cell
            int oidx = modelNum*alveolis_2_sem_grids->idx_pitch+k;
            int oeType = alveolis_2_sem_grids->elementType[oidx];

            if (oeType == alveolis_2_sem_grids->elementTypeTable[FINDEX]) {
              F_X2 = alveolis_2_sem_grids->X[oidx] + mf * X_in[oidx];
              F_Y2 = alveolis_2_sem_grids->Y[oidx] + mf * Y_in[oidx];
              F_Z2 = alveolis_2_sem_grids->Z[oidx] + mf * Z_in[oidx];

              V = 0;
              switch (alveolis_2_sem_grids->elementForceTypeTable[FINDEX]) {
                case 0:
                  // D_MORSE
                  r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
                  V = D_MORSE(r, PARAM_RHO, PARAM_EQUIL_SQ, PARAM_FACTOR);
                  break;

                case 1:
                  // PD_MORSE
                  r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
                  V = PD_MORSE(r, PARAM_RHO, PARAM_EQUIL_SQ, PARAM_FACTOR);
                  break;

                case 2:
                  // MORSE
                  r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
                  V = MORSE(r, PARAM_U0, PARAM_ETA0, PARAM_U1, PARAM_ETA1);
                  break;

                case 3:
                  // PMORSE
                  r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
                  V = PMORSE(r, PARAM_U0, PARAM_ETA0, PARAM_U1, PARAM_ETA1);
                  break;
              }
              forceX += V * (F_X1 - F_X2);
              forceY += V * (F_Y1 - F_Y2);
              forceZ += V * (F_Z1 - F_Z2);
            }
            break;
          }

          case FORCE_TYPE_INTER: {
            // elements of different cells
            break;
          }

          case FORCE_TYPE_COLONY: {
            // elements of different colonies
            break;
          }

        }


        // next force
        ++fIndex;
        if (fIndex > alveolis_2_sem_grids->maxForces) fNum = -1;
        else fNum = alveolis_2_sem_grids->elementForceTable[FINDEX];
      }
   }

   int sidx = 0*alveolis_2_sem_grids->idx_pPitch+modelNum;

   // check singular forces
   fIndex = 0;
   fNum = alveolis_2_sem_grids->elementForceTable[FINDEX];
   while (fNum >= 0) {
     F_X2 = 0; F_Y2 = 0; F_Z2 = 0;
     switch (alveolis_2_sem_grids->elementForceFlagTable[FINDEX]) {

       case FORCE_TYPE_CELL_CENTER: {
         // cell center
         int cidx = modelNum*alveolis_2_sem_grids->idx_cPitch+alveolis_2_sem_grids->cellNumber[idx];
         F_X2 = alveolis_2_sem_grids->cellCenterX[cidx];
         F_Y2 = alveolis_2_sem_grids->cellCenterY[cidx];
         F_Z2 = alveolis_2_sem_grids->cellCenterZ[cidx];
	       //printf("%d %d %d %f %f %f\n", modelNum, elemNum, cidx, F_X2, F_Y2, F_Z2);
         break;
       }

       case FORCE_TYPE_ELEMENT_CENTER: {
         // element center
         int oeType = alveolis_2_sem_grids->elementTypeTable[FINDEX];
         int cidx = ((modelNum*alveolis_2_sem_grids->maxElementTypes)+oeType)*alveolis_2_sem_grids->idx_cPitch+alveolis_2_sem_grids->cellNumber[idx];
         F_X2 = alveolis_2_sem_grids->elementCenterX[cidx];
         F_Y2 = alveolis_2_sem_grids->elementCenterY[cidx];
         F_Z2 = alveolis_2_sem_grids->elementCenterZ[cidx];
         break;
       }
     }

     V = 0;
     switch (alveolis_2_sem_grids->elementForceTypeTable[FINDEX]) {
     case 0:
       // D_MORSE
       r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
       V = D_MORSE(r, PARAM_RHO, PARAM_EQUIL_SQ, PARAM_FACTOR);
       break;

     case 1:
       // PD_MORSE
       r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
       V = PD_MORSE(r, PARAM_RHO, PARAM_EQUIL_SQ, PARAM_FACTOR);
       break;

     case 2:
       // MORSE
       r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
       V = MORSE(r, PARAM_U0, PARAM_ETA0, PARAM_U1, PARAM_ETA1);
       break;

     case 3:
       // PMORSE
       r = dist2(F_X1, F_Y1, F_Z1, F_X2, F_Y2, F_Z2);
       //V = PMORSE(r, PARAM_U0, PARAM_ETA0, PARAM_U1, PARAM_ETA1);
       // HACK HACK
       if ((fNum == 13) || (fNum == 17))
	       V = PMORSE(r, alveolis_2_sem_grids->speciesData[sidx] * PARAM_U0, PARAM_ETA0, PARAM_U1, PARAM_ETA1);
	     else
         V = PMORSE(r, PARAM_U0, PARAM_ETA0, PARAM_U1, PARAM_ETA1);
       //if (alveolis_2_sem_grids->speciesData[sidx] > 0) printf("%d %d %f\n", modelNum, elemNum, alveolis_2_sem_grids->speciesData[sidx]);
       //printf("sem: %d %d %d %f %f\n", modelNum, elemNum, sidx, alveolis_2_sem_grids->speciesData[sidx], V);
       break;
         }

     forceX += V * (F_X1 - F_X2);
     forceY += V * (F_Y1 - F_Y2);
     forceZ += V * (F_Z1 - F_Z2);

     // next force
     ++fIndex;
     if (fIndex > alveolis_2_sem_grids->maxForces) fNum = -1;
     else fNum = alveolis_2_sem_grids->elementForceTable[FINDEX];
   }

   // result
   X_out[idx] = mh * forceX;
   Y_out[idx] = mh * forceY;
   Z_out[idx] = mh * forceZ;
}

// F1 Kernel for 2nd order Runge-Kutta
__global__ void
alveolis_2_SEM_kernel1_2rk(float currentTime, float finalTime)
{
   int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
   int modelNum = blockIdx.y * blockDim.y + threadIdx.y;

   //printf("%d %d\n", modelNum, elemNum);

   if (elemNum >= alveolis_2_sem_grids->totalElements) return;
   if (modelNum >= alveolis_2_sem_grids->numOfModels) return;
	 int idx = modelNum*alveolis_2_sem_grids->idx_pitch+elemNum;
	
   // calculate F1
   alveolis_2_sem_grids->X_tmp[idx] = 0.0;
   alveolis_2_sem_grids->Y_tmp[idx] = 0.0;
   alveolis_2_sem_grids->Z_tmp[idx] = 0.0;
   //printf("k1: %d %d %d %f %f %f\n", modelNum, elemNum, idx, alveolis_2_sem_grids->X_tmp[idx], alveolis_2_sem_grids->Y_tmp[idx], alveolis_2_sem_grids->Z_tmp[idx]);
   alveolis_2_SEM_kernel_function(modelNum, elemNum, alveolis_2_sem_grids->X_tmp, alveolis_2_sem_grids->Y_tmp, alveolis_2_sem_grids->Z_tmp,
                          alveolis_2_sem_grids->X_F1, alveolis_2_sem_grids->Y_F1, alveolis_2_sem_grids->Z_F1, alveolis_2_sem_grids->dt, 0.0f);
   //printf("k1: %d %d %d %f %f %f\n", modelNum, elemNum, idx, alveolis_2_sem_grids->X_F1[idx], alveolis_2_sem_grids->Y_F1[idx], alveolis_2_sem_grids->Z_F1[idx]);
}

// F2 Kernel for 2nd order Runge-Kutta
__global__ void
alveolis_2_SEM_kernel2_2rk(float currentTime, float finalTime)
{
   int elemNum = blockIdx.x * blockDim.x + threadIdx.x;
   int modelNum = blockIdx.y * blockDim.y + threadIdx.y;

   if (elemNum >= alveolis_2_sem_grids->totalElements) return;
   if (modelNum >= alveolis_2_sem_grids->numOfModels) return;

   // calculate F2
   alveolis_2_SEM_kernel_function(modelNum, elemNum, alveolis_2_sem_grids->X_F1, alveolis_2_sem_grids->Y_F1, alveolis_2_sem_grids->Z_F1,
                          alveolis_2_sem_grids->X_F2, alveolis_2_sem_grids->Y_F2, alveolis_2_sem_grids->Z_F2, alveolis_2_sem_grids->dt, 0.5f);

   // calculate x(t + dt)
   alveolis_2_sem_grids->X[modelNum*alveolis_2_sem_grids->idx_pitch+elemNum] += alveolis_2_sem_grids->X_F2[modelNum*alveolis_2_sem_grids->idx_pitch+elemNum];
   alveolis_2_sem_grids->Y[modelNum*alveolis_2_sem_grids->idx_pitch+elemNum] += alveolis_2_sem_grids->Y_F2[modelNum*alveolis_2_sem_grids->idx_pitch+elemNum];
   alveolis_2_sem_grids->Z[modelNum*alveolis_2_sem_grids->idx_pitch+elemNum] += alveolis_2_sem_grids->Z_F2[modelNum*alveolis_2_sem_grids->idx_pitch+elemNum];
}
