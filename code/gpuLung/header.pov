global_settings {
  max_trace_level 10
  assumed_gamma 1
}

camera {
  //location    <-1.500000, 6.500000, -7.000000>
  //location    <2.500000, 2.500000, -7.000000>

  // top angle
  location    <0, 0, -20.000000>
  look_at     <0, 0, 0>

  // look at X-Y plane
  //location    <2.500000, 2.500000, -7.000000>
  //look_at     <2.500000, 2.500000, 0.0>

  // look at Z-Y plane
  //location    <-5.00000, 2.500000, 2.500000>
  //look_at     <0.00000, 2.500000, 2.5>

  rotate   <0,-360*clock,0>
}

light_source {
  <-5.000000, 2.500000, -2.000000>*100
  color rgb <2.0, 1.5, 1.0>*1.8
  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient
}

light_source {
  <5.000000, 2.500000, 2.000000>*100
  color rgb <2.0, 1.5, 1.0>*1.8
  area_light 500*z 500*y  9,9 jitter adaptive 1 circular orient
}

