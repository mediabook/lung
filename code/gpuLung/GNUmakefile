#
#  GNUmakefile
#  
#  Written by:	Scott Christley <schristley@mac.com>
#  Copyright (c) 2012 Scott Christley
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#  1. Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
#  2. Redistributions in binary form must reproduce the above copyright
#  notice, this list of conditions and the following disclaimer in the
#  documentation and/or other materials provided with the distribution.
#  3. The name of the author may not be used to endorse or promote products
#  derived from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
#  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
#  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
#  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
#  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# Use same makefile for GNUstep and OSX
PLATFORM = $(shell uname)

ifeq ($(MPICH_DIR),)
NVCCFLAGS = -Xptxas -v  -arch=compute_20 -code=sm_20
else
# if MPICH_DIR is defined, assume on Titan
NVCCFLAGS = -Xptxas -v  -arch=compute_35 -code=sm_35
CUDA_LIBRARY_PATH = /opt/nvidia/cudatoolkit/default/lib64
endif

ifeq ($(PLATFORM),Darwin)

ARCHFLAG = 
LIBS = -L/usr/local/cuda/lib -lcudart -framework BioSwarm -framework BioCocoa -framework Cocoa

all: gpuAlveolis gpuDuct

gpuAlveolis: gpuAlveolis.m
	modelToGPU alveolis.bioswarm alveolis
	clang $(ARCHFLAG) -DMODEL_FILE=@\"alveolis.bioswarm\" -g -c gpuAlveolis.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -c alveolis.cu
	clang $(ARCHFLAG) -fPIC -o gpuAlveolis gpuAlveolis.o alveolis.o $(LIBS)

gpuDuct: gpuDuct.m
	modelToGPU duct.bioswarm duct
	clang $(ARCHFLAG) -DMODEL_FILE=@\"duct.bioswarm\" -g -c gpuDuct.m
	nvcc $(ARCHFLAG) $(NVCCFLAGS) -c duct.cu
	clang $(ARCHFLAG) -fPIC -o gpuDuct gpuDuct.o duct.o $(LIBS)

read_image: read_image.m
	clang -g -I../processImage -o read_image read_image.m -framework Cocoa

clean:
	rm -f *.o
	rm -f gpuAlveolis gpuDuct

distclean: clean
	rm -f *.cu
	rm -f *~
	rm -f *.dat

else

#
# Assume Linux and thus GNUstep
#

# This usually happens when you forget to source GNUstep.sh
ifeq ($(GNUSTEP_MAKEFILES),)
  $(error You need to run the GNUstep configuration script before compiling!)
endif

include $(GNUSTEP_MAKEFILES)/common.make

# The tools to be compiled
TOOL_NAME = \
gpuAlveolis \
mpiDuct

APP_NAME = \
read_image

gpuAlveolis_OBJCFLAGS += -DMODEL_FILE=@\"alveolis.bioswarm\"
gpuAlveolis_OBJC_FILES = \
gpuAlveolis.m \
alveolis.cu.o

read_image_OBJCFLAGS += -I../processImage
read_image_OBJC_FILES = \
read_image.m

mpiDuct_OBJCFLAGS += -DMODEL_FILE=@\"duct.bioswarm\"
mpiDuct_OBJC_FILES = \
gpuDuct.m \
duct.cu.o

#NVCCFLAGS = -ccbin=/usr/bin/gcc-4.4

#ADDITIONAL_OBJCFLAGS += -Wno-unused-variable -Wno-unused-but-set-variable
#ADDITIONAL_LIB_DIRS += -L$(CUDA_LIBRARY_PATH)
#ADDITIONAL_TOOL_LIBS += -lBioSwarm -lBioCocoa -lcudart
read_image_TOOL_LIBS += -lBioSwarm -lBioCocoa

ifeq ($(MPICH_DIR),)
ADDITIONAL_LIB_DIRS += -L$(CUDA_LIBRARY_PATH)
ADDITIONAL_TOOL_LIBS += -lBioSwarm -lBioCocoa -lcudart
else
# if MPICH_DIR is defined, assume on Titan
ADDITIONAL_INCLUDE_DIRS += -I$(MPICH_DIR)/include
ADDITIONAL_LIB_DIRS += -L$(CUDA_LIBRARY_PATH) -L$(MPICH_DIR)/lib
ADDITIONAL_TOOL_LIBS += -lBioSwarm -lBioCocoa -lcudart -lmpich_gnu_48 -lmpichf90_gnu_48
endif

-include GNUmakefile.preamble

include $(GNUSTEP_MAKEFILES)/tool.make
include $(GNUSTEP_MAKEFILES)/application.make

-include GNUmakefile.postamble

obj/gpuAlveolis.obj/alveolis.cu.o: alveolis.bioswarm
	#modelToGPU alveolis.bioswarm alveolis
	nvcc $(NVCCFLAGS) -c -I $(GNUSTEP_HEADERS) alveolis.cu -o obj/gpuAlveolis.obj/alveolis.cu.o

obj/mpiDuct.obj/duct.cu.o: duct.bioswarm
	modelToGPU --mpi duct.bioswarm duct
	cp duct_2_new.cu duct_2.cu
	#cp limb_2_kernel_new.cu limb_2_kernel.cu
	#cp limb_5_new.cu limb_5.cu
	nvcc $(NVCCFLAGS) -c -I $(GNUSTEP_HEADERS) duct.cu -o obj/mpiDuct.obj/duct.cu.o

endif
