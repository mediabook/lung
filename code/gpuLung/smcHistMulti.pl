#
# generate histogram from multiple files
#

$file = $ARGV[0];
$start = $ARGV[1];
$end = $ARGV[2];
$samples = $ARGV[3];
$bins = 100;

%histograms = ();
%minValue = ();
%maxValue = ();
$first = 1;
$header = 1;

# get min/max values, create empty histograms
for ($i = $start; $i < $end; ++$i) {
    $header = 1;
    $samplesProcessed = 0;

    $fileName = $file . "_" . $i . ".dat";
    #print $fileName . "\n";
    open(INPUT, "<$fileName");
    while (<INPUT>) {

	@fields = split(/\s/, $_);

	if ($header) {
	    $weightCol = $#fields;
	    #print "weight col = $weightCol\n";

	    if ($first) {
		for ($j = 0; $j < $weightCol; ++$j) {
		    if ($j != 0) { print " "; }
		    print $fields[$j];
		}
		print "\n";
	    }

	    $header = 0;
	    next;
	}

	if ($first) {
	    for ($j = 0; $j < $weightCol; ++$j) {
		$histograms{$j} = [ ];
		$hist = $histograms{$j};
		for ($k = 0; $k < $bins; ++$k) {
		    $$hist[$k] = 0.0;
		}

		$minValue{$j} = $fields[$j];
		$maxValue{$j} = $fields[$j];
	    }

	    $first = 0;
	}

	for ($j = 0; $j < $weightCol; ++$j) {
	    if ($fields[$j] < $minValue{$j}) {
		$minValue{$j} = $fields[$j];
	    }
	    if ($fields[$j] > $maxValue{$j}) {
		$maxValue{$j} = $fields[$j];
	    }
	}

	++$samplesProcessed;
	if (defined($samples) && ($samplesProcessed >= $samples)) { last; }
    }
    close(INPUT);
}

# min/max values
for ($j = 0; $j < $weightCol; ++$j) {
    $minValue{$j} = $minValue{$j} - $minValue{$j} * 0.1;
    if ($minValue{$j} < 0) { $minValue{$j} = 0; }
    $maxValue{$j} = $maxValue{$j} + $maxValue{$j} * 0.1;

    if ($j != 0) { print " "; }
    print $minValue{$j};
}
print "\n";

for ($j = 0; $j < $weightCol; ++$j) {
    if ($j != 0) { print " "; }
    print $maxValue{$j};
}
print "\n";


# put weights in histogram
for ($i = $start; $i < $end; ++$i) {
    $header = 1;
    $samplesProcessed = 0;

    $fileName = $file . "_" . $i . ".dat";
    #print $fileName . "\n";
    open(INPUT, "<$fileName");
    while (<INPUT>) {

	if ($header) {
	    $header = 0;
	    next;
	}

	@fields = split(/\s/, $_);

	for ($j = 0; $j < $weightCol; ++$j) {
	    $delta = ($maxValue{$j} - $minValue{$j}) / $bins;
	    $k = int(($fields[$j] - $minValue{$j}) / $delta);
	    $hist = $histograms{$j};
	    #print $k . " " . ${$hist}[$k] . " " . $j . " " . $fields[$j] . "\n";
	    ${$hist}[$k] += $fields[$weightCol];
        }

	++$samplesProcessed;
	if (defined($samples) && ($samplesProcessed >= $samples)) { last; }
    }
    close(INPUT);
}

for ($i = 0; $i < $bins; ++$i) {
    for ($j = 0; $j < $weightCol; ++$j) {
	if ($j != 0) { print " "; }
	$hist = $histograms{$j};
	print ${$hist}[$i];
    }
    print "\n";
}
