#
# remove elements from sphere to create initial alveolis
#

$file = $ARGV[0];

$x = [ ];
$y = [ ];
$z = [ ];
$t = [ ];
$c = [ ];
# read in positions of elements
open(INPUT, "<$file");
while (<INPUT>) {

    @fields = split(/\s/, $_);

    # skip first line
    #if ($#fields == 1) { next; }

    $numElems = ($#fields - 1) / 5;
    #print "fields: = " . $#fields . " elements = " . $numElems . "\n";

    for ($j = 0; $j < $numElems; ++$j) {
	$x[$j] = $fields[$j*5+2];
	$y[$j] = $fields[$j*5+3];
	$z[$j] = $fields[$j*5+4];
	$t[$j] = $fields[$j*5+5];
	$c[$j] = $fields[$j*5+6];
	#print "$j: $x[$j] $y[$j] $z[$j] $t[$j] $c[$j]\n";
    }

    @index = sort{ $x[ $a ] <=> $x[ $b ] } 0 .. $#x;
    $j = $index[100];
    #print "$j: $x[$j] $y[$j] $z[$j] $t[$j]\n";
}
close(INPUT);

print "0.0 800";
#print "800";
for ($j = 200; $j < 300; ++$j) {
    $i = $index[$j];
    print " $x[$i] $y[$i] $z[$i] 1 0";
}
for ($j = 300; $j < $numElems; ++$j) {
    $i = $index[$j];
    print " $x[$i] $y[$i] $z[$i] 0 0";
}
print "\n";
