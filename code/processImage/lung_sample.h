//
// We hard-code much of the processing as each sample has specific
// processing requirements, and attempting to generalize is too much effort.
//

#ifndef LUNG_SAMPLE
#define LUNG_SAMPLE 1
#endif

#if LUNG_SAMPLE == 1
#define SAMPLE_DESC "Rat 120, Control - No Injury, Expiration, Sample 1"
#define SAMPLE_DIR "R120Expiration_1_250mmSamDetDist_30p5keVmono_80ms_0p9DegPerSec_5x_edge_25umLAG/"
#define SAMPLE_NAME "recon_proj_12/recon_proj_12_"
#define OUTPUT_DIR "tissue_12/"
#define OUTPUT_NAME "tissue_12_"

//#define START_IMAGE 911
//#define END_IMAGE 911
#define START_IMAGE 550
#define END_IMAGE 1500

#define TOPHAT_RADIUS 5
#define LOWER_THRESHOLD 0.00013
#define UPPER_THRESHOLD 1.0

#define MESH_START_IMAGE 575
#define MESH_END_IMAGE 1490

#define SEED_X 511
#define SEED_Y 934
#define SEED_Z 368

#elif LUNG_SAMPLE == 2
#define SAMPLE_DESC "Rat 120, Control - No Injury, Inspiration, Sample 2"
#define SAMPLE_DIR "R120Inspiration_2_250mmSamDetDist_30p5keVmono_80ms_0p9DegPerSec_5x_edge_25umLAG/"
#define SAMPLE_NAME "recon_proj_11/recon_proj_11_"
#define OUTPUT_DIR "tissue_11/"
#define OUTPUT_NAME "tissue_11_"

#define START_IMAGE 580
#define END_IMAGE 1525

#define TOPHAT_RADIUS 5
#define LOWER_THRESHOLD 0.00013
#define UPPER_THRESHOLD 1.0

#define MESH_START_IMAGE 590
#define MESH_END_IMAGE 1510

// these are default values, need to be changed
#define SEED_X 511
#define SEED_Y 934
#define SEED_Z 368

#elif LUNG_SAMPLE == 3
#define SAMPLE_DESC "Rat 120, Control - No Injury, Expiration, Sample 2"
#define SAMPLE_DIR "R120Expiration_2_250mmSamDetDist_30p5keVmono_80ms_0p9DegPerSec_5x_edge_25umLAG/"
#define SAMPLE_NAME "recon_proj_13/recon_proj_13_"
#define OUTPUT_DIR "tissue_13/"
#define OUTPUT_NAME "tissue_13_"

#define START_IMAGE 575
#define END_IMAGE 1550

#define TOPHAT_RADIUS 5
#define LOWER_THRESHOLD 0.00013
#define UPPER_THRESHOLD 1.0

#define MESH_START_IMAGE 600
#define MESH_END_IMAGE 1500

// these are default values, need to be changed
#define SEED_X 511
#define SEED_Y 934
#define SEED_Z 368

#elif LUNG_SAMPLE == 4
#define SAMPLE_DESC "Rat 75, TWEEN Injury, Tidal Volume 6cc/kg PEEP 5, Expiration, Sample 2"
#define SAMPLE_DIR "R75Expiration_2_250mmSamDetDist_30p5keVmono_80ms_0p9DegPerSec_5x_edge_25umLAG/"
#define SAMPLE_NAME "recon_proj_9/recon_proj_9_"
#define OUTPUT_DIR "tissue_9/"
#define OUTPUT_NAME "tissue_9_"

#define START_IMAGE 575
#define END_IMAGE 1500

#define TOPHAT_RADIUS 5
#define LOWER_THRESHOLD 0.00009
#define UPPER_THRESHOLD 1.0

#define MESH_START_IMAGE 600
#define MESH_END_IMAGE 1450

// these are default values, need to be changed
#define SEED_X 511
#define SEED_Y 934
#define SEED_Z 368

#elif LUNG_SAMPLE == 5
#define SAMPLE_DESC "Rat 75, TWEEN Injury, Tidal Volume 6cc/kg PEEP 5, Inspiration, Sample 1"
#define SAMPLE_DIR "R75Inspiration_1_250mmSamDetDist_30p5keVmono_80ms_0p9DegPerSec_5x_edge_25umLAG/"
#define SAMPLE_NAME "recon_proj_6/recon_proj_6_"
#define OUTPUT_DIR "tissue_6/"
#define OUTPUT_NAME "tissue_6_"

#define START_IMAGE 575
#define END_IMAGE 1500

#define TOPHAT_RADIUS 5
#define LOWER_THRESHOLD 0.00009
#define UPPER_THRESHOLD 1.0

#define MESH_START_IMAGE 600
#define MESH_END_IMAGE 1475

// these are default values, need to be changed
#define SEED_X 511
#define SEED_Y 934
#define SEED_Z 368

#elif LUNG_SAMPLE == 6
#define SAMPLE_DESC "Rat 79, TWEEN Injury, Tidal Volume 6cc/kg PEEP 16, Expiration, Sample 1"
#define SAMPLE_DIR "R79Expiration_2_250mmSamDetDist_30p5keVmono_80ms_0p9DegPerSec_5x_edge_25umLAG/"
#define SAMPLE_NAME "recon_proj_21/recon_proj_21_"
#define OUTPUT_DIR "tissue_21/"
#define OUTPUT_NAME "tissue_21_"

#define START_IMAGE 525
#define END_IMAGE 1500

#define TOPHAT_RADIUS 5
#define LOWER_THRESHOLD 0.00009
#define UPPER_THRESHOLD 1.0

#define MESH_START_IMAGE 550
#define MESH_END_IMAGE 1475

// these are default values, need to be changed
#define SEED_X 511
#define SEED_Y 934
#define SEED_Z 368

#elif LUNG_SAMPLE == 7
#define SAMPLE_DESC "Rat 79, TWEEN Injury, Tidal Volume 6cc/kg PEEP 16, Inspiration, Sample 1"
#define SAMPLE_DIR "R79Inspiration_1_250mmSamDetDist_30p5keVmono_80ms_0p9DegPerSec_5x_edge_25umLAG/"
#define SAMPLE_NAME "recon_proj_18/recon_proj_18_"
#define OUTPUT_DIR "tissue_18/"
#define OUTPUT_NAME "tissue_18_"

#define START_IMAGE 525
#define END_IMAGE 1500

#define TOPHAT_RADIUS 5
#define LOWER_THRESHOLD 0.00009
#define UPPER_THRESHOLD 1.0

#define MESH_START_IMAGE 550
#define MESH_END_IMAGE 1475

// these are default values, need to be changed
#define SEED_X 511
#define SEED_Y 934
#define SEED_Z 368

#elif LUNG_SAMPLE == 8
#define SAMPLE_DESC "Rat 83, TWEEN Injury, APRV 10%, Expiration, Sample 1"
#define SAMPLE_DIR "R83Expiration_1_250mmSamDetDist_30p5keVmono_80ms_0p9DegPerSec_5x_edge_25umLAG/"
#define SAMPLE_NAME "recon_proj_16/recon_proj_16_"
#define OUTPUT_DIR "tissue_16/"
#define OUTPUT_NAME "tissue_16_"

#define START_IMAGE 550
#define END_IMAGE 1525

#define TOPHAT_RADIUS 5
#define LOWER_THRESHOLD 0.00009
#define UPPER_THRESHOLD 1.0

#define MESH_START_IMAGE 575
#define MESH_END_IMAGE 1475

// these are default values, need to be changed
#define SEED_X 511
#define SEED_Y 934
#define SEED_Z 368

#elif LUNG_SAMPLE == 9
#define SAMPLE_DESC "Rat 83, TWEEN Injury, APRV 10%, Inpiration, Sample 1"
#define SAMPLE_DIR "R83Inspiration_1_250mmSamDetDist_30p5keVmono_80ms_0p9DegPerSec_5x_edge_25umLAG/"
#define SAMPLE_NAME "recon_proj_14/recon_proj_14_"
#define OUTPUT_DIR "tissue_14/"
#define OUTPUT_NAME "tissue_14_"

#define START_IMAGE 575
#define END_IMAGE 1550

#define TOPHAT_RADIUS 5
#define LOWER_THRESHOLD 0.00009
#define UPPER_THRESHOLD 1.0

#define MESH_START_IMAGE 600
#define MESH_END_IMAGE 1500

// these are default values, need to be changed
#define SEED_X 511
#define SEED_Y 934
#define SEED_Z 368

#elif LUNG_SAMPLE == 10
#define SAMPLE_DESC "Rat 84, TWEEN Injury, APRV 75%, Inpiration, Sample 1"
#define SAMPLE_DIR "R84Inspiration_1_250mmSamDetDist_30p5keVmono_80ms_0p9DegPerSec_5x_edge_25umLAG/"
#define SAMPLE_NAME "recon_proj_1/recon_proj_1_"
#define OUTPUT_DIR "tissue_1/"
#define OUTPUT_NAME "tissue_1_"

#define START_IMAGE 575
#define END_IMAGE 999

#define TOPHAT_RADIUS 5
#define LOWER_THRESHOLD 0.00009
#define UPPER_THRESHOLD 1.0

#define MESH_START_IMAGE 600
#define MESH_END_IMAGE 999

// these are default values, need to be changed
#define SEED_X 511
#define SEED_Y 934
#define SEED_Z 368

#endif

#ifdef GNUSTEP
// Assume Linux machine
#define DATA_DIR "/home/scottc/Projects/research/lung/data/APS_2014/"
#else
// Assume Mac machine
#define DATA_DIR "/Volumes/BigData/APS_2014/"
#endif

#define TISSUE_VALUE 0x40
#define BORDER_VALUE 0x20
#define AIRWAY_VALUE 0x10

// subcellular element data
#define X_START -38.0
#define X_OFFSET 0.2
#define Y_START -38.0
#define Y_OFFSET 0.2
#define Z_START -38.0
#define Z_OFFSET 0.2

