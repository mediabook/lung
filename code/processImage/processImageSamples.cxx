//
// processImageSamples.cxx
//
// Image processing of x-ray tomography of lung samples
// Uses the Insight toolkit
//
// Author: Scott Christley
// Copyright (c) 2014 Scott Christley
// All rights reserved.
//

#include <iostream>

#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkScalarToRGBPixelFunctor.h"
#include "itkUnaryFunctorImageFilter.h"
#include "itkVectorCastImageFilter.h"
#include "itkVectorGradientAnisotropicDiffusionImageFilter.h"
#include "itkWatershedImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkScalarToRGBColormapImageFilter.h"
#include "itkGradientMagnitudeImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkGradientAnisotropicDiffusionImageFilter.h"
#include "itkCastImageFilter.h"
#include <itkWhiteTopHatImageFilter.h> 
#include <itkBinaryBallStructuringElement.h> 
#include "itkBinaryThresholdImageFilter.h"
#include "itkLineIterator.h"
#include <itkExtractImageFilter.h>
#include "itkOrImageFilter.h"

//
// We hard-code much of the processing as each sample has specific
// processing requirements, and attempting to generalize is too much effort.
//
#include "lung_sample.h"


const unsigned int Dimension = 2;
typedef itk::Image<unsigned char, Dimension> UnsignedCharImageType;
typedef itk::Image<float, Dimension> FloatImageType;
typedef itk::RGBPixel<unsigned char>   RGBPixelType;
typedef itk::Image<RGBPixelType, Dimension>    RGBImageType;
typedef itk::Image<unsigned long, Dimension>   LabeledImageType;

typedef itk::BinaryBallStructuringElement<unsigned char, 2> KernelType; 
typedef itk::WhiteTopHatImageFilter<FloatImageType, FloatImageType, KernelType> TopHatFilterType; 

static int minX, maxX, minY, maxY;

static void ProcessImageSamples();
static void CreateImageMask(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, UnsignedCharImageType::Pointer imageMask);
static void AreaChop(FloatImageType::Pointer image, UnsignedCharImageType::Pointer imageMask);
static void ProcessImage(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, std::string outputFileName);
static void TrimWax(FloatImageType::Pointer image, std::string outputFileName);

static void TrySegment();
static void PerformSegmentation(FloatImageType::Pointer image, const float threshold, const float level);

int main( int argc, char *argv[] )
{
	ProcessImageSamples();
	
	//TrySegment();

  return EXIT_SUCCESS;
}

//
// Loop through the raw image files using the directory locations
// and file names defined in lung_sample.h for the sample.
//
// Processing involves cropping out a polygonal area defined
// for each sample, and then performing a series of filters
// and saving the resultant output image.
//
void ProcessImageSamples()
{
	int currentImage;

	std::cout << "Processing sample:" << std::endl;
	std::cout << SAMPLE_DESC << std::endl;

	for (currentImage = START_IMAGE; currentImage <= END_IMAGE; ++currentImage) {

		std::stringstream fileName, outputFileName;
	
		// input file name
		fileName << DATA_DIR << SAMPLE_DIR << SAMPLE_NAME;
		if (currentImage < 100) fileName << "000";
		else if (currentImage < 1000) fileName << "00";
		else fileName << "0";
		fileName << currentImage << ".tif";
		std::cout << "Processing image file:" << std::endl;
		std::cout << fileName.str() << std::endl;
	
		// output file name
		outputFileName << DATA_DIR << SAMPLE_DIR << OUTPUT_DIR << OUTPUT_NAME;
		if (currentImage < 100) outputFileName << "000";
		else if (currentImage < 1000) outputFileName << "00";
		else outputFileName << "0";
		outputFileName << currentImage << ".png";

		// read image
		typedef itk::ImageFileReader<FloatImageType>  ReaderType;
		ReaderType::Pointer reader = ReaderType::New();
		reader->SetFileName(fileName.str());
		reader->Update();

		FloatImageType::Pointer image = reader->GetOutput();

		UnsignedCharImageType::Pointer imageMask = UnsignedCharImageType::New();
		UnsignedCharImageType::Pointer polyMask = UnsignedCharImageType::New();
		CreateImageMask(image, polyMask, imageMask);
		if (currentImage == START_IMAGE)
			std::cout << "Cropping image in box (" << minX << ", " << minY << ") to (" << maxX << ", " << maxY << ")" << std::endl;

		AreaChop(image, imageMask);

		ProcessImage(image, polyMask, outputFileName.str());
	}
}

//
// Use Fiji to create a polygon selection which gets as much tissue as
// possible without artifacts from the wax edges and the camera circle.
// Then record the positions for each corner in the polygon, which are
// hard-coded in the mask creation function for each sample.
//

#if LUNG_SAMPLE == 1

void CreateImageMask(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, UnsignedCharImageType::Pointer imageMask)
{
  FloatImageType::RegionType fullRegion = image->GetLargestPossibleRegion();
	FloatImageType::SizeType size = fullRegion.GetSize();

	UnsignedCharImageType::RegionType region;
  UnsignedCharImageType::IndexType start;
  start[0] = 0;
  start[1] = 0;
 
 	minX = size[0];
 	minY = size[1];
 	maxX = 0;
 	maxY = 0;

  region.SetSize(size);
  region.SetIndex(start);

  imageMask->SetRegions(region);
  imageMask->Allocate();
  imageMask->FillBuffer(0);

  polyMask->SetRegions(region);
  polyMask->Allocate();
  polyMask->FillBuffer(0);

  FloatImageType::IndexType corners[10];
  int numCorners = 10;
  corners[0][0] = 710; corners[0][1] = 780;
  corners[1][0] = 1330; corners[1][1] = 550;
  corners[2][0] = 1480; corners[2][1] = 700;
  //corners[3][0] = 1760; corners[3][1] = 740;
  //corners[4][0] = 1640; corners[4][1] = 1590;
  //corners[5][0] = 1380; corners[5][1] = 1870;
  corners[3][0] = 1680; corners[3][1] = 780;
  corners[4][0] = 1560; corners[4][1] = 1420;
  corners[5][0] = 1340; corners[5][1] = 1780;
  corners[6][0] = 1250; corners[6][1] = 1880;
  corners[7][0] = 1180; corners[7][1] = 2040;
  corners[8][0] = 640; corners[8][1] = 1710;
  corners[9][0] = 710; corners[9][1] = 780;

	// draw lines for polygon area
	int i, j;
	for (i = 0; i < (numCorners - 1); ++i) {
	  itk::LineIterator<UnsignedCharImageType> it1(polyMask, corners[i], corners[i+1]);
  	it1.GoToBegin();
  	while (!it1.IsAtEnd()) {
    	it1.Set(BORDER_VALUE);
    	++it1;
    }

	  itk::LineIterator<UnsignedCharImageType> it2(imageMask, corners[i], corners[i+1]);
  	it2.GoToBegin();
  	while (!it2.IsAtEnd()) {
    	it2.Set(TISSUE_VALUE);
    	++it2;
    }
    
    if (corners[i][0] < minX) minX = corners[i][0];
    if (corners[i][1] < minY) minY = corners[i][1];
    if (corners[i][0] > maxX) maxX = corners[i][0];
    if (corners[i][1] > maxY) maxY = corners[i][1];
	}

	// fill polygon
	// this does not handle all edge cases but is good enough
	for (i = 0; i < size[0]; ++i) {
		bool fill = false, border = false;
		for (j = 0; j < size[1]; ++j) {
      UnsignedCharImageType::IndexType pixelIndex;
			pixelIndex[0] = i; pixelIndex[1] = j;

			UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
			if (pixelValue > 0) {
				if (!border) {
					fill = !fill; border = true;
				}
			} else border = false;

			//if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, 255);
		}
		
		if (!fill) {
			border = false;
			for (j = 0; j < size[1]; ++j) {
				UnsignedCharImageType::IndexType pixelIndex;
				pixelIndex[0] = i; pixelIndex[1] = j;

				UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
				if (pixelValue > 0) {
					if (!border) {
						fill = !fill; border = true;
					}
				} else border = false;

				if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, TISSUE_VALUE);
			}
		}
	}

  typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName("mask.png");
  writer->SetInput(imageMask);
  writer->Update();
}

#elif LUNG_SAMPLE == 2

void CreateImageMask(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, UnsignedCharImageType::Pointer imageMask)
{
  FloatImageType::RegionType fullRegion = image->GetLargestPossibleRegion();
	FloatImageType::SizeType size = fullRegion.GetSize();

	UnsignedCharImageType::RegionType region;
  UnsignedCharImageType::IndexType start;
  start[0] = 0;
  start[1] = 0;
 
 	minX = size[0];
 	minY = size[1];
 	maxX = 0;
 	maxY = 0;

  region.SetSize(size);
  region.SetIndex(start);

  imageMask->SetRegions(region);
  imageMask->Allocate();
  imageMask->FillBuffer(0);

  polyMask->SetRegions(region);
  polyMask->Allocate();
  polyMask->FillBuffer(0);

  FloatImageType::IndexType corners[8];
  int numCorners = 8;
  corners[0][0] = 760; corners[0][1] = 840;
  corners[1][0] = 870; corners[1][1] = 700;
  corners[2][0] = 1580; corners[2][1] = 580;
  corners[3][0] = 2040; corners[3][1] = 1900;
  corners[4][0] = 1070; corners[4][1] = 1740;
  corners[5][0] = 1060; corners[5][1] = 1610;
  corners[6][0] = 880; corners[6][1] = 1560;
  corners[7][0] = 760; corners[7][1] = 840;

	// draw lines for polygon area
	int i, j;
	for (i = 0; i < (numCorners - 1); ++i) {
	  itk::LineIterator<UnsignedCharImageType> it1(polyMask, corners[i], corners[i+1]);
  	it1.GoToBegin();
  	while (!it1.IsAtEnd()) {
    	it1.Set(BORDER_VALUE);
    	++it1;
    }

	  itk::LineIterator<UnsignedCharImageType> it2(imageMask, corners[i], corners[i+1]);
  	it2.GoToBegin();
  	while (!it2.IsAtEnd()) {
    	it2.Set(TISSUE_VALUE);
    	++it2;
    }
    
    if (corners[i][0] < minX) minX = corners[i][0];
    if (corners[i][1] < minY) minY = corners[i][1];
    if (corners[i][0] > maxX) maxX = corners[i][0];
    if (corners[i][1] > maxY) maxY = corners[i][1];
	}

	// fill polygon
	// this does not handle all edge cases but is good enough
	for (i = 0; i < size[0]; ++i) {
		bool fill = false, border = false;
		for (j = 0; j < size[1]; ++j) {
      UnsignedCharImageType::IndexType pixelIndex;
			pixelIndex[0] = i; pixelIndex[1] = j;

			UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
			if (pixelValue > 0) {
				if (!border) {
					fill = !fill; border = true;
				}
			} else border = false;

			//if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, 255);
		}
		
		if (!fill) {
			border = false;
			for (j = 0; j < size[1]; ++j) {
				UnsignedCharImageType::IndexType pixelIndex;
				pixelIndex[0] = i; pixelIndex[1] = j;

				UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
				if (pixelValue > 0) {
					if (!border) {
						fill = !fill; border = true;
					}
				} else border = false;

				if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, TISSUE_VALUE);
			}
		}
	}

  typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName("mask.png");
  writer->SetInput(imageMask);
  writer->Update();
}

#elif LUNG_SAMPLE == 3

void CreateImageMask(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, UnsignedCharImageType::Pointer imageMask)
{
  FloatImageType::RegionType fullRegion = image->GetLargestPossibleRegion();
	FloatImageType::SizeType size = fullRegion.GetSize();

	UnsignedCharImageType::RegionType region;
  UnsignedCharImageType::IndexType start;
  start[0] = 0;
  start[1] = 0;
 
 	minX = size[0];
 	minY = size[1];
 	maxX = 0;
 	maxY = 0;

  region.SetSize(size);
  region.SetIndex(start);

  imageMask->SetRegions(region);
  imageMask->Allocate();
  imageMask->FillBuffer(0);

  polyMask->SetRegions(region);
  polyMask->Allocate();
  polyMask->FillBuffer(0);

  FloatImageType::IndexType corners[6];
  int numCorners = 6;
  corners[0][0] = 720; corners[0][1] = 470;
  corners[1][0] = 1000; corners[1][1] = 350;
  corners[2][0] = 1980; corners[2][1] = 920;
  corners[3][0] = 1800; corners[3][1] = 1960;
  corners[4][0] = 660; corners[4][1] = 2070;
  corners[5][0] = 720; corners[5][1] = 470;

	// draw lines for polygon area
	int i, j;
	for (i = 0; i < (numCorners - 1); ++i) {
	  itk::LineIterator<UnsignedCharImageType> it1(polyMask, corners[i], corners[i+1]);
  	it1.GoToBegin();
  	while (!it1.IsAtEnd()) {
    	it1.Set(BORDER_VALUE);
    	++it1;
    }

	  itk::LineIterator<UnsignedCharImageType> it2(imageMask, corners[i], corners[i+1]);
  	it2.GoToBegin();
  	while (!it2.IsAtEnd()) {
    	it2.Set(TISSUE_VALUE);
    	++it2;
    }
    
    if (corners[i][0] < minX) minX = corners[i][0];
    if (corners[i][1] < minY) minY = corners[i][1];
    if (corners[i][0] > maxX) maxX = corners[i][0];
    if (corners[i][1] > maxY) maxY = corners[i][1];
	}

	// fill polygon
	// this does not handle all edge cases but is good enough
	for (i = 0; i < size[0]; ++i) {
		bool fill = false, border = false;
		for (j = 0; j < size[1]; ++j) {
      UnsignedCharImageType::IndexType pixelIndex;
			pixelIndex[0] = i; pixelIndex[1] = j;

			UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
			if (pixelValue > 0) {
				if (!border) {
					fill = !fill; border = true;
				}
			} else border = false;

			//if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, 255);
		}
		
		if (!fill) {
			border = false;
			for (j = 0; j < size[1]; ++j) {
				UnsignedCharImageType::IndexType pixelIndex;
				pixelIndex[0] = i; pixelIndex[1] = j;

				UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
				if (pixelValue > 0) {
					if (!border) {
						fill = !fill; border = true;
					}
				} else border = false;

				if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, TISSUE_VALUE);
			}
		}
	}

  typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName("mask.png");
  writer->SetInput(imageMask);
  writer->Update();
}

#elif LUNG_SAMPLE == 4

void CreateImageMask(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, UnsignedCharImageType::Pointer imageMask)
{
  FloatImageType::RegionType fullRegion = image->GetLargestPossibleRegion();
	FloatImageType::SizeType size = fullRegion.GetSize();

	UnsignedCharImageType::RegionType region;
  UnsignedCharImageType::IndexType start;
  start[0] = 0;
  start[1] = 0;
 
 	minX = size[0];
 	minY = size[1];
 	maxX = 0;
 	maxY = 0;

  region.SetSize(size);
  region.SetIndex(start);

  imageMask->SetRegions(region);
  imageMask->Allocate();
  imageMask->FillBuffer(0);

  polyMask->SetRegions(region);
  polyMask->Allocate();
  polyMask->FillBuffer(0);

  FloatImageType::IndexType corners[7];
  int numCorners = 7;
  corners[0][0] = 600; corners[0][1] = 190;
  corners[1][0] = 1330; corners[1][1] = 40;
  corners[2][0] = 2040; corners[2][1] = 310;
  corners[3][0] = 1880; corners[3][1] = 2340;
  corners[4][0] = 1010; corners[4][1] = 2280;
  corners[5][0] = 470; corners[5][1] = 1860;
  corners[6][0] = 600; corners[6][1] = 190;

	// draw lines for polygon area
	int i, j;
	for (i = 0; i < (numCorners - 1); ++i) {
	  itk::LineIterator<UnsignedCharImageType> it1(polyMask, corners[i], corners[i+1]);
  	it1.GoToBegin();
  	while (!it1.IsAtEnd()) {
    	it1.Set(BORDER_VALUE);
    	++it1;
    }

	  itk::LineIterator<UnsignedCharImageType> it2(imageMask, corners[i], corners[i+1]);
  	it2.GoToBegin();
  	while (!it2.IsAtEnd()) {
    	it2.Set(TISSUE_VALUE);
    	++it2;
    }
    
    if (corners[i][0] < minX) minX = corners[i][0];
    if (corners[i][1] < minY) minY = corners[i][1];
    if (corners[i][0] > maxX) maxX = corners[i][0];
    if (corners[i][1] > maxY) maxY = corners[i][1];
	}

	// fill polygon
	// this does not handle all edge cases but is good enough
	for (i = 0; i < size[0]; ++i) {
		bool fill = false, border = false;
		for (j = 0; j < size[1]; ++j) {
      UnsignedCharImageType::IndexType pixelIndex;
			pixelIndex[0] = i; pixelIndex[1] = j;

			UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
			if (pixelValue > 0) {
				if (!border) {
					fill = !fill; border = true;
				}
			} else border = false;

			//if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, 255);
		}
		
		if (!fill) {
			border = false;
			for (j = 0; j < size[1]; ++j) {
				UnsignedCharImageType::IndexType pixelIndex;
				pixelIndex[0] = i; pixelIndex[1] = j;

				UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
				if (pixelValue > 0) {
					if (!border) {
						fill = !fill; border = true;
					}
				} else border = false;

				if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, TISSUE_VALUE);
			}
		}
	}

  typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName("mask.png");
  writer->SetInput(imageMask);
  writer->Update();
}

#elif LUNG_SAMPLE == 5

void CreateImageMask(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, UnsignedCharImageType::Pointer imageMask)
{
  FloatImageType::RegionType fullRegion = image->GetLargestPossibleRegion();
	FloatImageType::SizeType size = fullRegion.GetSize();

	UnsignedCharImageType::RegionType region;
  UnsignedCharImageType::IndexType start;
  start[0] = 0;
  start[1] = 0;
 
 	minX = size[0];
 	minY = size[1];
 	maxX = 0;
 	maxY = 0;

  region.SetSize(size);
  region.SetIndex(start);

  imageMask->SetRegions(region);
  imageMask->Allocate();
  imageMask->FillBuffer(0);

  polyMask->SetRegions(region);
  polyMask->Allocate();
  polyMask->FillBuffer(0);

  FloatImageType::IndexType corners[6];
  int numCorners = 6;
  corners[0][0] = 540; corners[0][1] = 620;
  corners[1][0] = 1680; corners[1][1] = 530;
  corners[2][0] = 2030; corners[2][1] = 1970;
  corners[3][0] = 700; corners[3][1] = 1690;
  corners[4][0] = 600; corners[4][1] = 1500;
  corners[5][0] = 540; corners[5][1] = 620;

	// draw lines for polygon area
	int i, j;
	for (i = 0; i < (numCorners - 1); ++i) {
	  itk::LineIterator<UnsignedCharImageType> it1(polyMask, corners[i], corners[i+1]);
  	it1.GoToBegin();
  	while (!it1.IsAtEnd()) {
    	it1.Set(BORDER_VALUE);
    	++it1;
    }

	  itk::LineIterator<UnsignedCharImageType> it2(imageMask, corners[i], corners[i+1]);
  	it2.GoToBegin();
  	while (!it2.IsAtEnd()) {
    	it2.Set(TISSUE_VALUE);
    	++it2;
    }
    
    if (corners[i][0] < minX) minX = corners[i][0];
    if (corners[i][1] < minY) minY = corners[i][1];
    if (corners[i][0] > maxX) maxX = corners[i][0];
    if (corners[i][1] > maxY) maxY = corners[i][1];
	}

	// fill polygon
	// this does not handle all edge cases but is good enough
	for (i = 0; i < size[0]; ++i) {
		bool fill = false, border = false;
		for (j = 0; j < size[1]; ++j) {
      UnsignedCharImageType::IndexType pixelIndex;
			pixelIndex[0] = i; pixelIndex[1] = j;

			UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
			if (pixelValue > 0) {
				if (!border) {
					fill = !fill; border = true;
				}
			} else border = false;

			//if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, 255);
		}
		
		if (!fill) {
			border = false;
			for (j = 0; j < size[1]; ++j) {
				UnsignedCharImageType::IndexType pixelIndex;
				pixelIndex[0] = i; pixelIndex[1] = j;

				UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
				if (pixelValue > 0) {
					if (!border) {
						fill = !fill; border = true;
					}
				} else border = false;

				if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, TISSUE_VALUE);
			}
		}
	}

  typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName("mask.png");
  writer->SetInput(imageMask);
  writer->Update();
}

#elif LUNG_SAMPLE == 6

void CreateImageMask(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, UnsignedCharImageType::Pointer imageMask)
{
  FloatImageType::RegionType fullRegion = image->GetLargestPossibleRegion();
	FloatImageType::SizeType size = fullRegion.GetSize();

	UnsignedCharImageType::RegionType region;
  UnsignedCharImageType::IndexType start;
  start[0] = 0;
  start[1] = 0;
 
 	minX = size[0];
 	minY = size[1];
 	maxX = 0;
 	maxY = 0;

  region.SetSize(size);
  region.SetIndex(start);

  imageMask->SetRegions(region);
  imageMask->Allocate();
  imageMask->FillBuffer(0);

  polyMask->SetRegions(region);
  polyMask->Allocate();
  polyMask->FillBuffer(0);

  FloatImageType::IndexType corners[7];
  int numCorners = 7;
  corners[0][0] = 190; corners[0][1] = 690;
  corners[1][0] = 440; corners[1][1] = 350;
  corners[2][0] = 880; corners[2][1] = 360;
  corners[3][0] = 2500; corners[3][1] = 1320;
  corners[4][0] = 1760; corners[4][1] = 2020;
  corners[5][0] = 490; corners[5][1] = 2020;
  corners[6][0] = 190; corners[6][1] = 690;

	// draw lines for polygon area
	int i, j;
	for (i = 0; i < (numCorners - 1); ++i) {
	  itk::LineIterator<UnsignedCharImageType> it1(polyMask, corners[i], corners[i+1]);
  	it1.GoToBegin();
  	while (!it1.IsAtEnd()) {
    	it1.Set(BORDER_VALUE);
    	++it1;
    }

	  itk::LineIterator<UnsignedCharImageType> it2(imageMask, corners[i], corners[i+1]);
  	it2.GoToBegin();
  	while (!it2.IsAtEnd()) {
    	it2.Set(TISSUE_VALUE);
    	++it2;
    }
    
    if (corners[i][0] < minX) minX = corners[i][0];
    if (corners[i][1] < minY) minY = corners[i][1];
    if (corners[i][0] > maxX) maxX = corners[i][0];
    if (corners[i][1] > maxY) maxY = corners[i][1];
	}

	// fill polygon
	// this does not handle all edge cases but is good enough
	for (i = 0; i < size[0]; ++i) {
		bool fill = false, border = false;
		for (j = 0; j < size[1]; ++j) {
      UnsignedCharImageType::IndexType pixelIndex;
			pixelIndex[0] = i; pixelIndex[1] = j;

			UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
			if (pixelValue > 0) {
				if (!border) {
					fill = !fill; border = true;
				}
			} else border = false;

			//if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, 255);
		}
		
		if (!fill) {
			border = false;
			for (j = 0; j < size[1]; ++j) {
				UnsignedCharImageType::IndexType pixelIndex;
				pixelIndex[0] = i; pixelIndex[1] = j;

				UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
				if (pixelValue > 0) {
					if (!border) {
						fill = !fill; border = true;
					}
				} else border = false;

				if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, TISSUE_VALUE);
			}
		}
	}

  typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName("mask.png");
  writer->SetInput(imageMask);
  writer->Update();
}

#elif LUNG_SAMPLE == 7

void CreateImageMask(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, UnsignedCharImageType::Pointer imageMask)
{
  FloatImageType::RegionType fullRegion = image->GetLargestPossibleRegion();
	FloatImageType::SizeType size = fullRegion.GetSize();

	UnsignedCharImageType::RegionType region;
  UnsignedCharImageType::IndexType start;
  start[0] = 0;
  start[1] = 0;
 
 	minX = size[0];
 	minY = size[1];
 	maxX = 0;
 	maxY = 0;

  region.SetSize(size);
  region.SetIndex(start);

  imageMask->SetRegions(region);
  imageMask->Allocate();
  imageMask->FillBuffer(0);

  polyMask->SetRegions(region);
  polyMask->Allocate();
  polyMask->FillBuffer(0);

  FloatImageType::IndexType corners[9];
  int numCorners = 9;
  corners[0][0] = 1060; corners[0][1] = 120;
  corners[1][0] = 1740; corners[1][1] = 220;
  corners[2][0] = 2110; corners[2][1] = 510;
  corners[3][0] = 2050; corners[3][1] = 1230;
  corners[4][0] = 1540; corners[4][1] = 2350;
  corners[5][0] = 200; corners[5][1] = 1780;
  corners[6][0] = 60; corners[6][1] = 1290;
  corners[7][0] = 360; corners[7][1] = 540;
  corners[8][0] = 1060; corners[8][1] = 120;

	// draw lines for polygon area
	int i, j;
	for (i = 0; i < (numCorners - 1); ++i) {
	  itk::LineIterator<UnsignedCharImageType> it1(polyMask, corners[i], corners[i+1]);
  	it1.GoToBegin();
  	while (!it1.IsAtEnd()) {
    	it1.Set(BORDER_VALUE);
    	++it1;
    }

	  itk::LineIterator<UnsignedCharImageType> it2(imageMask, corners[i], corners[i+1]);
  	it2.GoToBegin();
  	while (!it2.IsAtEnd()) {
    	it2.Set(TISSUE_VALUE);
    	++it2;
    }
    
    if (corners[i][0] < minX) minX = corners[i][0];
    if (corners[i][1] < minY) minY = corners[i][1];
    if (corners[i][0] > maxX) maxX = corners[i][0];
    if (corners[i][1] > maxY) maxY = corners[i][1];
	}

	// fill polygon
	// this does not handle all edge cases but is good enough
	for (i = 0; i < size[0]; ++i) {
		bool fill = false, border = false;
		for (j = 0; j < size[1]; ++j) {
      UnsignedCharImageType::IndexType pixelIndex;
			pixelIndex[0] = i; pixelIndex[1] = j;

			UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
			if (pixelValue > 0) {
				if (!border) {
					fill = !fill; border = true;
				}
			} else border = false;

			//if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, 255);
		}
		
		if (!fill) {
			border = false;
			for (j = 0; j < size[1]; ++j) {
				UnsignedCharImageType::IndexType pixelIndex;
				pixelIndex[0] = i; pixelIndex[1] = j;

				UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
				if (pixelValue > 0) {
					if (!border) {
						fill = !fill; border = true;
					}
				} else border = false;

				if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, TISSUE_VALUE);
			}
		}
	}

  typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName("mask.png");
  writer->SetInput(imageMask);
  writer->Update();
}

#elif LUNG_SAMPLE == 8

void CreateImageMask(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, UnsignedCharImageType::Pointer imageMask)
{
  FloatImageType::RegionType fullRegion = image->GetLargestPossibleRegion();
	FloatImageType::SizeType size = fullRegion.GetSize();

	UnsignedCharImageType::RegionType region;
  UnsignedCharImageType::IndexType start;
  start[0] = 0;
  start[1] = 0;
 
 	minX = size[0];
 	minY = size[1];
 	maxX = 0;
 	maxY = 0;

  region.SetSize(size);
  region.SetIndex(start);

  imageMask->SetRegions(region);
  imageMask->Allocate();
  imageMask->FillBuffer(0);

  polyMask->SetRegions(region);
  polyMask->Allocate();
  polyMask->FillBuffer(0);

  FloatImageType::IndexType corners[6];
  int numCorners = 6;
  corners[0][0] = 630; corners[0][1] = 1180;
  corners[1][0] = 2160; corners[1][1] = 900;
  corners[2][0] = 2480; corners[2][1] = 1540;
  corners[3][0] = 2210; corners[3][1] = 2080;
  corners[4][0] = 750; corners[4][1] = 2000;
  corners[5][0] = 630; corners[5][1] = 1180;

	// draw lines for polygon area
	int i, j;
	for (i = 0; i < (numCorners - 1); ++i) {
	  itk::LineIterator<UnsignedCharImageType> it1(polyMask, corners[i], corners[i+1]);
  	it1.GoToBegin();
  	while (!it1.IsAtEnd()) {
    	it1.Set(BORDER_VALUE);
    	++it1;
    }

	  itk::LineIterator<UnsignedCharImageType> it2(imageMask, corners[i], corners[i+1]);
  	it2.GoToBegin();
  	while (!it2.IsAtEnd()) {
    	it2.Set(TISSUE_VALUE);
    	++it2;
    }
    
    if (corners[i][0] < minX) minX = corners[i][0];
    if (corners[i][1] < minY) minY = corners[i][1];
    if (corners[i][0] > maxX) maxX = corners[i][0];
    if (corners[i][1] > maxY) maxY = corners[i][1];
	}

	// fill polygon
	// this does not handle all edge cases but is good enough
	for (i = 0; i < size[0]; ++i) {
		bool fill = false, border = false;
		for (j = 0; j < size[1]; ++j) {
      UnsignedCharImageType::IndexType pixelIndex;
			pixelIndex[0] = i; pixelIndex[1] = j;

			UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
			if (pixelValue > 0) {
				if (!border) {
					fill = !fill; border = true;
				}
			} else border = false;

			//if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, 255);
		}
		
		if (!fill) {
			border = false;
			for (j = 0; j < size[1]; ++j) {
				UnsignedCharImageType::IndexType pixelIndex;
				pixelIndex[0] = i; pixelIndex[1] = j;

				UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
				if (pixelValue > 0) {
					if (!border) {
						fill = !fill; border = true;
					}
				} else border = false;

				if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, TISSUE_VALUE);
			}
		}
	}

  typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName("mask.png");
  writer->SetInput(imageMask);
  writer->Update();
}

#elif LUNG_SAMPLE == 9

void CreateImageMask(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, UnsignedCharImageType::Pointer imageMask)
{
  FloatImageType::RegionType fullRegion = image->GetLargestPossibleRegion();
	FloatImageType::SizeType size = fullRegion.GetSize();

	UnsignedCharImageType::RegionType region;
  UnsignedCharImageType::IndexType start;
  start[0] = 0;
  start[1] = 0;
 
 	minX = size[0];
 	minY = size[1];
 	maxX = 0;
 	maxY = 0;

  region.SetSize(size);
  region.SetIndex(start);

  imageMask->SetRegions(region);
  imageMask->Allocate();
  imageMask->FillBuffer(0);

  polyMask->SetRegions(region);
  polyMask->Allocate();
  polyMask->FillBuffer(0);

  FloatImageType::IndexType corners[5];
  int numCorners = 5;
  corners[0][0] = 500; corners[0][1] = 240;
  corners[1][0] = 2500; corners[1][1] = 320;
  corners[2][0] = 1880; corners[2][1] = 2230;
  corners[3][0] = 430; corners[3][1] = 1950;
  corners[4][0] = 500; corners[4][1] = 240;

	// draw lines for polygon area
	int i, j;
	for (i = 0; i < (numCorners - 1); ++i) {
	  itk::LineIterator<UnsignedCharImageType> it1(polyMask, corners[i], corners[i+1]);
  	it1.GoToBegin();
  	while (!it1.IsAtEnd()) {
    	it1.Set(BORDER_VALUE);
    	++it1;
    }

	  itk::LineIterator<UnsignedCharImageType> it2(imageMask, corners[i], corners[i+1]);
  	it2.GoToBegin();
  	while (!it2.IsAtEnd()) {
    	it2.Set(TISSUE_VALUE);
    	++it2;
    }
    
    if (corners[i][0] < minX) minX = corners[i][0];
    if (corners[i][1] < minY) minY = corners[i][1];
    if (corners[i][0] > maxX) maxX = corners[i][0];
    if (corners[i][1] > maxY) maxY = corners[i][1];
	}

	// fill polygon
	// this does not handle all edge cases but is good enough
	for (i = 0; i < size[0]; ++i) {
		bool fill = false, border = false;
		for (j = 0; j < size[1]; ++j) {
      UnsignedCharImageType::IndexType pixelIndex;
			pixelIndex[0] = i; pixelIndex[1] = j;

			UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
			if (pixelValue > 0) {
				if (!border) {
					fill = !fill; border = true;
				}
			} else border = false;

			//if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, 255);
		}
		
		if (!fill) {
			border = false;
			for (j = 0; j < size[1]; ++j) {
				UnsignedCharImageType::IndexType pixelIndex;
				pixelIndex[0] = i; pixelIndex[1] = j;

				UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
				if (pixelValue > 0) {
					if (!border) {
						fill = !fill; border = true;
					}
				} else border = false;

				if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, TISSUE_VALUE);
			}
		}
	}

  typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName("mask.png");
  writer->SetInput(imageMask);
  writer->Update();
}

#elif LUNG_SAMPLE == 10

void CreateImageMask(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, UnsignedCharImageType::Pointer imageMask)
{
  FloatImageType::RegionType fullRegion = image->GetLargestPossibleRegion();
	FloatImageType::SizeType size = fullRegion.GetSize();

	UnsignedCharImageType::RegionType region;
  UnsignedCharImageType::IndexType start;
  start[0] = 0;
  start[1] = 0;
 
 	minX = size[0];
 	minY = size[1];
 	maxX = 0;
 	maxY = 0;

  region.SetSize(size);
  region.SetIndex(start);

  imageMask->SetRegions(region);
  imageMask->Allocate();
  imageMask->FillBuffer(0);

  polyMask->SetRegions(region);
  polyMask->Allocate();
  polyMask->FillBuffer(0);

  FloatImageType::IndexType corners[6];
  int numCorners = 6;
  corners[0][0] = 480; corners[0][1] = 800;
  corners[1][0] = 1150; corners[1][1] = 720;
  corners[2][0] = 1980; corners[2][1] = 1300;
  corners[3][0] = 2060; corners[3][1] = 2340;
  corners[4][0] = 890; corners[4][1] = 2470;
  corners[5][0] = 480; corners[5][1] = 800;

	// draw lines for polygon area
	int i, j;
	for (i = 0; i < (numCorners - 1); ++i) {
	  itk::LineIterator<UnsignedCharImageType> it1(polyMask, corners[i], corners[i+1]);
  	it1.GoToBegin();
  	while (!it1.IsAtEnd()) {
    	it1.Set(BORDER_VALUE);
    	++it1;
    }

	  itk::LineIterator<UnsignedCharImageType> it2(imageMask, corners[i], corners[i+1]);
  	it2.GoToBegin();
  	while (!it2.IsAtEnd()) {
    	it2.Set(TISSUE_VALUE);
    	++it2;
    }
    
    if (corners[i][0] < minX) minX = corners[i][0];
    if (corners[i][1] < minY) minY = corners[i][1];
    if (corners[i][0] > maxX) maxX = corners[i][0];
    if (corners[i][1] > maxY) maxY = corners[i][1];
	}

	// fill polygon
	// this does not handle all edge cases but is good enough
	for (i = 0; i < size[0]; ++i) {
		bool fill = false, border = false;
		for (j = 0; j < size[1]; ++j) {
      UnsignedCharImageType::IndexType pixelIndex;
			pixelIndex[0] = i; pixelIndex[1] = j;

			UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
			if (pixelValue > 0) {
				if (!border) {
					fill = !fill; border = true;
				}
			} else border = false;

			//if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, 255);
		}
		
		if (!fill) {
			border = false;
			for (j = 0; j < size[1]; ++j) {
				UnsignedCharImageType::IndexType pixelIndex;
				pixelIndex[0] = i; pixelIndex[1] = j;

				UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );
				if (pixelValue > 0) {
					if (!border) {
						fill = !fill; border = true;
					}
				} else border = false;

				if ((fill) && (!border)) imageMask->SetPixel(pixelIndex, TISSUE_VALUE);
			}
		}
	}

  typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName("mask.png");
  writer->SetInput(imageMask);
  writer->Update();
}

#endif /* LUNG_SAMPLE */

//
// This function should be general enough for all samples as the mask should be
// the same size as the image, crop out everything except in the mask.
//
void AreaChop(FloatImageType::Pointer image, UnsignedCharImageType::Pointer imageMask)
{
  FloatImageType::RegionType fullRegion = image->GetLargestPossibleRegion();
	FloatImageType::SizeType size = fullRegion.GetSize();

	int i, j;
	for (i = 0; i < size[0]; ++i) {
		for (j = 0; j < size[1]; ++j) {
      UnsignedCharImageType::IndexType pixelIndex;
			pixelIndex[0] = i; pixelIndex[1] = j;
			UnsignedCharImageType::PixelType pixelValue = imageMask->GetPixel( pixelIndex );

			if (pixelValue == 0) {
	      FloatImageType::IndexType anIndex;
				anIndex[0] = i; anIndex[1] = j;
				image->SetPixel(anIndex, 0);
			}
			
		}
	}

#if 0
  itk::ImageRegionIterator<FloatImageType> imageIterator(image,fullRegion);
 
  while(!imageIterator.IsAtEnd()) {
  	if (imageIterator.GetIndex()[0] < 800) imageIterator.Set(0);
  	if (imageIterator.GetIndex()[1] < 800) imageIterator.Set(0);
  	if (imageIterator.GetIndex()[0] > 1400) imageIterator.Set(0);
  	if (imageIterator.GetIndex()[1] > 1400) imageIterator.Set(0);

    ++imageIterator;
  }
    
  FloatImageType::IndexType corner1, corner2;

	corner1[0] = 800; corner1[1] = 800;
	corner2[0] = 1400; corner2[1] = 1400;
 
  itk::LineIterator<FloatImageType> it1(image, corner1, corner2);
  it1.GoToBegin();
  while (!it1.IsAtEnd())
    {
 
    it1.Set(1);
    ++it1;
    }
#endif
}

void ProcessImage(FloatImageType::Pointer image, UnsignedCharImageType::Pointer polyMask, std::string outputFileName)
{
	// edge-preserving smooth filter
	typedef itk::GradientAnisotropicDiffusionImageFilter<FloatImageType, FloatImageType> FilterType;
	FilterType::Pointer filter = FilterType::New();
	filter->SetInput(image);

	filter->SetNumberOfIterations( 5 );
	filter->SetTimeStep( 0.01 );
	filter->SetConductanceParameter( 3 );
	filter->Update();

	// tophat filtering
  KernelType kernel; 
  kernel.SetRadius(TOPHAT_RADIUS); 
  kernel.CreateStructuringElement(); 

  TopHatFilterType::Pointer tophatFilter = TopHatFilterType::New(); 
  tophatFilter->SetKernel(kernel); 
  tophatFilter->SetInput( filter->GetOutput() ); 
	tophatFilter->Update();
	
	// binary threshold filter
	typedef itk::BinaryThresholdImageFilter < FloatImageType, UnsignedCharImageType > ThreshFilterType; 
  ThreshFilterType::Pointer thresh_filter = ThreshFilterType::New(); 
  thresh_filter->SetInsideValue( TISSUE_VALUE ); 
  thresh_filter->SetOutsideValue( 0 ); 
  thresh_filter->SetLowerThreshold( LOWER_THRESHOLD ); 
  thresh_filter->SetUpperThreshold( UPPER_THRESHOLD ); 
  thresh_filter->SetInput( tophatFilter->GetOutput() );
  thresh_filter->Update();

	// add polygon border
  typedef itk::OrImageFilter <UnsignedCharImageType> OrImageFilterType;
  OrImageFilterType::Pointer orFilter = OrImageFilterType::New();
  orFilter->SetInput(0, polyMask);
  orFilter->SetInput(1, thresh_filter->GetOutput());
  orFilter->Update();

  // trim image
  UnsignedCharImageType::IndexType trimStart;
	UnsignedCharImageType::SizeType trimSize;
  trimStart[0] = minX;
  trimStart[1] = minY;
 	trimSize[0] = maxX - minX + 1;
 	trimSize[1] = maxY - minY + 1;
	UnsignedCharImageType::RegionType desiredRegion(trimStart, trimSize);

  typedef itk::ExtractImageFilter< UnsignedCharImageType, UnsignedCharImageType > CropFilterType;
  CropFilterType::Pointer cropFilter = CropFilterType::New();
  cropFilter->SetExtractionRegion(desiredRegion);
  cropFilter->SetInput(orFilter->GetOutput());
#if ITK_VERSION_MAJOR >= 4
  cropFilter->SetDirectionCollapseToIdentity(); // This is required.
#endif
  cropFilter->Update();

	// save result
	std::cout << "Writing output file:" << std::endl;
	std::cout << outputFileName << std::endl;

  typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName(outputFileName);
  writer->SetInput(cropFilter->GetOutput());
  writer->Update();

}

//
// Testing code
//

void TrySegment()
{
	std::stringstream fileName, outputFileName;
	int currentImage = 1200;

	// input file name
	fileName << DATA_DIR << SAMPLE_DIR << SAMPLE_NAME;
	if (currentImage < 100) fileName << "000";
	else if (currentImage < 1000) fileName << "00";
	else fileName << "0";
	fileName << currentImage << ".tif";
	std::cout << "Processing image file:" << std::endl;
	std::cout << fileName.str() << std::endl;

	// read image
	typedef itk::ImageFileReader<FloatImageType>  ReaderType;
	ReaderType::Pointer reader = ReaderType::New();
	reader->SetFileName(fileName.str());
	reader->Update();

	FloatImageType::Pointer image = reader->GetOutput();
	
  typedef itk::GradientMagnitudeImageFilter<FloatImageType, FloatImageType> GradientMagnitudeImageFilterType;
  GradientMagnitudeImageFilterType::Pointer gradientMagnitudeImageFilter = GradientMagnitudeImageFilterType::New();
  gradientMagnitudeImageFilter->SetInput(image);
  gradientMagnitudeImageFilter->Update();
  std::cout << "1" << std::endl;

  PerformSegmentation(gradientMagnitudeImageFilter->GetOutput(), .0025, .25);
  std::cout << "2" << std::endl;
  PerformSegmentation(gradientMagnitudeImageFilter->GetOutput(), .005, .5);
  std::cout << "3" << std::endl;
  PerformSegmentation(gradientMagnitudeImageFilter->GetOutput(), .0075, .75);
  std::cout << "4" << std::endl;
  PerformSegmentation(gradientMagnitudeImageFilter->GetOutput(), .009, .9);
  std::cout << "5" << std::endl;
}

void PerformSegmentation(FloatImageType::Pointer image, const float threshold, const float level)
{
  typedef itk::WatershedImageFilter<FloatImageType> WatershedFilterType;
  WatershedFilterType::Pointer watershed = WatershedFilterType::New();
  watershed->SetThreshold(threshold);
  watershed->SetLevel(level);
  watershed->SetInput(image);
  watershed->Update();

  typedef itk::ScalarToRGBColormapImageFilter<LabeledImageType, RGBImageType> RGBFilterType;
  RGBFilterType::Pointer colormapImageFilter = RGBFilterType::New();
  colormapImageFilter->SetInput(watershed->GetOutput());
  colormapImageFilter->SetColormap( RGBFilterType::Jet );
  colormapImageFilter->Update();

  std::stringstream ss;
  ss << "output_" << threshold << "_" << level << ".png";
  
  typedef itk::ImageFileWriter<RGBImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName(ss.str());
  writer->SetInput(colormapImageFilter->GetOutput());
  writer->Update();
    
}


//
// OLD CODE
//

#if 0
void CreateImage(UnsignedCharImageType::Pointer image)
{
  // Create a white image with 3 dark regions of different values
  
  itk::Index<2> start;
  start.Fill(0);

  itk::Size<2> size;
  size.Fill(200);

  itk::ImageRegion<2> region(start,size);
  image->SetRegions(region);
  image->Allocate();
  image->FillBuffer(255);

  itk::ImageRegionIterator<UnsignedCharImageType> imageIterator(image,region);
 
  while(!imageIterator.IsAtEnd())
    {
    if(imageIterator.GetIndex()[0] > 20 && imageIterator.GetIndex()[0] < 50 &&
       imageIterator.GetIndex()[1] > 20 && imageIterator.GetIndex()[1] < 50)
    imageIterator.Set(50);
 
    ++imageIterator;
    }
    
  imageIterator.GoToBegin();
  
  while(!imageIterator.IsAtEnd())
    {
    if(imageIterator.GetIndex()[0] > 60 && imageIterator.GetIndex()[0] < 80 &&
       imageIterator.GetIndex()[1] > 60 && imageIterator.GetIndex()[1] < 80)
    imageIterator.Set(100);
 
    ++imageIterator;
    }
    
  imageIterator.GoToBegin();
  
  while(!imageIterator.IsAtEnd())
    {
    if(imageIterator.GetIndex()[0] > 100 && imageIterator.GetIndex()[0] < 130 &&
       imageIterator.GetIndex()[1] > 100 && imageIterator.GetIndex()[1] < 130)
    imageIterator.Set(150);
 
    ++imageIterator;
    }
    
  typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName("input.png");
  writer->SetInput(image);
  writer->Update();
}


void CircleChop(FloatImageType::Pointer image, float radius)
{
  FloatImageType::RegionType fullRegion = image->GetLargestPossibleRegion();
	FloatImageType::SizeType size = fullRegion.GetSize();

	//std::cout << size << std::endl;
	float xc = size[0] / 2;
	float yc = size[1] / 2;

#if 0
  FloatImageType::SizeType regionSize;
  regionSize[0] = 100;
  regionSize[1] = 100;
 
  FloatImageType::IndexType regionIndex;
  regionIndex[0] = 1000;
  regionIndex[1] = 1000;
 
  FloatImageType::RegionType region;
  region.SetSize(regionSize);
  region.SetIndex(regionIndex);
#endif

  itk::ImageRegionIterator<FloatImageType> imageIterator(image,fullRegion);
 
  while(!imageIterator.IsAtEnd()) {
  	float i = imageIterator.GetIndex()[0];
  	float j = imageIterator.GetIndex()[1];
		float d = sqrt( (i - xc)*(i - xc) + (j - yc)*(j - yc) );
	
		if (d > radius) imageIterator.Set(0);

    ++imageIterator;
    }
}

void TrimWax(FloatImageType::Pointer image, std::string outputFileName)
{
	// edge-preserving smooth filter
	typedef itk::GradientAnisotropicDiffusionImageFilter<FloatImageType, FloatImageType> FilterType;
	FilterType::Pointer filter = FilterType::New();
	filter->SetInput(image);

	filter->SetNumberOfIterations( 5 );
	filter->SetTimeStep( 0.01 );
	filter->SetConductanceParameter( 3 );
	filter->Update();

#if 0
  typedef itk::ImageFileWriter<FloatImageType> FileWriterType;
  FileWriterType::Pointer writer1 = FileWriterType::New();
  std::stringstream sf1, sf2, sf3;
  sf1 << "filter_1.tif";
  sf2 << "filter_2.tif";
  sf3 << "filter_3.tif";

  writer1->SetFileName(sf1.str());
  writer1->SetInput(filter->GetOutput());
  writer1->Update();

	filter->SetConductanceParameter( 10 );
	filter->Update();

  writer1->SetFileName(sf2.str());
  writer1->SetInput(filter->GetOutput());
  writer1->Update();

	filter->SetNumberOfIterations( 25 );
	filter->Update();

  writer1->SetFileName(sf3.str());
  writer1->SetInput(filter->GetOutput());
  writer1->Update();
#endif

	// tophat filtering
  KernelType kernel; 
  kernel.SetRadius(TOPHAT_RADIUS); 
  kernel.CreateStructuringElement(); 

  TopHatFilterType::Pointer tophatFilter = TopHatFilterType::New(); 
  tophatFilter->SetKernel(kernel); 
  tophatFilter->SetInput( filter->GetOutput() ); 
	tophatFilter->Update();

	// threshold filter for wax
	typedef itk::BinaryThresholdImageFilter < FloatImageType, FloatImageType > WaxThreshFilterType; 
  WaxThreshFilterType::Pointer wax_filter = WaxThreshFilterType::New(); 
  wax_filter->SetInsideValue( 1 ); 
  wax_filter->SetOutsideValue( 0 ); 
  wax_filter->SetLowerThreshold( LOWER_THRESHOLD ); 
  wax_filter->SetUpperThreshold( WAX_LOWER_THRESHOLD ); 
  wax_filter->SetInput( tophatFilter->GetOutput() );
  wax_filter->Update();
	
#if 0
	// binary threshold filter
	typedef itk::BinaryThresholdImageFilter < FloatImageType, UnsignedCharImageType > ThreshFilterType; 
  ThreshFilterType::Pointer thresh_filter = ThreshFilterType::New(); 
  thresh_filter->SetInsideValue( TISSUE_VALUE ); 
  thresh_filter->SetOutsideValue( 0 ); 
  thresh_filter->SetLowerThreshold( LOWER_THRESHOLD ); 
  thresh_filter->SetUpperThreshold( UPPER_THRESHOLD ); 
  thresh_filter->SetInput( tophatFilter->GetOutput() );
  thresh_filter->Update();

	// add polygon border
  typedef itk::OrImageFilter <UnsignedCharImageType> OrImageFilterType;
  OrImageFilterType::Pointer orFilter = OrImageFilterType::New();
  orFilter->SetInput(0, polyMask);
  orFilter->SetInput(1, thresh_filter->GetOutput());
  orFilter->Update();

  // trim image
  UnsignedCharImageType::IndexType trimStart;
	UnsignedCharImageType::SizeType trimSize;
  trimStart[0] = minX;
  trimStart[1] = minY;
 	trimSize[0] = maxX - minX + 1;
 	trimSize[1] = maxY - minY + 1;
	UnsignedCharImageType::RegionType desiredRegion(trimStart, trimSize);

  typedef itk::ExtractImageFilter< UnsignedCharImageType, UnsignedCharImageType > CropFilterType;
  CropFilterType::Pointer cropFilter = CropFilterType::New();
  cropFilter->SetExtractionRegion(desiredRegion);
  cropFilter->SetInput(orFilter->GetOutput());
#if ITK_VERSION_MAJOR >= 4
  cropFilter->SetDirectionCollapseToIdentity(); // This is required.
#endif
  cropFilter->Update();
#endif

	// save result
	std::cout << "Writing output file:" << std::endl;
	std::cout << outputFileName << std::endl;

  //typedef itk::ImageFileWriter<UnsignedCharImageType> FileWriterType;
  typedef itk::ImageFileWriter<FloatImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName(outputFileName);
  writer->SetInput(wax_filter->GetOutput());
  writer->Update();

}

#endif

#if 0
  UnsignedCharImageType::Pointer image = UnsignedCharImageType::New();
  CreateImage(image);
  
  typedef itk::GradientMagnitudeImageFilter<
    UnsignedCharImageType, FloatImageType >  GradientMagnitudeImageFilterType;
  GradientMagnitudeImageFilterType::Pointer gradientMagnitudeImageFilter = GradientMagnitudeImageFilterType::New();
  gradientMagnitudeImageFilter->SetInput(image);
  gradientMagnitudeImageFilter->Update();

  // Custom parameters
  PerformSegmentation(gradientMagnitudeImageFilter->GetOutput(), threshold, level);
  
  // Fixed parameters
  PerformSegmentation(gradientMagnitudeImageFilter->GetOutput(), .0025, .25);
  PerformSegmentation(gradientMagnitudeImageFilter->GetOutput(), .005, .5);
  PerformSegmentation(gradientMagnitudeImageFilter->GetOutput(), .0075, .75);
  PerformSegmentation(gradientMagnitudeImageFilter->GetOutput(), .009, .9);
  
#endif

#if 0
  //typedef   itk::GradientMagnitudeRecursiveGaussianImageFilter<FloatImageType, FloatImageType> GradientMagnitudeFilterType;

  //GradientMagnitudeFilterType::Pointer gradientMagnitudeFilter = GradientMagnitudeFilterType::New();

  //gradientMagnitudeFilter->SetInput( reader->GetOutput() );
  //gradientMagnitudeFilter->SetSigma( 0.05 );

	typedef itk::GradientAnisotropicDiffusionImageFilter<FloatImageType, FloatImageType> FilterType;
	FilterType::Pointer filter = FilterType::New();
	filter->SetInput( reader->GetOutput() );

	filter->SetNumberOfIterations( 5 );
	filter->SetTimeStep( 0.01 );
	filter->SetConductanceParameter( 3 );
	filter->Update();

  typedef itk::GradientMagnitudeImageFilter<FloatImageType, FloatImageType> GradientMagnitudeImageFilterType;
  GradientMagnitudeImageFilterType::Pointer gradientMagnitudeImageFilter = GradientMagnitudeImageFilterType::New();
  gradientMagnitudeImageFilter->SetInput( filter->GetOutput() );
  gradientMagnitudeImageFilter->Update();

  typedef itk::WatershedImageFilter<FloatImageType> WatershedFilterType;
  WatershedFilterType::Pointer watershed = WatershedFilterType::New();
  watershed->SetThreshold(threshold);
  watershed->SetLevel(level);
  watershed->SetInput(gradientMagnitudeImageFilter->GetOutput());
  watershed->Update();

#if 0  
  typedef itk::ImageFileWriter<FloatImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName(ss.str());
  //writer->SetInput(reader->GetOutput());
  //writer->SetInput(filter->GetOutput());
  //writer->SetInput(gradientMagnitudeImageFilter->GetOutput());
  writer->Update();
#endif

  typedef itk::CastImageFilter<LabeledImageType,FloatImageType> CastOutputType;
  CastOutputType::Pointer castOutput = CastOutputType::New();
  castOutput->SetInput(watershed->GetOutput());

  typedef itk::ImageFileWriter<FloatImageType> FileWriterType;
  FileWriterType::Pointer writer = FileWriterType::New();
  writer->SetFileName(ss.str());
  writer->SetInput(castOutput->GetOutput());
  writer->Update();

#endif
