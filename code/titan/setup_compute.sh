#
# Setup required files for compute nodes
# Should 'source' this file.
#

# shared libraries
rm -rf $PROJWORK/med101/lib
cp -rf /ccs/proj/med101/Projects/local/lib $PROJWORK/med101

export LD_LIBRARY_PATH=$PROJWORK/med101/lib:$LD_LIBRARY_PATH
