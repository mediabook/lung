% pre-processing of lung data

% import the data file
DELIMITER = '\t';
HEADERLINES = 0;
FILENAME = '../../data/HLD34samples_smoothMiner_sampleTimes_geneNames.txt';

% Import the file
newData1 = importdata(FILENAME, DELIMITER, HEADERLINES);

% Create new variables in the base workspace from those fields.
vars = fieldnames(newData1);
for i = 1:length(vars)
    assignin('base', vars{i}, newData1.(vars{i}));
end

% gene probes and names
A = size(textdata);
geneNames = textdata(2:A(1),3);

timePoints = data(1,:);
rdata = data(2:A(1),:);

rdata = 2.^rdata;

% divide each row by max
A = max(rdata');
B = size(rdata);
C = repmat(A,B(2),1);
ddata = rdata ./ C';

%ndata = rdata;
ndata = ddata;


%
% filter genes
%
meanQuantile = .5;
sdQuantile = .1;
%maxGenes = 100;

% mean/sd across observations
meanX = mean(rdata');
sdX = sqrt(var(ndata'));
meanSort = sort(abs(meanX),'descend');
meanCutoff = meanSort(1, ceil(meanQuantile * length(meanSort)));

% genes above the mean cutoff
meanFilter = abs(meanX) >= meanCutoff;
disp(strcat('Mean cutoff: ', num2str(meanCutoff)));

% pick sd cutoff to number of genes we want
sdSort = sort(sdX,'descend');
sdCutoff = sdSort(1, ceil(sdQuantile * length(sdSort)));

% genes above the sd cutoff
sdFilter = sdX >= sdCutoff;
disp(strcat('SD cutoff: ', num2str(sdCutoff)));

geneFilter = meanFilter & sdFilter;
disp(strcat(num2str(sum(geneFilter)), ' genes in cutoff data set.'));


% display the genes
%geneNames(geneFilter)

% gene by name
%nameIdx = strcmp(geneNames, {'DDR1'});
%geneNames(nameIdx)

